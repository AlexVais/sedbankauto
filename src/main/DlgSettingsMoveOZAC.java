package main;

import javax.swing.JDialog;
import javax.swing.JCheckBox;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class DlgSettingsMoveOZAC extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private File fileConfigProcessings;
	
	
	private JCheckBox chckbxAllOZAC;
	private JCheckBox chckbxRestoreRecipients;
	private JButton btnNewButton;
	private JCheckBox chckbxRestoreOnlyDeleted;
	
	public DlgSettingsMoveOZAC(final File fileConfigProcessings) {
		this.fileConfigProcessings = fileConfigProcessings;
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				//���������� �������� �� ����� ������������ ���������
				Properties prop = new Properties();
				try(FileInputStream fis = new FileInputStream(fileConfigProcessings)) {
					prop.load(fis);
					chckbxAllOZAC.setSelected(Boolean.valueOf(prop.getProperty("isAllOZAC")));
					chckbxRestoreRecipients.setSelected(Boolean.valueOf(prop.getProperty("isRestoreRecipients")));
					chckbxRestoreOnlyDeleted.setSelected(Boolean.valueOf(prop.getProperty("isRestoreOnlyDeleted")));
					
				} catch (Exception e1) {
					try {
						throw new Exception("������ ������ ����� ������������", e1);
					} catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
				
				if (!chckbxRestoreRecipients.isSelected()) {
					chckbxRestoreOnlyDeleted.setSelected(false);
					chckbxRestoreOnlyDeleted.setEnabled(false);
				}
			}
		});
		
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 468, 288);
		setTitle("��������� �������� OZAC � ���");
		getContentPane().setLayout(null);
		
		chckbxAllOZAC = new JCheckBox("����������� ���� OZAC (����������� � �������������)");
		chckbxAllOZAC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButton.setEnabled(true);
			}
		});
		chckbxAllOZAC.setBounds(6, 7, 381, 23);
		getContentPane().add(chckbxAllOZAC);
		
		chckbxRestoreRecipients = new JCheckBox("��������������� �������� �������� ���������� � �����������");
		chckbxRestoreRecipients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButton.setEnabled(true);
				
				if (chckbxRestoreRecipients.isSelected()) 
					chckbxRestoreOnlyDeleted.setEnabled(true);
				else {
					chckbxRestoreOnlyDeleted.setEnabled(false);
					chckbxRestoreOnlyDeleted.setSelected(false);
				}
				
			}
		});
		chckbxRestoreRecipients.setBounds(6, 33, 410, 23);
		getContentPane().add(chckbxRestoreRecipients);
		
		btnNewButton = new JButton("���������");
		btnNewButton.setEnabled(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//���������� ��������
				Properties prop = new Properties();
				try(FileOutputStream fos = new FileOutputStream(fileConfigProcessings)) {
					
					prop.setProperty("isAllOZAC", String.valueOf(chckbxAllOZAC.isSelected()));
					prop.setProperty("isRestoreRecipients", String.valueOf(chckbxRestoreRecipients.isSelected()));
					prop.setProperty("isRestoreOnlyDeleted", String.valueOf(chckbxRestoreOnlyDeleted.isSelected()));
					prop.store(fos, null);
					
				} catch (Exception e1) {
					try {
						throw new Exception("������ ������ ����� ������������", e1);
					} catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
				
				((JButton)e.getSource()).setEnabled(false);;
			}
		});
		btnNewButton.setBounds(331, 211, 100, 23);
		getContentPane().add(btnNewButton);
		
		chckbxRestoreOnlyDeleted = new JCheckBox("������ ����� ����� ��������");
		chckbxRestoreOnlyDeleted.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButton.setEnabled(true);
			}
		});
		chckbxRestoreOnlyDeleted.setBounds(27, 52, 269, 23);
		getContentPane().add(chckbxRestoreOnlyDeleted);
	}
}

