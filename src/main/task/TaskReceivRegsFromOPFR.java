package main.task;

import static main.SedbankAutoProcessing.VER_PRODUCTION;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserRegsFromOPFR;
import ru.pfrf.process.ProcessAdapter;
import ru.pfrf.process.ReceivingPOSD;
import ru.pfrf.process.ReceivingPOSDAdapter;
import ru.pfrf.process.ReceivingRegsFromOPFR;
import ru.pfrf.process.ReceivingRegsFromOPFRAdapter;
import ru.pfrf.register.RegisterTFFromOPFR;
import main.MainFrame;
import main.SedbankAutoProcessing;
import main.Utils;

public class TaskReceivRegsFromOPFR extends ThreadTask {
	
	private static Long intervalDefault = 300000L;  //�������� �� ���������
	
	private MainFrame frame;

	public TaskReceivRegsFromOPFR(SessionFactory sessionFactory, 
			File fileConfig,
			MainFrame frame) {

		super(sessionFactory, 
				fileConfig,
				frame.plIndReceivRegsFromOPFR,
				frame.prbReceivRegsFromOPFR, 
				frame.btnStartReceivRegsFromOPFR, 
				frame.btnStopReceivRegsFromOPFR,
				frame.lblIndErrorReceivRegsFromOPFR,
				frame.tabbedPane,
				0);
		
		this.frame = frame;
		this.interval = intervalDefault;

	}
	
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalRegsFromOPFR")) * 1000;
				this.addrMessage = p.getProperty("addrMessage");
				this.pathMimCmdExe = Paths.get(new String(p.getProperty("pathMimCmdExe").getBytes("ISO8859-1")));
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {

				this.beforeRun();
				
				ReceivingRegsFromOPFR receiving = new ReceivingRegsFromOPFR(sessionFactory, this.fileConfig);
				receiving.start();
				
				//��������
				ProcessAdapter processAdapter = new ProcessAdapter(this.sessionFactory,
															receiving.getDateBegin(),
															receiving.getDateEnd(),
															"ServerNamePC",
															"server");
				
				ReceivingRegsFromOPFRAdapter receivingAdapter = new ReceivingRegsFromOPFRAdapter(processAdapter);
				processAdapter.setReceivingRegsFromOPFRAdapter(receivingAdapter);
				for(RegisterTFFromOPFR reg : receiving.getSetRegistersTFFromOPFR()) {
					receivingAdapter.getSetRegistersTFFromOPFR().add(reg);
					reg.setReceivingRegsTFFromOPFRAdapter(receivingAdapter);
				}
			
				
				//���������� � ��
				if (!receiving.getSetRegistersTFFromOPFR().isEmpty()) {
					
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();

						session.save(receiving);
						session.save(processAdapter);
						t.commit();
						
						
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ������ � ��", ex);
						
					} finally {
						session.close();
					}
					
					//�������� ��������� � ����� �������� ������������ ���
					if (SedbankAutoProcessing.ver == VER_PRODUCTION) 
						Utils.sendMessageMiranda(this.pathMimCmdExe, "������(�) ������(�) �� ����", "babenko@10.38.23.176");
				
				}
			
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (Exception e) {
			
			try (FileOutputStream fos = new FileOutputStream(new File("logs/log_receivingRegsFromOPFR.txt"));
				 PrintWriter pw = new PrintWriter(fos)) {
				
					pw.print(new Date().toString() + "\n");
					e.printStackTrace(pw);
					
				} catch (Exception e1) {
					e1.printStackTrace();	
				}
			
			try {
				Utils.sendMessageMiranda(this.pathMimCmdExe, e.getMessage(), this.addrMessage);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					lblIndError.setVisible(true);
					btnStart.setEnabled(false);	
					tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
					
				}
			});
		
			
			
		} finally {
			this.procFinally();
		}
		
		
		
	}
}
