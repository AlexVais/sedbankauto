package main.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import main.MainFrame;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.entityinfoexchange.order.psmp.POSZ;
import ru.pfrf.entityinfoexchange.order.vozv.POSO;
import ru.pfrf.process.ReceivingPOSO;
import ru.pfrf.process.ReceivingPOSZ;
import ru.pfrf.process.ReceivingVOZVOut;

public class TaskReceivPOSO extends ThreadTask {
	private static Long intervalDefault = 300000L; //�������� �� ���������
	
	private MainFrame frame;
	
	public TaskReceivPOSO(SessionFactory sessionFactory, 
			File fileConfig,
			MainFrame frame) {
		
		super(sessionFactory, 
			fileConfig,
			frame.pnlIndReceivPOSO, 
			frame.prbPOSO, 
			frame.btnStartReceivPOSO, 
			frame.btnStopReceivPOSO,
			frame.lblIndErrorReceivPOSO,
			frame.tabbedPane,
			2);
		
		this.frame = frame;
		this.interval = intervalDefault;
	}
	
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalReceivPOSO")) * 1000;
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {

				this.beforeRun();
				
				ReceivingPOSO receiving = new ReceivingPOSO(sessionFactory, this.fileConfig);
				receiving.start();
				
				ReceivingVOZVOut receivingVOZVOut = new ReceivingVOZVOut(sessionFactory, this.fileConfig);
				receivingVOZVOut.start();
				
				
				if (receiving.getListPOSO().size() > 0) {
					
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();
						for (POSO poso : receiving.getListPOSO()) {
							session.save(poso);
						}
	
						t.commit();
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ������ � ��", ex);
						
					} finally {
						session.close();
					}
				}
			
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (Exception e) {
			
			try (FileOutputStream fos = new FileOutputStream(new File("logs/log_receivingPOSO.txt"));
				 PrintWriter pw = new PrintWriter(fos)) {
				
					pw.print(new Date().toString() + "\n");
					e.printStackTrace(pw);
					
				} catch (Exception e1) {
					e1.printStackTrace();	
				}
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					lblIndError.setVisible(true);
					btnStart.setEnabled(false);	
					tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
					
				}
			});
			
			
		} finally {
			this.procFinally();
		}
		
		
		
	}
}
