package main.task;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;

import javax.swing.*;

import main.MainFrame;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.process.ReceivingOSMP;

public abstract class ThreadTask extends Thread {
	
	protected SessionFactory sessionFactory;
	protected static File fileConfig;
	protected static File commonFileSettingsProcessing;
	protected Long interval;
	protected String addrMessage;
	protected Path pathMimCmdExe;
	
	protected JPanel indicatorProc;
	protected JProgressBar pb;
	protected JButton btnStart;
	protected JButton btnStop;
	protected JLabel lblIndError;
	protected JTabbedPane tpForIndicator;
	protected int indexTpIndicator;
	
	
	public ThreadTask(SessionFactory sessionFactory,
			File fileConfig,
			JPanel indicatorProc,
			JProgressBar pb,
			JButton btnStart,
			JButton btnStop,
			JLabel lblIndError,
			JTabbedPane tpForIndicator,
			int indexTpIndicator) {
		
		this.sessionFactory = sessionFactory;
		this.fileConfig = fileConfig;
		this.indicatorProc = indicatorProc;
		this.pb = pb;
		this.btnStart = btnStart;
		this.btnStop = btnStop;
		this.lblIndError = lblIndError;
		this.tpForIndicator = tpForIndicator;
		this.indexTpIndicator = indexTpIndicator;
		
		this.setPriority(Thread.MIN_PRIORITY);
		
		//���������� �������� �� ����� ������������
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(fileConfig));
			this.commonFileSettingsProcessing = new File(new String(prop.getProperty("commonFileSettingsProcessing").getBytes("ISO8859-1")));
			
		} catch (IOException e) {
			try {
				throw new Exception("������ ������ ����� ������������", e);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}
	
	protected void beforeRun() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				pb.setIndeterminate(true);
				pb.setToolTipText("���������� ��������");
			}
			
		});
	}
	
	protected void afterEndTask() throws InterruptedException  {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				pb.setIndeterminate(false);
				pb.setToolTipText("");
			}
			
		});
		
		//��������
		Thread.sleep(interval);
	}
	
	protected void procFinally() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				pb.setIndeterminate(false);
				pb.setToolTipText("");
				//btnStart.setEnabled(true);
				btnStop.setEnabled(false);
				indicatorProc.setBackground(Color.RED);
			}
			
		});
	}
	
	protected void funcException(Throwable e,
						    File logFile, 
							final JLabel lblIndError, 
							final JButton btnStart, 
							final JTabbedPane tpForIndicator) {
		
		try (FileOutputStream fos = new FileOutputStream(logFile);
				 PrintWriter pw = new PrintWriter(fos)) {
				
			pw.print(new Date().toString() + "\n");
			e.printStackTrace(pw);
			
		} catch (Exception e1) {
			e1.printStackTrace();	
		}
	
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				lblIndError.setVisible(true);
				btnStart.setEnabled(false);	
				tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
				
			}
		});
	}
	
	

}
