package main.task;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;

import main.MainFrame;
import main.Utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.entityinfoexchange.notification.Notification;
import ru.pfrf.entityinfoexchange.notification.NotificationFactory;
import ru.pfrf.entityinfoexchange.notification.NotificationLTToBank;
import ru.pfrf.entityinfoexchange.order.osmp.DocPOOS;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOOD;
import ru.pfrf.entityinfoexchange.order.psmp.MergedPSMP;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNamePfr;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;
import ru.pfrf.process.ReceivingOSMP;
import ru.pfrf.process.SendLTToBankAdapter;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.ListTransferToBank;

public class TaskReceivNotifications extends ThreadTask {
	private static Long intervalDefault = 300000L; //�������� �� ���������
	
	private MainFrame frame;
	private Path sourcePathNotificationOPFR;
	private Path sourcePathNotificationBanks;
	private Path archivePathNotification;
	private Path sourcePathReportLog;

	public TaskReceivNotifications(SessionFactory sessionFactory, 
						File fileConfig,
						MainFrame frame) {
		super(sessionFactory, 
				fileConfig,
				frame.pnlIndReceivNotification, 
				frame.prbNotification, 
				frame.btnStartReceivNotification, 
				frame.btnStopReceivNotification,
				frame.lblIndErrorReceivNotification,
				frame.tabbedPane,
				3);

		this.frame = frame;
		this.interval = intervalDefault;

	}
	
	/**
	 * ��������� ������� �� ����������, ������������ � ���� � ������� ���������� ����� ������
	 * @return List<ListTransferToBank>
	 * @throws Exception
	 */
	private List<ListTransferToBank> getLTWithEmptyNamePack() throws Exception {
		Session session = null;
		Transaction t = null;
		List<ListTransferToBank> listTransferToBank = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(ListTransferToBank.class, "lt_to_bank")
									   .createCriteria("lt_to_bank.registerTFToOPFR2", "r_opfr2", JoinType.INNER_JOIN)
									   .createCriteria("r_opfr2.registerTFMerger", "r_merger", JoinType.INNER_JOIN)
									   .createCriteria("r_merger.registerTFFromOPFR", "r_from_opfr", JoinType.INNER_JOIN)
									   .createCriteria("r_from_opfr.registerTFToOPFR", "r_opfr", JoinType.INNER_JOIN)
									   .createCriteria("r_opfr.createdRegisterTF", "r", JoinType.INNER_JOIN)
									   .add(Restrictions.eqOrIsNull("lt_to_bank.namePack", ""))
									   .add(Restrictions.ge("r.yearPay", 2017))
									   .add(Restrictions.ge("r.monthPay", 1))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listTransferToBank = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ ListTransferToBank", ex);
			
		} finally {
			session.close();
		}
		return listTransferToBank;
	}
	
	
	/**
	 * ��������� ������� POOS, ������������ � �������� � ������ ������ ������
	 * @return List<DocPOOS>
	 * @throws Exception
	 */
	private List<DocPOOS> getPOOSWithEmptyNamePack() throws Exception {
		Session session = null;
		Transaction t = null;
		List<DocPOOS> listDocPoos = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(DocPOOS.class, "poos")
									   .add(Restrictions.isNull("poos.namePack"))
									   .add(Restrictions.ge("poos.year", 2017))
									   .add(Restrictions.ge("poos.month", 6)) //�����, � �������� �������� �������� ������� � ����
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listDocPoos = criteria.list();
			
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ DocPOOS", ex);
			
		} finally {
			session.close();
		}
		return listDocPoos;
	}
	
	
	/**
	 * ��������� ������� PSMP, ������������ � �������� � ������ ������ ������
	 * @return List<MergedPSMP>
	 * @throws Exception
	 */
	private List<MergedPSMP> getPSMPWithEmptyNamePack() throws Exception {
		Session session = null;
		Transaction t = null;
		List<MergedPSMP> listMergedPSMP = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(MergedPSMP.class)
									   .add(Restrictions.isNull("packName"))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listMergedPSMP = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ MergedPSMP", ex);
			
		} finally {
			session.close();
		}
		return listMergedPSMP;
	}
	
	
	/**
	 * ������ ����� ������ Debut (report.log), ���������� ����������, ���������� ������������ ������� � ���������� � ��� ������. 
	 * ���������� List<String> ����������� ������� ��������
	 * @param file
	 * @return List<String>
	 * @throws Exception 
	 */
	private List<String> getListStringsOfReportFile(List<ListTransferToBank> listLTToBank) throws Exception {
		Pattern p = Pattern.compile("^.+(� ����� �������� ����:|��������� �����:).+$");
		
		//������ ��� ������ �� ��������������� ��������� ������
		Set<File> setFiles = new HashSet<>();
		for (ListTransferToBank lt : listLTToBank) {
			final CreatedRegisterTF reg = lt.getRegisterTFToOPFR2().getRegisterTFMerger()
									 				.getRegisterTFFromOPFR()
									 				.getRegisterTFToOPFR()
									 				.getCreatedRegisterTF();
			File[] dirs = this.sourcePathReportLog.toFile().listFiles(new FilenameFilter() {
								public boolean accept(File dir, String name) {
									Pattern p = Pattern.compile(reg.getYearPay().toString().substring(2) + "_" + String.valueOf(reg.getMonthPay() + 100).substring(1) + "_\\d{2}");
									Matcher m = p.matcher(name);
									if ((new File(dir + "/" + name).isDirectory()) && m.matches()) return true;
									else return false;
								}}); 
			
			
			for (File file : dirs) {
				setFiles.add(new File(file + "/report.log"));
			}
		}
		
		List<String> listStringsOfReportFile = new ArrayList<>();
		for (File file : setFiles) {
			try (BufferedReader bfr = new BufferedReader(new FileReader(file))) {
				String line = null;
				while ((line = bfr.readLine()) != null) {
					Matcher m = p.matcher(line);
					if (m.matches()) listStringsOfReportFile.add(m.group());
				}
			} catch (Exception e) {
				throw new Exception("������ ��� ������ " + file, e);
			}
		}
		
		return listStringsOfReportFile;
	}
	
	
	
	private List<String> getListStringsOfReportFileByPOOS(List<DocPOOS> listPOOS) throws Exception {
		
		//������ ��� ������ �� ��������������� ��������� ������
		Set<File> setFiles = new HashSet<>();
		for (final DocPOOS docPOOS : listPOOS) {
			final Pattern p = Pattern.compile(docPOOS.getYear().toString().substring(2) + "_" + String.valueOf(docPOOS.getMonth() + 100).substring(1) + "_\\d{2}");
			File[] dirs = this.sourcePathReportLog.toFile().listFiles(new FilenameFilter() {
								public boolean accept(File dir, String name) {
									Matcher m = p.matcher(name);
									if ((new File(dir + "/" + name).isDirectory()) && m.matches()) return true;
									else return false;
								}}); 
			
			for (File file : dirs) {
				setFiles.add(new File(file + "/report.log"));
			}
	
		}
		
		List<String> listStringsOfReportFile = new ArrayList<>();
		Pattern p = Pattern.compile("^.+(� ����� �������� ����:|��������� �����:).+$");
		for (File file : setFiles) {
			try (BufferedReader bfr = new BufferedReader(new FileReader(file))) {
				String line = null;
				while ((line = bfr.readLine()) != null) {
					Matcher m = p.matcher(line);
					if (m.matches()) listStringsOfReportFile.add(m.group());
				}
			} catch (Exception e) {
				throw new Exception("������ ��� ������ " + file, e);
			}
		}
		
		return listStringsOfReportFile;
	}
	
	
	private List<String> getListStringsOfReportFileByPSMP(List<MergedPSMP> listMergedPSMP) throws Exception {
		
		//������ ��� ������ �� ��������������� ��������� ������
		Set<File> setFiles = new HashSet<>();
		for (final MergedPSMP mergedPSMP : listMergedPSMP) {
			final Pattern p = Pattern.compile(mergedPSMP.getYear().toString().substring(2) + "_" + String.valueOf(mergedPSMP.getMonth() + 100).substring(1) + "_\\d{2}");
			File[] dirs = this.sourcePathReportLog.toFile().listFiles(new FilenameFilter() {
								public boolean accept(File dir, String name) {
									Matcher m = p.matcher(name);
									if ((new File(dir + "/" + name).isDirectory()) && m.matches()) return true;
									else return false;
								}}); 
			
			for (File file : dirs) {
				setFiles.add(new File(file + "/report.log"));
			}
	
		}
		
		List<String> listStringsOfReportFile = new ArrayList<>();
		Pattern p = Pattern.compile("^.+(� ����� �������� ����:|��������� �����:).+$");
		for (File file : setFiles) {
			try (BufferedReader bfr = new BufferedReader(new FileReader(file))) {
				String line = null;
				while ((line = bfr.readLine()) != null) {
					Matcher m = p.matcher(line);
					if (m.matches()) listStringsOfReportFile.add(m.group());
				}
			} catch (Exception e) {
				throw new Exception("������ ��� ������ " + file, e);
			}
		}
		
		return listStringsOfReportFile;
	}
	
	
	private List<String> getListStringsOfReportFileByPOOD(List<DocPOOD> listDocPOOD) throws Exception {
		//������ ��� ������ �� ��������������� ��������� ������
		Set<File> setFiles = new HashSet<>();
		for (DocPOOD docPOOD : listDocPOOD) {
			//������� ����� ��� ����������� ��������������� �������
			ParserFileNamePfr parser = new ParserFileNamePfr(new File(docPOOD.getFileName()), MainFrame.dictNameOrgInFile); 
			parser.parse();

			//��������� �����. �������
			Session session = null;
			Transaction t = null;
			CreatedRegisterTF reg = null;
			try {
				session = this.sessionFactory.openSession();
				t = session.beginTransaction();
				
				Criteria criteria = session.createCriteria(CreatedRegisterTF.class, "r")
										   .add(Restrictions.eq("r.number", Integer.valueOf(parser.getPackNumber())))
										   .add(Restrictions.eq("r.yearPay", parser.getYear()))
										   .add(Restrictions.eq("r.deliveryOrg", parser.getDeliveryOrg()))
										   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				
				List<CreatedRegisterTF> listRegs = criteria.list();
				if (listRegs.size() > 1) throw new Exception("���������� �������� ������ 1");
				reg = listRegs.get(0);
				
				t.commit();
			} catch (Exception e) {
				t.rollback();
				throw new Exception("������ ��������� ������ �������� ��", e);
				
			} finally {
				session.close();
			}
			
			final CreatedRegisterTF regForFind = reg;
			File[] dirs = this.sourcePathReportLog.toFile().listFiles(new FilenameFilter() {
								public boolean accept(File dir, String name) {
									Pattern p = Pattern.compile(regForFind.getYearPay().toString().substring(2) + "_" + String.valueOf(regForFind.getMonthPay() + 100).substring(1) + "_\\d{2}");
									Matcher m = p.matcher(name);
									if ((new File(dir + "/" + name).isDirectory()) && m.matches()) return true;
									else return false;
								}}); 
			
			for (File file : dirs) {
				setFiles.add(new File(file + "/report.log"));
			}
		}
		
		List<String> listStringsOfReportFile = new ArrayList<>();
		Pattern p = Pattern.compile("^.+(� ����� �������� ����:|��������� �����:).+$");
		for (File file : setFiles) {
			try (BufferedReader bfr = new BufferedReader(new FileReader(file))) {
				String line = null;
				while ((line = bfr.readLine()) != null) {
					Matcher m = p.matcher(line);
					if (m.matches()) listStringsOfReportFile.add(m.group());
				}
			} catch (Exception e) {
				throw new Exception("������ ��� ������ " + file, e);
			}
		}
		
		return listStringsOfReportFile;
	}
	
	
	
	private void updateListTransferToBank(List<ListTransferToBank> listLTToBank) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();

			for (ListTransferToBank lt : listLTToBank) {
				session.update(lt);
			}
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ������ � ��", e);
			
		} finally {
			session.close();
		}
	}
	
	
	private void updatePOOD(List<DocPOOD> listDocPOOD) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();

			for (DocPOOD docPOOD : listDocPOOD) {
				session.update(docPOOD);
			}
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ������ � ��", e);
			
		} finally {
			session.close();
		}
	}
	
	private void updatePOOS(List<DocPOOS> listDocPOOS) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();

			for (DocPOOS docPOOS : listDocPOOS) {
				session.update(docPOOS);
			}
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ������ � ��", e);
			
		} finally {
			session.close();
		}
	}
	
	
	private void updateMergedPSMP(List<MergedPSMP> listMergedPSMP) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();

			for (MergedPSMP mergedPSMP : listMergedPSMP) {
				session.update(mergedPSMP);
			}
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ������ � ��", e);
			
		} finally {
			session.close();
		}
	}
	
	
	private void findNamePackLT(List<ListTransferToBank> listLTToBank, List<String> listStringsOfReportFile) {
		for (ListTransferToBank lt : listLTToBank) {
			CreatedRegisterTF reg = lt.getRegisterTFToOPFR2()
							   		  .getRegisterTFMerger()
							          .getRegisterTFFromOPFR()
							          .getRegisterTFToOPFR()
							          .getCreatedRegisterTF();
			
			String strNumber = String.valueOf(reg.getNumber() + 100000).substring(1, 6);
			
			Pattern p = Pattern.compile(".+PFR.+DCK-" + strNumber + "-\\d{3}-DOC-SPIS-FSB-" + reg.getDeliveryOrg().getShortName() + ".+");
			Pattern p2 = Pattern.compile("^.+��������� �����:.+(Y54.+P22)");
			Matcher m = null;
			for (String line : listStringsOfReportFile) {
				m = p.matcher(line);
				if (m.matches()) {
					//����� ������������ ������ ���� �� ������
					Matcher m2 = null;
					Integer index = listStringsOfReportFile.indexOf(line);
					for (; index >= 0; index--) {
						m2 = p2.matcher(listStringsOfReportFile.get(index));
						if (m2.matches()) break;
					}
					if (m2.matches()) {
						String namePack = m2.group(1);
						lt.setNamePack(namePack);
					}
				}
			}
		}
	}
	
	
	private void findNamePackPOOD(List<DocPOOD> listDocPOOD, List<String> listStringsOfReportFile) {
		for (DocPOOD docPOOD : listDocPOOD) {
			Pattern p = Pattern.compile(".+" + docPOOD.getFileName() + ".+");
			Pattern p2 = Pattern.compile("^.+��������� �����:.+(Y54.+P22)");
			Matcher m = null;
			
			for (String line : listStringsOfReportFile) {
				m = p.matcher(line);
				if (m.matches()) {
					//����� ������������ ������ ���� �� ������
					Matcher m2 = null;
					Integer index = listStringsOfReportFile.indexOf(line);
					for (; index >= 0; index--) {
						m2 = p2.matcher(listStringsOfReportFile.get(index));
						if (m2.matches()) break;
					}
					if (m2.matches()) {
						String namePack = m2.group(1);
						docPOOD.setPackName(namePack);
					}
				}
			}
		}
	}
	
	
	private void findNamePackPOOS(List<DocPOOS> listDocPOOS, List<String> listStringsOfReportFile) {
		for (DocPOOS docPOOS : listDocPOOS) {
			Pattern p = Pattern.compile(".+" + docPOOS.getFileName() + ".+");
			Pattern p2 = Pattern.compile("^.+��������� �����:.+(Y54.+P22)");
			Matcher m = null;
			
			for (String line : listStringsOfReportFile) {
				m = p.matcher(line);
				if (m.matches()) {
					//����� ������������ ������ ���� �� ������
					Matcher m2 = null;
					Integer index = listStringsOfReportFile.indexOf(line);
					for (; index >= 0; index--) {
						m2 = p2.matcher(listStringsOfReportFile.get(index));
						if (m2.matches()) break;
					}
					if (m2.matches()) {
						String namePack = m2.group(1);
						docPOOS.setNamePack(namePack);
					}
				}
			}
		}
	}
	
	
	private void findNamePackPSMP(List<MergedPSMP> listMergedPSMP, List<String> listStringsOfReportFile) {
		for (MergedPSMP mergedPSMP : listMergedPSMP) {
			Pattern p = Pattern.compile(".+" + mergedPSMP.getFileName() + ".+");
			Pattern p2 = Pattern.compile("^.+��������� �����:.+(Y54.+P22)");
			Matcher m = null;
			
			for (String line : listStringsOfReportFile) {
				m = p.matcher(line);
				if (m.matches()) {
					//����� ������������ ������ ���� �� ������
					Matcher m2 = null;
					Integer index = listStringsOfReportFile.indexOf(line);
					for (; index >= 0; index--) {
						m2 = p2.matcher(listStringsOfReportFile.get(index));
						if (m2.matches()) break;
					}
					if (m2.matches()) {
						String namePack = m2.group(1);
						mergedPSMP.setPackName(namePack);
					}
				}
			}
		}
	}
	
	
	
	/**
	 * ��������� ������ DocPOOD ������������ � �������� � ������� ���������� ����� ������
	 * @return List<DocPOOD>
	 * @throws Exception
	 */
	private List<DocPOOD> getDocPOODEmptyNamePack() throws Exception {
		Session session = null;
		Transaction t = null;
		List<DocPOOD> listDocPOOD = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(DocPOOD.class, "pood")
									   //.createCriteria("lt_to_bank.registerTFToOPFR2", "r_opfr2", JoinType.INNER_JOIN)
									   .add(Restrictions.isNull("pood.packName"))
									   .add(Restrictions.ge("pood.id", 294)) //
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listDocPOOD = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ POOD", ex);
			
		} finally {
			session.close();
		}
		return listDocPOOD;
	}
	
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(prop.getProperty("intervalNotification")) * 1000;
				this.sourcePathNotificationOPFR = Paths.get(new String(prop.getProperty("sourcePathNotificationOPFR").getBytes("ISO8859-1")));
				this.sourcePathNotificationBanks = Paths.get(new String(prop.getProperty("sourcePathNotificationBanks").getBytes("ISO8859-1")));
				this.archivePathNotification = Paths.get(new String(prop.getProperty("archivePathNotification").getBytes("ISO8859-1")));
				this.sourcePathReportLog = Paths.get(new String(prop.getProperty("sourcePathReportLog").getBytes("ISO8859-1")));
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {
				this.beforeRun();
				
				//��������� ������ ������� �� ���������� �� ���������, ������������ � ���� �� ������� �� �������� ����� �������
				List<ListTransferToBank> listLTToBank = this.getLTWithEmptyNamePack();
				
				//������ ����� report.log ��� ������ ���������� �� ���, � ����� ����� ��� ������� ���� ������ �� ����������
				List<String> listStringsOfReportFile = getListStringsOfReportFile(listLTToBank);
				
				//����� �������� � ��� ����� debut � ������, ���������� � ������
				this.findNamePackLT(listLTToBank, listStringsOfReportFile);
				
				//���������� � ��
				if (listLTToBank.size() > 0) {
					this.updateListTransferToBank(listLTToBank);
				}
				
				
				
				//----------------------POOD------------------------------
				//��������� ������ DocPOOD, ������������ � ��������, �� ������� �� �������� ����� �������
				List<DocPOOD> listDocPOOD = this.getDocPOODEmptyNamePack();
		
				//������ ����� report.log ��� ������ ���������� �� ���, � ����� ����� ��� ������� ���� POOD
				listStringsOfReportFile = getListStringsOfReportFileByPOOD(listDocPOOD);
				
				
				//����� �������� � ��� ����� debut � POOD ������, ���������� � ������
				this.findNamePackPOOD(listDocPOOD, listStringsOfReportFile);
				
				//���������� � ��
				if (listDocPOOD.size() > 0) {
					this.updatePOOD(listDocPOOD);
				}
				
				
				//----------------------POOS------------------------------
				//��������� ������ POOS �� ���������, ������������ � ���� �� ������� �� �������� ����� �������
				List<DocPOOS> listPOOS = this.getPOOSWithEmptyNamePack();
				
				//������ ����� report.log ��� ������ ���������� �� ���, � ����� ����� ��� ������� ���� POOS
				listStringsOfReportFile = getListStringsOfReportFileByPOOS(listPOOS);
				
				//����� �������� � ��� ����� debut � POOS ������, ���������� � ������
				this.findNamePackPOOS(listPOOS, listStringsOfReportFile);
				
				//���������� � ��
				if (listPOOS.size() > 0) {
					this.updatePOOS(listPOOS);
				}
				
				//----------------------PSMP------------------------------
				//��������� ������ PSMP �� ���������, ������������ � ����, �� ������� �� �������� ����� �������
				List<MergedPSMP> listMergedPSMP = this.getPSMPWithEmptyNamePack();
				
				//������ ����� report.log ��� ������ ���������� �� ���, � ����� ����� ��� ������� ���� POOS
				listStringsOfReportFile = getListStringsOfReportFileByPSMP(listMergedPSMP);
				
				//����� �������� � ��� ����� debut � PSMP ������, ���������� � ������
				this.findNamePackPSMP(listMergedPSMP, listStringsOfReportFile);
				
				//���������� � ��
				if (listMergedPSMP.size() > 0) {
					this.updateMergedPSMP(listMergedPSMP);
				}
				
				

				List<File> listFileNotifications = new ArrayList<>();
				//������������ �������� � ����������� �� ����
				File[] filesNotificationsOPFR = 
						sourcePathNotificationOPFR.toFile().listFiles(new FileFilter() {
																		public boolean accept(File pathname) {
																			return (pathname.isFile()) ? true : false;
																		}});
				
				if (filesNotificationsOPFR != null)
					listFileNotifications.addAll(Arrays.asList(filesNotificationsOPFR));
				
				
				//������������ �������� � ����������� �� ������
				File[] filesNotificationsBank = 
						sourcePathNotificationBanks.toFile().listFiles(new FileFilter() {
																		public boolean accept(File pathname) {
																			return (pathname.isFile()) ? true : false;
																		}});
				
				if (filesNotificationsBank != null)
					listFileNotifications.addAll(Arrays.asList(filesNotificationsBank));
				
				//���������� ������. ������������� ������ � ������������ ������� ��������
				Set<String> setStrings = new HashSet<>();
				List<File> listFilterFiles = new ArrayList<>();
				for (File file : listFileNotifications) {
					
					//�������� ������, � ������� ���������� �� txt
					if (!file.toString().endsWith(".txt")) {
						file.delete();
						continue;
					}
					
					
					ParserNotification parser = new ParserNotification(file);
					try {
						parser.parse();
					} catch (Exception e) {
						//�������� ��������� ����� � � ����. ��������
						file.delete();
						continue;
					}
					
					String attachment = new String(parser.getAttachment());
					if (!setStrings.contains(attachment)) {
						setStrings.add(attachment);
						listFilterFiles.add(file);
					}
				}
				
				List<Notification> listNotifications = new ArrayList<>();
				for (File fileNotification : listFilterFiles) {
					Notification notification = NotificationFactory.getNotification(fileNotification, sessionFactory, archivePathNotification);
					if (notification != null)
						listNotifications.add(notification);
				}
				
				//���������� ��������� � �����
				for (Notification n : listNotifications) {
					n.saveToArchive();
				}
				
				//���������� � ��
				if (listNotifications.size() > 0) {
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();

						for (Notification notification : listNotifications) {
							session.saveOrUpdate(notification);
						}
						
						t.commit();
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ������ � ��", ex);
						
					} finally {
						session.close();
					}
				}
				
				//�������� �������� ���������
				for (File file : listFilterFiles) 
					if (!file.delete()) throw new Exception("�� ������� ������� ����: " + file);
				
				
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			//��������������� checkboxes
			Component[] components = this.frame.panelChbxes.getComponents();
			for (Component component : components) {
				component.setEnabled(true);
			}
			
		} catch (final Exception e) {
			
			this.funcException(e, 
					new File("logs/log_receivingNotification.txt"), 
					this.lblIndError, 
					this.btnStart, 
					this.tpForIndicator);
		
		} finally {
			
			this.procFinally();
		}
	}
}
