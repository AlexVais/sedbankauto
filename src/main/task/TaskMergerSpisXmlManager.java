package main.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import main.MainFrame;
import main.Utils;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.entityinfoexchange.order.spis.SPIS;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserSPIS;
import ru.pfrf.person.Recipient;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.person.RecipientSPIS;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.ListTransferToBank;

public class TaskMergerSpisXmlManager extends ThreadTask {
	private static Long intervalDefault = 300000L;  //�������� �� ���������
	
	private MainFrame frame;

	public TaskMergerSpisXmlManager(SessionFactory sessionFactory, 
			File fileConfig,
			MainFrame frame) {

		super(sessionFactory, 
				fileConfig,
				frame.plIndMergerSpisXmlManager,
				frame.prbMergerSpisXmlManager, 
				frame.btnStartMergerSpisXmlManager, 
				frame.btnStopMergerSpisXmlManager,
				frame.lblIndErrorMergerSpisXmlManager,
				frame.tabbedPane,
				0);
		
		this.frame = frame;
		this.interval = intervalDefault;
	}
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalMergerSpisXmlManager")) * 1000;
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {
				this.beforeRun();
				
				Session session = null;
				Transaction t = null;
				List<CreatedRegisterTF> listRegs = new ArrayList<>();
				try {
					session = this.sessionFactory.openSession();
					t = session.beginTransaction();
					
					String query = "SELECT DISTINCT registers_tf.*" +
							   " FROM registers_tf " + 
							   		//" INNER JOIN DOCUMENT ON ATTACHMENT.PARENT_ID = DOCUMENT.ID" +
							   " WHERE" + 
							   		" registers_tf.counterparty = 6" + 
							   	    " AND registers_tf.yearPay = 2017" + 
							   		" AND registers_tf.monthPay < 12" +
							   	    " AND registers_tf.typePay <> 2"
							   	    ;
					
					listRegs = session.createSQLQuery(query).addEntity(CreatedRegisterTF.class).list();
					
					t.commit();
				} catch (Exception ex) {
					t.rollback();
					throw new Exception("������ ��������� �������", ex);
					
				} finally {
					session.close();
				}
			
				for (CreatedRegisterTF reg : listRegs) {
					try{
						ListTransferToBank LTToBank = reg.getRegisterTFToOPFR().getSetRegisterTFFromOPFR().iterator().next().getRegisterTFMerger().getRegisterTFToOPFR2().getListTransferToBank().iterator().next();
					
						Path pathTargetDir = Paths.get("C:/Users/admin/pfr/SedBankAutoProc/SedbankAutoProc/test/" + 
										      reg.getMonthPay() + "." + reg.getYearPay() + 
										      "/" + new SimpleDateFormat("dd.MM.YYYY").format(reg.getDateOfFunding()) +
										      "/" + reg.getTypePay().getName() + (reg.getSubtypePay() != null ? " (" + reg.getSubtypePay().getName() + ")" : "") + "_" + reg.getNumber()  
										       
											);
						
						
						if (!pathTargetDir.toFile().exists()) {
							pathTargetDir.toFile().mkdirs();
						}
						
						String fileName = new String(LTToBank.getNamePack() + "_ID" + LTToBank.getSendLTToBank().getId());
						Path pathSource = Paths.get("S:/SED_BANK/�����/����� � ���. �����/" + reg.getDeliveryOrg().getShortName() + "/" + reg.getYearPay() + "/" + reg.getMonthPay() + "/" + fileName);
						Path pathTarget = Paths.get(pathTargetDir + "/" + fileName);
						Files.copy(pathSource, pathTarget, StandardCopyOption.REPLACE_EXISTING);
						
						
						
						//���������� �������
						Utils.unpack7zip(pathTarget.toFile(), pathTargetDir, Paths.get("S:/SED_BANK/7z.exe"));
						
						//�������� ����� ��� ����������
						Files.delete(pathTarget);
						
						
							//����������� �������
						String fileNameReg = "��1_" + reg.getYearPay() + String.valueOf(reg.getMonthPay() + 100).substring(1) + "_" + reg.getDeliveryOrg().getShortName() + "_" + reg.getNumber() + ".xlsx";
						Path PathReg = Paths.get("S:/SED_BANK/�����/������� ��/�� ����/" + fileNameReg); 
						Files.copy(PathReg, Paths.get(pathTargetDir + "/" + fileNameReg), StandardCopyOption.REPLACE_EXISTING);
						
						//�������� ��� ����� ������������� ������ �� ����������
						File[] files = pathTargetDir.toFile().listFiles(new FilenameFilter() {
																public boolean accept(File dir, String name) {
																	return name.matches(".+SPIS.+") ? true : false;
																}});
						if (files.length > 1) 
							throw new Exception("files > 1");
						
						File fileSPIS = files[0];
						
						//������� �������
						ParserSPIS parserSPIS = new ParserSPIS();
						parserSPIS.parse(fileSPIS);
						SPIS spis = new SPIS(parserSPIS);
						spis.init();
						
							//�������� xml ������ 
						files = pathTargetDir.toFile().listFiles(new FilenameFilter() {
													public boolean accept(File dir, String name) {
														return name.matches("^PFR-.+") ? true : false;
													}});
						for (File file : files) 
							Files.delete(file.toPath());
				
						
							//������������ ������� ������� ����������� �� �������
						List<RecipientSPIS>[] arrayListRecipients = new List[22];
						for (int i = 0; i < 22; i++)
							arrayListRecipients[i] = new ArrayList<>();
					
						for (RecipientSPIS r : spis.getSetRecipientListTransfer())
							arrayListRecipients[r.getArea() - 1].add(r);
						
						//����������� ������� ����������� � ������� Excel
						Integer area = 0;
						for (List<RecipientSPIS> listRecipients : arrayListRecipients) {
							
							area++;
							if (listRecipients.isEmpty()) 
								continue;
							
							//���������� �� ������ ��
							Collections.sort(listRecipients, new Comparator<RecipientSPIS>() {
																	public int compare(RecipientSPIS r1, RecipientSPIS r2) {
																		return r1.getNumberVD().compareTo(r2.getNumberVD());
																	}});
							
							
							XSSFWorkbook book = new XSSFWorkbook();
							XSSFSheet sheet = book.createSheet("���� 1");
							
							
							sheet.setColumnWidth(1, 3171);
							sheet.setColumnWidth(2, 10671);
							sheet.setColumnWidth(3, 6006);
							sheet.setColumnWidth(4, 4886);
							
					        XSSFRow row = null; 
					        XSSFCell cell = null;
					        
					        	//�������� �������
					        row = sheet.createRow(0);
					        row.createCell(0).setCellValue("������ ����������� ������: �������������� ������ ��� ��� \"�����-����\" �� " + reg.getMonthPay() + "." + reg.getYearPay() + " ���. �����: " + area + ". �������: " + reg.getTypePay().getName() + (reg.getSubtypePay() != null ? " (" + reg.getSubtypePay().getName() + ")" : ""));
					        
					        	//����� �������
					        row = sheet.createRow(2);
					        row.createCell(0).setCellValue("� �/�");
					        row.createCell(1).setCellValue("����. ����");
					        row.createCell(2).setCellValue("���");
					        row.createCell(3).setCellValue("����� �����");
					        row.createCell(4).setCellValue("����� � �������");
					        
					     		//����
					        BigDecimal totalAmount = new BigDecimal(0);
					        Integer i = 0;
					        short numberRow = 3;
							for(RecipientSPIS r : listRecipients) {
								totalAmount = totalAmount.add(r.getAmountDelivery());
								i++;
								
								row = sheet.createRow(numberRow++);
					        	row.createCell(0).setCellValue(i);
					        	row.createCell(1).setCellValue(r.getNumberVD());
					        	row.createCell(2).setCellValue(r.FIO());
					        	row.createCell(3).setCellValue(r.getAccountNumber());
					        	row.createCell(4).setCellValue(r.getAmountDelivery().toString());
					        	
					        	//�������������� 
					        	;
							}
							
								//�������� ����� �� ������
							numberRow++;
							row = sheet.createRow(numberRow);
							row.createCell(4).setCellValue("�����: " + totalAmount.toString());
							
							book.write(new FileOutputStream(pathTargetDir + "/" + "������_" + area + ".xlsx"));
					        book.close();
						}
					
				        
				     
						System.out.println(spis.getNumberPack() + " | " + spis.getSetRecipientListTransfer().size());
					} catch (Exception ex) {
						System.out.println("error: " + reg.getNumber());
						ex.printStackTrace();
					}
				}
				
				
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (Exception e) {
			
			try (FileOutputStream fos = new FileOutputStream(new File("logs/log_mergerSpisXmlManager.txt"));
				 PrintWriter pw = new PrintWriter(fos)) {
				
					pw.print(new Date().toString() + "\n");
					e.printStackTrace(pw);
					
				} catch (Exception e1) {
					e1.printStackTrace();	
				}
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					lblIndError.setVisible(true);
					btnStart.setEnabled(false);	
					tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
					
				}
			});
			
			
		} finally {
			this.procFinally();
		}
		
		
		
	}

}
