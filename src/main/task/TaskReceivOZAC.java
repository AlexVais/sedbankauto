package main.task;

import java.io.BufferedReader;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import main.MainFrame;
import main.SedbankAutoProcessing;
import main.Utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.entityinfoexchange.order.ozac.DifferenceSPIS_OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOZAC;
import ru.pfrf.person.DifferenceRecipient;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.process.ProcessAdapter;
import ru.pfrf.process.ReceivingOZAC;
import ru.pfrf.process.ReceivingOZACAdapter;
import static main.SedbankAutoProcessing.VER_PRODUCTION;

public class TaskReceivOZAC extends ThreadTask{
	
	private static Long intervalDefault = 300000L;  //�������� �� ���������
	
	private MainFrame frame;

	public TaskReceivOZAC(SessionFactory sessionFactory, 
			File fileConfig,
			MainFrame frame) {

		super(sessionFactory, 
				fileConfig,
				frame.plIndReceivOZAC,
				frame.prbReceivOZAC, 
				frame.btnStartReceivOZAC, 
				frame.btnStopReceivOZAC,
				frame.lblIndErrorReceivOZAC,
				frame.tabbedPane,
				0);
		
		this.frame = frame;
		this.interval = intervalDefault;
	}
	
	@Override
	public void run() {
		ReceivingOZAC receiving = null;
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalReceivOZAC")) * 1000;
				this.pathMimCmdExe = Paths.get(new String(p.getProperty("pathMimCmdExe").getBytes("ISO8859-1")));
				this.addrMessage = p.getProperty("addrMessage");
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {

				this.beforeRun();
				
				receiving = new ReceivingOZAC(sessionFactory, this.fileConfig);
				try {
					receiving.start();
				} catch (Exception e) {
					throw new Exception("������ ��� ����� OZAC", e);
				}
				
					
				
				//��������
				ProcessAdapter processAdapter = new ProcessAdapter(this.sessionFactory,
															receiving.getDateBegin(),
															receiving.getDateEnd(),
															"ServerNamePC",
															"server");
				
				ReceivingOZACAdapter receivingAdapter = new ReceivingOZACAdapter(processAdapter);
				processAdapter.setReceivingOZACAdapter(receivingAdapter);
				for(DocOZAC docOZAC : receiving.getSetDocOZAC()) {
					receivingAdapter.getSetOPVF().add(docOZAC.getOpvf());
					receivingAdapter.getSetOZAC().add(docOZAC.getOzac());
					receivingAdapter.getSetSPRA().add(docOZAC.getSpra());
					docOZAC.getOpvf().setReceivingOZACAdapter(receivingAdapter);
					docOZAC.getOzac().setReceivingOZACAdapter(receivingAdapter);
					if (docOZAC.getSpra() != null)
						docOZAC.getSpra().setReceivingOZACAdapter(receivingAdapter);
				}
				
				if (!receiving.getSetDocOZAC().isEmpty()) {
					
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();

						session.saveOrUpdate(receiving);
						session.save(processAdapter);
						
						t.commit();
						
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ������ � ��", ex);
						
					} finally {
						session.close();
					}
					
					//�������� ��������� � ����� �������� ������������ ���
					if (SedbankAutoProcessing.ver == VER_PRODUCTION) 
						Utils.sendMessageMiranda(this.pathMimCmdExe, "������� ����� OZAC �� ������", "babenko@10.38.23.176");
				}
				
				
				
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (Exception e) {
			
			try (FileOutputStream fos = new FileOutputStream(new File("logs/log_receivingOZAC.txt"));
				 PrintWriter pw = new PrintWriter(fos)) {
				
					pw.print(new Date().toString() + "\n");
					e.printStackTrace(pw);
					
			} catch (Exception e1) {
					e1.printStackTrace();	
			}
			
			//�������� ��������� �� ������
			if (SedbankAutoProcessing.ver == VER_PRODUCTION)
			try {
				Utils.sendMessageMiranda(this.pathMimCmdExe, e.toString(), addrMessage);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//���������� ������ ����� � ��
			Session session = null;
			Transaction t = null;
			try {
				session = this.sessionFactory.openSession();
				t = session.beginTransaction();

				session.save(receiving.getOzacReceivError());
				
				t.commit();
				
			} catch (Exception ex) {
				t.rollback();
				
			} finally {
				session.close();
			}
			
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					lblIndError.setVisible(true);
					btnStart.setEnabled(false);	
					tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
					
				}
			});
			
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			this.procFinally();
		}
		
		
		
	}
}
