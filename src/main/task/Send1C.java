package main.task;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.person.Person;

@Entity
@Table(name = "sending1c")
public class Send1C implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID") 
	protected Integer id;
	
	@Column(name = "fileName")
	private String fileName;
	
	
	@Column(name = "dateSend1C")
	private Date dateSend1C;
	
	@Column(name = "dateSendVipnet")
	private Date dateSendVipnet;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getDateSend1C() {
		return dateSend1C;
	}

	public void setDateSend1C(Date dateSend1C) {
		this.dateSend1C = dateSend1C;
	}

	public Date getDateSendVipnet() {
		return dateSendVipnet;
	}

	public void setDateSendVipnet(Date dateSendVipnet) {
		this.dateSendVipnet = dateSendVipnet;
	}

	public Send1C() {};
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		Send1C send1C = (Send1C) obj;
		return new EqualsBuilder()
		                 .append(this.fileName, send1C.fileName)
		                 .append(this.dateSend1C, send1C.dateSend1C)
		                 .append(this.dateSendVipnet,  send1C.dateSendVipnet)
		                 .isEquals();
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public String toString() {
		return this.fileName + 
				" | " + this.dateSend1C + 
				" | " + this.dateSendVipnet;
	}
	
	
}
