package main.task;

import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.swing.*;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.ListTransferToBank;
import ru.pfrf.register.RegisterTFToOPFR;
import main.MainFrame;

public class TaskDefinVipnet extends ThreadTask {
	private static Long intervalDefault = 300000L; //�������� �� ���������
	
	private MainFrame frame;
	private Path pathVipnetDB;

	public TaskDefinVipnet(SessionFactory sessionFactory, 
						File fileConfig,
						MainFrame frame) {
		super(sessionFactory, 
			fileConfig,
			frame.pnlIndDefinVipnet, 
			frame.prbDefinVipnet, 
			frame.btnStartDefinVipnet, 
			frame.btnStopDefinVipnet,
			frame.lblIndErrorDefinVipnet,
			frame.tabbedPane,
			4);

		this.frame = frame;
		this.interval = intervalDefault;

	}
	
	
	
	private List<ListTransferToBank> getListTransferToBank() throws Exception {
		
		Session session = null;
		Transaction t = null;
		List<ListTransferToBank> listLT = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(ListTransferToBank.class)
									   .add(Restrictions.isNull("dateSendVipnet"))
									   .add(Restrictions.ge("id", 302)) //�� ����� ID �����
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listLT = criteria.list();
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ ListTransferToBank", ex);
			
		} finally {
			session.close();
		}
		return listLT;
	}
	
	private void findAndSetLT(List<ListTransferToBank> listLT) throws Exception {
		//������
		Class.forName("org.sqlite.JDBC");
		try (Connection connection = DriverManager.getConnection("jdbc:sqlite:" + this.pathVipnetDB)) {  
			
			Statement s = connection.createStatement();
			
			for (ListTransferToBank lt : listLT) {
				String fileName = lt.getNamePack();
				long time = lt.getSendLTToBank().getProcessAdapter().getDateBegin().getTime() / 1000;
				String query = "SELECT document.*, ATTACHMENT.FLAG as a_flag" +
							   " FROM ATTACHMENT" + 
							   		" INNER JOIN DOCUMENT ON ATTACHMENT.PARENT_ID = DOCUMENT.ID" +
							   " WHERE" + 
							   		" ATTACHMENT.FILE_NAME = \"" + fileName + "\"" + 
							   	    " AND document.REGNUMBER <> 0" + 
							   		" AND document.CREATION_TIME >= " + time;
				
				ResultSet rs = s.executeQuery(query);
				int count = 0;
				while (rs.next()) {
					if (count > 0) 
						throw new Exception("count > 0; " + fileName);
					String user_name = rs.getString("name"); 
					Date date = new Date(rs.getLong("CREATION_TIME") * 1000); 
					lt.setDateSendVipnet(date);
					
					count++;
				}
				rs.close();
			}
		} finally {
		}
	}
	
	
	private void updateLT(List<ListTransferToBank> listLT) throws Exception {
		//���������� � ��
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			for (ListTransferToBank lt : listLT)
				session.update(lt);
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� ListTransferToBank", ex);
			
		} finally {
			session.close();
		}
	}
	
	
	private List<PackFromCombank> getListPacks() throws Exception {
		List<PackFromCombank> listPacks = null;
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(PackFromCombank.class)
									   .add(Restrictions.isNull("dateReceivVipnet"))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			listPacks = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ PackFromCombank", ex);
			
		} finally {
			session.close();
		}
		return listPacks;
	}
	
	
	private void findAndSetPacks(List<PackFromCombank> listPacks) throws Exception {
		//������
		Class.forName("org.sqlite.JDBC");
		try (Connection connection = DriverManager.getConnection("jdbc:sqlite:" + this.pathVipnetDB)) {  
			
			Statement s = connection.createStatement();
			
			for (PackFromCombank pack : listPacks) {
				String fileName = pack.getFileName();
				long time = pack.getReceivingPacksFromCombanksAdapter().getProcessAdapter().getDateBegin().getTime() / 1000;
				String query = "SELECT document.*, ATTACHMENT.FLAG as a_flag" +
							   " FROM ATTACHMENT" + 
							   		" INNER JOIN DOCUMENT ON ATTACHMENT.PARENT_ID = DOCUMENT.ID" +
							   " WHERE" + 
							   		" document.REGNUMBER = 0" + 
							   		" AND ATTACHMENT.FILE_NAME = \"" + fileName + "\"" +
							   		" AND RECEIVER_TIME_RECIPIENT < " + time + 
							   " ORDER BY " + 
							   		" RECEIVER_TIME_RECIPIENT DESC";
				
				ResultSet rs = s.executeQuery(query);
				if (rs.next()) {
					Date date = new Date(rs.getLong("RECEIVER_TIME_RECIPIENT") * 1000);
					pack.setDateReceivVipnet(date);
				}
				rs.close();
			}
		
		} finally {
		}  
	}
	
	
	private void updatePacks(List<PackFromCombank> listPacks) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			for (PackFromCombank pack : listPacks) {
				session.update(pack);
				
				for (OZAC ozac : pack.getSetOZAC()) {
					DocOZAC docOZAC = ozac.getDocOZAC();
					if (docOZAC != null) {
						docOZAC.setDateReceivVipnet(pack.getDateReceivVipnet());
						session.update(docOZAC);
					}
				}
			}	
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� PackFromCombank", ex);
			
		} finally {
			session.close();
		} 
	}
	
	
	private List<RegisterTFToOPFR> getListRegs() throws Exception {
		List<RegisterTFToOPFR> listRegs = null;
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(RegisterTFToOPFR.class)
									   .add(Restrictions.isNull("dateSendVipnet"))
									   .add(Restrictions.ge("id", 298))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			listRegs = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ RegisterTFToOPFR", ex);
			
		} finally {
			session.close();
		}
		return listRegs;
	}
	
	
	private void findAndSetRegs(List<RegisterTFToOPFR> listRegs) throws Exception {
		//������
		Class.forName("org.sqlite.JDBC");
		try (Connection connection = DriverManager.getConnection("jdbc:sqlite:" + this.pathVipnetDB)) {  
			
			Statement s = connection.createStatement();
			
			for (RegisterTFToOPFR reg : listRegs) {
				CreatedRegisterTF createdRegisterTF = reg.getCreatedRegisterTF();
				
				String fileName = "��1_" + 
								  new SimpleDateFormat("YYYYMM").format(createdRegisterTF.getDateOfFunding()) + 
								  "_" + createdRegisterTF.getDeliveryOrg().getShortName() + 
								  "_" + createdRegisterTF.getNumber() + ".xlsx";
				//long time = pack.getReceivingPacksFromCombanksAdapter().getProcessAdapter().getDateBegin().getTime() / 1000;
				String query = "SELECT document.*, ATTACHMENT.FLAG as a_flag, ATTACHMENT.ID as A_ID" +
							   " FROM ATTACHMENT" + 
							   		" INNER JOIN DOCUMENT ON ATTACHMENT.PARENT_ID = DOCUMENT.ID" +
							   " WHERE" + 
							   		" document.REGNUMBER <> 0" +  
							   		" AND ATTACHMENT.FILE_NAME = \"" + fileName + "\"" +
							   " ORDER BY " + 
							   		" RECEIVER_TIME_RECIPIENT ASC";
				
				ResultSet rs = s.executeQuery(query);
				if (rs.next()) {
					Date date = new Date(rs.getLong("CREATION_TIME") * 1000);
					reg.setDateSendVipnet(date);
				}
				rs.close();
			}
		
		} finally {
		}  
	}
	
	
	private void updateRegs(List<RegisterTFToOPFR> listRegs) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			for (RegisterTFToOPFR reg : listRegs) {
				session.update(reg);
			}	
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� RegisterTFToOPFR", ex);
			
		} finally {
			session.close();
		} 
	}
	
	
	private List<Send1C> getListSend1C() throws Exception {
		List<Send1C> listSend1C = null;
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(Send1C.class)
									   .add(Restrictions.isNull("dateSendVipnet"))
									   //.add(Restrictions.ge("id", 800))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			listSend1C = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ Send1C", ex);
			
		} finally {
			session.close();
		}
		return listSend1C;
	}
	
	
	private void findAndSetSend1C(List<Send1C> listSend1C) throws Exception {
		//������
		Class.forName("org.sqlite.JDBC");
		try (Connection connection = DriverManager.getConnection("jdbc:sqlite:" + this.pathVipnetDB)) {  
			
			Statement s = connection.createStatement();
			
			for (Send1C send1C : listSend1C) {
				
				String fileName = send1C.getFileName().substring(0, 3) + "a" + send1C.getFileName().substring(4);
				long time = send1C.getDateSend1C().getTime() / 1000;
				String query = "SELECT document.*, ATTACHMENT.FLAG as a_flag, ATTACHMENT.ID as A_ID" +
							   " FROM ATTACHMENT" + 
							   		" INNER JOIN DOCUMENT ON ATTACHMENT.PARENT_ID = DOCUMENT.ID" +
							   " WHERE" + 
							   		" document.REGNUMBER <> 0" +  
							   		" AND ATTACHMENT.FILE_NAME = \"" + fileName + "\"" +
							   		" AND document.CREATION_TIME >= " + time + 
							   " ORDER BY " + 
							   		" RECEIVER_TIME_RECIPIENT ASC";
				
				ResultSet rs = s.executeQuery(query);
				if (rs.next()) {
					Date date = new Date(rs.getLong("CREATION_TIME") * 1000);
					//System.out.println(fileName + "  " + new SimpleDateFormat("YYYY.MM.dd HH:mm:ss").format(date));
					send1C.setDateSendVipnet(date);
				}
				rs.close();
			}
		
		} finally {
		}  
	}
	
	
	private void updateSend1C(List<Send1C> listSend1C) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			for (Send1C send1C : listSend1C) {
				session.update(send1C);
			}	
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� Send1C", ex);
			
		} finally {
			session.close();
		} 
	}
	
	
	private List<DocOZAC> getListDocOZAC() throws Exception {
		
		Session session = null;
		Transaction t = null;
		List<DocOZAC> listDocOZAC = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(DocOZAC.class, "docOZAC")
									   .createCriteria("docOZAC.ozac", "ozac", JoinType.INNER_JOIN)
									   .createCriteria("ozac.deliveryOrg", "deliveryOrg", JoinType.INNER_JOIN)
									   .add(Restrictions.ne("deliveryOrg.shortName", "8636"))
									   .add(Restrictions.isNull("docOZAC.dateReceivVipnet"))
									   .add(Restrictions.ge("docOZAC.id", 700)) 
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listDocOZAC = criteria.list();
			for (DocOZAC docOZAC : listDocOZAC) 
				System.out.println(docOZAC + " fileName = " + docOZAC.getOzac().getFileName());
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ DocOZAC", ex);
			
		} finally {
			session.close();
		}
		return listDocOZAC;
	}
	
	
	private void findAndSetDocOZAC(List<DocOZAC> listDocOZAC) throws Exception {
		//������
		Class.forName("org.sqlite.JDBC");
		try (Connection connection = DriverManager.getConnection("jdbc:sqlite:" + this.pathVipnetDB)) {  
			
			Statement s = connection.createStatement();
			
			for (DocOZAC docOZAC : listDocOZAC) {
				PackFromCombank pack = docOZAC.getOzac().getPackFromCombank();
				if (pack == null)
					continue;
				
				String fileName = pack.getFileName();
				long time = pack.getReceivingPacksFromCombanksAdapter().getProcessAdapter().getDateBegin().getTime() / 1000;
				String query = "SELECT document.*, ATTACHMENT.FLAG as a_flag" +
							   " FROM ATTACHMENT" + 
							   		" INNER JOIN DOCUMENT ON ATTACHMENT.PARENT_ID = DOCUMENT.ID" +
							   " WHERE" + 
							   		" document.REGNUMBER = 0" + 
							   		" AND ATTACHMENT.FILE_NAME = \"" + fileName + "\"" +
							   		" AND RECEIVER_TIME_RECIPIENT < " + time + 
							   " ORDER BY " + 
							   		" RECEIVER_TIME_RECIPIENT DESC";
				System.out.println(query);
				
				ResultSet rs = s.executeQuery(query);
				if (rs.next()) {
					Date date = new Date(rs.getLong("RECEIVER_TIME_RECIPIENT") * 1000);
				 	docOZAC.setDateReceivVipnet(date);
					System.out.println(date);
				}
				rs.close();
			}
		
		} finally {
		}  
	}
	
	
	
	private void updateDocOZAC(List<DocOZAC> listDocOZAC) throws Exception {
		//���������� � ��
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			for (DocOZAC docOZAC : listDocOZAC)
				session.update(docOZAC);
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� DocOZAC", ex);
			
		} finally {
			session.close();
		}
	}
	
	
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(prop.getProperty("intervalDefinVipnet")) * 1000;
				this.pathVipnetDB = Paths.get(new String(prop.getProperty("pathVipnetDB").getBytes("ISO8859-1")));
				
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {
				this.beforeRun();
				
				
				//----------���������� ���� �������� ������� �� ���������� �� vipnet------
					//��������� ������ ������� �� ����������, ������������ � ����, � ������� ������ ���� �������� �� vipnet
				List<ListTransferToBank> listLT = this.getListTransferToBank();
				
					//����� � �� vipnet ���������� � ��������� �����. ���� �������� � ������ �� ����������
				this.findAndSetLT(listLT);
				
					//���������� � ��
				this.updateLT(listLT);
				
				
				
				//----------����������� ���� ����� docOZAC �� ���. ������-----------
					//��������� ������� docOZAC � ������ ��������� dateReceivVipnet
				List<DocOZAC> listDocOZAC = this.getListDocOZAC();
				
					//����� � �� vipnet ���������� � ��������� �����. ���� ����� DocOZAC �� ���. ������
				this.findAndSetDocOZAC(listDocOZAC);
				
				//���������� � ��
				this.updateDocOZAC(listDocOZAC); 
				
				
				
				//----------����������� ���� ����� ������� �� ���. ������-----------
					//��������� ������� ������� � ������ ��������� dateReceivVipnet
				List<PackFromCombank> listPacks = this.getListPacks();
			
					//����� � �� vipnet ���������� � ��������� �����. ���� ����� ������� �� ���. ������
				this.findAndSetPacks(listPacks);
			
					//���������� � ��
				this.updatePacks(listPacks); 
				
				
				
				//---------����������� ���� �������� �������� �� � ����----------
					//��������� ������� ��������, ������������ � ����
				List<RegisterTFToOPFR> listRegs = this.getListRegs();
				
					//����� � �� vipnet ���������� � ��������� �����. ���� �������� � ������� �� ��� ���������
				this.findAndSetRegs(listRegs);
				
					//���������� � ��
				this.updateRegs(listRegs); 
				
				
				
				//---------����������� ���� �������� ������� � ���. ����� ����������� 1� ������������ �����----------
					//��������� ������� ������, ������������ � �����
				List<Send1C> listSend1C = this.getListSend1C();
				
					//����� � �� vipnet ���������� � ��������� �����. ���� �������� �
				this.findAndSetSend1C(listSend1C);
				
					//���������� � ��
				this.updateSend1C(listSend1C); 
				
					
				
				//��������� ������
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (final Exception e) {
			
			this.funcException(e, 
					new File("logs/log_definVipnet.txt"), 
					this.lblIndError, 
					this.btnStart, 
					this.tpForIndicator);
		
		} finally {
			this.procFinally();
		}
	}

	
}
