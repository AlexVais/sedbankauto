package main.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import main.MainFrame;

import org.hibernate.SessionFactory;

public class TaskSeparationOzacXmlManager extends ThreadTask {
	private static Long intervalDefault = 300000L;  //�������� �� ���������
	
	private MainFrame frame;

	public TaskSeparationOzacXmlManager(SessionFactory sessionFactory, 
			File fileConfig,
			MainFrame frame) {

		super(sessionFactory, 
				fileConfig,
				frame.plIndSeparationOzacXmlManager,
				frame.prbSeparationOzacXmlManager, 
				frame.btnStartSeparationOzacXmlManager, 
				frame.btnStopSeparationOzacXmlManager,
				frame.lblIndErrorSeparationOzacXmlManager,
				frame.tabbedPane,
				0);
		
		this.frame = frame;
		this.interval = intervalDefault;
	}
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalSeparationOzacXmlManager")) * 1000;
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {

				this.beforeRun();
				
			
				
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (Exception e) {
			
			try (FileOutputStream fos = new FileOutputStream(new File("logs/log_separationOzacXmlManager.txt"));
				 PrintWriter pw = new PrintWriter(fos)) {
				
					pw.print(new Date().toString() + "\n");
					e.printStackTrace(pw);
					
				} catch (Exception e1) {
					e1.printStackTrace();	
				}
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					lblIndError.setVisible(true);
					btnStart.setEnabled(false);	
					tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
					
				}
			});
			
			
		} finally {
			this.procFinally();
		}
		
		
		
	}
}
