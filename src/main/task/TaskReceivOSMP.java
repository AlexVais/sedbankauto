package main.task;

import java.awt.Color;
import java.awt.Frame;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import main.MainFrame;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.process.ReceivingOSMP;

public class TaskReceivOSMP extends ThreadTask {
	
	private static Long intervalDefault = 300000L; //�������� �� ���������
	
	private MainFrame frame;
	
	public TaskReceivOSMP(SessionFactory sessionFactory, 
					File fileConfig,
					MainFrame frame) {
		super(sessionFactory, 
				fileConfig,
				frame.pnlIndReceivOSMP, 
				frame.prbOSMP, 
				frame.btnStartOSMP, 
				frame.btnStopOSMP,
				frame.lblIndErrorReceivOSMP,
				frame.tabbedPane,
				1);
		
		this.frame = frame;
		this.interval = intervalDefault;
		
	}
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalOSMP")) * 1000;
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {

				this.beforeRun();
				
				ReceivingOSMP receivingOSMP = new ReceivingOSMP(sessionFactory, fileConfig);
				receivingOSMP.start();
				
				if (receivingOSMP.getListDocOSMP().size() > 0) {
					
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();

						session.persist(receivingOSMP);
						t.commit();
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ������ � ��", ex);
						
					} finally {
						session.close();
					}
				}
			
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (final Exception e) {
			
			this.funcException(e, 
					new File("logs/log_receivingOSMP.txt"), 
					this.lblIndError, 
					this.btnStart, 
					this.tpForIndicator);
			
			
		} finally {
			
			this.procFinally();
		}
		
		
		
	}
	
}
