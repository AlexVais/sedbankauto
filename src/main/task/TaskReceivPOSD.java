package main.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ProcessAdapter;
import ru.pfrf.process.ReceivingPOSD;
import ru.pfrf.process.ReceivingPOSDAdapter;
import ru.pfrf.process.ReceivingPacksFromCombanks;
import ru.pfrf.process.ReceivingPacksFromCombanksAdapter;
import main.MainFrame;

public class TaskReceivPOSD extends ThreadTask {
	
	private static Long intervalDefault = 300000L; //�������� �� ���������
	
	private MainFrame frame;
	
	public TaskReceivPOSD(SessionFactory sessionFactory, 
			File fileConfig,
			MainFrame frame)  {

		super(sessionFactory, 
				fileConfig,
				frame.plIndReceivPOSD,
				frame.prbReceivPOSD, 
				frame.btnStartReceivPOSD, 
				frame.btnStopReceivPOSD,
				frame.lblIndErrorReceivPOSD,
				frame.tabbedPane,
				0);
		
		this.frame = frame;
		this.interval = intervalDefault;

	}
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalPOSD")) * 1000;
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {

				this.beforeRun();
				
				ReceivingPOSD receiving = new ReceivingPOSD(sessionFactory, this.fileConfig);
				receiving.start();
				
				//��������
				ProcessAdapter processAdapter = new ProcessAdapter(this.sessionFactory,
															receiving.getDateBegin(),
															receiving.getDateEnd(),
															"ServerNamePC",
															"server");
				
				ReceivingPOSDAdapter receivingAdapter = new ReceivingPOSDAdapter(processAdapter);
				processAdapter.setReceivingPOSDAdapter(receivingAdapter);
				for(DocPOSD docPOSD : receiving.getSetDocPOSD()) {
					receivingAdapter.getSetDocPOSD().add(docPOSD);
					docPOSD.setReceivingPOSDAdapter(receivingAdapter);
				}
				
				if (receiving.getSetDocPOSD().size() > 0) {
					
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();

						session.save(receiving);
						session.save(processAdapter);
						t.commit();
						
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ������ � ��", ex);
						
					} finally {
						session.close();
					}
				}
			
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (Exception e) {
			
			try (FileOutputStream fos = new FileOutputStream(new File("logs/log_receivingPOSD.txt"));
				 PrintWriter pw = new PrintWriter(fos)) {
				
					pw.print(new Date().toString() + "\n");
					e.printStackTrace(pw);
					
				} catch (Exception e1) {
					e1.printStackTrace();	
				}
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					lblIndError.setVisible(true);
					btnStart.setEnabled(false);	
					tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
					
				}
			});
			
			
		} finally {
			this.procFinally();
		}
		
		
		
	}

}
