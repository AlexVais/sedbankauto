package main.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;

import main.MainFrame;
import main.Utils;

import org.dom4j.Element;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.classifier.CodeCredited;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.SeparatedOZAC;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOZAC;
import ru.pfrf.person.CorrectRecipient;
import ru.pfrf.person.Recipient;
import ru.pfrf.person.RecipientListTransfer;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.person.RecipientSPIS;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.ListTransferToBank;
import ru.pfrf.register.RegisterTFToOPFR;

public class TaskMoveOZAC extends ThreadTask {
	private static Long intervalDefault = 300000L; //�������� �� ���������
	
	private MainFrame frame;
	private Path pathArchiveOZACAfterSeparation;
	private Path pathOZACForONVP;
	private Boolean isAllOZAC;
	private Boolean isRestoreRecipients;
	private Boolean isRestoreOnlyDeleted;
	

	public TaskMoveOZAC(SessionFactory sessionFactory, 
						File fileConfig,
						MainFrame frame) {
		super(sessionFactory, 
				fileConfig,
				frame.plIndMoveOZAC, 
				frame.prbMoveOZAC, 
				frame.btnStartMoveOZAC, 
				frame.btnStopMoveOZAC,
				frame.lblIndErrorMoveOZAC,
				frame.tabbedPane,
				0);

		this.frame = frame;
		this.interval = intervalDefault;
	}
	
	
	private List<CreatedRegisterTF> getListRegNotWorked() throws Exception {
		List<CreatedRegisterTF> listRegNotWorked = null;
		
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(CreatedRegisterTF.class, "reg")
									   .createCriteria("reg.setDocPOSD", "docPOSD", JoinType.INNER_JOIN)
									   .createCriteria("docPOSD.docOZAC", "docOZAC", JoinType.INNER_JOIN)
									   .createCriteria("docOZAC.docPOOD", "docPOOD", JoinType.INNER_JOIN)
									   .add(Restrictions.isNotNull("reg.dateMoveOZAC"))
									   .add(Restrictions.isNull("reg.isWorkedMoveOZAC"))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			
			
			listRegNotWorked = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ CreatedRegisterTF", ex);
			
		} finally {
			session.close();
		}
		
		return listRegNotWorked;
	}
	
	
	private List<OZAC> getListOZAC(List<CreatedRegisterTF> listRegNotWorked)
			throws Exception {
		
		Set<OZAC> setOZAC = null;
		List<OZAC> totalListOZAC = new ArrayList<>();
		
		for (CreatedRegisterTF reg : listRegNotWorked) {
			
			Session session = null;
			Transaction t = null;
			try {
				session = this.sessionFactory.openSession();
				t = session.beginTransaction();
				
				Criteria criteria = session.createCriteria(OZAC.class, "ozac")
										   .createCriteria("ozac.setSeparatedOZAC", "separatedOZAC", JoinType.INNER_JOIN)
										   .createCriteria("ozac.docOZAC", "docOZAC", JoinType.INNER_JOIN)
										   .add(Restrictions.eq("ozac.year", reg.getYearPay()))
										   .add(Restrictions.eq("ozac.numberPack", reg.getNumber()));
										   //.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				
				
				setOZAC = new HashSet<>(criteria.list());
				totalListOZAC.addAll(setOZAC);
				
				t.commit();
			} catch (Exception ex) {
				t.rollback();
				throw new Exception("������ ��������� ������ SeparatedOZAC", ex);
				
			} finally {
				session.close();
			}
			
		}
		
		return totalListOZAC;	
	}
	
	/**
	 * �������������� ������ OZAC
	 * @param dirTempUnpack
	 * @param listSeparatedOZAC
	 * @throws Exception 
	 */
	private void restoreFileOZAC(File dirTempUnpack, 
							List<SeparatedOZAC> listSeparatedOZAC, 
							Map<RecipientSPIS, RecipientSPIS> mapForFind,
							Boolean isRestoreOnlyAccountNumber) throws Exception {
		
		for (SeparatedOZAC separatedOZAC : listSeparatedOZAC) {
			File fileSeparatedOZAC = new File(dirTempUnpack + "/" + separatedOZAC.getFileName()); 
			ParserOZAC parserOZAC = new ParserOZAC();
			parserOZAC.parse(fileSeparatedOZAC);
			SeparatedOZAC separatedOZACByFile = new SeparatedOZAC(parserOZAC);
			separatedOZACByFile.init();
			
			
			for (RecipientSPIS recipientSPIS : separatedOZACByFile.getSetRecipientsSPIS())  {
				RecipientSPIS recipientForCompare = new RecipientSPIS();
				recipientForCompare.setSurname(recipientSPIS.getSurname());
				recipientForCompare.setName(recipientSPIS.getName());
				recipientForCompare.setPatr(recipientSPIS.getPatr());
				recipientForCompare.setAccountNumber(recipientSPIS.getAccountNumber());
				recipientForCompare.setSNILS(recipientSPIS.getSNILS());
				recipientForCompare.setAmountDelivery(recipientSPIS.getAmountDelivery());
				
				RecipientSPIS recipientFind = mapForFind.get(recipientForCompare);
				if (recipientFind == null) 
					continue;
				
				Element element = recipientSPIS.getElement();
				
				if (isRestoreOnlyAccountNumber) {
					element.element("����������").setText(recipientFind.getAccountNumber());
				} else {
					Element elementFIO = element.element("���");
					elementFIO.element("�������").setText(recipientFind.getSurname());
					elementFIO.element("���").setText(recipientFind.getName());
					elementFIO.element("��������").setText(recipientFind.getPatr());
					element.element("����������").setText(recipientFind.getAccountNumber());
					element.element("��������������").setText(recipientFind.getSNILS());
					
				}

			}
			
			//���������� � ����
			String content = separatedOZACByFile.getRootElement().getDocument().asXML();
			try (FileWriter fw = new FileWriter(fileSeparatedOZAC)) {
				fw.write(content, 0, content.length());
				
			} catch (Exception e) {
				throw new Exception("������ ��� ���������� SeparatedOZAC: " +separatedOZACByFile.getFileName(), e);
			}
		}
	}
	
	/**
	 * ����������� � �������� ��� ����
	 * @param dirTempUnpack
	 * @param listSeparatedOZAC
	 * @throws Exception
	 */
	private void copyToNVP(File dirTempUnpack, List<SeparatedOZAC> listSeparatedOZAC) throws Exception {
		for (SeparatedOZAC separatedOZAC : listSeparatedOZAC) {
			
			File fileSeparatedOZAC = new File(dirTempUnpack + "/" + separatedOZAC.getFileName()); 
			ParserFileNameOut parser = new ParserFileNameOut(fileSeparatedOZAC, MainFrame.dictNameOrgInFile);
			parser.parse();
			Files.copy(fileSeparatedOZAC.toPath(), 
					Paths.get(this.pathOZACForONVP + "/" + String.valueOf(parser.getArea() + 100) + "/BANK/" + separatedOZAC.getFileName()),
					StandardCopyOption.REPLACE_EXISTING);
		}
	}
	
	/**
	 * ���������� � �� SeparatedOZAC
	 * @param listSeparatedOZAC
	 * @throws Exception 
	 */
	private void updateSeparatedOZAC(List<SeparatedOZAC> listSeparatedOZAC) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			for (SeparatedOZAC separatedOZAC : listSeparatedOZAC) {
				separatedOZAC.setIsMoved(true);
				session.update(separatedOZAC);
			}	
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� separatedOZAC", ex);
			
		} finally {
			session.close();
		} 
	}
	
	
	/**
	 * ������������ ������ �����������, ������� ������������ �������������
	 * @param listOZAC
	 * @throws Exception
	 */
	private List<CorrectRecipient> getListCorrectRecipients(OZAC ozac, Boolean isRestoreOnlyDeleted) throws Exception {
		List<CorrectRecipient> listCorrectRecipients = new ArrayList<>();
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			
			String query = 
					"SELECT" +
						" cr.*" +
					" FROM " +
						" correct_recipients cr" +
						" INNER JOIN recipients rlt   ON cr.recipient_ID = rlt.ID" +
						" INNER JOIN list_transfer lt ON rlt.ListTransfer = lt.ID" + 
						" INNER JOIN docs_list_transfer dlt ON lt.ID = dlt.ListTransfer" +
						" INNER JOIN docs_list_transfer_inc_registerTF docsInc ON dlt.ID = docsInc.DocListTransfer" +
						" INNER JOIN registers_tf reg ON docsInc.RegisterTF = reg.ID" +
					" WHERE " +
						" reg.number = " + ozac.getNumberPack() + 
						" AND reg.yearPay = " + ozac.getYear() + 
						" AND (cr.correctFamily       <> \"\"" + 
							   " OR cr.correctName    <> \"\"" +
							   " OR cr.correctSurname <> \"\"" +
							   " OR cr.correctAccount <> \"\"" +
							   " OR cr.correctSnils   <> \"\")" +
						(isRestoreOnlyDeleted ? "AND cr.deleteSign = 1" : "");
			
			SQLQuery sqlquery = session.createSQLQuery(query).addEntity(CorrectRecipient.class);
			
			listCorrectRecipients = sqlquery.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ CorrectRecipient", ex);
			
		} finally {
			session.close();
		}
		
		return listCorrectRecipients;
	}
	
	
	/**
	 * ����������� Map ��� ������ ����������� ���������� � ���������� ����� OZAC � ���������� � �������� ������
	 * @param listCorrectRecipients
	 * @return
	 */
	private Map<RecipientSPIS, RecipientSPIS> getMapForFind(List<CorrectRecipient> listCorrectRecipients) {
		Map<RecipientSPIS, RecipientSPIS> mapForFind = new HashMap<>();
		for (CorrectRecipient cr : listCorrectRecipients) {
			
			RecipientListTransfer recipientLT = cr.getRecipientListTransfer();
			RecipientSPIS recipientAfterCorrect = new RecipientSPIS();

			//Surname
			if ("".equals(cr.getSurname()))
				recipientAfterCorrect.setSurname(recipientLT.getSurname());
			else
				recipientAfterCorrect.setSurname(cr.getSurname());
			
			//Name
			if ("".equals(cr.getName()))
				recipientAfterCorrect.setName(recipientLT.getName());
			else
				recipientAfterCorrect.setName(cr.getName());
			
			//Patr
			if ("".equals(cr.getPatr()))
				recipientAfterCorrect.setPatr(recipientLT.getPatr());
			else
				recipientAfterCorrect.setPatr(cr.getPatr());
			
			//AccountNumber
			if ("".equals(cr.getAccountNumber()))
				recipientAfterCorrect.setAccountNumber(recipientLT.getAccountNumber());
			else
				recipientAfterCorrect.setAccountNumber(cr.getAccountNumber());
			
			//SNILS
			if ("".equals(cr.getSNILS()))
				recipientAfterCorrect.setSNILS(recipientLT.getSNILS());
			else
				recipientAfterCorrect.setSNILS(cr.getSNILS());
			
			//Sum
			recipientAfterCorrect.setAmountDelivery(recipientLT.getAmountDelivery());
			
			mapForFind.put(recipientAfterCorrect, recipientLT);
		}
		
		return mapForFind;
	}
	
	
	private File unpackSeparatedOZACToTempDir(Path pathArchiveOZACAfterSeparation, OZAC ozac) throws Exception {
		//����������� ����� ������ 
		File fileName = new File(pathArchiveOZACAfterSeparation + "/" + "������_" + ozac.getYear() + "_" + ozac.getNumberPack() + ".zip");
		
		File dirTempUnpack = new File(pathArchiveOZACAfterSeparation + "/tempUnpack/" + fileName.getName());
		if (!dirTempUnpack.exists())
			dirTempUnpack.mkdirs();
		Utils.unpack(fileName, dirTempUnpack.toPath(), ".*OZAC.*");
		
		
		return dirTempUnpack;
	}
	
	
	private void moveOZAC(List<OZAC> listOZAC) throws Exception {
		
		for (OZAC ozac : listOZAC) {
	
			//����������� ������ SeparatedOZAC
			List<SeparatedOZAC> listSeparatedOZAC = new ArrayList<>();
			for (SeparatedOZAC separatedOZAC : ozac.getSetSeparatedOZAC()) {
				
				//���� ����������� ��������� ����������� ������ ������, � ������� ���� ������������� � ��� ���� ���� �� �������� �������������, �� ����������
				if (this.isAllOZAC == false && separatedOZAC.getCountRecipientsNotCredited().equals(0)) 
					continue;
				
				listSeparatedOZAC.add(separatedOZAC);
			}
			

			//���������� �� ��������� �������
			File dirTempUnpack = this.unpackSeparatedOZACToTempDir(this.pathArchiveOZACAfterSeparation, ozac);
			
			//�������������� �������������� ���������� �����������
			if (this.isRestoreRecipients) {
				
				//������������ ������ �����������, ������� ������������ �������������
				List<CorrectRecipient> listCorrectRecipients = this.getListCorrectRecipients(ozac, this.isRestoreOnlyDeleted);
				
				//����������� Map ��� ������ ����������� ���������� � ���������� ����� OZAC � ���������� � �������� ������
				Map<RecipientSPIS, RecipientSPIS> mapForFind = this.getMapForFind(listCorrectRecipients);
			
				//�������������� ���������� ���������� � �����
				if (this.isRestoreOnlyDeleted)
					this.restoreFileOZAC(dirTempUnpack, listSeparatedOZAC, mapForFind, true);
				else
					this.restoreFileOZAC(dirTempUnpack, listSeparatedOZAC, mapForFind, false);
			}
			
			//����������� � �������� ��� ������ ����
			this.copyToNVP(dirTempUnpack, listSeparatedOZAC);
			
			//���������� � �� SeparatedOZAC
			this.updateSeparatedOZAC(listSeparatedOZAC);
			
			//�������� ���������� ��������
			Utils.deleteDirNotEmpty(dirTempUnpack.toPath());
		}	
	}
	
	
	
	private void updateRegs(List<CreatedRegisterTF> listRegNotWorked) throws Exception {
		
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			for (CreatedRegisterTF reg : listRegNotWorked) {
				//����������� � ���������� �������
				reg.setDateMoveOZAC(new Date());
				reg.setIsWorkedMoveOZAC(true);
				
				session.update(reg);
			}	
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� CreateRegisterTF", ex);
			
		} finally {
			session.close();
		} 
	}
	
	
	private List<OZAC> getListOZACNotWorked() throws Exception {
		List<OZAC> listOZAC = null;
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(OZAC.class, "ozac")
									   .createCriteria("setSeparatedOZAC", "separatedOZAC", JoinType.LEFT_OUTER_JOIN)
									   .createCriteria("ozac.docOZAC", "docOZAC", JoinType.INNER_JOIN)
									   .createCriteria("docOZAC.docPOOD", "docPOOD", JoinType.INNER_JOIN)
									   .add(Restrictions.ge("ozac.year", 2017))
									   .add(Restrictions.isNull("separatedOZAC.id"))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listOZAC = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ OZAC", ex);
			
		} finally {
			session.close();
		}
		return listOZAC;
	}
	
	/**
	 * ��������� ������ SeparatedOZAC, ������� ����� ����������� ��������
	 * @return
	 * @throws Exception 
	 */
	private List<SeparatedOZAC> getSeparatedOZAC() throws Exception {
		List<SeparatedOZAC> listSeparatedOZAC = new ArrayList<>();
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(SeparatedOZAC.class, "sepOZAC")
									   .createCriteria("sepOZAC.ozac", "ozac", JoinType.INNER_JOIN)
									   .createCriteria("ozac.docOZAC", "docOZAC", JoinType.INNER_JOIN)
									   .add(Restrictions.eq("sepOZAC.isMoved", false))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listSeparatedOZAC = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ SeparatedOZAC", ex);
			
		} finally {
			session.close();
		}
		return listSeparatedOZAC;
	}
	
	
	/**
	 * ����������� ���������� ����� OZAC ��� ����
	 * @param listSeparatedOZAC
	 * @throws Exception 
	 */
	private void moveSeparatedOZAC(List<SeparatedOZAC> listSeparatedOZAC) throws Exception {
		
		//���������� ������ SeparatedOZAC
		List<SeparatedOZAC> listFilteredSO = new ArrayList<>();
		for (SeparatedOZAC separatedOZAC : listSeparatedOZAC) {

			//���� ����������� ��������� ����������� ������ ������, � ������� ���� ������������� � ��� ���� ���� �� �������� �������������, �� ����������
			if (this.isAllOZAC == false && separatedOZAC.getCountRecipientsNotCredited().equals(0)) 
				continue;
			
			listFilteredSO.add(separatedOZAC);	
		}
		
		//������������ ������ ���������� OZAC, � ������� ���������� ����������� separatedOZAC � �� ����������
		Set<OZAC> setOZAC = new HashSet<>();
		for (SeparatedOZAC separatedOZAC : listFilteredSO) 
			setOZAC.add(separatedOZAC.getOzac());
		
		for (OZAC ozac : setOZAC) {
			//���������� �� ��������� �������
			File dirTempUnpack = this.unpackSeparatedOZACToTempDir(this.pathArchiveOZACAfterSeparation, ozac);
			
			//�������������� �������������� ���������� �����������
			if (this.isRestoreRecipients) {
				
				//������������ ������ �����������, ������� ������������ �������������
				List<CorrectRecipient> listCorrectRecipients = this.getListCorrectRecipients(ozac, this.isRestoreOnlyDeleted);
				
				//����������� Map ��� ������ ����������� ���������� � ���������� ����� OZAC � ���������� � �������� ������
				Map<RecipientSPIS, RecipientSPIS> mapForFind = this.getMapForFind(listCorrectRecipients);
			
				//�������������� ���������� ���������� � �����
				if (this.isRestoreOnlyDeleted)
					this.restoreFileOZAC(dirTempUnpack, listSeparatedOZAC, mapForFind, true);
				else
					this.restoreFileOZAC(dirTempUnpack, listSeparatedOZAC, mapForFind, false);
			}
			
			//����������� � �������� ��� ������ ����
			List<SeparatedOZAC> listSeparatedForCopy = new ArrayList<>();
			for(SeparatedOZAC separatedOZAC : listFilteredSO) {
				if (ozac.getSetSeparatedOZAC().contains(separatedOZAC))
					listSeparatedForCopy.add(separatedOZAC);
			}
			
			this.copyToNVP(dirTempUnpack, listSeparatedForCopy);
			
			//���������� � �� SeparatedOZAC
			this.updateSeparatedOZAC(listSeparatedForCopy);
			
			
			//�������� ���������� ��������
			Utils.deleteDirNotEmpty(dirTempUnpack.toPath());
			
		}
		
	}
	
	/**
	 * ��������� ������ CreatedRegisterTF, �� ������� ����� �������� ����������� OZAC ��� ������ ����
	 * @return
	 * @throws Exception
	 */
	private List<CreatedRegisterTF> getListRegsForCancelMoveOZAC() throws Exception {
		List<CreatedRegisterTF> listRegsForCancelMoveOZAC = new ArrayList<>();
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(CreatedRegisterTF.class, "reg")
											.add(Restrictions.isNotNull("dateCancelMoveOZAC"))
											.add(Restrictions.isNull("isWorkedCancelMoveOZAC"))
											.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			listRegsForCancelMoveOZAC = criteria.list();
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ������ SeparatedOZAC", ex);
			
		} finally {
			session.close();
		}
		
		return listRegsForCancelMoveOZAC;
	}
	
	
	private void removeFilesForNVP(List<CreatedRegisterTF> listRegsForCancelMoveOZAC) throws Exception {
		for (CreatedRegisterTF reg : listRegsForCancelMoveOZAC) {
			OZAC ozac = reg.getSetDocPOSD().iterator().next().getDocOZAC().getOzac();
			
			for (SeparatedOZAC separatedOZAC : ozac.getSetSeparatedOZAC()) {
				Path pathSeparatedOZAC = Paths.get(this.pathOZACForONVP + "/" + String.valueOf(separatedOZAC.getArea() + 100) + "/BANK/" + separatedOZAC.getFileName());
				Files.deleteIfExists(pathSeparatedOZAC);
			}
		}
	}
	
	
	private void updateRegsForCancelMoveOZAC(List<CreatedRegisterTF> listRegsForCancelMoveOZAC) throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			for (CreatedRegisterTF reg : listRegsForCancelMoveOZAC) {
				//����������� � ���������� �������
				reg.setDateCancelMoveOZAC(new Date());
				reg.setIsWorkedCancelMoveOZAC(true);
				
				
				//����� ������� ����������� ������
				OZAC ozac = reg.getSetDocPOSD().iterator().next().getDocOZAC().getOzac();
				for (SeparatedOZAC separatedOZAC : ozac.getSetSeparatedOZAC()) {
					separatedOZAC.setIsMoved(null);
					session.update(separatedOZAC);
				}
			
				session.update(reg);
			}	
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ���������� CreateRegisterTF", ex);
			
		} finally {
			session.close();
		} 
	}
	
	/**
	 * //��������� ��������� ������� ����������� ��������� �������� OZAC
	 * @return
	 * @throws Exception 
	 */
	private Boolean getStateModifSettingsMoveOZAC() throws Exception {
		//������ ������� ����������� ��������� �������� OZAC
		Boolean state = null;
		Session session = null;
		Transaction t = null;
		try {
			session = this.sessionFactory.openSession();
			t = session.beginTransaction();
			
			SQLQuery sqlQuery = session.createSQLQuery("SELECT state FROM events WHERE `name` = \"modifSettingsMoveOZAC\"");
			state = (Boolean)sqlQuery.list().get(0);
			
			//������� �� ��������� �������
			if (state) {
				sqlQuery = session.createSQLQuery("UPDATE events SET state = 0 WHERE `name` = \"modifSettingsMoveOZAC\"");
				sqlQuery.executeUpdate();
			}
			
			t.commit();
		} catch (Exception ex) {
			t.rollback();
			throw new Exception("������ ��������� ��������� ������� modifSettinsMoveOZAC", ex);
			
		} finally {
			session.close();
		}
		return state;
	}
	
	
	/**
	 * ���������� �������� �� ����� ��������� �����������
	 * @param fileConfigProcessings
	 * @throws Exception 
	 */
	private void readSettingsProcessing(File fileConfigProcessings) throws Exception {
		Properties prop = new Properties();
		try(FileInputStream fis = new FileInputStream(this.commonFileSettingsProcessing)) {
			
			prop.load(fis);
			this.isAllOZAC = Boolean.valueOf(prop.getProperty("isAllOZAC"));
			this.isRestoreRecipients = Boolean.valueOf(prop.getProperty("isRestoreRecipients"));
			this.isRestoreOnlyDeleted = Boolean.valueOf(prop.getProperty("isRestoreOnlyDeleted")); 
			
		} catch (IOException e) {
			throw new Exception("������ ������ ����� ������������: " + fileConfigProcessings, e);
		}
	}
	
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(prop.getProperty("intervalMoveOZAC")) * 1000;
				this.pathArchiveOZACAfterSeparation = Paths.get(new String(prop.getProperty("pathArchiveOZACAfterSeparation").getBytes("ISO8859-1")));
				this.pathOZACForONVP = Paths.get(new String(prop.getProperty("pathOZACForONVP").getBytes("ISO8859-1")));
				
			} catch (IOException e) {
				throw new Exception("������ ������ ����� ������������", e);
			}
			
			//���������� �������� �� ����� ��������� �����������
			readSettingsProcessing(this.commonFileSettingsProcessing);
			
			while (!this.isInterrupted()) {
				this.beforeRun();
				

				//���� ��������� ��������� �������� �������� ������ OZAC � ����
				if (getStateModifSettingsMoveOZAC())
					readSettingsProcessing(this.commonFileSettingsProcessing);
				
				
				//----------������� ���������� ������ OZAC � ����������� �� � OZAC � recipientOZAC------
					//��������� ������ OZAC, � ������� �� ���������� ���������� �����
				List<OZAC> listOZACNotWorked = this.getListOZACNotWorked();
				
				
				//��������� �� ������ OZAC
				for (OZAC ozac : listOZACNotWorked) {
					//����������� ����� ������
					CreatedRegisterTF reg = ozac.getDocOZAC().getDocPOSD().getCreatedRegisterTF();
					File fileName = new File(this.pathArchiveOZACAfterSeparation + "/" + "������_" + ozac.getYear() + "_" + reg.getNumber() + ".zip");
				
					//���������� �� ��������� �������
					File dirTempUnpack = new File(this.pathArchiveOZACAfterSeparation + "/tempUnpack/" + fileName.getName());
					if (!dirTempUnpack.exists())
						dirTempUnpack.mkdirs();
					Utils.unpack(fileName, dirTempUnpack.toPath(), ".*OZAC.*");
					
					//��������� ������ ������������� ������
					File[] filesOZAC = dirTempUnpack.listFiles();
					
					//������� ������
					List<SeparatedOZAC> listSeparatedOZAC = new ArrayList<>();
					for (File fileOZAC : filesOZAC) {
						ParserOZAC parserOZAC = new ParserOZAC();
						parserOZAC.parse(fileOZAC);
						
						SeparatedOZAC separatedOZAC = new SeparatedOZAC(parserOZAC);
						separatedOZAC.init();
						listSeparatedOZAC.add(separatedOZAC);
			
					}
					
					
					//������� recipientOZAC �� �����. �������
					List<RecipientOZAC> listRecipientsOZAC = null;
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();
						
						Criteria criteria = session.createCriteria(RecipientOZAC.class, "r")
												   .createCriteria("r.ozac", "ozac", JoinType.INNER_JOIN)
												   .createCriteria("ozac.docOZAC", "docOZAC", JoinType.INNER_JOIN)
												   .createCriteria("docOZAC.docPOSD", "docPOSD", JoinType.INNER_JOIN)
												   .createCriteria("docPOSD.createdRegisterTF", "reg", JoinType.INNER_JOIN)
												   .add(Restrictions.eq("reg.number", reg.getNumber()))
												   .add(Restrictions.eq("reg.yearPay", reg.getYearPay()))
												   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
						
						listRecipientsOZAC = criteria.list();
						
						t.commit();
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ��������� ������ CreatedRegisterTF", ex);
						
					} finally {
						session.close();
					}
					
					//�������������
					//���������� Map
					Map<Integer, RecipientOZAC> mapRecipientOZAC = new HashMap<>();
					for (RecipientOZAC recipientOZAC : listRecipientsOZAC)
						mapRecipientOZAC.put(recipientOZAC.getNumberInArray(), recipientOZAC);
					
					ozac = listRecipientsOZAC.get(0).getOzac();
					for (SeparatedOZAC separatedOZAC : listSeparatedOZAC) {
						separatedOZAC.setOzac(ozac);
						ozac.getSetSeparatedOZAC().add(separatedOZAC);
						
						for (RecipientSPIS recipient : separatedOZAC.getSetRecipientsSPIS()) {
							RecipientOZAC recipientOZAC = mapRecipientOZAC.get(recipient.getNumberInArray());
							recipientOZAC.setSeparatedOZAC(separatedOZAC);
							separatedOZAC.getSetRecipientsOZAC().add(recipientOZAC);
						}
						
					}
				
					//���������� � ��
					session = null;
					t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();
						
						for (SeparatedOZAC separatedOZAC : listSeparatedOZAC) {
							session.save(separatedOZAC);
							for (RecipientOZAC recipientOZAC : separatedOZAC.getSetRecipientsOZAC()) {
								session.update(recipientOZAC);
							}
						}
						
						t.commit();
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ���������� SeparatedOZAC", ex);
						
					} finally {
						session.close();
					} 

				}
				
				
				
				//----------���������� ����������� ���������� OZAC � ������� ��� ����------
					//��������� ������ ��������, � ������� ����������� ������� �� ����������� OZAC � ��� ��� �� ���� ����������
				List<CreatedRegisterTF> listRegNotWorked = this.getListRegNotWorked();
				
					//��������� ������ OZAC, ��������������� �������
				List<OZAC> listOZAC = this.getListOZAC(listRegNotWorked);
				
					//����������� ������ 
				this.moveOZAC(listOZAC);
				
					//���������� � �� � �������� OZAC � 
				this.updateRegs(listRegNotWorked);
				
				
				
				//----------���������� ���������� ����������� ���������� OZAC ��� ����
					//��������� ������ ���������� ������, ������� ���������� �������� �����������
				List<SeparatedOZAC> listSeparatedOZAC = this.getSeparatedOZAC();
				
					//����������� ������ SeparatedOZAC
				this.moveSeparatedOZAC(listSeparatedOZAC);
				
				
				//----------���������� ������ ����������� OZAC ��� ����
					//��������� ������ CreatedRegisterTF, �� ������� ����� �������� ����������� OZAC ��� ����
				List<CreatedRegisterTF> listRegsForCancelMoveOZAC = this.getListRegsForCancelMoveOZAC();
				
					//�������� ������ �� ��������� ���, ��������������� ��� �������� � ���
				this.removeFilesForNVP(listRegsForCancelMoveOZAC);
				
					//���������� � �� �� ������ �������� OZAC � ���� 
				this.updateRegsForCancelMoveOZAC(listRegsForCancelMoveOZAC);
				
				
				
				//��������� ������
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
		 	MainFrame.btnConfigMoveOZAC.setEnabled(true);
			
		} catch (final Exception e) {
			
			this.funcException(e, 
					new File("logs/log_MoveOZAC.txt"), 
					this.lblIndError, 
					this.btnStart, 
					this.tpForIndicator);
		
		
		} finally {
			this.procFinally();
		}
	}
	
	
	

}
