package main.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import main.MainFrame;
import main.Utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ProcessAdapter;
import ru.pfrf.process.ReceivingOSMP;
import ru.pfrf.process.ReceivingPacksFromCombanks;
import ru.pfrf.process.ReceivingPacksFromCombanksAdapter;


public class TaskReceivPacksFromCombank extends ThreadTask {
	
	private static final Long intervalDefault = 300000L; //�������� �� ���������
	
	private MainFrame frame;


	public TaskReceivPacksFromCombank(SessionFactory sessionFactory,
								File fileConfig,
								MainFrame frame) {
		
		super(sessionFactory, 
				fileConfig,
				frame.plIndReceivPacksCombanks, 
				frame.prbReceivPacksCombanks, 
				frame.btnStartReceivPacksCombanks, 
				frame.btnStopReceivPacksCombanks,
				frame.lblIndErrorReceivPackCombanks,
				frame.tabbedPane,
				0);
		
		this.frame = frame;
		this.interval = intervalDefault;
	}
	
	@Override
	public void run() {
		try {
			
			//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				this.interval = Long.valueOf(p.getProperty("intervalPacksFromCombank")) * 1000;
				this.addrMessage = p.getProperty("addrMessage");
				this.pathMimCmdExe = Paths.get(new String(p.getProperty("pathMimCmdExe").getBytes("ISO8859-1")));
				
			} catch (IOException e) {
				throw new Exception(e);
			}
			
			while (!this.isInterrupted()) {

				this.beforeRun();
				
				ReceivingPacksFromCombanks receiving = new ReceivingPacksFromCombanks(sessionFactory, this.fileConfig);
				receiving.start();
				
				//��������
				ProcessAdapter processAdapter = new ProcessAdapter(this.sessionFactory,
															receiving.getDateBegin(),
															receiving.getDateEnd(),
															"ServerNamePC",
															"server");
				
				ReceivingPacksFromCombanksAdapter receivingAdapter = new ReceivingPacksFromCombanksAdapter(processAdapter);
				processAdapter.setReceivngPacksFromCombanksAdapter(receivingAdapter);
				for(PackFromCombank pack : receiving.getSetPacks()) {
					receivingAdapter.getSetPacks().add(pack);
					pack.setReceivingPacksFromCombanksAdapter(receivingAdapter);
					
					
				}
				
				if (receiving.getSetPacks().size() > 0) {
					
					Session session = null;
					Transaction t = null;
					try {
						session = this.sessionFactory.openSession();
						t = session.beginTransaction();

						session.save(receiving);
						session.save(processAdapter);
						t.commit();
						
					} catch (Exception ex) {
						t.rollback();
						throw new Exception("������ ������ � ��", ex);
						
					} finally {
						session.close();
					}
				}
				
				//�������. �������������� ���������. ������������ ID �����
				for(PackFromCombank pack : receiving.getSetPacks()) {
					for(FileOfPackFromCombankAdapter fileAdapter : pack.getSetFileOfPackFromCombankAdapter()) {
						File fileSource = new File(receiving.getPathDest() + "/" + fileAdapter.getFileName() + (pack.getPostfix() == null ? "" : "_" + pack.getPostfix()));
						File fileDest = new File(receiving.getPathDest() + "/" + fileAdapter.getFileName() + "_ID" + fileAdapter.getId());
						fileSource.renameTo(fileDest);
					}
				}
				
				
				//�������� ���������, ���� ���� ������ � ������� ��� �����
				for (PackFromCombank pack : receiving.getSetPacks()) {
					if (pack.getTypeError() != null)
						Utils.sendMessageMiranda(this.pathMimCmdExe, "������ ��� ����� ������ " + 
																     pack.getFileName() + " �� " + 
																     pack.getDeliveryOrg().getName() + ": " + 
																     pack.getTypeError().getDescription(), 
																     this.addrMessage);
				}
				
			
				this.afterEndTask();
			}
			
		} catch (InterruptedException e) {
			System.out.println("The task is interrupted");
			this.btnStart.setEnabled(true);
			
		} catch (Exception e) {
			
			try (FileOutputStream fos = new FileOutputStream(new File("logs/log_receivingPackFromCombanks.txt"));
				 PrintWriter pw = new PrintWriter(fos)) {
				
					pw.print(new Date().toString() + "\n");
					e.printStackTrace(pw);
					
				} catch (Exception e1) {
					e1.printStackTrace();	
				}
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					lblIndError.setVisible(true);
					btnStart.setEnabled(false);	
					tpForIndicator.setIconAt(indexTpIndicator, new ImageIcon(MainFrame.class.getResource("/warn_tsk.gif")));
					
				}
			});
			
			
		} finally {
			this.procFinally();
		}
		
		
		
	}

}
