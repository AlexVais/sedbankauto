package main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.SwingConstants;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import javax.swing.ScrollPaneConstants;
import java.awt.Cursor;

public class DlgShowLogError extends JDialog {
	private static final long serialVersionUID = 1L;
	
	public int result = 0;
	private File logFile;

	private final JPanel contentPanel = new JPanel();
	private JLabel label;
	private JTextArea textArea;
	private JButton btnYes;
	private JButton btnNo;
	private JScrollPane scrollPane;

	/**
	 * Create the dialog.
	 */
	public DlgShowLogError(Frame frame, File logFile, String title) {
		super(frame);
		setFont(new Font("Dialog", Font.PLAIN, 17));
		this.logFile = logFile;
		
		this.setTitle(title);
		setMinimumSize(new Dimension(300, 100));
		
		setModal(true);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 974, 332);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				label = new JLabel("�������� ������?    ");
				label.setHorizontalAlignment(SwingConstants.LEFT);
				label.setFont(new Font("Tahoma", Font.BOLD, 13));
				buttonPane.add(label);
			}
			{
				btnYes = new JButton("��");
				btnYes.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						result = 1;
						btnYes.setEnabled(false);
						btnNo.setEnabled(true);
					}
				});
				btnYes.setActionCommand("OK");
				buttonPane.add(btnYes);
				getRootPane().setDefaultButton(btnYes);
			}
			{
				btnNo = new JButton("���");
				btnNo.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						result = 0;
						btnYes.setEnabled(true);
						btnNo.setEnabled(false);
					}
				});
				btnNo.setEnabled(false);
				btnNo.setActionCommand("");
				buttonPane.add(btnNo);
			}
			
		}
		
		textArea = new JTextArea();
		textArea.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		textArea.setEditable(false);
		textArea.setMinimumSize(new Dimension(4, 50));
		textArea.setLineWrap(true);
		contentPanel.add(textArea);
		{
			scrollPane = new JScrollPane(textArea);
			contentPanel.add(scrollPane, BorderLayout.CENTER);
		}
		
		showLog();
		
		
	}
	
	private void showLog() {
		//���������� ���
		try(FileInputStream fis = new FileInputStream(logFile)) {
			
			byte[] buf = new byte[(int) logFile.length()];
			fis.read(buf, 0, buf.length);
			this.textArea.setText(new String(buf));
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
