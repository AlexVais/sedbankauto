package main;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.*;

public class Utils {
	public static <T> List<T> binarySearch(List<T> list, T key, Comparator<? super T> c) {		
		int i = Collections.binarySearch(list, key, c);
		if (i >= 0) {
			List<T> listOut = new ArrayList<>();
			listOut.add(list.get(i));
			int iStart = i;
			int iEnd = i;
			
			//�������� �����
			while (++iEnd < list.size() && c.compare(key, list.get(iEnd)) == 0) {
				listOut.add(list.get(iEnd));
			}
			
			//�������� �����
			while (--iStart >= 0 && c.compare(key, list.get(iStart)) == 0) {
				listOut.add(list.get(iStart));
			}
			
			return listOut;
		} else
			return null;
		
	}
	
	public static String getTimeStr(Long ms) {
		Long day = null, 
			 hour = null, 
			 min = null, 
			 sec = null;
		
		day = ms / 86400000;
		ms %= 86400000;
		
		hour = ms / 3600000;
		ms %= 3600000;
		
		min = ms / 60000;
		ms %= 60000;
		
		sec = ms / 1000;
		ms %= 1000;
		
		StringBuilder sb = new StringBuilder();
		sb.append(day + ":");
		sb.append(hour + ":");
		sb.append(min + ":");
		sb.append(sec);
		return sb.toString();
	}
	
	public static void clearDir(Path path) throws Exception {
		File[] files = path.toFile().listFiles();
		for(File file : files) {
			if (file.isFile()) {
				if (!file.delete()) {
					throw new Exception("������� �������� " + path + ". �� ������� ������� ����: " + file);
				}
			} else if (file.isDirectory()) {
				clearDir(file.toPath());
				if (!file.delete()) 
					throw new Exception("�� ������ ������� ����: " + file);
			}
		}
	}
	
	public static void deleteDirNotEmpty(Path path) throws Exception {
		clearDir(path);
		path.toFile().delete();
	}
	
	private static void write(InputStream in, BufferedOutputStream out) throws Exception {
		byte[] buffer = new byte[1024];
		int len;
		while ((len = in.read(buffer)) >= 0)
			out.write(buffer, 0, len);
		out.close();
		in.close();
	}
	
	public static void compress(List<File> listFiles, File fileArchive) throws Exception {
		try (FileOutputStream fout = new FileOutputStream(fileArchive);
				ZipOutputStream zout = new ZipOutputStream(fout)) {
			
			zout.setLevel(Deflater.DEFAULT_COMPRESSION);
			byte[] buffer = new byte[2048];
			
			for (File fileToArchive : listFiles) {
				try (FileInputStream in = new FileInputStream(fileToArchive)) {
					
					int len = 0;
					zout.putNextEntry(new ZipEntry(fileToArchive.getName()));
					while ((len = in.read(buffer)) > 0) {
						zout.write(buffer, 0, len);
					}
					zout.closeEntry();
					
				} catch (Exception e) {
					throw new Exception(e);
				}
			}
			
			
		} catch (Exception e) {
			throw new Exception("�� ������� ������� ����� " + fileArchive, e);
		}
	}
	
	public static void unpack(File fileArchive, Path pathDest, String regexpr) throws Exception{
		try(ZipFile zip = new ZipFile(fileArchive, Charset.forName("CP866"))) {
			Enumeration<?> entries = zip.entries();
			List<ZipEntry> listEntries = new ArrayList<>();
			while (entries.hasMoreElements()) {
				ZipEntry zipEntry = (ZipEntry) entries.nextElement();
				if (zipEntry.getName().matches(regexpr))
					listEntries.add(zipEntry);
			}
			
			int i = 0;
			for (ZipEntry entry : listEntries) {
	
				if (entry.isDirectory()) {
					new File(pathDest.toFile(), entry.getName()).mkdirs();
				} else {
					try (BufferedOutputStream bos = 
							new BufferedOutputStream(
								new FileOutputStream(
									new File(pathDest.toFile(), entry.getName())))) {
						
						write(zip.getInputStream(entry), bos);		
					}	
				}
				i++;
			}

		} catch (Exception e) {
			throw new Exception("������ ���������� ������ " + fileArchive, e);
		}
			
	}
	
	public static void unpack7zip(File fileArchive, Path pathDest, Path path7zipExe) throws Exception {
		Runtime rt = Runtime.getRuntime();
		String commandLine = "\"" + path7zipExe + "\"" + " x " + "\"" + fileArchive + "\"" + " -aoa -o" + "\"" +  pathDest + "\"";
		Process p = rt.exec(commandLine);
		p.waitFor();
		
		if (p.exitValue() != 0)
			throw new Exception("���������� ������ " + fileArchive + " exitcode = " + p.exitValue());
	}
	
	public static void sendMessageMiranda(Path pathMirandaExe, String message, String addr) throws Exception {
		Runtime rt = Runtime.getRuntime();
		String commandLine = pathMirandaExe.toString() + " status online";
		Process p = rt.exec(commandLine);
		p.waitFor();
		
		commandLine = "\"" + pathMirandaExe.toString() + "\"" + " message " + addr + " \"" + message + "\"";
		Runtime rt2 = Runtime.getRuntime();
		Thread.sleep(600);
		Process p2 = rt2.exec(commandLine);
		p2.waitFor();
	}

}
