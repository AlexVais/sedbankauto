package main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DlgAbout extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JLabel lblVaisAlexandr;

	/**
	 * Create the dialog.
	 */
	public DlgAbout() {
		setResizable(false);
		setTitle("� ���������");
		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 403, 284);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(DlgAbout.class.getResource("/pfr.png")));
		lblNewLabel.setBounds(0, 0, 150, 154);
		contentPanel.add(lblNewLabel);
		{
			JLabel lblSedbankAutoprocessing = new JLabel("Sedbank autoprocessing");
			lblSedbankAutoprocessing.setFont(new Font("Tahoma", Font.BOLD, 11));
			lblSedbankAutoprocessing.setBounds(177, 11, 157, 14);
			contentPanel.add(lblSedbankAutoprocessing);
		}
		{
			lblVaisAlexandr = new JLabel("\u00A9 Vays Alexandr, 2016-2017");
			lblVaisAlexandr.setFont(new Font("Tahoma", Font.PLAIN, 11));
			lblVaisAlexandr.setBounds(177, 44, 171, 14);
			contentPanel.add(lblVaisAlexandr);
		}
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(DlgAbout.class.getResource("/java.png")));
		lblNewLabel_1.setBounds(341, 152, 56, 71);
		contentPanel.add(lblNewLabel_1);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						DlgAbout.this.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
}
