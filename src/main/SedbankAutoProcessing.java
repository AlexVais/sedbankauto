package main;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.Timer;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.hibernate.*;
import org.xml.sax.SAXException;








import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.DictDeliveryOrg;
import ru.pfrf.classifier.DictStateAccountNumber;
import ru.pfrf.classifier.StateAccountNumber;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.OutMainFile;
import ru.pfrf.entityinfoexchange.order.Order;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.osmp.OPVZ;
import ru.pfrf.entityinfoexchange.order.osmp.OSMP;

import ru.pfrf.person.Recipient;
import ru.pfrf.person.RecipientDead;
import ru.pfrf.process.ReceivingOSMP;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.RegisterTFToOPFR;

import java.awt.event.KeyEvent;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.event.MenuKeyListener;
import javax.swing.event.MenuKeyEvent;
import javax.swing.table.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class SedbankAutoProcessing {
	

	public static final int VER_TEST = 1;
	public static final int VER_PRODUCTION = 2;
	static File fileConfig = null;
	
	public static int ver = 0;
	
	static {
		//�������� ������������
		ver = VER_TEST;
		
		//������������ ������������
		//ver = VER_PRODUCTION;
		
		if (ver == VER_PRODUCTION)
			fileConfig = new File("config_prom.properties");
		else if (ver == VER_TEST)
			fileConfig = new File("config_test.properties");
				
		HibernateUtil.fileConfig = fileConfig;
	}
	
	//��������� ��������� ����
	static {
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+9"));
	}
	
	/**
	 * Create the application.
	 */
	public SedbankAutoProcessing() {
		
	}

	
	public static void main(String[] args) { 
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame(fileConfig);
					window.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
}
