package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static SessionFactory sessionFactory = null;
	public static File fileConfig;
	private static String fileNameCfgHibernate;
	
	private static SessionFactory buildSessionFactory() throws HibernateException { 
        try { 
        	//���������� �������� �� ����� ������������
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(fileConfig));
				fileNameCfgHibernate = p.getProperty("fileCfgHibernate");
				
			} catch (IOException e) {
				throw new Exception(e);
			}
        
        	return new AnnotationConfiguration().configure(Class.class.getResource("/" + fileNameCfgHibernate)).buildSessionFactory(); 
 
        } catch (Throwable ex) { 
        	ex.printStackTrace();
            throw new HibernateException(ex); 
        } 
    } 
	
	public static SessionFactory getSessionFactory() throws HibernateException {
		if (sessionFactory == null) {
			sessionFactory = buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static void shutdown() throws HibernateException {
		if (sessionFactory != null)
			getSessionFactory().close();
		sessionFactory = null;
		
    }
}
