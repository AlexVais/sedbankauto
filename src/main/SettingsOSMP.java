package main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.GridLayout;

import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.UIManager;
import java.awt.Color;

public class SettingsOSMP extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField txtC;
	
	private Path sourcePath = null; 
	
	private Long interval;
	
	private JTextField textField;


	public Path getSourcePath() {
		return sourcePath;
	}


	/**
	 * Create the dialog.
	 */
	public SettingsOSMP(Frame owner, boolean modal) {
		setModal(true);
		setTitle("\u041D\u0430\u0441\u0442\u0440\u043E\u0439\u043A\u0438");
//		setBounds((int)owner.getBounds().getCenterX(), 
//				  (int)owner.getBounds().getCenterY(), 
//				  518,  
//				  166);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			txtC = new JTextField();
			txtC.setBackground(Color.WHITE);
			txtC.setText("S:\\SED_BANK\\����� 1\\����� ��������� ������");
			txtC.setBounds(144, 11, 307, 20);
			txtC.setHorizontalAlignment(SwingConstants.LEFT);
			contentPanel.add(txtC);
			txtC.setColumns(10);
		}
		{
			JLabel lblNewLabel = new JLabel("\u041F\u0443\u0442\u044C \u043A \u0438\u0441\u0445\u043E\u0434\u043D\u044B\u043C \u0444\u0430\u0439\u043B\u0430\u043C");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 11));
			lblNewLabel.setBounds(10, 14, 140, 14);
			contentPanel.add(lblNewLabel);
		}
		{
			JLabel label = new JLabel("\u0418\u043D\u0442\u0435\u0440\u0432\u0430\u043B , \u0441\u0435\u043A.");
			label.setFont(new Font("Tahoma", Font.PLAIN, 11));
			label.setBounds(10, 49, 88, 14);
			contentPanel.add(label);
		}
		
		textField = new JTextField();
		textField.setText("300");
		textField.setBounds(96, 46, 55, 20);
		contentPanel.add(textField);
		textField.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnOk = new JButton("\u041F\u0440\u0438\u043C\u0435\u043D\u0438\u0442\u044C");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						sourcePath = Paths.get(txtC.getText()); 
					}
				});
				btnOk.setFont(new Font("Tahoma", Font.PLAIN, 10));
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("\u041E\u0442\u043C\u0435\u043D\u0430");
				btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 10));
				buttonPane.add(btnCancel);
			}
		}
		this.setVisible(true);
	}
}
