package main;

import static main.SedbankAutoProcessing.VER_PRODUCTION;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.persistence.EntityManager;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import java.awt.Font;

import javax.swing.JDesktopPane;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.AbstractAction;
import javax.swing.Action;

import main.task.*;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.classifier.*;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.notification.Notification;
import ru.pfrf.entityinfoexchange.notification.NotificationFactory;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.osmp.OSMP;
import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.SPRA;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfilename.ParserPackFromCombank;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIEOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVF;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSD;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserSPRA;
import ru.pfrf.entityinfoexchange.receipt.ReceiptProcessToBank;
import ru.pfrf.main.logproc.osmp.LogOSMP;
import ru.pfrf.payment.Payment;
import ru.pfrf.payment.PaymentOZAC;
import ru.pfrf.payment.PaymentSPIS;
import ru.pfrf.person.Recipient;
import ru.pfrf.person.RecipientListTransfer;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.person.RecipientSPIS;
import ru.pfrf.process.ProcessAdapter;
import ru.pfrf.process.ReceivingOSMP;
import ru.pfrf.process.ReceivingPOSD;
import ru.pfrf.register.CreatedRegisterTF;

import javax.swing.JProgressBar;
import javax.swing.JSeparator;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.ImageIcon;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.ComponentOrientation;

import javax.swing.UIManager;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;

import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.Icon;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	static SessionFactory sessionFactory;
	static File fileConfig;
	
	private TaskReceivOSMP processingOSMP;
	private TaskReceivPacksFromCombank processingCombanks;
	private TaskReceivPOSD processingPOSD;
	private TaskReceivNotifications processingNotifications;
	private TaskReceivRegsFromOPFR processingRegsFromOPFR;
	private TaskReceivOZAC processingReceivOZAC;
	private TaskSendPOOD processingSendPOOD;
	private TaskSeparationOzacXmlManager processingSeparationOzacXmlManager;
	private TaskMergerSpisXmlManager processingMergerSpisXmlManager;
	private TaskReceivPOSZ processingPOSZ;
	private TaskDefinVipnet processingDefinVipnet;
	private TaskMoveOZAC processingMoveOZAC;
	private TaskReceivPOSO processingReceivPOSO;
	
	/**
	 * Create the frame.a
	 */
	private Timer timerWorkServer;
	private Date dateServerStart;
	private Date dateServerStop;
	public static Dictionary<CodeCredited> dictCodeCredited = null;
	public static Dictionary<TypeArrayAssignment> dictTypeArrayAssignment = null;
	public static Dictionary<DeliveryOrg> dictDeliveryOrg = null;
	public static Dictionary<NameOrgInFile> dictNameOrgInFile = null;
	public static Dictionary<TypeError> dictTypeError = null;
	public static Dictionary<DeliveryOrg> dictDeliveryOrgByShortName = null;
	public static Dictionary<TypeResultNotification> dictTypesResultsNotifications = null;
	public static Dictionary<TypePay> dictTypesPay = null;
	public static Dictionary<SubtypePay> dictSubtypesPay = null;
	public static Dictionary<SignPayment> dictSignPayments = null;
	public static Dictionary<TypeDifferenceRecipient> dictTypeDifferenceRecipient = null;
	public static Dictionary<TypeDifferenceSPIS> dictTypeDifferenceSPIS = null;
	public static Dictionary<TypeDocDead> dictTypeDocDead = null;
	public static Dictionary<TypeCauseTermination> dictTypeCauseTermination = null;
	//public static Dictionary<DeliveryOrg> dictDeliveryOrg = null;
	
	private MainFrame frame;
	private JPanel contentPane;
	
	public JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	public JPanel panelLists = new JPanel();
	public JPanel panelReceivingRegsFromOPFR = new JPanel();
	public JLabel lblReceivRegsFromOPFR = new JLabel("���� �������� �� ����");
	public JButton btnStartReceivRegsFromOPFR = new JButton("�����");
	public JButton btnStopReceivRegsFromOPFR = new JButton("����");
	public JLabel lblOsmp = new JLabel("���� � ����������");
	public JPanel panelOSMP = new JPanel();
	public JPanel panelReceivOSMP = new JPanel();
	public JLabel lblTimerServer = new JLabel("");
	public JButton btnStartOSMP = new JButton("�����");
	public JButton btnStopOSMP = new JButton("����");
	public JPanel pnlIndReceivOSMP = new JPanel();
	public JProgressBar prbOSMP = new JProgressBar();
	public JProgressBar prbIndServer = new JProgressBar();
	public JPanel pnlIndServer = new JPanel();
	public final JProgressBar prbReceivRegsFromOPFR = new JProgressBar();
	
	private final Action showLogReceivOSMP = new ShowLogReceivOSMP();
	private final Action stopReceivRegsFromOPFR = new StopReceivRegsFromOPFR();
	private final Action startReceivRegsFromOPFR = new StartReceivRegsFromOPFR();
	private final Action startReceivOSMP = new StartReceivOSMP();
	private final Action stopReceivOSMP = new StopReceivOSMP();
	private final Action initServer = new InitServer();
	private final Action stopServer = new StopServer();
	private final Action startReceivPOSD = new StartReceivPOSD();
	private final Action stopReceivPOSD = new StopReceivPOSD();
	private final Action startReceivOZAC = new StartReceivOZAC();
	private final Action stopReceivOZAC = new StopReceivOZAC();
	private final Action startReceivPacksCombanks = new StartReceivPacksCombanks();
	private final Action stopReceivPacksCombanks = new StopReceivPacksCombanks();
	private final Action startReceivPOSZ = new StartReceivPOSZ();
	private final Action startDefinVipnet = new StartDefinVipnet();
	private final Action stopDefinVipnet = new StopDefinVipnet();
	private final Action startMoveOZAC = new StartMoveOZAC();
	private final Action stopMoveOZAC = new StopMoveOZAC();
	private final Action startPOSO = new StartPOSO();
	private final Action stopPOSO = new StopPOSO();
	
	public JButton btnStartServer;
	public JButton btnStopServer;
	public JLabel lblIndErrorReceivOSMP;
	public JLabel label;
	public JButton btnStopReceivPacksCombanks;
	public JButton btnStartReceivPacksCombanks;
	public JLabel label_1;
	public final JLabel lblIndErrorReceivPOSD = new JLabel("");
	public JButton btnStartReceivPOSD;
	public JButton btnStopReceivPOSD;
	public JProgressBar prbReceivPOSD;
	public JPanel plIndReceivPOSD;
	private final JPanel panelReceivOZAC = new JPanel();
	public final JPanel plIndReceivOZAC = new JPanel();
	public final JProgressBar prbReceivOZAC = new JProgressBar();
	private final JLabel label_2 = new JLabel("���� OZAC");
	public JButton btnStopReceivOZAC;
	public JLabel label_4;
	public JButton btnStartSendPOOD;
	public JButton btnStopSendPOOD;
	private final Action startSendPOOD = new StartSendPOOD();
	private final Action stopSendPOOD = new StopSendPOOD();
	public final JLabel lblIndErrorSendPOOD = new JLabel("");
	public JPanel plIndReceivPacksCombanks;
	public JProgressBar prbReceivPacksCombanks;
	public JLabel lblIndErrorReceivPackCombanks;
	private final Action settingsReceivRegsFromOPFR = new SettingsReceivRegsFromOPFR();
	private JPanel panelReceivPackFromComBanks;
	private JPanel panelReceivPOSD;
	private JPanel panelSendPOOD;
	private final Action showDlgAbout = new showDlgAbout();
	private JMenu mnNewMenu;
	private final JMenuItem mntmNewMenuItem = new JMenuItem("� ���������");
	public JLabel lblIndErrorReceivRegsFromOPFR;
	private JPanel panelNotification;
	private JLabel lblNotification;
	public JButton btnStartReceivNotification;
	public JButton btnStopReceivNotification;
	public JProgressBar prbNotification;
	public JPanel pnlIndReceivNotification;
	private JCheckBox chbxIsRegisterToOPFR;
	private JCheckBox chbxIsListTransferToBank;
	private JCheckBox chbxReceiptToBank;
	private JCheckBox chbxPOOD;
	private final Action startReceivNotification = new StartReceivNotification();
	private final Action stopReceivNotification = new StopReceivNotification();
	public JLabel lblIndErrorReceivNotification;
	public JPanel panelChbxes;
	private JCheckBox chckbxNewCheckBox;
	private JMenu mnNewMenu_settings;
	private final Action showDlgSettingSendMessages = new ShowDlgSendMessages();
	public JPanel plIndReceivRegsFromOPFR;
	public JButton btnStartReceivOZAC;
	public JLabel lblIndErrorReceivOZAC;
	public JProgressBar prbSendPOOD;
	public JPanel plIndSendPOOD;
	public JPanel plIndSeparationOzacXmlManager;
	private final Action startSeparationOzacXmlManager = new StartSeparationOzacXmlManager();
	private final Action stopSeparationOzacXmlManager = new StopSeparationOzacXmlManager();
	public JButton btnStartSeparationOzacXmlManager;
	public JButton btnStopSeparationOzacXmlManager;
	public JLabel lblIndErrorSeparationOzacXmlManager;
	public JProgressBar prbSeparationOzacXmlManager;
	private JPanel panelMergerSpisXmlManager;
	public JPanel plIndMergerSpisXmlManager;
	private JLabel lblMergerSpisXmlManager;
	public JButton btnStartMergerSpisXmlManager;
	public JButton btnStopMergerSpisXmlManager;
	public JProgressBar prbMergerSpisXmlManager;
	private final Action startMergerSpisXmlManager = new StartMergerSpisXmlManager();
	private final Action stopMergerSpisXmlManager = new StopMergerSpisXmlManager();
	public JLabel lblIndErrorMergerSpisXmlManager;
	private final Action stopReceivPOSZ = new StopReceivPOSZ();
	public JLabel lblIndErrorReceivPOSZ;
	public JPanel pnlIndReceivPOSZ;
	public JButton btnStartReceivPOSZ;
	public JButton btnStopReceivPOSZ;
	public JProgressBar prbReceivPOSZ;
	public JButton btnStartDefinVipnet;
	public JButton btnStopDefinVipnet;
	public JPanel pnlIndDefinVipnet;
	public JProgressBar prbDefinVipnet;
	private JPanel panel_4;
	public JLabel lblIndErrorDefinVipnet;
	private JLabel label_3;
	public JPanel plIndMoveOZAC;
	public JProgressBar prbMoveOZAC;
	public JLabel lblIndErrorMoveOZAC;
	public JButton btnStartMoveOZAC;
	public JButton btnStopMoveOZAC;
	public static JButton btnConfigMoveOZAC;
	private JPanel panelVOZV;
	public JButton btnStopReceivPOSO;
	public JLabel lblIndErrorReceivPOSO;
	public JPanel pnlIndReceivPOSO;
	public JProgressBar prbPOSO;
	public JButton btnStartReceivPOSO;
	

	/**
	 * Create the frame.
	 */
	public MainFrame(final File fileConfig) {
		startSendPOOD.setEnabled(false);
		startSeparationOzacXmlManager.setEnabled(false);
		stopPOSO.setEnabled(false);
		stopMoveOZAC.setEnabled(false);
		stopDefinVipnet.setEnabled(false);
		stopReceivPOSZ.setEnabled(false);
		stopMergerSpisXmlManager.setEnabled(false);
		stopSeparationOzacXmlManager.setEnabled(false);
		stopReceivNotification.setEnabled(false);
		stopReceivOZAC.setEnabled(false);
		stopReceivPacksCombanks.setEnabled(false);
		stopReceivOSMP.setEnabled(false);
		stopSendPOOD.setEnabled(false);
		stopReceivPOSD.setEnabled(false);
		stopReceivRegsFromOPFR.setEnabled(false);
		stopServer.setEnabled(false);
		this.frame = this;
		this.fileConfig = fileConfig;
		
		
		
		setTitle("�������������� ��� ����");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 583, 800);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnNewMenu_settings = new JMenu("���������");
		menuBar.add(mnNewMenu_settings);
		
		JMenuItem mntmNewMenuItem_messages = new JMenuItem("�������� ���������");
		mntmNewMenuItem_messages.setAction(showDlgSettingSendMessages);
		mnNewMenu_settings.add(mntmNewMenuItem_messages);
		
		mnNewMenu = new JMenu("������");
		menuBar.add(mnNewMenu);
		mntmNewMenuItem.setAction(showDlgAbout);
		
		mnNewMenu.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	
		tabbedPane.setFont(new Font("Tahoma", Font.BOLD, 12));
		tabbedPane.setVisible(false);
		
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		tabbedPane.addTab("������ �� ����������", (Icon) null, panelLists, null);
		
		panelLists.setLayout(null);
		panelReceivingRegsFromOPFR.setBounds(10, 11, 367, 65);
		panelReceivingRegsFromOPFR.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelLists.add(panelReceivingRegsFromOPFR);
		panelReceivingRegsFromOPFR.setLayout(null);
		
		lblReceivRegsFromOPFR.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblReceivRegsFromOPFR.setBounds(10, 21, 140, 21);
		lblReceivRegsFromOPFR.setHorizontalAlignment(SwingConstants.LEFT);
		panelReceivingRegsFromOPFR.add(lblReceivRegsFromOPFR);
		btnStartReceivRegsFromOPFR.setMargin(new Insets(2, 2, 2, 14));
		btnStartReceivRegsFromOPFR.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartReceivRegsFromOPFR.setIcon(new ImageIcon(MainFrame.class.getResource("/javax/swing/plaf/metal/icons/ocean/warning.png")));
		btnStartReceivRegsFromOPFR.setAction(startReceivRegsFromOPFR);
		
		
		btnStartReceivRegsFromOPFR.setBounds(184, 5, 87, 22);
		panelReceivingRegsFromOPFR.add(btnStartReceivRegsFromOPFR);
		btnStopReceivRegsFromOPFR.setMargin(new Insets(2, 2, 2, 14));
		btnStopReceivRegsFromOPFR.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopReceivRegsFromOPFR.setAction(stopReceivRegsFromOPFR);
		
		
		btnStopReceivRegsFromOPFR.setBounds(184, 33, 87, 22);
		panelReceivingRegsFromOPFR.add(btnStopReceivRegsFromOPFR);
		
		plIndReceivRegsFromOPFR = new JPanel();
		plIndReceivRegsFromOPFR.setBackground(Color.RED);
		plIndReceivRegsFromOPFR.setBounds(1, 1, 10, 10);
		panelReceivingRegsFromOPFR.add(plIndReceivRegsFromOPFR);
		prbReceivRegsFromOPFR.setBounds(12, 1, 40, 10);
		panelReceivingRegsFromOPFR.add(prbReceivRegsFromOPFR);
		prbReceivRegsFromOPFR.setToolTipText("8787");
		
		lblIndErrorReceivRegsFromOPFR = new JLabel("");
		lblIndErrorReceivRegsFromOPFR.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
						new File("logs/log_receivingRegsFromOPFR.txt"),
						"��� ������ ����� �������� �� ����");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
				MainFrame.this.lblIndErrorReceivRegsFromOPFR.setVisible(false);
				MainFrame.this.btnStartReceivRegsFromOPFR.setEnabled(true);
				//������ ��������� ������ �� �������
				MainFrame.this.tabbedPane.setIconAt(0, null);
				}
			}
		});
		lblIndErrorReceivRegsFromOPFR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivRegsFromOPFR.setVisible(false);
		lblIndErrorReceivRegsFromOPFR.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorReceivRegsFromOPFR.setBounds(162, 5, 16, 21);
		panelReceivingRegsFromOPFR.add(lblIndErrorReceivRegsFromOPFR);
		
		JButton btnNewButton = new JButton("Settings");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnNewButton.setAction(settingsReceivRegsFromOPFR);
		btnNewButton.setBounds(304, 4, 30, 23);
		panelReceivingRegsFromOPFR.add(btnNewButton);
		
		panelReceivPackFromComBanks = new JPanel();
		panelReceivPackFromComBanks.setLayout(null);
		panelReceivPackFromComBanks.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelReceivPackFromComBanks.setBounds(10, 163, 367, 65);
		panelLists.add(panelReceivPackFromComBanks);
		
		label = new JLabel("���� ������� �� ���. ������");
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setFont(new Font("Tahoma", Font.PLAIN, 11));
		label.setBounds(10, 21, 164, 22);
		panelReceivPackFromComBanks.add(label);
		
		plIndReceivPacksCombanks = new JPanel();
		plIndReceivPacksCombanks.setBackground(Color.RED);
		plIndReceivPacksCombanks.setBounds(1, 1, 10, 10);
		panelReceivPackFromComBanks.add(plIndReceivPacksCombanks);
		
		prbReceivPacksCombanks = new JProgressBar();
		prbReceivPacksCombanks.setToolTipText("8787");
		prbReceivPacksCombanks.setBounds(12, 1, 40, 10);
		panelReceivPackFromComBanks.add(prbReceivPacksCombanks);
		
		btnStartReceivPacksCombanks = new JButton("�����");
		btnStartReceivPacksCombanks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnStartReceivPacksCombanks.setAction(startReceivPacksCombanks);
		btnStartReceivPacksCombanks.setMargin(new Insets(2, 5, 2, 14));
		btnStartReceivPacksCombanks.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartReceivPacksCombanks.setBounds(184, 5, 87, 22);
		panelReceivPackFromComBanks.add(btnStartReceivPacksCombanks);
		
		btnStopReceivPacksCombanks = new JButton("����");
		btnStopReceivPacksCombanks.setAction(stopReceivPacksCombanks);
		btnStopReceivPacksCombanks.setMargin(new Insets(2, 5, 2, 14));
		btnStopReceivPacksCombanks.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopReceivPacksCombanks.setBounds(184, 33, 87, 22);
		panelReceivPackFromComBanks.add(btnStopReceivPacksCombanks);
		
		lblIndErrorReceivPackCombanks = new JLabel("");
		lblIndErrorReceivPackCombanks.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivPackCombanks.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
													new File("logs/log_receivingPackFromCombanks.txt"),
													"��� ������ ����� ������� �� ���. ������");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
					MainFrame.this.lblIndErrorReceivPackCombanks.setVisible(false);
					MainFrame.this.btnStartReceivPacksCombanks.setEnabled(true);
					//������ ��������� ������ �� �������
					MainFrame.this.tabbedPane.setIconAt(0, null);
				}
			}
		});
		lblIndErrorReceivPackCombanks.setVisible(false);
		lblIndErrorReceivPackCombanks.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorReceivPackCombanks.setBounds(162, 5, 16, 21);
		panelReceivPackFromComBanks.add(lblIndErrorReceivPackCombanks);
		
		panelReceivPOSD = new JPanel();
		panelReceivPOSD.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelReceivPOSD.setBounds(10, 239, 367, 65);
		panelLists.add(panelReceivPOSD);
		panelReceivPOSD.setLayout(null);
		
		btnStartReceivPOSD = new JButton("�����");
		btnStartReceivPOSD.setAction(startReceivPOSD);
		btnStartReceivPOSD.setBounds(184, 5, 87, 22);
		btnStartReceivPOSD.setMargin(new Insets(2, 5, 2, 14));
		btnStartReceivPOSD.setHorizontalAlignment(SwingConstants.LEFT);
		panelReceivPOSD.add(btnStartReceivPOSD);
		
		btnStopReceivPOSD = new JButton("����");
		btnStopReceivPOSD.setAction(stopReceivPOSD);
		btnStopReceivPOSD.setBounds(184, 33, 87, 22);
		btnStopReceivPOSD.setMargin(new Insets(2, 5, 2, 14));
		btnStopReceivPOSD.setHorizontalAlignment(SwingConstants.LEFT);
		panelReceivPOSD.add(btnStopReceivPOSD);
		
		label_1 = new JLabel("���� POSD");
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_1.setBounds(10, 21, 114, 22);
		panelReceivPOSD.add(label_1);
		
		prbReceivPOSD = new JProgressBar();
		prbReceivPOSD.setBounds(12, 1, 40, 10);
		panelReceivPOSD.add(prbReceivPOSD);
		prbReceivPOSD.setToolTipText("8787");
		
		plIndReceivPOSD = new JPanel();
		plIndReceivPOSD.setBounds(1, 1, 10, 10);
		panelReceivPOSD.add(plIndReceivPOSD);
		plIndReceivPOSD.setBackground(Color.RED);
		lblIndErrorReceivPOSD.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
												new File("logs/log_receivingPOSD.txt"),
												"��� ������ ����� POSD");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
					MainFrame.this.lblIndErrorReceivPOSD.setVisible(false);
					MainFrame.this.btnStartReceivPOSD.setEnabled(true);
					//������ ��������� ������ �� �������
					MainFrame.this.tabbedPane.setIconAt(0, null);
				}
			}
		});
		lblIndErrorReceivPOSD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivPOSD.setVisible(false);
		lblIndErrorReceivPOSD.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorReceivPOSD.setBounds(162, 6, 16, 21);
		
		panelReceivPOSD.add(lblIndErrorReceivPOSD);
		panelReceivOZAC.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelReceivOZAC.setBounds(10, 315, 367, 65);
		
		panelLists.add(panelReceivOZAC);
		panelReceivOZAC.setLayout(null);
		plIndReceivOZAC.setBackground(Color.RED);
		plIndReceivOZAC.setBounds(1, 1, 10, 10);
		
		panelReceivOZAC.add(plIndReceivOZAC);
		prbReceivOZAC.setToolTipText("8787");
		prbReceivOZAC.setBounds(12, 1, 40, 10);
		
		panelReceivOZAC.add(prbReceivOZAC);
		label_2.setHorizontalAlignment(SwingConstants.LEFT);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_2.setBounds(11, 22, 114, 22);
		
		panelReceivOZAC.add(label_2);
		
		btnStartReceivOZAC = new JButton("�����");
		btnStartReceivOZAC.setAction(startReceivOZAC);
		btnStartReceivOZAC.setMargin(new Insets(2, 5, 2, 14));
		btnStartReceivOZAC.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartReceivOZAC.setBounds(184, 5, 87, 22);
		panelReceivOZAC.add(btnStartReceivOZAC);
		
		btnStopReceivOZAC = new JButton("����");
		btnStopReceivOZAC.setEnabled(false);
		btnStopReceivOZAC.setAction(stopReceivOZAC);
		btnStopReceivOZAC.setMargin(new Insets(2, 5, 2, 14));
		btnStopReceivOZAC.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopReceivOZAC.setBounds(184, 33, 87, 22);
		panelReceivOZAC.add(btnStopReceivOZAC);
		
		lblIndErrorReceivOZAC = new JLabel("");
		lblIndErrorReceivOZAC.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
						new File("logs/log_receivingOZAC.txt"),
						"��� ������ ����� OZAC");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
				MainFrame.this.lblIndErrorReceivOZAC.setVisible(false);
				MainFrame.this.btnStartReceivOZAC.setEnabled(true);
				//������ ��������� ������ �� �������
				MainFrame.this.tabbedPane.setIconAt(1, null);
				}
			}
		});
		lblIndErrorReceivOZAC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivOZAC.setVisible(false);
		lblIndErrorReceivOZAC.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorReceivOZAC.setBounds(162, 6, 16, 21);
		panelReceivOZAC.add(lblIndErrorReceivOZAC);
		
		panelSendPOOD = new JPanel();
		panelSendPOOD.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelSendPOOD.setBounds(10, 467, 367, 65);
		panelLists.add(panelSendPOOD);
		panelSendPOOD.setLayout(null);
		
		prbSendPOOD = new JProgressBar();
		prbSendPOOD.setToolTipText("8787");
		prbSendPOOD.setBounds(12, 1, 40, 10);
		panelSendPOOD.add(prbSendPOOD);
		
		plIndSendPOOD = new JPanel();
		plIndSendPOOD.setBackground(Color.RED);
		plIndSendPOOD.setBounds(1, 1, 10, 10);
		panelSendPOOD.add(plIndSendPOOD);
		
		btnStartSendPOOD = new JButton("�����");
		btnStartSendPOOD.setAction(startSendPOOD);
		btnStartSendPOOD.setMargin(new Insets(2, 5, 2, 14));
		btnStartSendPOOD.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartSendPOOD.setBounds(185, 5, 87, 22);
		panelSendPOOD.add(btnStartSendPOOD);
		
		btnStopSendPOOD = new JButton("����");
		btnStopSendPOOD.setAction(stopSendPOOD);
		btnStopSendPOOD.setMargin(new Insets(2, 5, 2, 14));
		btnStopSendPOOD.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopSendPOOD.setBounds(185, 33, 87, 22);
		panelSendPOOD.add(btnStopSendPOOD);
		
		label_4 = new JLabel("�������� POOD � ����");
		label_4.setHorizontalAlignment(SwingConstants.LEFT);
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 11));
		label_4.setBounds(12, 22, 148, 22);
		panelSendPOOD.add(label_4);
		lblIndErrorSendPOOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorSendPOOD.setVisible(false);
		lblIndErrorSendPOOD.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorSendPOOD.setBounds(162, 6, 16, 21);
		
		panelSendPOOD.add(lblIndErrorSendPOOD);
		
		JPanel panelSeparationOzacXmlManager = new JPanel();
		panelSeparationOzacXmlManager.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelSeparationOzacXmlManager.setBounds(10, 391, 367, 65);
		panelLists.add(panelSeparationOzacXmlManager);
		panelSeparationOzacXmlManager.setLayout(null);
		
		plIndSeparationOzacXmlManager = new JPanel();
		plIndSeparationOzacXmlManager.setBackground(Color.RED);
		plIndSeparationOzacXmlManager.setBounds(1, 1, 10, 10);
		panelSeparationOzacXmlManager.add(plIndSeparationOzacXmlManager);
		
		prbSeparationOzacXmlManager = new JProgressBar();
		prbSeparationOzacXmlManager.setToolTipText("8787");
		prbSeparationOzacXmlManager.setBounds(12, 1, 40, 10);
		panelSeparationOzacXmlManager.add(prbSeparationOzacXmlManager);
		
		JLabel lblSeparationOzacXmlManager = new JLabel("���������� � XMLManager");
		lblSeparationOzacXmlManager.setHorizontalAlignment(SwingConstants.LEFT);
		lblSeparationOzacXmlManager.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblSeparationOzacXmlManager.setBounds(11, 22, 156, 22);
		panelSeparationOzacXmlManager.add(lblSeparationOzacXmlManager);
		
		btnStartSeparationOzacXmlManager = new JButton("�����");
		btnStartSeparationOzacXmlManager.setAction(startSeparationOzacXmlManager);
		btnStartSeparationOzacXmlManager.setMargin(new Insets(2, 5, 2, 14));
		btnStartSeparationOzacXmlManager.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartSeparationOzacXmlManager.setBounds(184, 5, 87, 22);
		panelSeparationOzacXmlManager.add(btnStartSeparationOzacXmlManager);
		
		btnStopSeparationOzacXmlManager = new JButton("����");
		btnStopSeparationOzacXmlManager.setAction(stopSeparationOzacXmlManager);
		btnStopSeparationOzacXmlManager.setMargin(new Insets(2, 5, 2, 14));
		btnStopSeparationOzacXmlManager.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopSeparationOzacXmlManager.setBounds(184, 33, 87, 22);
		panelSeparationOzacXmlManager.add(btnStopSeparationOzacXmlManager);
		
		lblIndErrorSeparationOzacXmlManager = new JLabel("");
		lblIndErrorSeparationOzacXmlManager.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorSeparationOzacXmlManager.setVisible(false);
		lblIndErrorSeparationOzacXmlManager.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorSeparationOzacXmlManager.setBounds(162, 6, 16, 21);
		panelSeparationOzacXmlManager.add(lblIndErrorSeparationOzacXmlManager);
		
		panelMergerSpisXmlManager = new JPanel();
		panelMergerSpisXmlManager.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelMergerSpisXmlManager.setBounds(10, 87, 367, 65);
		panelLists.add(panelMergerSpisXmlManager);
		panelMergerSpisXmlManager.setLayout(null);
		
		plIndMergerSpisXmlManager = new JPanel();
		plIndMergerSpisXmlManager.setBackground(Color.RED);
		plIndMergerSpisXmlManager.setBounds(1, 1, 10, 10);
		panelMergerSpisXmlManager.add(plIndMergerSpisXmlManager);
		
		lblMergerSpisXmlManager = new JLabel("����������� �������\r\n � XMLManager");
		lblMergerSpisXmlManager.setHorizontalAlignment(SwingConstants.LEFT);
		lblMergerSpisXmlManager.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblMergerSpisXmlManager.setBounds(10, 18, 165, 32);
		panelMergerSpisXmlManager.add(lblMergerSpisXmlManager);
		
		btnStartMergerSpisXmlManager = new JButton("�����");
		btnStartMergerSpisXmlManager.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnStartMergerSpisXmlManager.setAction(startMergerSpisXmlManager);
		btnStartMergerSpisXmlManager.setMargin(new Insets(2, 2, 2, 14));
		btnStartMergerSpisXmlManager.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartMergerSpisXmlManager.setBounds(184, 5, 87, 22);
		panelMergerSpisXmlManager.add(btnStartMergerSpisXmlManager);
		
		btnStopMergerSpisXmlManager = new JButton("����");
		btnStopMergerSpisXmlManager.setAction(stopMergerSpisXmlManager);
		btnStopMergerSpisXmlManager.setMargin(new Insets(2, 2, 2, 14));
		btnStopMergerSpisXmlManager.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopMergerSpisXmlManager.setBounds(184, 33, 87, 22);
		panelMergerSpisXmlManager.add(btnStopMergerSpisXmlManager);
		
		prbMergerSpisXmlManager = new JProgressBar();
		prbMergerSpisXmlManager.setBounds(12, 1, 40, 10);
		panelMergerSpisXmlManager.add(prbMergerSpisXmlManager);
		
		lblIndErrorMergerSpisXmlManager = new JLabel("");
		lblIndErrorMergerSpisXmlManager.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorMergerSpisXmlManager.setVisible(false);
		lblIndErrorMergerSpisXmlManager.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorMergerSpisXmlManager.setBounds(162, 5, 16, 21);
		panelMergerSpisXmlManager.add(lblIndErrorMergerSpisXmlManager);
		
		JPanel panelMoveOZAC = new JPanel();
		panelMoveOZAC.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelMoveOZAC.setBounds(10, 543, 367, 65);
		panelLists.add(panelMoveOZAC);
		panelMoveOZAC.setLayout(null);
		
		plIndMoveOZAC = new JPanel();
		plIndMoveOZAC.setBounds(1, 1, 10, 10);
		panelMoveOZAC.add(plIndMoveOZAC);
		plIndMoveOZAC.setBackground(Color.RED);
		
		label_3 = new JLabel("�������� OZAC � ����");
		label_3.setHorizontalAlignment(SwingConstants.LEFT);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 11));
		label_3.setBounds(10, 20, 148, 22);
		panelMoveOZAC.add(label_3);
		
		btnStartMoveOZAC = new JButton("\u0421\u0442\u0430\u0440\u0442");
		btnStartMoveOZAC.setAction(startMoveOZAC);
		btnStartMoveOZAC.setMargin(new Insets(2, 5, 2, 14));
		btnStartMoveOZAC.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartMoveOZAC.setBounds(185, 5, 87, 22);
		panelMoveOZAC.add(btnStartMoveOZAC);
		
		btnStopMoveOZAC = new JButton("\u0421\u0442\u043E\u043F");
		btnStopMoveOZAC.setAction(stopMoveOZAC);
		btnStopMoveOZAC.setMargin(new Insets(2, 5, 2, 14));
		btnStopMoveOZAC.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopMoveOZAC.setBounds(185, 33, 87, 22);
		panelMoveOZAC.add(btnStopMoveOZAC);
		
		prbMoveOZAC = new JProgressBar();
		prbMoveOZAC.setToolTipText("8787");
		prbMoveOZAC.setBounds(12, 1, 40, 10);
		panelMoveOZAC.add(prbMoveOZAC);
		
		lblIndErrorMoveOZAC = new JLabel("");
		lblIndErrorMoveOZAC.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
						new File("logs/log_MoveOZAC.txt"),
						"��� ������ ����� ������� �������");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
					MainFrame.this.lblIndErrorMoveOZAC.setVisible(false);
					MainFrame.this.btnStartMoveOZAC.setEnabled(true);
					MainFrame.btnConfigMoveOZAC.setEnabled(true);
					//������ ��������� ������ �� �������
					MainFrame.this.tabbedPane.setIconAt(0, null);
				}
			}
		});
		lblIndErrorMoveOZAC.setVisible(false);
		lblIndErrorMoveOZAC.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorMoveOZAC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorMoveOZAC.setBounds(162, 6, 16, 21);
		panelMoveOZAC.add(lblIndErrorMoveOZAC);
		
		btnConfigMoveOZAC = new JButton("");
		btnConfigMoveOZAC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//���������� �������� �� ����� ������������
				Properties prop = new Properties();
				File commonFileSettingsProcessing = null;
				try {
					prop.load(new FileInputStream(fileConfig));
					commonFileSettingsProcessing = new File(new String(prop.getProperty("commonFileSettingsProcessing").getBytes("ISO8859-1")));
					
				} catch (IOException ex) {
					try {
						throw new Exception("������ ������ ����� ������������", ex);
					} catch (Exception ex2) {
						// TODO Auto-generated catch block
						ex2.printStackTrace();
					}
				}
				new DlgSettingsMoveOZAC(commonFileSettingsProcessing).setVisible(true);
				
			}
		});
		btnConfigMoveOZAC.setIcon(new ImageIcon(MainFrame.class.getResource("/configure.gif")));
		btnConfigMoveOZAC.setBounds(328, 5, 29, 23);
		panelMoveOZAC.add(btnConfigMoveOZAC);
		
		tabbedPane.addTab("������ �������", null, panelOSMP, null);
		panelOSMP.setLayout(null);
		
		panelReceivOSMP.setLayout(null);
		panelReceivOSMP.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelReceivOSMP.setBounds(10, 11, 439, 65);
		panelOSMP.add(panelReceivOSMP);
		
		lblIndErrorReceivOSMP = new JLabel("");
		lblIndErrorReceivOSMP.setVisible(false);
		lblIndErrorReceivOSMP.setToolTipText("������� ������");
		lblIndErrorReceivOSMP.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
													new File("logs/log_receivingOSMP.txt"),
													"��� ������ ����� ������� �������");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
					MainFrame.this.lblIndErrorReceivOSMP.setVisible(false);
					MainFrame.this.btnStartOSMP.setEnabled(true);
					//������ ��������� ������ �� �������
					MainFrame.this.tabbedPane.setIconAt(1, null);
				}
			}
		});
		lblIndErrorReceivOSMP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivOSMP.setBounds(132, 5, 16, 21);
		panelReceivOSMP.add(lblIndErrorReceivOSMP);
		lblIndErrorReceivOSMP.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		
		lblOsmp.setHorizontalAlignment(SwingConstants.LEFT);
		lblOsmp.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblOsmp.setBounds(10, 21, 130, 21);
		panelReceivOSMP.add(lblOsmp);
		btnStartOSMP.setMargin(new Insets(2, 5, 2, 14));
		btnStartOSMP.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartOSMP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		btnStartOSMP.setAction(startReceivOSMP);

		
		btnStartOSMP.setBounds(150, 5, 87, 22);
		panelReceivOSMP.add(btnStartOSMP);
		btnStopOSMP.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopOSMP.setMargin(new Insets(2, 5, 2, 14));
		btnStopOSMP.setAction(stopReceivOSMP);
		
		btnStopOSMP.setBounds(150, 33, 87, 22);
		panelReceivOSMP.add(btnStopOSMP);
	
		pnlIndReceivOSMP.setBackground(Color.RED);
		pnlIndReceivOSMP.setBounds(1, 1, 10, 10);
		panelReceivOSMP.add(pnlIndReceivOSMP);
		prbOSMP.setToolTipText("8787");
		prbOSMP.setBounds(12, 1, 40, 10);
		panelReceivOSMP.add(prbOSMP);
		
		JButton button = new JButton("������");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 9));
		button.setMargin(new Insets(2, 9, 2, 14));
		button.setAction(showLogReceivOSMP);
		button.setHorizontalAlignment(SwingConstants.LEFT);
		button.setBounds(264, 5, 130, 22);
		panelReceivOSMP.add(button);
		
		JPanel panelReceivPOSZ = new JPanel();
		panelReceivPOSZ.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelReceivPOSZ.setBounds(10, 87, 439, 65);
		panelOSMP.add(panelReceivPOSZ);
		panelReceivPOSZ.setLayout(null);
		
		JLabel lblPOSZ = new JLabel("���� POSZ");
		lblPOSZ.setBounds(10, 21, 119, 15);
		lblPOSZ.setHorizontalAlignment(SwingConstants.LEFT);
		lblPOSZ.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panelReceivPOSZ.add(lblPOSZ);
		
		pnlIndReceivPOSZ = new JPanel();
		pnlIndReceivPOSZ.setBackground(Color.RED);
		pnlIndReceivPOSZ.setBounds(1, 1, 10, 10);
		panelReceivPOSZ.add(pnlIndReceivPOSZ);
		
		prbReceivPOSZ = new JProgressBar();
		prbReceivPOSZ.setToolTipText("8787");
		prbReceivPOSZ.setBounds(10, 1, 40, 10);
		panelReceivPOSZ.add(prbReceivPOSZ);
		
		btnStartReceivPOSZ = new JButton("�����");
		btnStartReceivPOSZ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnStartReceivPOSZ.setAction(startReceivPOSZ);
		btnStartReceivPOSZ.setMargin(new Insets(2, 5, 2, 14));
		btnStartReceivPOSZ.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartReceivPOSZ.setBounds(150, 5, 87, 22);
		panelReceivPOSZ.add(btnStartReceivPOSZ);
		
		btnStopReceivPOSZ = new JButton("����");
		btnStopReceivPOSZ.setAction(stopReceivPOSZ);
		btnStopReceivPOSZ.setMargin(new Insets(2, 5, 2, 14));
		btnStopReceivPOSZ.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopReceivPOSZ.setBounds(150, 33, 87, 22);
		panelReceivPOSZ.add(btnStopReceivPOSZ);
		
		lblIndErrorReceivPOSZ = new JLabel("");
		lblIndErrorReceivPOSZ.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
												new File("logs/log_receivingPOSZ.txt"),
												"��� ������ ����� POSZ");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
					MainFrame.this.lblIndErrorReceivPOSZ.setVisible(false);
					MainFrame.this.btnStartReceivPOSZ.setEnabled(true);
					//������ ��������� ������ �� �������
					MainFrame.this.tabbedPane.setIconAt(1, null);
			}
		}
		});
		lblIndErrorReceivPOSZ.setVisible(false);
		lblIndErrorReceivPOSZ.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorReceivPOSZ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivPOSZ.setBounds(132, 5, 16, 21);
		lblIndErrorReceivOSMP.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		panelReceivPOSZ.add(lblIndErrorReceivPOSZ);
		
		panelVOZV = new JPanel();
		tabbedPane.addTab("������", null, panelVOZV, null);
		panelVOZV.setLayout(null);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_5.setBounds(10, 11, 399, 65);
		panelVOZV.add(panel_5);
		panel_5.setLayout(null);
		
		pnlIndReceivPOSO = new JPanel();
		pnlIndReceivPOSO.setBackground(Color.RED);
		pnlIndReceivPOSO.setBounds(1, 1, 10, 10);
		panel_5.add(pnlIndReceivPOSO);
		
		prbPOSO = new JProgressBar();
		prbPOSO.setToolTipText("8787");
		prbPOSO.setBounds(12, 1, 40, 10);
		panel_5.add(prbPOSO);
		
		JLabel lblPOSO = new JLabel("���� POSO");
		lblPOSO.setHorizontalAlignment(SwingConstants.LEFT);
		lblPOSO.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPOSO.setBounds(10, 22, 103, 21);
		panel_5.add(lblPOSO);
		
		btnStartReceivPOSO = new JButton("\u0421\u0442\u0430\u0440\u0442");
		btnStartReceivPOSO.setAction(startPOSO);
		btnStartReceivPOSO.setMargin(new Insets(2, 5, 2, 14));
		btnStartReceivPOSO.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartReceivPOSO.setBounds(150, 5, 87, 22);
		panel_5.add(btnStartReceivPOSO);
		
		btnStopReceivPOSO = new JButton("\u0421\u0442\u043E\u043F");
		btnStopReceivPOSO.setAction(stopPOSO);
		btnStopReceivPOSO.setMargin(new Insets(2, 5, 2, 14));
		btnStopReceivPOSO.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopReceivPOSO.setBounds(150, 33, 87, 22);
		panel_5.add(btnStopReceivPOSO);
		
		lblIndErrorReceivPOSO = new JLabel("");
		lblIndErrorReceivPOSO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivPOSO.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
						new File("logs/log_receivingPOSO.txt"),
						"��� ������ ����� POSO");
				
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
				MainFrame.this.lblIndErrorReceivPOSO.setVisible(false);
				MainFrame.this.btnStartReceivPOSO.setEnabled(true);
				//������ ��������� ������ �� �������
				MainFrame.this.tabbedPane.setIconAt(2, null);
				}
			}
		});
		lblIndErrorReceivPOSO.setVisible(false);
		lblIndErrorReceivPOSO.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorReceivPOSO.setBounds(132, 5, 16, 21);
		panel_5.add(lblIndErrorReceivPOSO);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("���������", null, panel_2, null);
		panel_2.setLayout(null);
		
		panelNotification = new JPanel();
		panelNotification.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelNotification.setBounds(10, 11, 465, 275);
		panel_2.add(panelNotification);
		panelNotification.setLayout(null);
		
		pnlIndReceivNotification = new JPanel();
		pnlIndReceivNotification.setBackground(Color.RED);
		pnlIndReceivNotification.setBounds(1, 1, 10, 10);
		panelNotification.add(pnlIndReceivNotification);
		
		prbNotification = new JProgressBar();
		prbNotification.setToolTipText("8787");
		prbNotification.setBounds(12, 1, 40, 10);
		panelNotification.add(prbNotification);
		
		lblNotification = new JLabel("���� ���������");
		lblNotification.setHorizontalAlignment(SwingConstants.LEFT);
		lblNotification.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNotification.setBounds(11, 21, 114, 21);
		panelNotification.add(lblNotification);
		
		btnStartReceivNotification = new JButton("�����");
		btnStartReceivNotification.setAction(startReceivNotification);
		btnStartReceivNotification.setMargin(new Insets(2, 5, 2, 14));
		btnStartReceivNotification.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartReceivNotification.setBounds(150, 5, 87, 22);
		panelNotification.add(btnStartReceivNotification);
		
		btnStopReceivNotification = new JButton("����");
		btnStopReceivNotification.setAction(stopReceivNotification);
		btnStopReceivNotification.setMargin(new Insets(2, 5, 2, 14));
		btnStopReceivNotification.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopReceivNotification.setBounds(150, 33, 87, 22);
		panelNotification.add(btnStopReceivNotification);
		
		lblIndErrorReceivNotification = new JLabel("");
		lblIndErrorReceivNotification.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
						new File("logs/log_receivingNotification.txt"),
						"��� ������ ����� ���������");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
				MainFrame.this.lblIndErrorReceivNotification.setVisible(false);
				MainFrame.this.btnStartReceivNotification.setEnabled(true);
				//������ ��������� ������ �� �������
				MainFrame.this.tabbedPane.setIconAt(2, null);
}
			}
		});
		lblIndErrorReceivNotification.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorReceivNotification.setVisible(false);
		lblIndErrorReceivNotification.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorReceivNotification.setBounds(132, 5, 16, 21);
		panelNotification.add(lblIndErrorReceivNotification);
		
		panelChbxes = new JPanel();
		panelChbxes.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelChbxes.setBounds(12, 66, 386, 198);
		panelNotification.add(panelChbxes);
		panelChbxes.setLayout(null);
		
		chbxIsRegisterToOPFR = new JCheckBox("�������� ������� � ����");
		chbxIsRegisterToOPFR.setBounds(6, 32, 242, 23);
		panelChbxes.add(chbxIsRegisterToOPFR);
		
		chbxIsListTransferToBank = new JCheckBox("�������� ������ �� ���������� � ����");
		chbxIsListTransferToBank.setBounds(6, 58, 323, 23);
		panelChbxes.add(chbxIsListTransferToBank);
		
		chbxReceiptToBank = new JCheckBox("�������� ��������� � ��������� ������ � ����");
		chbxReceiptToBank.setBounds(6, 84, 357, 23);
		panelChbxes.add(chbxReceiptToBank);
		
		chbxPOOD = new JCheckBox("�������� POOD");
		chbxPOOD.setBounds(6, 110, 279, 23);
		panelChbxes.add(chbxPOOD);
		
		chckbxNewCheckBox = new JCheckBox("����������� ����� ������������ ������� � ��������");
		chckbxNewCheckBox.setBounds(6, 7, 357, 23);
		panelChbxes.add(chckbxNewCheckBox);
		
		JCheckBox chbxPSMP = new JCheckBox("�������� PSMP");
		chbxPSMP.setBounds(6, 136, 279, 23);
		panelChbxes.add(chbxPSMP);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("������", null, panel_3, null);
		panel_3.setLayout(null);
		
		panel_4 = new JPanel();
		panel_4.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_4.setBounds(10, 11, 367, 65);
		panel_3.add(panel_4);
		panel_4.setLayout(null);
		
		btnStartDefinVipnet = new JButton("");
		btnStartDefinVipnet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnStartDefinVipnet.setAction(startDefinVipnet);
		btnStartDefinVipnet.setBounds(155, 7, 87, 22);
		btnStartDefinVipnet.setMargin(new Insets(2, 5, 2, 14));
		btnStartDefinVipnet.setHorizontalAlignment(SwingConstants.LEFT);
		panel_4.add(btnStartDefinVipnet);
		
		btnStopDefinVipnet = new JButton("");
		btnStopDefinVipnet.setAction(stopDefinVipnet);
		btnStopDefinVipnet.setMargin(new Insets(2, 5, 2, 14));
		btnStopDefinVipnet.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopDefinVipnet.setBounds(155, 33, 87, 22);
		panel_4.add(btnStopDefinVipnet);
		
		JLabel lblVipnet = new JLabel("����������� ��� Vipnet");
		lblVipnet.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblVipnet.setBounds(10, 15, 124, 30);
		panel_4.add(lblVipnet);
		
		pnlIndDefinVipnet = new JPanel();
		pnlIndDefinVipnet.setBackground(Color.RED);
		pnlIndDefinVipnet.setBounds(1, 1, 10, 10);
		panel_4.add(pnlIndDefinVipnet);
		
		prbDefinVipnet = new JProgressBar();
		prbDefinVipnet.setToolTipText("8787");
		prbDefinVipnet.setBounds(12, 1, 40, 10);
		panel_4.add(prbDefinVipnet);
		
		lblIndErrorDefinVipnet = new JLabel("");
		lblIndErrorDefinVipnet.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				DlgShowLogError dlg = new DlgShowLogError(MainFrame.this, 
						new File("logs/log_definVipnet.txt"),
						"��� ������ ����������� ��� Vipnet");
				dlg.setVisible(true);
				
				if (dlg.result == 1) {
				MainFrame.this.lblIndErrorDefinVipnet.setVisible(false);
				MainFrame.this.btnStartDefinVipnet.setEnabled(true);
				//������ ��������� ������ �� �������
				MainFrame.this.tabbedPane.setIconAt(3, null);
				}	
			}
		});
		lblIndErrorDefinVipnet.setVisible(false);
		lblIndErrorDefinVipnet.setIcon(new ImageIcon(MainFrame.class.getResource("/warning.gif")));
		lblIndErrorDefinVipnet.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblIndErrorDefinVipnet.setBounds(135, 11, 18, 14);
		panel_4.add(lblIndErrorDefinVipnet);
		
		JLabel lblNewLabel = new JLabel("�������");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		
		contentPane.add(panel, BorderLayout.SOUTH);
		FlowLayout fl_panel = new FlowLayout(FlowLayout.LEFT, 5, 5);
		panel.setLayout(fl_panel);
		
		JPanel panel_1 = new JPanel();
	
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		pnlIndServer.setBackground(Color.RED);
		FlowLayout fl_pnlIndServer = (FlowLayout) pnlIndServer.getLayout();
		panel_1.add(pnlIndServer);
		
		prbIndServer.setToolTipText("");
		panel_1.add(prbIndServer);
		
		btnStartServer = new JButton("�����");
		btnStartServer.setHorizontalAlignment(SwingConstants.LEFT);
		btnStartServer.setMargin(new Insets(2, 7, 2, 14));
		btnStartServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		btnStartServer.setAction(initServer);
		
		panel_1.add(btnStartServer);
		
		btnStopServer = new JButton("����");
		btnStopServer.setMargin(new Insets(2, 2, 2, 14));
		btnStopServer.setHorizontalAlignment(SwingConstants.LEFT);
		btnStopServer.setIcon(new ImageIcon(MainFrame.class.getResource("/progress_stop.gif")));
		btnStopServer.setEnabled(false);
		btnStopServer.setAction(stopServer);
		panel_1.add(btnStopServer);
		
		JLabel lblNewLabel_1 = new JLabel("����� ������:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(lblNewLabel_1);
		
	
		panel_1.add(lblTimerServer);
		
		
		//������������� � ������
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Object[] options = {"��", "���"};
				int n = JOptionPane.showOptionDialog(e.getWindow(), 
										"�� ��������, ��� ������ ������� ���������?", 
										"�������������", JOptionPane.YES_NO_OPTION, 
										JOptionPane.QUESTION_MESSAGE, 
										null, 
										options, 
										options[1]);
				if (n == 0) {
					e.getWindow().setVisible(false);
					System.exit(0);
				}
			}
		});
		
		//������������� 
		this.initServer.actionPerformed(null);
		
	}
	
	@SuppressWarnings("serial")
	private class StopReceivRegsFromOPFR extends AbstractAction {
		public StopReceivRegsFromOPFR() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			btnStopReceivRegsFromOPFR.setEnabled(false);
			prbReceivRegsFromOPFR.setVisible(true);
			prbReceivRegsFromOPFR.setToolTipText("��������� ��������");
			processingRegsFromOPFR.interrupt();
		}
	}
	
	@SuppressWarnings("serial")
	private class StartReceivRegsFromOPFR extends AbstractAction {
		public StartReceivRegsFromOPFR() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndReceivRegsFromOPFR.setBackground(Color.GREEN);
			btnStartReceivRegsFromOPFR.setEnabled(false);
			btnStopReceivRegsFromOPFR.setEnabled(true);
			
			processingRegsFromOPFR = new TaskReceivRegsFromOPFR(sessionFactory, fileConfig, frame);
			processingRegsFromOPFR.start();
		}
	}
	@SuppressWarnings("serial")
	private class StartReceivOSMP extends AbstractAction {
		public StartReceivOSMP() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		
		public void actionPerformed(ActionEvent e) {
			pnlIndReceivOSMP.setBackground(Color.GREEN);
			btnStartOSMP.setEnabled(false);
			btnStopOSMP.setEnabled(true);
			
			processingOSMP = new TaskReceivOSMP(sessionFactory, fileConfig, frame);
			processingOSMP.start();
		}
	}
		
	@SuppressWarnings("serial")
	private class StopReceivOSMP extends AbstractAction {
		public StopReceivOSMP() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			btnStopOSMP.setEnabled(false);
			prbOSMP.setVisible(true);
			prbOSMP.setToolTipText("��������� ��������");
			processingOSMP.interrupt();
		}
	}
	private class InitServer extends AbstractAction {
		public InitServer() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "������");
			putValue(SHORT_DESCRIPTION, "");
		}
		
		public void actionPerformed(ActionEvent e) {
			btnStartServer.setEnabled(false);
			prbIndServer.setIndeterminate(true);
			prbIndServer.setToolTipText("������ �������");
			
			new Thread(new Runnable() {
				public void run() {
					
					try {
						sessionFactory = HibernateUtil.getSessionFactory();
						/*
						//��������, ������� �� ���������� "�������"
						if (SedbankAutoProcessing.ver == VER_PRODUCTION) {
							Properties prop = new Properties();
							Path pathMiranda32Exe = null;
							try {
								prop.load(new FileInputStream(fileConfig));
								pathMiranda32Exe = Paths.get(new String(prop.getProperty("pathMiranda32Exe").getBytes("ISO8859-1")));
								
							} catch (IOException e) {
								throw new Exception(e);
							}
								
							try {
								Runtime rt = Runtime.getRuntime();
								String commandLine = pathMiranda32Exe.toString();
								Process process = rt.exec(commandLine);
								if (process == null)
									throw new Exception("start process Miranda32.exe error");
								
							} catch (Exception e) {
								throw new Exception(e);
							}
						} */
						
						//���������� ���� ����������
						dictCodeCredited = new Dictionary<>(sessionFactory, CodeCredited.class.getName(), 
													new Dictionary.ValueKey<CodeCredited>() {
														public String getValueKey(CodeCredited obj) {
															return obj.getName();
														}});
						dictCodeCredited.init();
						
						//���������� "���� �������� ���������"
						dictTypeArrayAssignment = new Dictionary<>(sessionFactory, TypeArrayAssignment.class.getName(),
															new Dictionary.ValueKey<TypeArrayAssignment>() {
																public String getValueKey(TypeArrayAssignment obj) {
																	return obj.getName();
																}});
						dictTypeArrayAssignment.init();
						
						//���������� "����������� �����������"
						dictDeliveryOrg = new Dictionary<>(sessionFactory, DeliveryOrg.class.getName(),
													new Dictionary.ValueKey<DeliveryOrg>() {
														public String getValueKey(DeliveryOrg obj) {
															return obj.getNameNVP();
												}});
						dictDeliveryOrg.init();
						
						//���������� "����������� ����� � �����"
						dictNameOrgInFile = new Dictionary<>(sessionFactory, NameOrgInFile.class.getName(),
													new Dictionary.ValueKey<NameOrgInFile>() {
														public String getValueKey(NameOrgInFile obj) {
															return obj.getCodeBranch();
														}}); 
						dictNameOrgInFile.init();
						
						//���������� "���� ������"
						dictTypeError = new Dictionary<>(sessionFactory, TypeError.class.getName(),
												new Dictionary.ValueKey<TypeError>() {
													public String getValueKey(TypeError obj) {
														return obj.getDescription();
													}});
						dictTypeError.init();
						
						//���������� "����������� �����������" �� ShortName
						dictDeliveryOrgByShortName = new Dictionary<>(sessionFactory, DeliveryOrg.class.getName(),
															new Dictionary.ValueKey<DeliveryOrg>() {
																public String getValueKey(DeliveryOrg obj) {
																	return obj.getShortName();
																}});
						dictDeliveryOrgByShortName.init();
						
						//���������� "���� ����������� ���������"
						dictTypesResultsNotifications = new Dictionary<>(sessionFactory, TypeResultNotification.class.getName(),
																new Dictionary.ValueKey<TypeResultNotification>() {
															@Override
															public String getValueKey(TypeResultNotification obj) {
																return obj.getName();
															}});
						dictTypesResultsNotifications.init();
						
						//���������� "���� �����"
						dictTypesPay = new Dictionary<>(sessionFactory, TypePay.class.getName(),
												new Dictionary.ValueKey<TypePay>() {
											@Override
											public String getValueKey(TypePay obj) {
												return obj.getName();
											}});
						dictTypesPay.init();
						
						//���������� "������� ������"
						dictSubtypesPay = new Dictionary<>(sessionFactory, SubtypePay.class.getName(),
												new Dictionary.ValueKey<SubtypePay>() {
											@Override
											public String getValueKey(SubtypePay obj) {
												return obj.getName();
											}});
						dictSubtypesPay.init();
						
						//���������� "������� �������"
						dictSignPayments = new Dictionary<>(sessionFactory, SignPayment.class.getName(),
								new Dictionary.ValueKey<SignPayment>() {
							@Override
							public String getValueKey(SignPayment obj) {
								return obj.getName();
							}
						});
						dictSignPayments.init();
						
						
						//���������� "����������� ����� ������������"
						dictTypeDifferenceRecipient = new Dictionary<>(sessionFactory, TypeDifferenceRecipient.class.getName(),
												new Dictionary.ValueKey<TypeDifferenceRecipient>() {
													@Override
													public String getValueKey(TypeDifferenceRecipient obj) {
														return obj.getName();
													}});
						dictTypeDifferenceRecipient.init();
						
						//���������� "����������� ����� ��������"
						dictTypeDifferenceSPIS = new Dictionary<>(sessionFactory, TypeDifferenceSPIS.class.getName(),
													new Dictionary.ValueKey<TypeDifferenceSPIS>() {
												@Override
												public String getValueKey(TypeDifferenceSPIS obj) {
													return obj.getName();
												}});
						dictTypeDifferenceSPIS.init();
						
						//���������� "���� ����������, ����������������� � ������ ��������� �����"
						dictTypeDocDead = new Dictionary<>(sessionFactory, TypeDocDead.class.getName(),
								new Dictionary.ValueKey<TypeDocDead>() {
							@Override
							public Integer getValueKey(TypeDocDead obj) {
								return obj.getCode();
							}});
						dictTypeDocDead.init();
						
						
						//���������� "������� ����������� ������"
						dictTypeCauseTermination = new Dictionary<>(sessionFactory, TypeCauseTermination.class.getName(),
								new Dictionary.ValueKey<TypeCauseTermination>() {
							@Override
							public String getValueKey(TypeCauseTermination obj) {
								return obj.getName();
							}});
						dictTypeCauseTermination.init();
						
						
					} catch (Exception e) {
						btnStartServer.setEnabled(true);
						e.printStackTrace();
						
						final Exception e2 = e;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								Object[] options = {"OK"};
								int n = JOptionPane.showOptionDialog(null, 
														"��� ������������� �������� ������: " + e2.getMessage(), 
														"�������������", JOptionPane.YES_OPTION, 
														JOptionPane.WARNING_MESSAGE, 
														null, 
														options, 
														options[0]);
							}		
						});
					} finally {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								prbIndServer.setIndeterminate(false);
								prbIndServer.setToolTipText("");
							}		
						});
						
					}
					
					
					//������������� � ������ �������
					dateServerStart = new Date();
					timerWorkServer = new Timer();
					timerWorkServer.schedule(new TimerTask() {
						@Override
						public void run() {
							Long d = new Date().getTime() - dateServerStart.getTime();
							lblTimerServer.setText(Utils.getTimeStr(d));
						}
						
					}, 0, 1000);
					
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							pnlIndServer.setBackground(Color.GREEN);
							tabbedPane.setVisible(true);	
							btnStopServer.setEnabled(true);
						}		
					});
					
					//�������������� ������ ���������
					Boolean startOSMP = false;
					Boolean startPacksFromCombanks = false;
					Boolean startPOSD = false;
					Boolean startReceivRegsFromOPFR = false;
					Boolean startNotifications = false;
					Boolean startReceivOZAC = false;
					Boolean startSendPOOD = false;
					Boolean startSeparationOzacXmlManager = false;
					Boolean startMergerSpisXmlManager = false;
					 
					//������ ����� ������������
					Properties p = new Properties();
					try {
						p.load(new FileInputStream(fileConfig));
						
						startOSMP = Boolean.valueOf(p.getProperty("startOSMP"));
						startPacksFromCombanks = Boolean.valueOf(p.getProperty("startPacksFromCombanks"));
						startPOSD = Boolean.valueOf(p.getProperty("startPOSD"));
						startReceivRegsFromOPFR = Boolean.valueOf(p.getProperty("startReceivRegsFromOPFR"));
						startNotifications = Boolean.valueOf(p.getProperty("startNotifications"));
						startReceivOZAC = Boolean.valueOf(p.getProperty("startReceivOZAC"));
						startSendPOOD = Boolean.valueOf(p.getProperty("startSendPOOD"));
						startSeparationOzacXmlManager = Boolean.valueOf(p.getProperty("startSeparationOzacXmlManager"));
						startMergerSpisXmlManager = Boolean.valueOf(p.getProperty("startMergerSpisXmlManager"));
						
						
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					//������ ����� OSMP
					if (startOSMP) {
						MainFrame.this.startReceivOSMP.actionPerformed(null);
					}
					
					//������ ����� Packs from combanks
					if (startPacksFromCombanks) {
						MainFrame.this.startReceivPacksCombanks.actionPerformed(null);
					}
					
					//������ ����� POSD
					if (startPOSD) {
						MainFrame.this.startReceivPOSD.actionPerformed(null);
					}
					
					//������ ����� �������� �� ����
					if (startReceivRegsFromOPFR) {
						MainFrame.this.startReceivRegsFromOPFR.actionPerformed(null);
					}
					
					//������ ����� ���������
					if (startNotifications) {
						MainFrame.this.startReceivNotification.actionPerformed(null);
					}
					
					//������ ����� OZAC
					if (startReceivOZAC) {
						MainFrame.this.startReceivOZAC.actionPerformed(null);
					}
					
					//������ �������� POOD
					if (startSendPOOD) {
						MainFrame.this.startSendPOOD.actionPerformed(null);
					}
					
					//������ ���������� OZAC � XMLManager
					if (startSeparationOzacXmlManager) {
						MainFrame.this.startSeparationOzacXmlManager.actionPerformed(null);
					}
					
					//������ ����������� SPIS � XMLManager
					if (startMergerSpisXmlManager) {
						MainFrame.this.startMergerSpisXmlManager.actionPerformed(null);
					}
					
					
				}	
			}).start();
			
		}
	}
	private class StopServer extends AbstractAction {
		public StopServer() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (3).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			prbIndServer.setIndeterminate(true);
			prbIndServer.setToolTipText("������� �������");
			btnStopServer.setEnabled(false);
			
			new Thread(new Runnable() {
				public void run() {

					try {
						HibernateUtil.shutdown();
						
						//��������� ������������
						dictCodeCredited = null;
						dictTypeArrayAssignment = null;
						dictDeliveryOrg = null;
						dictNameOrgInFile = null;
						dictTypeError = null;
						dictDeliveryOrgByShortName = null;
						dictTypesResultsNotifications = null;
						dictTypesPay = null;
						dictSubtypesPay = null;
						dictSignPayments = null;
						dictTypeDifferenceRecipient = null;
						dictTypeDifferenceSPIS = null;
						dictTypeDocDead = null;
						dictTypeCauseTermination = null;
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					//������� �������
					timerWorkServer.cancel();
					
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							prbIndServer.setIndeterminate(false);
							prbIndServer.setToolTipText("");
							pnlIndServer.setBackground(Color.RED);
							tabbedPane.setVisible(false);	
							btnStartServer.setEnabled(true);
						}		
					});
				}	
			}).start();
			
		}
	}
	
	
	private class ShowLogReceivOSMP extends AbstractAction {
		public ShowLogReceivOSMP() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/prop_ps.gif")));
			putValue(NAME, "������ �����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			new LogOSMP(sessionFactory).setVisible(true);
			
		}
	}
	private class StartReceivPacksCombanks extends AbstractAction {
		public StartReceivPacksCombanks() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndReceivPacksCombanks.setBackground(Color.GREEN);
			btnStartReceivPacksCombanks.setEnabled(false);
			btnStopReceivPacksCombanks.setEnabled(true);
			
			processingCombanks = new TaskReceivPacksFromCombank(sessionFactory, fileConfig, frame);
			processingCombanks.start();
		}
	}
	private class StopReceivPacksCombanks extends AbstractAction {
		public StopReceivPacksCombanks() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (3).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			btnStopReceivPacksCombanks.setEnabled(false);
			prbReceivPacksCombanks.setVisible(true);
			prbReceivPacksCombanks.setToolTipText("��������� ��������");
			processingCombanks.interrupt();
		}
	}
	private class StartReceivPOSD extends AbstractAction {
		public StartReceivPOSD() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndReceivPOSD.setBackground(Color.GREEN);
			btnStartReceivPOSD.setEnabled(false);
			btnStopReceivPOSD.setEnabled(true);
			
			processingPOSD = new TaskReceivPOSD(sessionFactory, fileConfig, frame);
			processingPOSD.start();
		}
	}
	private class StopReceivPOSD extends AbstractAction {
		public StopReceivPOSD() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopReceivPOSD.setEnabled(false);
			MainFrame.this.prbReceivPOSD.setVisible(true);
			MainFrame.this.prbReceivPOSD.setToolTipText("��������� ��������");
			processingPOSD.interrupt();
		}
	}
	private class StartReceivOZAC extends AbstractAction {
		public StartReceivOZAC() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndReceivOZAC.setBackground(Color.GREEN);
			btnStartReceivOZAC.setEnabled(false);
			btnStopReceivOZAC.setEnabled(true);
			
			processingReceivOZAC = new TaskReceivOZAC(sessionFactory, fileConfig, frame);
			processingReceivOZAC.start();
		}
	}
	private class StopReceivOZAC extends AbstractAction {
		public StopReceivOZAC() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopReceivOZAC.setEnabled(false);
			MainFrame.this.prbReceivOZAC.setVisible(true);
			MainFrame.this.prbReceivOZAC.setToolTipText("��������� ��������");
			processingReceivOZAC.interrupt();
		}
	}
	private class StartSendPOOD extends AbstractAction {
		public StartSendPOOD() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndSendPOOD.setBackground(Color.GREEN);
			btnStartSendPOOD.setEnabled(false);
			btnStopSendPOOD.setEnabled(true);
			
			processingSendPOOD = new TaskSendPOOD(sessionFactory, fileConfig, frame);
			processingSendPOOD.start();
		}
	}
	private class StopSendPOOD extends AbstractAction {
		public StopSendPOOD() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopSendPOOD.setEnabled(false);
			MainFrame.this.prbSendPOOD.setVisible(true);
			MainFrame.this.prbSendPOOD.setToolTipText("��������� ��������");
			processingSendPOOD.interrupt();
		}
	}
	private class SettingsReceivRegsFromOPFR extends AbstractAction {
		public SettingsReceivRegsFromOPFR() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/configure.gif")));
			putValue(NAME, "");
			putValue(SHORT_DESCRIPTION, "���������");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class showDlgAbout extends AbstractAction {
		public showDlgAbout() {
			putValue(NAME, "� ���������");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			new DlgAbout().setVisible(true);
		}
	}
	private class StartReceivNotification extends AbstractAction {
		public StartReceivNotification() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			pnlIndReceivNotification.setBackground(Color.GREEN);
			btnStartReceivNotification.setEnabled(false);
			btnStopReceivNotification.setEnabled(true);
			
			//��������������� checkboxes
			Component[] components = MainFrame.this.panelChbxes.getComponents();
			for (Component component : components) {
				component.setEnabled(false);
			}
			
			processingNotifications = new TaskReceivNotifications(sessionFactory, fileConfig, frame);
			processingNotifications.start();
		}
	}
	private class StopReceivNotification extends AbstractAction {
		public StopReceivNotification() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopReceivNotification.setEnabled(false);
			MainFrame.this.prbNotification.setVisible(true);
			MainFrame.this.prbNotification.setToolTipText("��������� ��������");
			processingNotifications.interrupt();
		}
	}
	private class ShowDlgSendMessages extends AbstractAction {
		public ShowDlgSendMessages() {
			putValue(NAME, "�������� ���������");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			new DlgSettingSendMessages().setVisible(true);
		}
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	private class StartSeparationOzacXmlManager extends AbstractAction {
		public StartSeparationOzacXmlManager() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndSeparationOzacXmlManager.setBackground(Color.GREEN);
			btnStartSeparationOzacXmlManager.setEnabled(false);
			btnStopSeparationOzacXmlManager.setEnabled(true);
			
			processingSeparationOzacXmlManager = new TaskSeparationOzacXmlManager(sessionFactory, fileConfig, frame);
			processingSeparationOzacXmlManager.start();
		}
	}
	private class StopSeparationOzacXmlManager extends AbstractAction {
		public StopSeparationOzacXmlManager() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopSeparationOzacXmlManager.setEnabled(false);
			MainFrame.this.prbSeparationOzacXmlManager.setVisible(true);
			MainFrame.this.prbSeparationOzacXmlManager.setToolTipText("��������� ��������");
			processingSeparationOzacXmlManager.interrupt();
		}
	}
	private class StartMergerSpisXmlManager extends AbstractAction {
		public StartMergerSpisXmlManager() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndMergerSpisXmlManager.setBackground(Color.GREEN);
			btnStartMergerSpisXmlManager.setEnabled(false);
			btnStopMergerSpisXmlManager.setEnabled(true);
			
			processingMergerSpisXmlManager = new TaskMergerSpisXmlManager(sessionFactory, fileConfig, frame);
			processingMergerSpisXmlManager.start();	
		}
	}
	private class StopMergerSpisXmlManager extends AbstractAction {
		public StopMergerSpisXmlManager() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopMergerSpisXmlManager.setEnabled(false);
			MainFrame.this.prbMergerSpisXmlManager.setVisible(true);
			MainFrame.this.prbMergerSpisXmlManager.setToolTipText("��������� ��������");
			processingMergerSpisXmlManager.interrupt();
		}
	}
	private class StartReceivPOSZ extends AbstractAction {
		public StartReceivPOSZ() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			pnlIndReceivPOSZ.setBackground(Color.GREEN);
			btnStartReceivPOSZ.setEnabled(false);
			btnStopReceivPOSZ.setEnabled(true);
			
			processingPOSZ = new TaskReceivPOSZ(sessionFactory, fileConfig, frame);
			processingPOSZ.start();
		}
	}
	private class StopReceivPOSZ extends AbstractAction {
		public StopReceivPOSZ() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/progress_stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopReceivPOSZ.setEnabled(false);
			MainFrame.this.prbReceivPOSZ.setVisible(true);
			MainFrame.this.prbReceivPOSZ.setToolTipText("��������� ��������");
			processingPOSZ.interrupt();
		}
	}
	private class StartDefinVipnet extends AbstractAction {
		public StartDefinVipnet() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			pnlIndDefinVipnet.setBackground(Color.GREEN);
			btnStartDefinVipnet.setEnabled(false);
			btnStopDefinVipnet.setEnabled(true);
			
			processingDefinVipnet = new TaskDefinVipnet(sessionFactory, fileConfig, frame);
			processingDefinVipnet.start();
		}
	}
	private class StopDefinVipnet extends AbstractAction {
		public StopDefinVipnet() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/terminate_co (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopDefinVipnet.setEnabled(false);
			MainFrame.this.prbDefinVipnet.setVisible(true);
			MainFrame.this.prbDefinVipnet.setToolTipText("��������� ��������");
			processingDefinVipnet.interrupt();
		}
	}
	
	
	private class StartMoveOZAC extends AbstractAction {
		public StartMoveOZAC() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			plIndMoveOZAC.setBackground(Color.GREEN);
			btnStartMoveOZAC.setEnabled(false);
			btnStopMoveOZAC.setEnabled(true);
			btnConfigMoveOZAC.setEnabled(false);
			
			processingMoveOZAC = new TaskMoveOZAC(sessionFactory, fileConfig, frame);
			processingMoveOZAC.start();
		}
	}
	private class StopMoveOZAC extends AbstractAction {
		public StopMoveOZAC() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/terminate_co (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopMoveOZAC.setEnabled(false);
			MainFrame.this.prbMoveOZAC.setVisible(true);
			MainFrame.this.prbMoveOZAC.setToolTipText("��������� ��������");
			processingMoveOZAC.interrupt();
		}
	}
	private class StartPOSO extends AbstractAction {
		public StartPOSO() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/execution_obj.gif")));
			putValue(NAME, "�����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			pnlIndReceivPOSO.setBackground(Color.GREEN);
			btnStartReceivPOSO.setEnabled(false);
			btnStopReceivPOSO.setEnabled(true);
			
			//processingReceivPOSO = new TaskReceivPOSO(sessionFactory, fileConfig, frame);
			//processingReceivPOSO.start();
		}
	}
	private class StopPOSO extends AbstractAction {
		public StopPOSO() {
			putValue(LARGE_ICON_KEY, new ImageIcon(MainFrame.class.getResource("/stop (2).gif")));
			putValue(NAME, "����");
			putValue(SHORT_DESCRIPTION, "");
		}
		public void actionPerformed(ActionEvent e) {
			MainFrame.this.btnStopReceivPOSO.setEnabled(false);
			MainFrame.this.prbPOSO.setVisible(true);
			MainFrame.this.prbPOSO.setToolTipText("��������� ��������");
			//processingReceivPOSO.interrupt();
		}
	}
}
