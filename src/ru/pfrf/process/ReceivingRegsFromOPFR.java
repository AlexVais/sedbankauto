package ru.pfrf.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

import javax.persistence.*;

import main.MainFrame;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserRegsFromOPFR;
import ru.pfrf.errors.ErrorReceivRegsFromOPFR;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.PaymentOrder;
import ru.pfrf.register.RegisterTFFromOPFR;
import ru.pfrf.register.RegisterTFToOPFR;


@Entity
@Table(name = "new_receivings_regs_from_opfr")
public class ReceivingRegsFromOPFR extends Process {
	private static final long serialVersionUID = 1L;
	
	@Transient
	private Path pathSource;
	
	@Transient
	private Path pathArchive;
	
	@OneToMany(mappedBy = "newReceivingRegsFromOPFR", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<RegisterTFFromOPFR> setRegistersTFFromOPFR = new HashSet<>();
	
	
	public Set<RegisterTFFromOPFR> getSetRegistersTFFromOPFR() {
		return setRegistersTFFromOPFR;
	}

	public void setSetRegistersTFFromOPFR(
			Set<RegisterTFFromOPFR> setRegistersTFFromOPFR) {
		this.setRegistersTFFromOPFR = setRegistersTFFromOPFR;
	}

	public ReceivingRegsFromOPFR() {}
	
	public ReceivingRegsFromOPFR(SessionFactory sessionFactory, File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.pathSource = Paths.get(new String(p.getProperty("sourcePathRegsFromOPFR").getBytes("ISO8859-1")));
			this.pathArchive = Paths.get(new String(p.getProperty("archivePathRegsFromOPFR").getBytes("ISO8859-1")));
		
		} catch (IOException e) {
			throw new Exception(e);
		}
	}
	
	
	private boolean isReceivingAgain() {
		return false;
	}

	@Override 
	protected void exec() throws Exception {
		try {
			//������������ ��������, � ������� ������������� ������� �� ����		
			File[] files = new File(pathSource.toString()).listFiles(new FileFilter() {
																public boolean accept(File pathname) {
																	if (pathname.isHidden()) return false;
																	return (pathname.isFile()) ? true : false;
																}});
			
			Set<RegisterTFFromOPFR> setRegistersTFFromOPFR = new HashSet<>();
			for (File file : files) {
				//������� ��������
				ParserRegsFromOPFR parser = new ParserRegsFromOPFR(file);
				parser.parse();
			
				//���� � �������� ���������� ����������
				if (!parser.getErrors().isEmpty()) {
					//����������� ������ � ��������
					StringBuilder sb = new StringBuilder();
					for (TypeError typeError : parser.getErrors()) 
						sb.append(typeError.getDescription() + " | ");
					String strError = "����: " + file.getName() + ". ������: " + sb;
					
					throw new Exception(strError);
				}
				
				//��������� ���������� �������, ���������������� �������������
				Session session = null;
				Transaction t = null;
				Criteria criteria = null;
				CreatedRegisterTF createdRegisterTF = null;
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					//RegisterTF
					criteria = session.createCriteria(CreatedRegisterTF.class, "registerTF")
											   .add(Restrictions.eq("registerTF.yearPay", parser.getYearPay()))
											   .add(Restrictions.eq("registerTF.monthPay", parser.getMonthPay()))
											   .add(Restrictions.eq("registerTF.number", parser.getNumberReg()))
											   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
											   
					
					List<CreatedRegisterTF> listCreatedRegisterTF = criteria.list();
					if (listCreatedRegisterTF.size() > 1)
						throw new Exception("���������� �������� ����� ������");
					
					if (listCreatedRegisterTF.isEmpty()) 
						throw new Exception("������ � ������� " + parser.getNumberReg() + " " + parser.getYearPay() + " ���� �� ������");
					
					createdRegisterTF = listCreatedRegisterTF.get(0);
					
					t.commit();
					
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� RegisterToOPFR. " + e.getMessage() , e);
				} finally {
					session.close();	
				}
				
				
				//����������, �� ��� �� ������� ������ ������ �����
				RegisterTFToOPFR registerTFToOPFR = createdRegisterTF.getRegisterTFToOPFR();
				
				for (RegisterTFFromOPFR registerTFFromOPFR : registerTFToOPFR.getSetRegisterTFFromOPFR()) {
					if (registerTFFromOPFR.getSetErrorReceivRegsFromOPFR().isEmpty()) {
						throw new Exception("������ � ������� " + parser.getNumberReg() +  " " + parser.getYearPay() + "���� ����� ��� ��� ������� ������");
					} 
				}
				
				
				
				//��������� ���������� ������������� ������� � ������������
				RegisterTFFromOPFR registerTFFromOPFR = new RegisterTFFromOPFR(2, registerTFToOPFR,	this, parser.getFile());
				this.setRegistersTFFromOPFR.add(registerTFFromOPFR);
				
				Set<TypeError> setTypeErrors = new HashSet<>();
				
				if ((!parser.getMonthPay().equals(createdRegisterTF.getMonthPay())) && (!parser.getYearPay().equals(createdRegisterTF.getYearPay()))) 
					setTypeErrors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_DATEPAY));
				
				if (!parser.getTotalRecipients().equals(createdRegisterTF.getCountRecipients())) {
					setTypeErrors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_COUNTRECIPIENTS));
				}
				
				if (!parser.getTotalSum().equals(createdRegisterTF.getSum())) 
					setTypeErrors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_TOTALSUM));
				
				if (!parser.getDateOfFunding().equals(createdRegisterTF.getDateOfFunding())) 
					setTypeErrors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_DATEFUNDING));
				
				
				//�������. �� ���������� �� ����� ������ ����� �� ������� ���������� ���������
				session = null;
				t = null;
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					criteria = session.createCriteria(PaymentOrder.class, "paymentOrder")
										.createCriteria("paymentOrder.registerTFFromOPFR", "registerTFFromOPFR", JoinType.INNER_JOIN)
										.createCriteria("registerTFFromOPFR.registerTFToOPFR", "registerTFToOPFR", JoinType.INNER_JOIN)
										.createCriteria("registerTFToOPFR.createdRegisterTF", "reg", JoinType.INNER_JOIN)
									    .add(Restrictions.eq("reg.yearPay", createdRegisterTF.getYearPay()))
									    .add(Restrictions.eq("paymentOrder.number", parser.getNumberPaymentOrder()))
										.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
											   
					
					List<PaymentOrder> listPaymentsOrder = criteria.list();
					if (!listPaymentsOrder.isEmpty()) {
						setTypeErrors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_NUMBERORDEREXIST));
						System.out.println(listPaymentsOrder);
					}
					
					t.commit();
					
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� PaymentOrder. " + e.getMessage() , e);
				} finally {
					session.close();	
				}
				
				
				//������ ���� �������������� � ���� ��������� ���������
				if (!parser.getDatePaymentOrder().equals(createdRegisterTF.getDateOfFunding())) 
					setTypeErrors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_DATEFUNDING_NOTEQUALS_DATEORDER));
				
				
				for (TypeError typeError : setTypeErrors) {
					ErrorReceivRegsFromOPFR error = new ErrorReceivRegsFromOPFR(typeError, registerTFFromOPFR);
					registerTFFromOPFR.getSetErrorReceivRegsFromOPFR().add(error);
				}
				
				//���� ���� ������, �� ������������� ����������
				if (!registerTFFromOPFR.getSetErrorReceivRegsFromOPFR().isEmpty()) {
					StringBuilder sb = new StringBuilder();
					for(ErrorReceivRegsFromOPFR error : registerTFFromOPFR.getSetErrorReceivRegsFromOPFR())
						sb.append(error.getTypeError().getDescription() + " | ");
					
					String strError = "�������� � ������������ � �������� �������� �" + createdRegisterTF.getNumber() + ": " + sb.toString();
					throw new Exception(strError);
				}	
				
				//�������� �������
				PaymentOrder paymentOrder = new PaymentOrder(parser.getNumberPaymentOrder(),
														parser.getDatePaymentOrder(),
														registerTFFromOPFR);
				registerTFFromOPFR.setPaymentOrder(paymentOrder);
				
				//���������� � ���������
				setRegistersTFFromOPFR.add(registerTFFromOPFR);

			}
			
			//����������� � ����� �������� ������
			for (RegisterTFFromOPFR reg : setRegistersTFFromOPFR) {
				Files.move(reg.getFile().toPath(), 
						Paths.get(this.pathArchive + "/" + reg.getFile().getName()), 
						StandardCopyOption.REPLACE_EXISTING);
			}
			
		} catch (Exception e) {
			throw new Exception("������ ��� ����� ������� �� ����. " + e.getMessage(), e);
		}
	}
	
}
