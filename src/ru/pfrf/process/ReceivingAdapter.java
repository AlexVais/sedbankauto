package ru.pfrf.process;

import javax.persistence.*;

@MappedSuperclass
public abstract class ReceivingAdapter {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Process")
	private ProcessAdapter processAdapter;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public ProcessAdapter getProcessAdapter() {
		return processAdapter;
	}

	public void setProcessAdapter(ProcessAdapter processAdapter) {
		this.processAdapter = processAdapter;
	}

	public ReceivingAdapter() {}
	
	public ReceivingAdapter(ProcessAdapter processAdapter) {
		this.processAdapter = processAdapter;
	}
}
