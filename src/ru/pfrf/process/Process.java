package ru.pfrf.process;

import java.io.File;
import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import ru.pfrf.register.PaymentOrder;


@MappedSuperclass
public abstract class Process implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Transient
	protected SessionFactory sessionFactory;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@Column(name = "dateBegin")
	protected Date dateBegin;

	@Column(name = "dateEnd")
	protected Date dateEnd;
	
	@Transient
	private File fileConfig;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Date getDateBegin() {
		return dateBegin;
	}

	public void setDateBegin(Date dateBegin) {
		this.dateBegin = dateBegin;
	} 
	
	public Process() {}
	
	public Process(SessionFactory sessionFactory, File fileConfig) {
		this.sessionFactory = sessionFactory;
		this.fileConfig = fileConfig;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		Process p = (Process) obj;
		return new EqualsBuilder()
		                 .append(this.dateBegin, this.dateBegin)
		                 .append(this.dateEnd, p.dateEnd)
		                 .isEquals();	
	}
	
	
	protected abstract void exec() throws Exception;
	
	public final void start() throws Exception {
		dateBegin = new Date();
		exec();
		dateEnd = new Date();
	}

}
