package ru.pfrf.process;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.OutMainFile;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.SPRA;

@Entity
@Table(name = "files_of_pack_from_com_banks")
public class FileOfPackFromCombankAdapter {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@Transient
	private File file;
	
	@Column(name = "fileName")
	private String fileName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;


	public File getFile() {
		return file;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}
	
	public FileOfPackFromCombankAdapter() {}
	
	public FileOfPackFromCombankAdapter(File file,
				PackFromCombank pack) {
		this.file = file;
		this.fileName = file.getName();
		this.packFromCombank = pack;
		
	}
	
	@Override
	public String toString() {
		return fileName + " | ";
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
			
		FileOfPackFromCombankAdapter file = (FileOfPackFromCombankAdapter) obj;
		return new EqualsBuilder().append(this.fileName, file.fileName)
								  .append(this.packFromCombank, file.getPackFromCombank())
						   		  .isEquals();
	}
}
