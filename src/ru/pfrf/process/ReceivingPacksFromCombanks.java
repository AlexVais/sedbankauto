package ru.pfrf.process;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.persistence.*;

import org.hibernate.Transaction;

import main.MainFrame;
import main.Utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.omg.CORBA_2_3.portable.OutputStream;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.NumberPackInBank;
import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.ConfirmRead;
import ru.pfrf.entityinfoexchange.DocOfInfoExchange;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.UnknownFile;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.SPRA;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfilename.ParserPackFromCombank;
import ru.pfrf.entityinfoexchange.receipt.ReceiptProcessToBank;

@Entity
@Table(name = "new_receiving_packs_from_combanks")
public class ReceivingPacksFromCombanks extends Process {
	private static final long serialVersionUID = 1L;
	
	
	@Transient
	private Path pathSource;
	
	@Transient
	private Path pathArchive;
	
	@Transient
	private Path pathReceiptsProcessToBank;
	
	@Transient
	private Path pathArchiveReceipts;
	
	/**
	 * ������� ���������� ��� �������� ������ �� ������ ��� ����������� �� �����
	*/
	@Transient
	private Path pathDest;
	
	@Transient
	private Path path7zipExe;
	

	@OneToMany(mappedBy = "receivingPacksFromCombanks", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PackFromCombank> setPacks = new HashSet<>();


	public Path getPathReceiptsProcessToBank() {
		return pathReceiptsProcessToBank;
	}

	public Path getPathDest() {
		return pathDest;
	}

	public Set<PackFromCombank> getSetPacks() {
		return setPacks;
	}

	public void setSetPacks(Set<PackFromCombank> setPacks) {
		this.setPacks = setPacks;
	}

	public ReceivingPacksFromCombanks() {}

	public ReceivingPacksFromCombanks(SessionFactory sessionFactory,
							File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.pathSource = Paths.get(new String(p.getProperty("sourcePathPacksFromCombanks").getBytes("ISO8859-1")));
			this.pathArchive = Paths.get(new String(p.getProperty("archivePathPacksFromCombanks").getBytes("ISO8859-1")));
			this.pathDest = Paths.get(new String(p.getProperty("pathDestPacksFromCombanks").getBytes("ISO8859-1")));
			this.path7zipExe = Paths.get(new String(p.getProperty("path7zipExe").getBytes("ISO8859-1")));
			this.pathReceiptsProcessToBank = Paths.get(new String(p.getProperty("pathDestReceiptsProcessToBank").getBytes("ISO8859-1")));
			this.pathArchiveReceipts = Paths.get(new String(p.getProperty("archivePathReceipts").getBytes("ISO8859-1")));
			
		} catch (IOException e) {
			throw new Exception(e);
		}
	}
	
	private boolean isCorrectName(ParserPackFromCombank parser) {
		return (parser.getNumberPack() != null) ? true : false;
	}
	
	private boolean isPackNull(ParserPackFromCombank parser) {
		return (parser.getFile().length() == 0) ? true : false;
	}
	
	private boolean isPackEmpty(Path path) {
		return (path.toFile().listFiles().length == 0) ? true : false;
	}
	
	private boolean isCorrectStructureArchive(Path path) {
		for(File file : path.toFile().listFiles()) { 
			if (!file.isFile())
				return false;
		}
		return true;
		
	}
	
	private void createOutFileOfIE(Path path, 
						PackFromCombank pack,
						DeliveryOrg deliveryOrg) throws Exception {
		//������ ������ ��������
		File[] files = path.toFile().listFiles();
		
		
		for(File file : files) {
			
			//�������
			FileOfPackFromCombankAdapter fileAdapter = new FileOfPackFromCombankAdapter(file, pack);
			pack.getSetFileOfPackFromCombankAdapter().add(fileAdapter);
			
			ParserFileNameOut parser = new ParserFileNameOut(file, MainFrame.dictNameOrgInFile);
			parser.parse();
			
			//������ ��� �������������
			List<FileOfInfoExchange> listFileOfIE = new ArrayList<>();
			List<ConfirmRead> listConfirmRead = new ArrayList<>();
			
			if (parser.getTypeDoc() != null ) {
				switch (parser.getTypeDoc()) {
				case "OZAC" :
					OZAC ozac = new OZAC();
					ozac.setPackFromCombank(pack);
					pack.getSetOZAC().add(ozac);
					ozac.setFileOfPackFromCombankAdapter(fileAdapter);
					listFileOfIE.add(ozac);
					break;
					
				case "OPVF": 
					OPVF opvf = new OPVF();
					opvf.setPackFromCombank(pack);
					pack.getSetOPVF().add(opvf);
					opvf.setFileOfPackFromCombankAdapter(fileAdapter);
					listFileOfIE.add(opvf);
					break;
					
				case "SPRA":
					SPRA spra = new SPRA();
					spra.setPackFromCombank(pack);
					pack.getSetSPRA().add(spra);
					spra.setFileOfPackFromCombankAdapter(fileAdapter);
					listFileOfIE.add(spra);
					break;
					
				case "POSD":
					DocPOSD docPOSD = new DocPOSD();
					docPOSD.setPackFromCombank(pack);
					pack.getSetPOSD().add(docPOSD);
					docPOSD.setFileOfPackFromCombankAdapter(fileAdapter);
					listConfirmRead.add(docPOSD);
					break;
					
				}
			} else {
				UnknownFile unknownFile = new UnknownFile(deliveryOrg, file, pack);
				pack.setTypeError(MainFrame.dictTypeError.get(TypeError.ERR_PACKCOMBANK_UNKNOWNFILES));
				pack.getSetUnknownFile().add(unknownFile);
			}
			
			//������������� FileOfInfoExchange
			for(FileOfInfoExchange fileOfIE : listFileOfIE) {
				fileOfIE.setFileName(file.getName());
				fileOfIE.setYear(parser.getYear());
				fileOfIE.setNumberPack(Integer.valueOf(parser.getPackNumber()));
				fileOfIE.setDeliveryOrg(parser.getDeliveryOrg());
			}
			
			//������������� ConfirmRead
			for(ConfirmRead confirm : listConfirmRead) {
				confirm.setFileName(file.getName());
				confirm.setYear(parser.getYear());
				confirm.setNumberPack(Integer.valueOf(parser.getPackNumber()));
				confirm.setDeliveryOrg(parser.getDeliveryOrg());
			}
			
		}
	}
	
	private void unpack(File fileArchive, Path pathDest, Path path7zipExe) throws Exception {
		try {
			Utils.unpack(fileArchive, pathDest, ".*");
		} catch (Exception e) {
			Utils.unpack7zip(fileArchive, pathDest, path7zipExe);
		}
	}
	
	private void saveToArchive(PackFromCombank pack) throws Exception {
		
		//�������� ����� �� fileName
		Session session = null;
		Transaction t = null;
		
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(PackFromCombank.class)
										.add(Restrictions.eq("fileName", pack.getFileName()))
										.addOrder(Order.desc("postfix"))
										.setMaxResults(1);
			
			List<PackFromCombank> listPacks = criteria.list();
			int count = listPacks.size();
			if (count > 0) {
				PackFromCombank packFind = listPacks.get(0);
				if (packFind.getPostfix() == null)
					pack.setPostfix(1);
				else
					pack.setPostfix(packFind.getPostfix() + 1);
			}
			t.commit();
		} catch (Exception e) {
			t.rollback();
			e.printStackTrace();
			throw new Exception("������ ������ ������ � ��", e);
		} finally {
			session.close();
		}
		
		try {
			//�������� �������� ��� ���������� ������
			//���� ���� ������ � ������, �� ��������� � ������� � ��������
			SimpleDateFormat formatyyyy = new SimpleDateFormat("yyyy");
			SimpleDateFormat formatM = new SimpleDateFormat("M");
			
			Path pathDest = null;
			if (pack.getTypeError() == null) {
				pathDest = Paths.get(pathArchive + 
						"/" + formatyyyy.format(this.dateBegin) + 
						"/" + formatM.format(this.dateBegin) + 
						"/" + pack.getDeliveryOrg().getShortName());
			} else {
				pathDest = Paths.get(pathArchive + "/� ��������" +
						"/" + formatyyyy.format(this.dateBegin) + 
						"/" + formatM.format(this.dateBegin) + 
						"/" + pack.getDeliveryOrg().getShortName());
			}
				
			
			pathDest.toFile().mkdirs();
			
			//����������� ����� � �����
			File fileDest = new File(pathDest + "/" + pack.getFileName() + (pack.getPostfix() == null ? "" : "_" + pack.getPostfix()));
			Files.copy(pack.getFile().toPath(), fileDest.toPath());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("������ ����������� ����� " + pack.getFile() + " � �����", e);
		}
		
	}
	
	private void moveFilesToPathDest(Path path, PackFromCombank pack) throws IOException {
		//�������� ������ ������ � �������� ����������
		File[] files = path.toFile().listFiles();
		
		for(File file : files) {
			//������� ������� ��� ������� �����
			Path pathDestForFileOfPack = Paths.get(this.pathDest + "/" + file.getName() + (pack.getPostfix() == null ? "" : "_" + pack.getPostfix()));
			pathDestForFileOfPack.toFile().mkdirs();
			
			//����������� ����
			File fileDest = new File(pathDestForFileOfPack + "/" + file.getName());
			Files.copy(file.toPath(), fileDest.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}
	}
	
	private void deleteSourcePack(PackFromCombank pack) throws Exception {
		try {
			Files.delete(pack.getFile().toPath());
		} catch (IOException e) {
			throw new Exception("�� ������� ������� ���� ��������� ������ " + pack.getFile(), e);
		}
	}
		
	
	private void sendReceipt(PackFromCombank pack) throws Exception {
		ReceiptProcessToBank receipt = new ReceiptProcessToBank(pack, 1);
		pack.setReceiptProcessToBank(receipt);
		Path pathDest = Paths.get(pack.getFile().getParent() + "/temp");
		receipt.saveToFileTxt(pathDest);
		
		List<File> listFiles = new ArrayList<>();
		listFiles.add(receipt.getFile());
		
		//���������� ��� ������
		Session session = null;
		Transaction t = null;
		Integer numberPack = NumberPackInBank.lastNumberPack(sessionFactory, pack.getDeliveryOrg()) + 1;
		
		//�������� ������
		String fileName = pack.getDeliveryOrg().getShortName() + 
						  "p" + String.valueOf(numberPack + 10000).substring(1, 5) +
						  "." + pack.getDeliveryOrg().getIndexOfParticipant();
		receipt.setPackNameSend(fileName);
		File fileArchive = new File(this.pathReceiptsProcessToBank + "/" + fileName);
		Utils.compress(listFiles, fileArchive);
		
		//���������� ������ � �����
		SimpleDateFormat formatyyyy = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatM = new SimpleDateFormat("M");
		
		File dir = new File(this.pathArchiveReceipts +
							"/" + pack.getDeliveryOrg().getShortName() + 
						    "/" + formatyyyy.format(this.dateBegin) +
						    "/" + formatM.format(this.dateBegin) + 
						    "/���������");
		dir.mkdirs();
		Utils.compress(listFiles, new File(dir + "/" + receipt.getPackNameSend()));
		
		
		//������ � ��
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			session.save(new NumberPackInBank(pack.getDeliveryOrg(), numberPack));
			t.commit();
			
		} catch (Throwable e) {
			t.rollback();
			throw new  Exception(e);
		}
	}
	
	
	@Override
	protected void exec() throws Exception {
		
		//��������� ������ ��������� � ��. ������������� ������
		File[] dirs = this.pathSource.toFile().listFiles(new FilenameFilter() {
													public boolean accept(File dir, String name) {
														return (new File(dir + "/" + name).isDirectory()) ? true : false;
													}});
		//
		for(File dir : dirs) {
			//��������� ������ ������ 
			File[] files = dir.listFiles(new FilenameFilter() {
									public boolean accept(File dir, String name) {
										File file = new File(dir + "/" + name);
										if (!file.isFile()) return false;
										if (file.isHidden()) return false;
										//�������� ������ blank.txt, ������� �������� �����-�� ���������� �������
										if (name.matches("^blank.*\\.txt$")) {
											file.delete();
											return false;
										} else 
											return true;
									}});
			
			//�������� �������� /temp
			Path pathTemp = Paths.get(dir + "/temp");
			if (!pathTemp.toFile().exists()) 
				pathTemp.toFile().mkdir(); 
			else 
				Utils.clearDir(pathTemp);
			
			
			for(File filePack : files) {
				DeliveryOrg deliveryOrg = MainFrame.dictDeliveryOrgByShortName.get(dir.getName());
				
				//������� ����� ������
				ParserPackFromCombank parser = new ParserPackFromCombank(filePack, deliveryOrg);
				parser.parse();
				
				//�������� ������
				PackFromCombank packFromCombank = new PackFromCombank(parser, this);
				this.setPacks.add(packFromCombank);
				
				//�������� ������
				if (!isCorrectName(parser)) {
					packFromCombank.setTypeError(MainFrame.dictTypeError.get(TypeError.ERR_PACKCOMBANK_PACKNAME));
				} else if (isPackNull(parser)) {
					packFromCombank.setTypeError(MainFrame.dictTypeError.get(TypeError.ERR_PACKCOMBANK_NULLSIZE));
				}
				
				Path pathDestForFileOfPack = null;
				if (packFromCombank.getTypeError() == null) {
					//���������� ������ � ������� temp/��� ������
					pathDestForFileOfPack = Paths.get(pathTemp + "/" + parser.getFile().getName());
					pathDestForFileOfPack.toFile().mkdir();
					this.unpack(parser.getFile(), pathDestForFileOfPack, path7zipExe);
					
					//�������� ������
					if (isPackEmpty(pathDestForFileOfPack)) {
						packFromCombank.setTypeError(MainFrame.dictTypeError.get(TypeError.ERR_PACKCOMBANK_EMPTY));
					} else if (!isCorrectStructureArchive(pathDestForFileOfPack)) {
						packFromCombank.setTypeError(MainFrame.dictTypeError.get(TypeError.ERR_PACKCOMBANK_STRUCT_ARCHIVE));
					}
					
				}
				
				//�������� ��������� �������� ������, ������������ � ������
				if (packFromCombank.getTypeError() == null) {
					createOutFileOfIE(pathDestForFileOfPack, packFromCombank, deliveryOrg);
				}
			
				//���������� ������ � ������ ��
				this.saveToArchive(packFromCombank);
				
				//�������� ������ � ������� ����� ���. ������
				if (packFromCombank.getTypeError() == null) {
					this.moveFilesToPathDest(pathDestForFileOfPack, packFromCombank);
				}
				
				//�������� �������� �������
				this.deleteSourcePack(packFromCombank);
				
			}
		}
		
		//�������� ��������� � ����� ������ � ���������
		for(PackFromCombank pack : setPacks) {
			this.sendReceipt(pack);
		}
		
	}

}
