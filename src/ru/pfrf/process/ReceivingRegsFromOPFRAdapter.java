package ru.pfrf.process;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.register.RegisterTFFromOPFR;

@Entity
@Table(name = "receivings_regs_from_opfr")
public class ReceivingRegsFromOPFRAdapter extends ReceivingAdapter{
	
	@OneToMany(mappedBy = "receivingRegsTFFromOPFRAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<RegisterTFFromOPFR> setRegistersTFFromOPFR = new HashSet<>();

	public Set<RegisterTFFromOPFR> getSetRegistersTFFromOPFR() {
		return setRegistersTFFromOPFR;
	}

	public void setSetRegistersTFFromOPFR(Set<RegisterTFFromOPFR> setRegistersTFFromOPFR) {
		this.setRegistersTFFromOPFR = setRegistersTFFromOPFR;
	}
	
	public ReceivingRegsFromOPFRAdapter() {}
	
	public ReceivingRegsFromOPFRAdapter(ProcessAdapter processAdapter) {
		super(processAdapter);
	}
}
