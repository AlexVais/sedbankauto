package ru.pfrf.process;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Transient;

import main.MainFrame;
import main.Utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.OzacReceivError;
import ru.pfrf.entityinfoexchange.order.ozac.SPRA;
import ru.pfrf.entityinfoexchange.order.vozv.MergedVOZV;
import ru.pfrf.entityinfoexchange.order.vozv.ONVZ;
import ru.pfrf.entityinfoexchange.order.vozv.OPVO;
import ru.pfrf.entityinfoexchange.order.vozv.OVZR;
import ru.pfrf.entityinfoexchange.order.vozv.POSO;
import ru.pfrf.entityinfoexchange.order.vozv.VOZVOut;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserONVZ;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVF;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVO;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOVZR;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSO;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserSPRA;

public class ReceivingVOZVOut extends Process {
	private static final long serialVersionUID = 1L;
	
	@Transient 
	private Path sourcePathVOZVOutSber;
	
	@Transient Path pathSourceCombanks;
	
	@Transient
	private Path archivePathVOZVOut;
	
	@Transient
	private List<OPVO> listOPVO = new ArrayList<>();
	
	public ReceivingVOZVOut(SessionFactory sessionFactory, File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.sourcePathVOZVOutSber = Paths.get(new String(p.getProperty("sourcePathVOZVOutSber").getBytes("ISO8859-1")));
			this.archivePathVOZVOut = Paths.get(new String(p.getProperty("archivePathVOZVOut").getBytes("ISO8859-1")));
			this.pathSourceCombanks = Paths.get(new String(p.getProperty("sourceVOZVOutCombanks").getBytes("ISO8859-1")));
			
		} catch (IOException e) {
			throw new Exception(e);
		}
	}

	
	
	
	private void prepare() throws Exception {
		List<File> listFilesOPVO = new ArrayList<>();
		
		//�������� ������ ������ OPVO �� ���������
		File[] listOPVOBySber = this.sourcePathVOZVOutSber.toFile().listFiles(new FileFilter() {
														public boolean accept(File pathname) {
															if (!pathname.isFile()) return false;
															if (pathname.isHidden()) return false;
															return pathname.getName().matches("^OUT.+DOC-OPVO.+XML$");
														}});
	
		listFilesOPVO.addAll(Arrays.asList(listOPVOBySber));
		
		//�������� ������ ������ OPVO �� ���.������
		//�������� ������ ���������, � ������� � ����� ����� _ID***
		File[] dirs = this.pathSourceCombanks.toFile().listFiles(new FileFilter() {
												public boolean accept(File pathname) {
													if (!pathname.isDirectory()) return false;
													if (pathname.isHidden()) return false;
													return (pathname.getName().matches("^OUT.+DOC-OPVO.+XML_ID.+$")) ? true : false;
												}});
		
		//�������� ������� ������ OPVO �� ���������
		for (File dir : dirs) {
			File[] tempListOPVOByCombanks = dir.listFiles();
			if (tempListOPVOByCombanks.length != 1)
				throw new Exception("������ ��� ��������� ������ ������ OPVO �� ���. ������");
			listFilesOPVO.add(tempListOPVOByCombanks[0]);
		}
		
		//����������� ������ OPVO � ������� ������ ���. ������/temp/tempVozvOut
		List<File> filesOPVOTemp = new ArrayList<>();
		Path pathTempVozvOut = Paths.get(this.pathSourceCombanks + "/temp/tempVozvOut");
		if (!listFilesOPVO.isEmpty()) {
			//�������� �������� tempVozvOut ��� ������������� ��������� OPVO
			if (pathTempVozvOut.toFile().exists()) {
				Utils.clearDir(pathTempVozvOut);
			} else
				pathTempVozvOut.toFile().mkdirs();
	
			//����������� ������ � ������� ������ ���. ������/temp/tempVozvOut
			for(File file : listFilesOPVO) {
				File fileDest = new File(pathTempVozvOut + "/" + file.getName());
				Files.copy(file.toPath(), fileDest.toPath());
				filesOPVOTemp.add(fileDest);
			}
		}
		
		//���� ���
		for(File file : filesOPVOTemp) {
			FileOfInfoExchange.deleteDigitalSignature(file);
		}
		
		
		//�������� ���������� VOZVOut
		List<OPVO> listOPVO = new ArrayList<>();
		Session session = null;
		Transaction t = null;
		for (File file : filesOPVOTemp) {
			try {
			
				//�������� �������� OPVO
				OPVO opvo = null;
				ParserOPVO parserOPVO = new ParserOPVO();
				parserOPVO.parse(file);
				
				//�������� POSO, ���������������� OPVO
				POSO findPOSO = null;
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					Criteria criteria = session.createCriteria(POSO.class)
												.add(Restrictions.eq("decision", true))
												.add(Restrictions.eq("year", parserOPVO.getYear()))
												.add(Restrictions.eq("numberPack", parserOPVO.getPackNumber()))
												.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					
					List<POSO> listPOSO = criteria.list();
					if (listPOSO.size() > 1) 
						throw new Exception("����� ������� � POSO ����� 1. ����� ������: " + parserOPVO.getPackNumber());
					
					if (!listPOSO.isEmpty())
						findPOSO = listPOSO.get(0);
		
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					e.printStackTrace();
					throw new Exception("������ ��� ������� POSO", e);
				} finally {
					session.close();
				}
				if (findPOSO == null) continue;
					
				
				VOZVOut vozvOut = new VOZVOut();
				vozvOut.setPoso(findPOSO);
				
				//��� ���������
				//������� OPVO �� ��
				OPVO findOPVO = null;
				if (!"8636".equals(findPOSO.getDeliveryOrg().getShortName())) {
					try {
						session = sessionFactory.openSession();
						t = session.beginTransaction();
						
						Criteria criteria = session.createCriteria(OPVO.class)
													.add(Restrictions.eq("fileName", parserOPVO.getFile().getName()))
													.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
						
						List<OPVO> listFindOPVO = criteria.list();
						if (listFindOPVO.size() > 1) 
							throw new Exception("����� ������� � ������ ����� OPVO ����� 1: " + parserOPVO.getFile().getName());
						
						if (listFindOPVO.isEmpty())
							throw new Exception("�� ������� ������ � ������ ����� OPVO: " + parserOPVO.getFile().getName());
						
						findOPVO = listFindOPVO.get(0);
						
						t.commit();
					} catch (Exception e) {
						t.rollback();
						e.printStackTrace();
						throw new Exception("������ ��� ������� OPVO", e);
					} finally {
						session.close();
					}
					
					opvo = findOPVO;	
					opvo.setNamesMainFile(parserOPVO.getNamesMainFile());
					opvo.setMonth(parserOPVO.getMonth());
					opvo.setFile(parserOPVO.getFile());
					
				} else {
					opvo = new OPVO(parserOPVO, vozvOut);
				}
				
				vozvOut.setOpvo(opvo);
				
				
				//��������. ���������� �� ������� ����� DocOZAC
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					Criteria criteria = session.createCriteria(VOZVOut.class, "vozvOut")
												.createCriteria("vozvOut.poso", "poso", JoinType.INNER_JOIN)
												.add(Restrictions.eq("numberPack", opvo.getNumberPack()))
												.add(Restrictions.eq("year", opvo.getYear()))
												.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					
					List<VOZVOut> listFindVOZVOut = criteria.list();
					if (listFindVOZVOut.size() != 0) 
						throw new Exception("VOZVOut ��� ����� ��� ������� ������. ����� ������: " + opvo.getNumberPack());
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					e.printStackTrace();
					throw new Exception("������ ��� ������� VOZVOut", e);
				} finally {
					session.close();
				}
		
				
				//�������� �������� OVZR, ONVZ �������� �����
				//��������� ���� ������, �������� � �����
				String fileNameOVZR = null;
				String fileNameONVZ = null;
				
				for (String fileNameInOPVO : opvo.getNamesMainFile()) {
					if (fileNameInOPVO.matches(".+OVZR.+")) fileNameOVZR = fileNameInOPVO;
					if (fileNameInOPVO.matches(".+ONVZ.+")) fileNameONVZ = fileNameInOPVO;
				}
				
				

				File fileOVZR = null;
				final Integer yearPay = opvo.getYear();
				final String strNumberPack = String.valueOf(opvo.getNumberPack() + 100000).substring(1, 6);
				
				if (fileNameOVZR != null) {
					//��������� ������ ���������, ���������� � ����� OVZR � ����� �����. ������ (��� ���. ������)
					if (!opvo.getDeliveryOrg().getShortName().equals("8636")) {
						/*File[] dirsOZAC = this.pathSourceCombanks.toFile().listFiles(new FileFilter() {
																			public boolean accept(File pathname) {
																				if (pathname.isHidden()) return false;
																				if (!pathname.isDirectory()) return false;
																				return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-OZAC.+?(XML|xml)_ID.+$")) ? true : false;
																			}});
						if (dirsOZAC.length == 0) continue;
						if (dirsOZAC.length > 1) 
							throw new Exception("� �������� ����� ������ OZAC �� ���. ������ ���������� ����� ������ ����� OZAC, ��������������� ������ ������: " + opvf.getNumberPack());
			
						fileOZAC = new File(dirsOZAC[0] + "/" + fileNameOZAC);*/
						throw new Exception("not realized");
						
					} else {
						File[] filesOVZR = this.sourcePathVOZVOutSber.toFile().listFiles(new FileFilter() {
																			public boolean accept(File pathname) {
																				if (pathname.isHidden()) return false;
																				if (!pathname.isFile()) return false;
																				return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-OVZR.+?XML$")) ? true : false;
						
																			}});
						if (filesOVZR.length == 0) continue;
						if (filesOVZR.length > 1)
							throw new Exception("� �������� ����� ������ OVZR �� ��������� ���������� ����� ������ ����� OVZR, ��������������� ������ ������: " + opvo.getNumberPack());
						
						fileOVZR = filesOVZR[0];
					}
					
					File fileTempOVZR = new File(pathTempVozvOut + "/" + fileOVZR.getName());
					
					//����������� ����� OVZR � ������� ������ ���. ������/temp/temp
					Files.copy(fileOVZR.toPath(), fileTempOVZR.toPath());
					
					//�ڸ� ���
					FileOfInfoExchange.deleteDigitalSignature(fileTempOVZR);
					
					//�������� ������� OVZR
					OVZR ovzr = null;
					ParserOVZR parserOVZR = new ParserOVZR();
					parserOVZR.parse(fileTempOVZR);
					
					//��������� OVZR �� �� �� filename
					if (!"8636".equals(opvo.getDeliveryOrg().getShortName())) {
						throw new Exception("not realized");
						/*OVZR findOVZR = null;
						try {
							session = sessionFactory.openSession();
							t = session.beginTransaction();
							
							Criteria criteria = session.createCriteria(OVZR.class)
														.add(Restrictions.eq("fileName", parserOVZR.getFile().getName()))
														.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
							
							List<OVZR> listFindOVZR = criteria.list();
							if (listFindOVZR.size() > 1) 
								throw new Exception("����� ������� � ������ ����� OVZR ����� 1: " + parserOVZR.getFile().getName());
							
							if (listFindOVZR.isEmpty())
								throw new Exception("�� ������� ������ � ������ ����� OVZR: " + parserOVZR.getFile().getName());
							
							findOVZR = listFindOVZR.get(0);
							
							t.commit();
						} catch (Exception e) {
							t.rollback();
							e.printStackTrace();
							throw new Exception("������ ��� ������� OVZR", e);
						} finally {
							session.close();
						}
						
						ovzr = findOVZR;
						//ovzr.initByParser(parserOVZR);
						ovzr.init();*/
						
					} else {
						ovzr = new OVZR(parserOVZR);
						ovzr.init();
					}
					
					ovzr.setVozvOut(vozvOut);
					vozvOut.setOvzr(ovzr);
				}
				
				
				File fileONVZ = null;
				
				if (fileNameONVZ != null) {
					//��������� ������ ���������, ���������� � ����� OVZR � ����� �����. ������ (��� ���. ������)
					if (!opvo.getDeliveryOrg().getShortName().equals("8636")) {
						/*File[] dirsOZAC = this.pathSourceCombanks.toFile().listFiles(new FileFilter() {
																			public boolean accept(File pathname) {
																				if (pathname.isHidden()) return false;
																				if (!pathname.isDirectory()) return false;
																				return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-OZAC.+?(XML|xml)_ID.+$")) ? true : false;
																			}});
						if (dirsOZAC.length == 0) continue;
						if (dirsOZAC.length > 1) 
							throw new Exception("� �������� ����� ������ OZAC �� ���. ������ ���������� ����� ������ ����� OZAC, ��������������� ������ ������: " + opvf.getNumberPack());
			
						fileOZAC = new File(dirsOZAC[0] + "/" + fileNameOZAC);*/
						throw new Exception("not realized");
						
					} else {
						File[] filesONVZ = this.sourcePathVOZVOutSber.toFile().listFiles(new FileFilter() {
																			public boolean accept(File pathname) {
																				if (pathname.isHidden()) return false;
																				if (!pathname.isFile()) return false;
																				return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-ONVZ.+?XML$")) ? true : false;
						
																			}});
						if (filesONVZ.length == 0) continue;
						if (filesONVZ.length > 1)
							throw new Exception("� �������� ����� ������ OVZR �� ��������� ���������� ����� ������ ����� ONVZ, ��������������� ������ ������: " + opvo.getNumberPack());
						
						fileONVZ = filesONVZ[0];
					}
					
					File fileTempONVZ = new File(pathTempVozvOut + "/" + fileONVZ.getName());
					
					//����������� ����� OVZR � ������� ������ ���. ������/temp/temp
					Files.copy(fileONVZ.toPath(), fileTempONVZ.toPath());
					
					//�ڸ� ���
					FileOfInfoExchange.deleteDigitalSignature(fileTempONVZ);
					
					//�������� ������� ONVZ
					ONVZ onvz = null;
					ParserONVZ parserONVZ = new ParserONVZ();
					parserONVZ.parse(fileTempONVZ);
					
					//��������� ONVZ �� �� �� filename
					if (!"8636".equals(opvo.getDeliveryOrg().getShortName())) {
						throw new Exception("not realized");
						/*OVZR findOVZR = null;
						try {
							session = sessionFactory.openSession();
							t = session.beginTransaction();
							
							Criteria criteria = session.createCriteria(OVZR.class)
														.add(Restrictions.eq("fileName", parserOVZR.getFile().getName()))
														.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
							
							List<OVZR> listFindOVZR = criteria.list();
							if (listFindOVZR.size() > 1) 
								throw new Exception("����� ������� � ������ ����� OVZR ����� 1: " + parserOVZR.getFile().getName());
							
							if (listFindOVZR.isEmpty())
								throw new Exception("�� ������� ������ � ������ ����� OVZR: " + parserOVZR.getFile().getName());
							
							findOVZR = listFindOVZR.get(0);
							
							t.commit();
						} catch (Exception e) {
							t.rollback();
							e.printStackTrace();
							throw new Exception("������ ��� ������� OVZR", e);
						} finally {
							session.close();
						}
						
						ovzr = findOVZR;
						//ovzr.initByParser(parserOVZR);
						ovzr.init();*/
						
					} else {
						//onvz = new OVZR(parserONVZ);
						//onvz.init();
					}
					
					onvz.setVozvOut(vozvOut);
					vozvOut.setOnvz(onvz);
				}
					
				
				/*
				//�������� ������� SPRA
				if (fileNameSPRA != null) {
					
					if ("8636".equals(opvf.getDeliveryOrg().getShortName())) {
						File[] filesSPRA = this.pathSourceSber.toFile().listFiles(new FileFilter() {
							public boolean accept(File pathname) {
								if (pathname.isHidden()) return false;
								if (!pathname.isFile()) return false;
								return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-SPRA.+?XML$")) ? true : false;
			
							}});
						if (filesSPRA.length == 0) continue;
						if (filesSPRA.length > 1)
						throw new Exception("� �������� ����� ������ OZAC �� ��������� ���������� ����� ������ ����� SPRA, ��������������� ������ ������: " + opvf.getNumberPack());
			
						File fileSPRA = filesSPRA[0];
						
						//����������� ����� SPRA � ������� ������ ���. ������/temp/tempOZAC
						File fileTempSPRA = new File(pathTempOZAC + "/" + fileSPRA.getName());
						Files.copy(fileSPRA.toPath(), fileTempSPRA.toPath());
						
						//�ڸ� ���
						FileOfInfoExchange.deleteDigitalSignature(fileTempSPRA);
						
						//�������� ������� SPRA
						ParserSPRA parserSPRA = new ParserSPRA();
						parserSPRA.parse(fileTempSPRA);
						SPRA spra = new SPRA(parserSPRA, null, docOZAC);
						spra.setDocOZAC(docOZAC);
						docOZAC.setSpra(spra);
						
					} else {
						//��������� SPRA �� �� �� filename
						SPRA findSPRA = null;
						SPRA spra = null;
						try {
							session = sessionFactory.openSession();
							t = session.beginTransaction();
						
							
							Criteria criteria = session.createCriteria(SPRA.class)
														.add(Restrictions.eq("fileName", fileNameSPRA))
														.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
							
							List<SPRA> listFindSPRA = criteria.list();
							if (listFindSPRA.size() > 1) 
								throw new Exception("����� ������� � ������ ����� SPRA ����� 1: " + fileNameSPRA);
							
							if (listFindSPRA.isEmpty())
								throw new Exception("�� ������� ������ � ������ ����� SPRA: " + fileNameSPRA);
							
							findSPRA = listFindSPRA.get(0);
							spra = findSPRA;
							
							t.commit();
						} catch (Exception e) {
							t.rollback();
							e.printStackTrace();
							throw new Exception("������ ��� ������� OZAC", e);
						} finally {
							session.close();
						}
						
						//����������� ����� SPRA � ������� ������ ���. ������/temp/tempOZAC
						File fileTempSPRA   = new File(pathTempOZAC + "/" + findSPRA.getFileName());
						File fileSourceSPRA = new File(this.pathSourceCombanks.toFile() + "/" + findSPRA.getFileName() + "_ID" + findSPRA.getFileOfPackFromCombankAdapter().getId() + "/" + findSPRA.getFileName());
						Files.copy(fileSourceSPRA.toPath(), fileTempSPRA.toPath());
				
						//������������� SPRA
						ParserSPRA parserSPRA = new ParserSPRA();
						parserSPRA.parse(fileTempSPRA);
						spra.setMonth(parserSPRA.getMonth());
						spra.setIsRead(true);
						spra.setOutNumber(parserSPRA.getOutNumber());
						spra.setDocOZAC(docOZAC);
						docOZAC.setSpra(spra);
					}
				}
				
				this.setDocOZAC.add(docOZAC);
				docOZAC.setReceivingOZAC(this);*/
				
			} catch (Exception e) {
				ParserFileNameOut parserFileNameOut = new ParserFileNameOut(file, MainFrame.dictNameOrgInFile);
				parserFileNameOut.parse();
//				this.ozacReceivError = new OzacReceivError(parserFileNameOut.getYear(), 
//												Integer.valueOf(parserFileNameOut.getPackNumber()));
//				
//				throw new Exception("������ ��� ���������� � ����� OZAC. �����: " + ozacReceivError, e);
			} 
		}
		
	}
	
	
	@Override
	protected void exec() throws Exception {
		this.prepare();
		
	}

}
