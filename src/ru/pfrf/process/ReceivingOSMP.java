package ru.pfrf.process;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static java.nio.file.StandardCopyOption.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.persistence.*;

import main.MainFrame;
import main.Utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.DictDeliveryOrg;
import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.osmp.DocPOOS;
import ru.pfrf.entityinfoexchange.order.osmp.OPVZ;
import ru.pfrf.entityinfoexchange.order.osmp.OSMP;
import ru.pfrf.entityinfoexchange.order.osmp.OSMPByArea;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIEOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserMainFileOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVZ;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOSMP;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserRegisterOut;

@Entity
@Table(name = "receivings_osmp")
public class ReceivingOSMP extends Process{
	private static final long serialVersionUID = 1L;
	
	public static final String ERROR_PACK_REPEATED = "������. ��������� ���� ������";
	
	@Transient
	private final int BUFFER = 2048;
	
	@Transient
	private Path sourcePath;
	
	@Transient
	private Path sourcePathComBank;
	
	@Transient 
	private Path pathArchive;
	
	@Transient
	private Path pathOsmpByAreaForNVP;
	
	@Transient
	private Path pathOSMPForSignature;
	
	
	@OneToMany(mappedBy = "receivingOSMP", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public List<DocOSMP> listDocOSMP = new ArrayList<>();
	
	public List<DocOSMP> getListDocOSMP() {
		return listDocOSMP;
	}

	public void setListDocOSMP(List<DocOSMP> listDocOSMP) {
		this.listDocOSMP = listDocOSMP;
	}

	public Path getSourcePath() {
		return sourcePath;
	}

	public void setSourcePathComBank(Path sourcePathCombank) {
		this.sourcePath = sourcePath;
	}

	public Path getSourcePathComBank() {
		return sourcePathComBank;
	}

	public void setSourcePath(Path sourcePath) {
		this.sourcePath = sourcePath;
	}
	
	public ReceivingOSMP() {}
	
	public ReceivingOSMP(SessionFactory sessionFactory,
					File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.sourcePath = Paths.get(new String(p.getProperty("sourcePathOSMP").getBytes("ISO8859-1")));
			this.sourcePathComBank = Paths.get(new String(p.getProperty("sourcePathOSMPCombanks").getBytes("ISO8859-1")));			
			this.pathArchive = Paths.get(new String(p.getProperty("archivePathOSMP").getBytes("ISO8859-1")));
			this.pathOsmpByAreaForNVP = Paths.get(new String(p.getProperty("osmpByAreaForNVP").getBytes("ISO8859-1")));
			this.pathOSMPForSignature = Paths.get(new String(p.getProperty("pathOSMPForSignature").getBytes("ISO8859-1")));
			
		} catch (IOException e) {
			throw new Exception(e);
		}
	}

	/**
	 * ����������� ����� � �������� ��� �������� � ���
	 * @throws IOException 
	 */
	public void copyFilesForNVP(OSMP osmp) throws IOException {
		for (OSMPByArea osmpByArea : osmp.getListOsmpByArea()) {
			String areaStr = String.valueOf(osmpByArea.getArea() + 100);
			Path dest = Paths.get(this.pathOsmpByAreaForNVP + "/" + areaStr  + "/BANK/" + osmpByArea.getFileName());
			
			Files.copy(osmpByArea.getFile().toPath(), 
					dest, 
					StandardCopyOption.REPLACE_EXISTING);
		}
	}
	
	
	@Override
	protected void exec() throws Exception {
		//������������ ��������, � ������� ������������� ������� �� ����
		//if (!Files.exists(sourcePath)) {
		//	throw new Exception("������� " + sourcePath.toString() + " �� ����������");
		//}
		
		//File[] files = new File(sourcePath.toString()).listFiles();
		
		//�������� � �������� ���������� ��������
		//Path tempPath = Paths.get(this.pathArchive + "/temp/tempOSMP");
		//if (tempPath.toFile().exists())
		//	Utils.clearDir(tempPath);
		//else
		//	tempPath.toFile().mkdirs();
		
		List<File> listFilesOPVZ = new ArrayList<>();
		
		//�������� ������ ������ OPVF �� ���������
		File[] listOPVZBySber = this.sourcePath.toFile().listFiles(new FileFilter() {
														public boolean accept(File pathname) {
															if (!pathname.isFile()) return false;
															if (pathname.isHidden()) return false;
															return (pathname.getName().matches("^OUT.+DOC-OPVZ.+XML$")) ? true : false;
														}});
	
		listFilesOPVZ.addAll(Arrays.asList(listOPVZBySber));
		
		//�������� ������ ������ OPVZ �� ���.������
		//�������� ������ ���������, � ������� � ����� ����� _ID***
		File[] dirs = this.sourcePathComBank.toFile().listFiles(new FileFilter() {
												public boolean accept(File pathname) {
													if (!pathname.isDirectory()) return false;
													if (pathname.isHidden()) return false;
													return (pathname.getName().matches("^OUT.+DOC-OPVZ.+XML_ID.+$")) ? true : false;
												}});
		
		//�������� ������� ������ OPVZ �� ���������
		for (File dir : dirs) {
			File[] tempListOPVZByCombanks = dir.listFiles();
			if (tempListOPVZByCombanks.length != 1)
				throw new Exception("������ ��� ��������� ������ ������ OPVZ �� ���. ������");
			listFilesOPVZ.add(tempListOPVZByCombanks[0]);
		}
		
		//����������� ������ OPVZ � ������� ������ ���. ������/temp/tempOSMP
		List<File> filesOPVZTemp = new ArrayList<>();
		Path pathTempOSMP = Paths.get(this.sourcePathComBank + "/temp/tempOSMP");
		if (!listFilesOPVZ.isEmpty()) {
			//�������� �������� tempOSMP ��� ������������� ��������� OPVF
			if (pathTempOSMP.toFile().exists()) {
				Utils.clearDir(pathTempOSMP);
			} else
				pathTempOSMP.toFile().mkdirs();
	
			//����������� ������ � ������� ������ ���. ������/temp/tempOSMP
			for(File file : listFilesOPVZ) {
				File fileDest = new File(pathTempOSMP + "/" + file.getName());
				Files.copy(file.toPath(), fileDest.toPath());
				filesOPVZTemp.add(fileDest);
			}
		}
		
		//���� ���
		for(File file : filesOPVZTemp) {
			FileOfInfoExchange.deleteDigitalSignature(file);
		}		
		
		for (File fileOpvz : filesOPVZTemp) {
			//if (fileOpvz.isFile()) {	
				//DictDeliveryOrg dict = new DictDeliveryOrg();
				//dict.init(sessionFactory);
				//DeliveryOrg deliveryOrg = dict.get(DeliveryOrg.ORG_SBER);
				
				ParserFileNameOut parserFileName = new ParserFileNameOut(fileOpvz, MainFrame.dictNameOrgInFile);
				parserFileName.parse();
				if ("OPVZ".equals(parserFileName.getTypeDoc())) {
					
					//����������� ����� OPVZ � ������� /temp
					//File fileOPVZInTemp = new File(tempPath + "/" + fileOpvz.getName());
					//Files.copy(fileOpvz.toPath(), fileOPVZInTemp.toPath(), StandardCopyOption.REPLACE_EXISTING);
					
					//�������� ��� � OPVZ
					//FileOfInfoExchange.deleteDigitalSignature(fileOPVZInTemp);
					ParserOPVZ parserOPVZ = new ParserOPVZ();
					parserOPVZ.parse(fileOpvz);
					ParserFileNameOut parserFileNameOPVZ = new ParserFileNameOut(fileOpvz, MainFrame.dictNameOrgInFile);
					parserFileNameOPVZ.parse();
					
					//��������� ���� ����� ����� vipnet
					Date dateOfVipnet = new Date(fileOpvz.lastModified());
					
					//����������� ������ ������. ������-�� � ������ OSMP ��������� ����� ������ ������� �� ������ ����� DCK
					Integer packNumber = null;
					Integer packNumberPart1 = null;
					Integer packNumberPart2 = null;
					if (("8636".equals(parserFileName.getDeliveryOrg().getShortName())) || ("RSH".equals(parserFileName.getDeliveryOrg().getShortName()))) {
						packNumberPart1 = Integer.valueOf(parserFileName.getPackNumber());
						packNumberPart2 = Integer.valueOf(parserFileName.getPart2PackNumber());
						packNumber = packNumberPart1 * 1000 + packNumberPart2;
					
					} else {
						//throw new Exception("����������� ����� ������. ��� ���. ������ ���� ������ OSMP �� ����������");
					}
					
					DocOSMP docOSMP = new DocOSMP(parserFileNameOPVZ.getYear(),
							parserOPVZ.getMonth(), 
							parserFileName.getDeliveryOrg(), 
							packNumber,
							null,
							this);
					
					this.listDocOSMP.add(docOSMP); 
					
					OPVZ opvz = new OPVZ(parserFileNameOPVZ.getYear(), 
							parserOPVZ.getMonth(), 
							parserFileName.getDeliveryOrg(), 
							packNumber,
							fileOpvz, 
							parserOPVZ.getRootElement(),
							parserFileNameOPVZ.getOutNumber(),
							parserOPVZ.getIsRead(),
							parserOPVZ.getResultOfRead(),
							parserOPVZ.getNamesMainFile(),
							null,
							docOSMP,
							dateOfVipnet);
					
					docOSMP.setOpvz(opvz);					
					File fileOSMP = null;
					String fileNameOSMP = null;
					
					for (String fileNameInOPVZ : opvz.getNamesMainFile()) {
						if (fileNameInOPVZ.matches(".+OSMP.+")) fileNameOSMP = fileNameInOPVZ;
					}
				
					final Integer yearPay = opvz.getYear();
					final String strNumberPack = String.valueOf(opvz.getNumberPack() + 1000).substring(1, 4);
				
					if (!opvz.getDeliveryOrg().getShortName().equals("8636")) {
						File[] dirsOSMP = this.sourcePathComBank.toFile().listFiles(new FileFilter() {
																			public boolean accept(File pathname) {
																				if (pathname.isHidden()) return false;
																				if (!pathname.isDirectory()) return false;
																				return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-.+?" + strNumberPack + ".+?DOC-OSMP.+?(XML|xml)_ID.+$")) ? true : false;
																			}});
						if (dirsOSMP.length == 0) continue;
						if (dirsOSMP.length > 1) 
							throw new Exception("� �������� ����� ������ OSMP �� ���. ������ ���������� ����� ������ ����� OSMP, ��������������� ������ ������: " + opvz.getNumberPack());
			
						fileOSMP = new File(dirsOSMP[0] + "/" + fileNameOSMP);
						
					} else {
						File[] filesOSMP = this.sourcePath.toFile().listFiles(new FileFilter() {
																			public boolean accept(File pathname) {
																				if (pathname.isHidden()) return false;
																				if (!pathname.isFile()) return false;
																				return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-OSMP.+?XML$")) ? true : false;
						
																			}});
						if (filesOSMP.length == 0) continue;
						if (filesOSMP.length > 1)
							throw new Exception("� �������� ����� ������ OSMP �� ��������� ���������� ����� ������ ����� OSMP, ��������������� ������ ������: " + opvz.getNumberPack());
						
						fileOSMP = filesOSMP[0];
					}
								

					//if (parserOPVZ.getResultOfRead() == null) {
					//	fileOSMP = new File(this.sourcePath + "/" + parserOPVZ.getNamesMainFile().get(0));
					//	if (fileOSMP.exists()) {	
							
							
							//����������� ����� OSMP � ������� /temp
							//File fileOSMPInTemp = new File(tempPath + "/" + fileOSMP.getName());
							//Files.copy(fileOSMP.toPath(), fileOSMPInTemp.toPath(), StandardCopyOption.REPLACE_EXISTING);
							
							//�������� ���
							//FileOfInfoExchange.deleteDigitalSignature(fileOSMPInTemp);
				
					File fileTempOSMP = new File(pathTempOSMP + "/" + fileOSMP.getName());
					
					//����������� ����� OSMP � ������� ������ ���. ������/temp/tempOZAC
					Files.copy(fileOSMP.toPath(), fileTempOSMP.toPath());
					
					//�ڸ� ���
					FileOfInfoExchange.deleteDigitalSignature(fileTempOSMP);
					
					//�������� ������� OSMP
					OSMP osmp = null;
					ParserOSMP parserOSMP = new ParserOSMP();
					parserOSMP.parse(fileTempOSMP);
					ParserFileNameOut parserFileNameOSMP = new ParserFileNameOut(fileOSMP, MainFrame.dictNameOrgInFile);
					parserFileNameOSMP.parse();
							
									
					osmp = new OSMP(parserFileNameOSMP.getYear(), 
												parserOSMP.getMonth(),
												parserFileName.getDeliveryOrg(), 
												packNumber,
												fileTempOSMP, 
												parserOSMP.getRootElement(),
												parserFileNameOSMP.getOutNumber(),
												parserOSMP.getIsRead(),
												parserOSMP.getResultOfRead(),
												null,
												docOSMP,
												dateOfVipnet);
			
					//���� �� ���� ������ ��� ������, ��
					if (parserOSMP.getResultOfRead() == null) 
						osmp.init();
							
							
					docOSMP.getListOSMP().add(osmp);
					//	}	
						
					//}
					
					//�������� docPOOS
					//���������, �� ��� �� ����� ������� ������ OSMP
				
					Session session = null;
					Transaction t = null;
					boolean isReceived = false;
					try {
						session = sessionFactory.openSession();
						t = session.beginTransaction();
						
						Criteria criteria = session.createCriteria(DocPOOS.class, "poos")
													.createCriteria("poos.docOSMP", "docOSMP", JoinType.INNER_JOIN)
													.add(Restrictions.eq("poos.year", docOSMP.getYear()))
													.add(Restrictions.eq("poos.numberPack", docOSMP.getNumberPack()))
													.add(Restrictions.eq("poos.decision", true))
													.add(Restrictions.eq("docOSMP.isCancelled", false))
													.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
						
						List<DocPOOS> listPOOS = criteria.list();
						if (listPOOS.size() > 0) isReceived = true;
						
						t.commit();
					} catch (Exception e) {
						t.rollback();
						throw new Exception("������ ��� ������� DocPOOS", e);
					} finally {
						session.close();
					}
					
					
					//���� ����� ��� ����� ������� ������, �� ���������� ��������� �������� osmp � opvz "������. ��������� ���� ������"
					if (isReceived) {
						docOSMP.setTypeError(MainFrame.dictTypeError.get(TypeError.ERR_OSMP_REPEAT));
						
						opvz.setResultOfRead(ERROR_PACK_REPEATED);
						if (osmp != null)
							osmp.setResultOfRead(ERROR_PACK_REPEATED);
					}
					
					
					File filePOOS = new File(this.pathArchive + "/template_POOS.XML");
					DocPOOS docPOOS = new DocPOOS(filePOOS, docOSMP, null, false);
					docPOOS.createFilePOOS(Paths.get(this.sourcePathComBank+ "/temp/tempOSMP"), filePOOS);
					
					docOSMP.setDocPOOS(docPOOS);
					
					if (!docOSMP.getListOSMP().isEmpty())
						osmp = docOSMP.getListOSMP().iterator().next();
					
					//���������� �������
					if (osmp != null) {
						osmp.separation(Paths.get(this.sourcePathComBank+ "/temp/tempOSMP"), isReceived);		
						//����������� ���������� ����� � ������� ��� �������� ���
						if (!isReceived) 
							copyFilesForNVP(osmp);
					}
					
					//���������� � �����
					this.toArchive(docOSMP, this.sourcePathComBank);
					
					//����������� POOS � ������� ��� �������
					docPOOS = docOSMP.getDocPOOS();
					Files.copy(docPOOS.getFile().toPath(), Paths.get(pathOSMPForSignature + "/" + docPOOS.getFileName()), StandardCopyOption.REPLACE_EXISTING);
	
					
					//�������� �������� ������
					fileOpvz.delete(); 
					if (osmp != null)
						fileOSMP.delete();
					
				}	
			}
//		}
		
	}
	
	public void toArchive(DocOSMP docOSMP, Path tempPath) throws Exception {
		File file = null;
		Path monthPath = null; //���� ����������
		
		//�������� ������������� � ������������� ����
		file = new File(this.pathArchive.toString() + "/" + docOSMP.getYear().toString());
		if (!file.exists()) {
			Files.createDirectories(file.toPath());
		}
		
		//�������� ������������� � ������������� ������
		String monthStr = null;
		if (docOSMP.getMonth() != null)
			monthStr = docOSMP.getMonth().toString();
		else
			monthStr = new SimpleDateFormat("M").format(new Date());
		file = new File(this.pathArchive.toString() + "/" + docOSMP.getYear().toString() + "/" + monthStr);
		if (!file.exists()) {
			Files.createDirectories(file.toPath());
		}
		monthPath = file.toPath();

		
		//�������� ������
		OPVZ opvz = docOSMP.getOpvz();
		OSMP osmp = null;
		if (!docOSMP.getListOSMP().isEmpty())
			osmp = docOSMP.getListOSMP().iterator().next();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd.HHmmss");
		File fileArchive = new File(monthPath.toString() + "/" + opvz.getFileName() + 
									"_" + dateFormat.format(opvz.getDateOfVipnet()) + 
									"_" + dateFormat.format(this.dateBegin) + 
									".zip");
		
		  //�������� ������ ������ ��� �������������
		List<File> listFiles = new ArrayList<>();
		if (osmp != null) 
			listFiles.add(osmp.getFile());
		listFiles.add(opvz.getFile());
		listFiles.add(docOSMP.getDocPOOS().getFile());
		if (osmp != null) {
			for (OSMPByArea osmpByArea : osmp.getListOsmpByArea()) {
				listFiles.add(osmpByArea.getFile());
			}
		}
		
		try (FileOutputStream fout = new FileOutputStream(fileArchive);
				ZipOutputStream zout = new ZipOutputStream(fout)) {
			
			zout.setLevel(Deflater.DEFAULT_COMPRESSION);
			byte[] buffer = new byte[BUFFER];
			
			for (File fileToArchive : listFiles) {
				try (FileInputStream in = new FileInputStream(fileToArchive)) {
					
					int len = 0;
					zout.putNextEntry(new ZipEntry(fileToArchive.getName()));
					while ((len = in.read(buffer)) > 0) {
						zout.write(buffer, 0, len);
					}
					zout.closeEntry();
					
				} catch (Exception e) {
					throw new Exception(e);
				}
			}
			
		} catch (Exception e) {
			throw new Exception(e);
		}
		
	
	}
	

}
