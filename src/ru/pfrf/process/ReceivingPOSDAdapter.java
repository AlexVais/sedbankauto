package ru.pfrf.process;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;

@Entity
@Table(name = "receivings_posd")
public class ReceivingPOSDAdapter extends ReceivingAdapter{
	
	@OneToMany(mappedBy = "receivingPOSDAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<DocPOSD> setDocPOSD = new HashSet<>();


	public Set<DocPOSD> getSetDocPOSD() {
		return setDocPOSD;
	}

	public void setSetDocPOSD(Set<DocPOSD> setDocPOSD) {
		this.setDocPOSD = setDocPOSD;
	}
	
	public ReceivingPOSDAdapter() {}
	
	public ReceivingPOSDAdapter(ProcessAdapter processAdapter) {
		super(processAdapter);
	}
	
}
