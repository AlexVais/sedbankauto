package ru.pfrf.process;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.order.osmp.OPVZ;

@Entity
@Table(name = "processes")
public class ProcessAdapter implements Serializable{
	private static final long serialVersionUID = 1L;

	@Transient
	protected SessionFactory sessionFactory;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@Column(name = "dateBegin")
	protected Date dateBegin;

	@Column(name = "dateEnd")
	protected Date dateEnd;
	
	@Column(name = "namePC")
	protected String namePC;
	
	@Column(name = "account")
	protected String account;
	
	@OneToOne(mappedBy = "processAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected ReceivingPacksFromCombanksAdapter receivingPacksFromCombanksAdapter;
	
	@OneToOne(mappedBy = "processAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected ReceivingPOSDAdapter receivingPOSDAdapter;
	
	@OneToOne(mappedBy = "processAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected ReceivingRegsFromOPFRAdapter receivingRegsFromOPFRAdapter;
	
	@OneToOne(mappedBy = "processAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected ReceivingOZACAdapter receivingOZACAdapter;
	

	public void setReceivingPOSDAdapter(ReceivingPOSDAdapter receivingPOSDAdapter) {
		this.receivingPOSDAdapter = receivingPOSDAdapter;
	}

	public void setReceivingRegsFromOPFRAdapter(
			ReceivingRegsFromOPFRAdapter receivingRegsFromOPFRAdapter) {
		this.receivingRegsFromOPFRAdapter = receivingRegsFromOPFRAdapter;
	}

	public ReceivingPacksFromCombanksAdapter getReceivingPacksFromCombanksAdapter() {
		return receivingPacksFromCombanksAdapter;
	}

	public void setReceivingPacksFromCombanksAdapter(
			ReceivingPacksFromCombanksAdapter receivingPacksFromCombanksAdapter) {
		this.receivingPacksFromCombanksAdapter = receivingPacksFromCombanksAdapter;
	}

	public ReceivingOZACAdapter getReceivingOZACAdapter() {
		return receivingOZACAdapter;
	}

	public void setReceivingOZACAdapter(ReceivingOZACAdapter receivingOZACAdapter) {
		this.receivingOZACAdapter = receivingOZACAdapter;
	}

	public ReceivingRegsFromOPFRAdapter getReceivingRegsFromOPFRAdapter() {
		return receivingRegsFromOPFRAdapter;
	}

	public ReceivingPOSDAdapter getReceivingPOSDAdapter() {
		return receivingPOSDAdapter;
	}


	public ReceivingPacksFromCombanksAdapter getReceiivngPacksFromCombanksAdapter() {
		return receivingPacksFromCombanksAdapter;
	}

	public void setReceivngPacksFromCombanksAdapter(
			ReceivingPacksFromCombanksAdapter receivingPacksFromCombanksAdapter) {
		this.receivingPacksFromCombanksAdapter = receivingPacksFromCombanksAdapter;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNamePC() {
		return namePC;
	}

	public void setNamePC(String namePC) {
		this.namePC = namePC;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Date getDateBegin() {
		return dateBegin;
	}

	public void setDateBegin(Date dateBegin) {
		this.dateBegin = dateBegin;
	} 
	
	public ProcessAdapter() {}
	
	public ProcessAdapter(SessionFactory sessionFactory,
				Date dateBegin,
				Date dateEnd,
				String namePC,
				String account) {
		
		this.sessionFactory = sessionFactory;
		this.dateBegin = dateBegin;
		this.dateEnd = dateEnd;
		this.namePC = namePC;
		this.account = account;
	}
	

	@Override
	public String toString() {
		return dateBegin + 
				" | " + dateEnd +
				" | " + namePC + 
				" | " + account;
	}
	
	@Override 
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
			
		ProcessAdapter process = (ProcessAdapter) obj;
		return new EqualsBuilder().append(this.dateBegin, process.dateBegin)
								  .append(this.dateEnd, process.dateEnd)
								  .append(this.namePC, process.namePC)
								  .append(this.account, process.account)
						   		  .isEquals();
	}
	
}
