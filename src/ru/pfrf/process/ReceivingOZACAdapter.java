package ru.pfrf.process;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.SPRA;

@Entity
@Table(name = "receivings_rca")
public class ReceivingOZACAdapter extends ReceivingAdapter {
	
	@OneToMany(mappedBy = "receivingOZACAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<OZAC> setOZAC = new HashSet<>();
	
	@OneToMany(mappedBy = "receivingOZACAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<OPVF> setOPVF = new HashSet<>();
	
	@OneToMany(mappedBy = "receivingOZACAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<SPRA> setSPRA = new HashSet<>();
	

	public Set<OZAC> getSetOZAC() {
		return setOZAC;
	}

	public void setSetOZAC(Set<OZAC> setOZAC) {
		this.setOZAC = setOZAC;
	}

	public Set<OPVF> getSetOPVF() {
		return setOPVF;
	}

	public void setSetOPVF(Set<OPVF> setOPVF) {
		this.setOPVF = setOPVF;
	}

	public Set<SPRA> getSetSPRA() {
		return setSPRA;
	}

	public void setSetSPRA(Set<SPRA> setSPRA) {
		this.setSPRA = setSPRA;
	}

	public ReceivingOZACAdapter() {}
	
	public ReceivingOZACAdapter(ProcessAdapter processAdapter) {
		super(processAdapter);
	}
	
}
