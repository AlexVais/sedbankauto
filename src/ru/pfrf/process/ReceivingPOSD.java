package ru.pfrf.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.*;

import main.MainFrame;
import main.Utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSD;
import ru.pfrf.register.CreatedRegisterTF;

@Entity
@Table(name = "new_receivings_posd")
public class ReceivingPOSD extends Process {
	private static final long serialVersionUID = 1L;

	@Transient
	private Path pathSourceCombanks;
	
	@Transient 
	private Path pathSourceSber;
	
	@Transient
	private Path pathArchive;
	
	@OneToMany(mappedBy = "receivingPOSD", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<DocPOSD> setDocPOSD = new HashSet<>();
	
	public Set<DocPOSD> getSetDocPOSD() {
		return setDocPOSD;
	}

	public void setSetDocPOSD(Set<DocPOSD> setDocPOSD) {
		this.setDocPOSD = setDocPOSD;
	}

	public Path getPathSourceCombanks() {
		return pathSourceCombanks;
	}

	public Path getPathSourceSber() {
		return pathSourceSber;
	}

	public Path getPathArchive() {
		return pathArchive;
	}

	public ReceivingPOSD() {}
	
	public ReceivingPOSD(SessionFactory sessionFactory, File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.pathSourceCombanks = Paths.get(new String(p.getProperty("sourcePathPOSDCombanks").getBytes("ISO8859-1")));
			this.pathSourceSber = Paths.get(new String(p.getProperty("sourcePathPOSDSber").getBytes("ISO8859-1")));
			this.pathArchive = Paths.get(new String(p.getProperty("archivePathPOSD").getBytes("ISO8859-1")));
		
		} catch (IOException e) {
			throw new Exception(e);
		}
	}
	
	private List<File> listFilesPOSD() {
		//������������ ������, � ������� ����� ������� ����� POSD �� ����. ������ � ����� �� ���������
		//�� ���������
		File[] filesSberbank = this.pathSourceSber.toFile().listFiles(new FilenameFilter() {
																		public boolean accept(File dir, String name) {
																			 if (new File(dir + "/" + name).isFile()) {
																				 Pattern p = Pattern.compile("^OUT-.+POSD.+XML$");
																				 Matcher m = p.matcher(name);
																				 if (m.find()) return true;
																			 }
																			 return false;			 
																		}});
			//�� ���. ������
		File[] dirsCombank = this.pathSourceCombanks.toFile().listFiles(new FilenameFilter() {
																		public boolean accept(File dir, String name) {
																			 if (new File(dir + "/" + name).isDirectory()) {
																				 Pattern p = Pattern.compile("^OUT-.+POSD.+XML_ID\\d+");
																				 Matcher m = p.matcher(name);
																				 if (m.find()) return true;
																			 }
																			 return false; 
																		}});
		List<File> filesCombank = new ArrayList<>();
		for(File dir : dirsCombank) {
			filesCombank.add(dir.listFiles()[0]);
		}
		//����������� ������� � ����
		List<File> listFiles = new ArrayList<>();
		listFiles.addAll(Arrays.asList(filesSberbank));
		listFiles.addAll(filesCombank);
		
		return listFiles;
	}
	
	
	private List<File> copyFilesPOSDToTemp(List<File> listFilesPOSDSource) throws Exception {
		List<File> filesPOSDTemp = new ArrayList<>();
		if (!listFilesPOSDSource.isEmpty()) {
			//�������� �������� tempPOSD ��� ������������� ��������� POSD
			Path pathTempPOSD = Paths.get(this.pathSourceCombanks + "/temp/tempPOSD");
			if (pathTempPOSD.toFile().exists()) 
				Utils.clearDir(pathTempPOSD);
			else
				pathTempPOSD.toFile().mkdirs();
	
			//����������� ������ � ������� ������ ���. ������\temp\tempPOSD
			for(File file : listFilesPOSDSource) {
				File fileDest = new File(pathTempPOSD + "/" + file.getName());
				Files.copy(file.toPath(), fileDest.toPath());
				filesPOSDTemp.add(fileDest);
			}
		}
		return filesPOSDTemp;
	}
	
	
	private List<DocPOSD> createDocPOSD(List<File> listFilesPOSDTemp) throws Exception {
		
		List<DocPOSD> listDocPOSD = new ArrayList<>();
		
		for(File file : listFilesPOSDTemp) {
			ParserFileNameOut parserFileNameOut = new ParserFileNameOut(file, MainFrame.dictNameOrgInFile);
			parserFileNameOut.parse();
			ParserPOSD parserPOSD = new ParserPOSD();
			parserPOSD.parse(file);
			
			Session session = null;
			Transaction t = null;
			DocPOSD docPOSD = null;
			Criteria criteria = null;
			CreatedRegisterTF createdRegisterTF = null;
			
			//��������� ��� ������ ������
			if (!parserPOSD.getDeliveryOrg().equals(MainFrame.dictDeliveryOrgByShortName.get("8636"))) {
				//��������� ��� ���. ������
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					//������� POSD
					
					//�� ��� �� ����� ��� ������� ������. ���� ��, �� ���������� ���
					criteria = session.createCriteria(DocPOSD.class)
												   .add(Restrictions.eq("deliveryOrg", parserPOSD.getDeliveryOrg()))
												   .add(Restrictions.eq("year", parserPOSD.getYear()))
												   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
												   .add(Restrictions.eq("decision", true));
					if (!criteria.list().isEmpty()) {
						System.out.println("��� ���");
						continue;
					}
					
					//������� �������������������� POSD
					criteria = session.createCriteria(DocPOSD.class)
											   .add(Restrictions.eq("deliveryOrg", parserPOSD.getDeliveryOrg()))
											   .add(Restrictions.eq("year", parserPOSD.getYear()))
											   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
											   .add(Restrictions.isNull("decision"));
					
					List<DocPOSD> listDocPOSDFind = criteria.list();
					docPOSD = listDocPOSDFind.get(0);
					
					//����������� postfix POSD
					criteria = session.createCriteria(DocPOSD.class)
											   .add(Restrictions.eq("deliveryOrg", parserPOSD.getDeliveryOrg()))
											   .add(Restrictions.eq("year", parserPOSD.getYear()))
											   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
											   .addOrder(Order.desc("postfix"))
											   .setMaxResults(1);
					
					List<DocPOSD> listPOSDForPostfix = criteria.list();
					if (listPOSDForPostfix.size() > 0) {
						DocPOSD docPOSDForPostfix = listPOSDForPostfix.get(0);
						if (docPOSDForPostfix.getPostfix() == null) {
							docPOSD.setPostfix(1);
						} else {
							docPOSD.setPostfix(docPOSDForPostfix.getPostfix() + 1);
						}
					}
					
					//������� �������
					criteria = session.createCriteria(CreatedRegisterTF.class)
									  .add(Restrictions.eq("deliveryOrg", parserPOSD.getDeliveryOrg()))
									  .add(Restrictions.eq("number", Integer.valueOf(parserFileNameOut.getPackNumber())))
									  .add(Restrictions.eq("yearPay", parserPOSD.getYear()));
	
					
					List<CreatedRegisterTF> listCreatedRegisterTF = criteria.list();
					createdRegisterTF = listCreatedRegisterTF.get(0);
					
					t.commit();		
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� CreatedRegisterTF", e);
				} finally {
					session.close();	
				}
				//������������� DocPOSD
				docPOSD.setFile(file);
				docPOSD.setFileName(file.getName());
				docPOSD.setDecision(parserPOSD.isDecision());
				docPOSD.setMonth(parserPOSD.getMonth());
				docPOSD.setReceivingPOSD(this);
				docPOSD.setCreatedRegisterTF(createdRegisterTF);
				createdRegisterTF.getSetDocPOSD().add(docPOSD);
				this.setDocPOSD.add(docPOSD);
				listDocPOSD.add(docPOSD);
			} else {
				//��������� ��� ���������
				
				//������� POSD
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					criteria = session.createCriteria(DocPOSD.class)
											   .add(Restrictions.eq("deliveryOrg", parserPOSD.getDeliveryOrg()))
											   .add(Restrictions.eq("year", parserPOSD.getYear()))
											   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
											   .add(Restrictions.eq("decision", true));
					
					List<DocPOSD> listDocPOSDFind = criteria.list();
					if (!listDocPOSDFind.isEmpty()) {
						System.out.println("��� ���");
						t.commit();
						continue;
					}
					t.commit();
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� POSD", e);
				} finally {
					session.close();	
				}
				
				//������� �������
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					criteria = session.createCriteria(CreatedRegisterTF.class)
									  .add(Restrictions.eq("deliveryOrg", parserPOSD.getDeliveryOrg()))
									  .add(Restrictions.eq("number", Integer.valueOf(parserFileNameOut.getPackNumber())))
									  .add(Restrictions.eq("yearPay", parserPOSD.getYear()));
	
					
					List<CreatedRegisterTF> listCreatedRegisterTF = criteria.list();
					createdRegisterTF = listCreatedRegisterTF.get(0);
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� POSD", e);
				} finally {
					session.close();	
				}
				//������������� DocPOSD
				docPOSD = new DocPOSD(parserPOSD, null, this, null); //TODO ���������� dateSend
				docPOSD.setCreatedRegisterTF(createdRegisterTF);
				createdRegisterTF.getSetDocPOSD().add(docPOSD);
				this.setDocPOSD.add(docPOSD);
				listDocPOSD.add(docPOSD);
			}
		}
		
		return listDocPOSD;
	}
	
	
	private void saveToArchive(List<DocPOSD> listDocPOSD) throws Exception {
		for (DocPOSD docPOSD : listDocPOSD) {
			List<File> listFiles = new ArrayList<>(Arrays.asList(docPOSD.getFile()));
			File fileDest = new File(pathArchive + "/" + docPOSD.getFileName() + (docPOSD.getPostfix() == null ? "" : "_" + docPOSD.getPostfix()) + ".zip");
			Utils.compress(listFiles, fileDest);
		}
	}
	
	@Override
	protected void exec() throws Exception {
		//������������ ������, � ������� ����� ������� ����� POSD �� ����. ������ � ����� �� ���������
		List<File> listFilesPOSDSource = listFilesPOSD();
		
		//����������� ������ � ������� ������ ���. ������\temp\tempPOSD
		List<File> listFilesPOSDTemp = copyFilesPOSDToTemp(listFilesPOSDSource);
		
		//���� ���
		for(File file : listFilesPOSDTemp) {
			FileOfInfoExchange.deleteDigitalSignature(file);
		}
		
		//�������� �������� POSD
		List<DocPOSD> listDocPOSD = createDocPOSD(listFilesPOSDTemp);
		
		//���������� � ������
		saveToArchive(listDocPOSD);
		
		//�������� �������� ������
		for (File file : listFilesPOSDSource) {
			File parentFile = file.getParentFile();
			if (parentFile.equals(this.pathSourceSber.toFile())) {
				file.delete();
			} else if (parentFile.getParentFile().equals(this.pathSourceCombanks.toFile()))
				Utils.deleteDirNotEmpty(parentFile.toPath());
	
		}
		
	}

}
