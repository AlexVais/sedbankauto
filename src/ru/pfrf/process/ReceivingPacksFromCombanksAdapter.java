package ru.pfrf.process;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.PackFromCombank;

@Entity
@Table(name = "receivings_packs_from_com_banks")
public class ReceivingPacksFromCombanksAdapter {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Process")
	private ProcessAdapter processAdapter;
	
	@OneToMany(mappedBy = "receivingPacksFromCombanksAdapter", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<PackFromCombank> setPacks = new HashSet<>();
	
	public Set<PackFromCombank> getSetPacks() {
		return setPacks;
	}

	public void setSetPacks(Set<PackFromCombank> setPacks) {
		this.setPacks = setPacks;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProcessAdapter getProcessAdapter() {
		return processAdapter;
	}

	public void setProcessAdapter(ProcessAdapter processAdapter) {
		this.processAdapter = processAdapter;
	}

	public ReceivingPacksFromCombanksAdapter() {}
	
	public ReceivingPacksFromCombanksAdapter(ProcessAdapter processAdapter) {
		this.processAdapter = processAdapter;
	}
	
}
