package ru.pfrf.process;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.swing.JOptionPane;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.mysql.fabric.xmlrpc.base.Array;

import main.MainFrame;
import main.Utils;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOOD;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.OzacReceivError;
import ru.pfrf.entityinfoexchange.order.ozac.SPRA;
import ru.pfrf.entityinfoexchange.order.spis.SPIS;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVF;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserSPIS;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserSPRA;
import ru.pfrf.person.RecipientOZAC;

@Entity
@Table(name = "new_receivings_ozac")
public class ReceivingOZAC extends Process {
	
public ReceivingOZAC() {}

	@Transient
	private Path pathSourceCombanks;
	
	@Transient
	private Path pathSourceSber;
	
	@Transient
	private Path pathArchive;
	
	@Transient
	private Path pathOZACFor1C;
	
	@Transient
	private Path pathOZACToOPFR;
	
	@Transient
	private Path pathArchiveSPISMergerXMLManager;
	
	@Transient
	private OzacReceivError ozacReceivError;
	
	@OneToMany(mappedBy = "receivingOZAC", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<DocOZAC> setDocOZAC = new HashSet<>();


	public OzacReceivError getOzacReceivError() {
		return ozacReceivError;
	}

	public void setOzacReceivError(OzacReceivError ozacReceivError) {
		this.ozacReceivError = ozacReceivError;
	}

	public Set<DocOZAC> getSetDocOZAC() {
		return setDocOZAC;
	}

	public void setSetDocOZAC(Set<DocOZAC> setDocOZAC) {
		this.setDocOZAC = setDocOZAC;
	}

	public ReceivingOZAC(SessionFactory sessionFactory, File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.pathSourceCombanks = Paths.get(new String(p.getProperty("sourceOZACCombanks").getBytes("ISO8859-1")));
			this.pathSourceSber = Paths.get(new String(p.getProperty("sourceOZACSber").getBytes("ISO8859-1")));
			this.pathArchive = Paths.get(new String(p.getProperty("archivePathOZAC").getBytes("ISO8859-1")));
			this.pathOZACFor1C = Paths.get(new String(p.getProperty("pathOZACFor1C").getBytes("ISO8859-1")));
			this.pathOZACToOPFR = Paths.get(new String(p.getProperty("pathOZACToOPFR").getBytes("ISO8859-1")));
			this.pathArchiveSPISMergerXMLManager = Paths.get(new String(p.getProperty("pathArchiveSPISMergerXMLManager").getBytes("ISO8859-1")));
		
		} catch (Exception e) {
			throw new Exception("������ ������ ����� ���������");
		}
	}
	
	/**
	 * ��������� ������ ������ 
	 * @throws Exception 
	 */
	private void prepare() throws Exception {
		List<File> listFilesOPVF = new ArrayList<>();
		
		//�������� ������ ������ OPVF �� ���������
		File[] listOPVFBySber = this.pathSourceSber.toFile().listFiles(new FileFilter() {
														public boolean accept(File pathname) {
															if (!pathname.isFile()) return false;
															if (pathname.isHidden()) return false;
															return (pathname.getName().matches("^OUT.+DOC-OPVF.+XML$")) ? true : false;
														}});
	
		listFilesOPVF.addAll(Arrays.asList(listOPVFBySber));
		
		//�������� ������ ������ OPVF �� ���.������
		//�������� ������ ���������, � ������� � ����� ����� _ID***
		File[] dirs = this.pathSourceCombanks.toFile().listFiles(new FileFilter() {
												public boolean accept(File pathname) {
													if (!pathname.isDirectory()) return false;
													if (pathname.isHidden()) return false;
													return (pathname.getName().matches("^OUT.+DOC-OPVF.+XML_ID.+$")) ? true : false;
												}});
		
		//�������� ������� ������ OPVF �� ���������
		for (File dir : dirs) {
			File[] tempListOPVFByCombanks = dir.listFiles();
			if (tempListOPVFByCombanks.length != 1)
				throw new Exception("������ ��� ��������� ������ ������ OPVF �� ���. ������");
			listFilesOPVF.add(tempListOPVFByCombanks[0]);
		}
		
		//����������� ������ OPVF � ������� ������ ���. ������/temp/tempOZAC
		List<File> filesOPVFTemp = new ArrayList<>();
		Path pathTempOZAC = Paths.get(this.pathSourceCombanks + "/temp/tempOZAC");
		if (!listFilesOPVF.isEmpty()) {
			//�������� �������� tempOZAC ��� ������������� ��������� OPVF
			if (pathTempOZAC.toFile().exists()) {
				Utils.clearDir(pathTempOZAC);
			} else
				pathTempOZAC.toFile().mkdirs();
	
			//����������� ������ � ������� ������ ���. ������/temp/tempOZAC
			for(File file : listFilesOPVF) {
				File fileDest = new File(pathTempOZAC + "/" + file.getName());
				Files.copy(file.toPath(), fileDest.toPath());
				filesOPVFTemp.add(fileDest);
			}
		}
		
		//���� ���
		for(File file : filesOPVFTemp) {
			FileOfInfoExchange.deleteDigitalSignature(file);
		}
		
		//�������� ���������� OZAC (DocOZAC)
		List<OPVF> listOPVF = new ArrayList<>();
		Session session = null;
		Transaction t = null;
		for (File file : filesOPVFTemp) {
			try {
			
				//�������� �������� OPVF
				OPVF opvf = null;
				ParserOPVF parserOPVF = new ParserOPVF();
				parserOPVF.parse(file);
				
				//�������� DocPOSD, ���������������� OPVF
				DocPOSD findDocPOSD = null;
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					Criteria criteria = session.createCriteria(DocPOSD.class, "posd")
												.createCriteria("posd.createdRegisterTF", "reg", JoinType.INNER_JOIN)
												.add(Restrictions.eq("posd.decision", true))
												.add(Restrictions.eq("reg.yearPay", parserOPVF.getYear()))
												.add(Restrictions.eq("reg.number", parserOPVF.getPackNumber()))
												.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					
					List<DocPOSD> listPOSD = criteria.list();
					if (listPOSD.size() > 1) 
						throw new Exception("����� ������� � DocPOSD ����� 1. ����� ������: " + parserOPVF.getPackNumber());
					
					if (!listPOSD.isEmpty())
						findDocPOSD = listPOSD.get(0);
		
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					e.printStackTrace();
					throw new Exception("������ ��� ������� DocPOSD", e);
				} finally {
					session.close();
				}
				if (findDocPOSD == null) continue;
					
				
				DocOZAC docOZAC = new DocOZAC();
				docOZAC.setDocPOSD(findDocPOSD);
				
				//��� ���������
				//������� OPVF �� ��
				OPVF findOPVF = null;
				if (!"8636".equals(findDocPOSD.getCreatedRegisterTF().getDeliveryOrg().getShortName())) {
					try {
						session = sessionFactory.openSession();
						t = session.beginTransaction();
						
						Criteria criteria = session.createCriteria(OPVF.class)
													.add(Restrictions.eq("fileName", parserOPVF.getFile().getName()))
													.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
						
						List<OPVF> listFindOPVF = criteria.list();
						if (listFindOPVF.size() > 1) 
							throw new Exception("����� ������� � ������ ����� OPVF ����� 1: " + parserOPVF.getFile().getName());
						
						if (listFindOPVF.isEmpty())
							throw new Exception("�� ������� ������ � ������ ����� OPVF: " + parserOPVF.getFile().getName());
						
						findOPVF = listFindOPVF.get(0);
						
						t.commit();
					} catch (Exception e) {
						t.rollback();
						e.printStackTrace();
						throw new Exception("������ ��� ������� OPVF", e);
					} finally {
						session.close();
					}
					
					opvf = findOPVF;	
					opvf.setNamesMainFile(parserOPVF.getNamesMainFile());
					opvf.setMonth(parserOPVF.getMonth());
					opvf.setFile(parserOPVF.getFile());
					
				} else {
					opvf = new OPVF(parserOPVF, null, docOZAC);
				}
				
				docOZAC.setOpvf(opvf);
				
				
				//��������. ���������� �� ������� ����� DocOZAC
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					
					Criteria criteria = session.createCriteria(DocOZAC.class, "doc")
												.createCriteria("doc.opvf", "opvf", JoinType.INNER_JOIN)
												.add(Restrictions.eq("numberPack", opvf.getNumberPack()))
												.add(Restrictions.eq("year", opvf.getYear()))
												.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					
					List<DocOZAC> listFindDocOZAC = criteria.list();
					if (listFindDocOZAC.size() != 0) 
						throw new Exception("DocOZAC ��� ����� ��� ������� ������. ����� ������: " + opvf.getNumberPack());
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					e.printStackTrace();
					throw new Exception("������ ��� ������� DocOZAC", e);
				} finally {
					session.close();
				}
		
				
				//�������� �������� OZAC, SPRA �������� �����
				//��������� ���� ������, �������� � �����
				String fileNameOZAC = null;
				String fileNameSPRA = null;
				
				for (String fileNameInOPVF : opvf.getNamesMainFile()) {
					if (fileNameInOPVF.matches(".+OZAC.+")) fileNameOZAC = fileNameInOPVF;
					if (fileNameInOPVF.matches(".+SPRA.+")) fileNameSPRA = fileNameInOPVF;
				}
				
				//��������� ������ ���������, ���������� � ����� OZAC � ����� �����. ������ (��� ���. ������)
				File fileOZAC = null;
				final Integer yearPay = opvf.getYear();
				final String strNumberPack = String.valueOf(opvf.getNumberPack() + 100000).substring(1, 6);
				
				if (!opvf.getDeliveryOrg().getShortName().equals("8636")) {
					File[] dirsOZAC = this.pathSourceCombanks.toFile().listFiles(new FileFilter() {
																		public boolean accept(File pathname) {
																			if (pathname.isHidden()) return false;
																			if (!pathname.isDirectory()) return false;
																			return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-OZAC.+?(XML|xml)_ID.+$")) ? true : false;
																		}});
					if (dirsOZAC.length == 0) continue;
					if (dirsOZAC.length > 1) 
						throw new Exception("� �������� ����� ������ OZAC �� ���. ������ ���������� ����� ������ ����� OZAC, ��������������� ������ ������: " + opvf.getNumberPack());
		
					fileOZAC = new File(dirsOZAC[0] + "/" + fileNameOZAC);
					
				} else {
					File[] filesOZAC = this.pathSourceSber.toFile().listFiles(new FileFilter() {
																		public boolean accept(File pathname) {
																			if (pathname.isHidden()) return false;
																			if (!pathname.isFile()) return false;
																			return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-OZAC.+?XML$")) ? true : false;
					
																		}});
					if (filesOZAC.length == 0) continue;
					if (filesOZAC.length > 1)
						throw new Exception("� �������� ����� ������ OZAC �� ��������� ���������� ����� ������ ����� OZAC, ��������������� ������ ������: " + opvf.getNumberPack());
					
					fileOZAC = filesOZAC[0];
				}
					
				File fileTempOZAC = new File(pathTempOZAC + "/" + fileOZAC.getName());
				
				//����������� ����� OZAC � ������� ������ ���. ������/temp/tempOZAC
				Files.copy(fileOZAC.toPath(), fileTempOZAC.toPath());
				
				//�ڸ� ���
				FileOfInfoExchange.deleteDigitalSignature(fileTempOZAC);
				
				//�������� ������� OZAC
				OZAC ozac = null;
				ParserOZAC parserOZAC = new ParserOZAC();
				parserOZAC.parse(fileTempOZAC);
				
				//��������� OZAC �� �� �� filename
				if (!"8636".equals(opvf.getDeliveryOrg().getShortName())) {
					OZAC findOZAC = null;
					try {
						session = sessionFactory.openSession();
						t = session.beginTransaction();
						
						Criteria criteria = session.createCriteria(OZAC.class)
													.add(Restrictions.eq("fileName", parserOZAC.getFile().getName()))
													.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
						
						List<OZAC> listFindOZAC = criteria.list();
						if (listFindOZAC.size() > 1) 
							throw new Exception("����� ������� � ������ ����� OZAC ����� 1: " + parserOZAC.getFile().getName());
						
						if (listFindOZAC.isEmpty())
							throw new Exception("�� ������� ������ � ������ ����� OZAC: " + parserOZAC.getFile().getName());
						
						findOZAC = listFindOZAC.get(0);
						
						t.commit();
					} catch (Exception e) {
						t.rollback();
						e.printStackTrace();
						throw new Exception("������ ��� ������� OZAC", e);
					} finally {
						session.close();
					}
					
					ozac = findOZAC;
					ozac.initByParser(parserOZAC);
					ozac.init();
					
				} else {
					ozac = new OZAC(parserOZAC, null, docOZAC);
					ozac.init();
				}
				
				ozac.setDocOZAC(docOZAC);
				docOZAC.setOzac(ozac);
				
				//�������� ������� SPRA
				if (fileNameSPRA != null) {
					
					if ("8636".equals(opvf.getDeliveryOrg().getShortName())) {
						File[] filesSPRA = this.pathSourceSber.toFile().listFiles(new FileFilter() {
							public boolean accept(File pathname) {
								if (pathname.isHidden()) return false;
								if (!pathname.isFile()) return false;
								return (pathname.getName().matches("^OUT-.+?Y-" + yearPay + ".+?DCK-" + strNumberPack + ".+?DOC-SPRA.+?XML$")) ? true : false;
			
							}});
						if (filesSPRA.length == 0) continue;
						if (filesSPRA.length > 1)
						throw new Exception("� �������� ����� ������ OZAC �� ��������� ���������� ����� ������ ����� SPRA, ��������������� ������ ������: " + opvf.getNumberPack());
			
						File fileSPRA = filesSPRA[0];
						
						//����������� ����� SPRA � ������� ������ ���. ������/temp/tempOZAC
						File fileTempSPRA = new File(pathTempOZAC + "/" + fileSPRA.getName());
						Files.copy(fileSPRA.toPath(), fileTempSPRA.toPath());
						
						//�ڸ� ���
						FileOfInfoExchange.deleteDigitalSignature(fileTempSPRA);
						
						//�������� ������� SPRA
						ParserSPRA parserSPRA = new ParserSPRA();
						parserSPRA.parse(fileTempSPRA);
						SPRA spra = new SPRA(parserSPRA, null, docOZAC);
						spra.setDocOZAC(docOZAC);
						docOZAC.setSpra(spra);
						
					} else {
						//��������� SPRA �� �� �� filename
						SPRA findSPRA = null;
						SPRA spra = null;
						try {
							session = sessionFactory.openSession();
							t = session.beginTransaction();
						
							
							Criteria criteria = session.createCriteria(SPRA.class)
														.add(Restrictions.eq("fileName", fileNameSPRA))
														.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
							
							List<SPRA> listFindSPRA = criteria.list();
							if (listFindSPRA.size() > 1) 
								throw new Exception("����� ������� � ������ ����� SPRA ����� 1: " + fileNameSPRA);
							
							if (listFindSPRA.isEmpty())
								throw new Exception("�� ������� ������ � ������ ����� SPRA: " + fileNameSPRA);
							
							findSPRA = listFindSPRA.get(0);
							spra = findSPRA;
							
							t.commit();
						} catch (Exception e) {
							t.rollback();
							e.printStackTrace();
							throw new Exception("������ ��� ������� OZAC", e);
						} finally {
							session.close();
						}
						
						//����������� ����� SPRA � ������� ������ ���. ������/temp/tempOZAC
						File fileTempSPRA   = new File(pathTempOZAC + "/" + findSPRA.getFileName());
						File fileSourceSPRA = new File(this.pathSourceCombanks.toFile() + "/" + findSPRA.getFileName() + "_ID" + findSPRA.getFileOfPackFromCombankAdapter().getId() + "/" + findSPRA.getFileName());
						Files.copy(fileSourceSPRA.toPath(), fileTempSPRA.toPath());
				
						//������������� SPRA
						ParserSPRA parserSPRA = new ParserSPRA();
						parserSPRA.parse(fileTempSPRA);
						spra.setMonth(parserSPRA.getMonth());
						spra.setIsRead(true);
						spra.setOutNumber(parserSPRA.getOutNumber());
						spra.setDocOZAC(docOZAC);
						docOZAC.setSpra(spra);
					}
				}
				
				this.setDocOZAC.add(docOZAC);
				docOZAC.setReceivingOZAC(this);
				
			} catch (Exception e) {
				ParserFileNameOut parserFileNameOut = new ParserFileNameOut(file, MainFrame.dictNameOrgInFile);
				parserFileNameOut.parse();
				this.ozacReceivError = new OzacReceivError(parserFileNameOut.getYear(), 
												Integer.valueOf(parserFileNameOut.getPackNumber()));
				
				throw new Exception("������ ��� ���������� � ����� OZAC. �����: " + ozacReceivError, e);
			}
		}
		
	}
	
	
	private void copyToOPFRVipnet(DocOZAC docOZAC) throws IOException {
		OZAC ozac = docOZAC.getOzac();
		Path pathFileDest = Paths.get(pathOZACToOPFR + "/" + ozac.getFileName());
		
		if ("8636".equals(ozac.getDeliveryOrg().getShortName())) {
			//����� ��� ���������
			Files.copy(Paths.get(this.pathSourceSber + "/" + ozac.getFileName()), pathFileDest);
		} else {
			//����� ���. ������
			Files.copy(Paths.get(this.pathSourceCombanks + "/" + ozac.getFileName() + "_ID" + ozac.getFileOfPackFromCombankAdapter().getId() + "/" + ozac.getFileName()), pathFileDest);
		}			
	}
	
	/**
	 * ����������� ������ OZAC � ������� 1� (��� ���)
	 * @throws IOException 
	 */
	private void copyTo1C(DocOZAC docOZAC) throws IOException {
		OZAC ozac = docOZAC.getOzac();
		Files.copy(ozac.getFile().toPath(), Paths.get(this.pathOZACFor1C + "/" + ozac.getFileName()));
	}
	
	/**
	 * �������� ������ ������ OZAC
	 * @throws Exception 
	 */
	private void createArchive(DocOZAC docOZAC) throws Exception {
		OZAC ozac = docOZAC.getOzac();
		OPVF opvf = docOZAC.getOpvf();
		SPRA spra = docOZAC.getSpra();

		//���������� ����� OZAC, OPVF � SPRA �� ��������� �������
		Path archivePathOZACTemp = Paths.get(this.pathArchive + "/temp");
		Utils.clearDir(archivePathOZACTemp);
		
		Path pathDestOpvf = Paths.get(archivePathOZACTemp + "/" + opvf.getFileName());
		Path pathDestOzac = Paths.get(archivePathOZACTemp + "/" + ozac.getFileName());
		Path pathDestSpra = null;
		if (spra != null)
			pathDestSpra = Paths.get(archivePathOZACTemp + "/" + spra.getFileName());
		
		if ("8636".equals(opvf.getDeliveryOrg().getShortName())) {
			//����� ��� ���������
			Files.copy(Paths.get(this.pathSourceSber + "/" + opvf.getFileName()), pathDestOpvf);
			Files.copy(Paths.get(this.pathSourceSber + "/" + ozac.getFileName()), pathDestOzac);
			if (pathDestSpra != null) 
				Files.copy(Paths.get(this.pathSourceSber + "/" + spra.getFileName()), pathDestSpra);
		} else {
			//����� ���. ������
			Files.copy(Paths.get(this.pathSourceCombanks + "/" + opvf.getFileName() + "_ID" + opvf.getFileOfPackFromCombankAdapter().getId() + "/" + opvf.getFileName()), pathDestOpvf);
			Files.copy(Paths.get(this.pathSourceCombanks + "/" + ozac.getFileName() + "_ID" + ozac.getFileOfPackFromCombankAdapter().getId() + "/" + ozac.getFileName()), pathDestOzac);
			if (pathDestSpra != null) {
				File file = new File(this.pathSourceCombanks + "/" + spra.getFileName() + "_ID" + spra.getFileOfPackFromCombankAdapter().getId() + "/" + spra.getFileName());
				Files.copy(Paths.get(this.pathSourceCombanks + "/" + spra.getFileName() + "_ID" + spra.getFileOfPackFromCombankAdapter().getId() + "/" + spra.getFileName()), pathDestSpra);
			}
		}		
		
		List<File> listFiles = new ArrayList<>();
		listFiles.add(pathDestOpvf.toFile());
		listFiles.add(pathDestOzac.toFile());
		if (pathDestSpra != null)
			listFiles.add(pathDestSpra.toFile());
		
		Integer numberReg = docOZAC.getDocPOSD().getCreatedRegisterTF().getNumber();
		Utils.compress(listFiles, new File(this.pathArchive + "/������_" + numberReg + "_" + opvf.getYear() + ".zip"));
		
	}
	
	
	private void deleteSourceFiles(DocOZAC docOZAC) throws Exception {
		OZAC ozac = docOZAC.getOzac();
		OPVF opvf = docOZAC.getOpvf();
		SPRA spra = docOZAC.getSpra();
		
		if ("8636".equals(opvf.getDeliveryOrg().getShortName())) {
			//����� ��� ���������
			Files.delete(Paths.get(this.pathSourceSber + "/" + opvf.getFileName()));
			Files.delete(Paths.get(this.pathSourceSber + "/" + ozac.getFileName()));
			if (spra != null) 
				Files.delete(Paths.get(this.pathSourceSber + "/" + spra.getFileName()));
		} else {
			//����� ���. ������
			Utils.deleteDirNotEmpty(Paths.get(this.pathSourceCombanks + "/" + opvf.getFileName() + "_ID" + opvf.getFileOfPackFromCombankAdapter().getId()));
			Utils.deleteDirNotEmpty(Paths.get(this.pathSourceCombanks + "/" + ozac.getFileName() + "_ID" + ozac.getFileOfPackFromCombankAdapter().getId()));
			if (spra != null) 
				Utils.deleteDirNotEmpty(Paths.get(this.pathSourceCombanks + "/" + spra.getFileName() + "_ID" + spra.getFileOfPackFromCombankAdapter().getId()));
		}			
	}
	
	
	/**
	 * ������ ������ OZAC � SPIS
	 * @throws Exception
	 */
	private void compareOZACToSPIS(DocOZAC docOZAC) throws Exception {
		//���������� �� ������ SPIS
		final OPVF opvf = docOZAC.getOpvf();
		final Integer numberReg = docOZAC.getDocPOSD().getCreatedRegisterTF().getNumber();
		File sourceFile = new File(pathArchiveSPISMergerXMLManager + "/" + opvf.getYear() + String.valueOf(opvf.getMonth() + 100).substring(1) + "_" + numberReg + "_" + opvf.getDeliveryOrg().getShortName() + ".zip");
		Utils.unpack(sourceFile, Paths.get(this.pathSourceCombanks + "/temp/tempOZAC"), ".*SPIS.+?XML$");
		
		//��������� ����� SPIS
		File[] filesSPIS = new File(this.pathSourceCombanks + "/temp/tempOZAC").listFiles(new FilenameFilter() {
																		public boolean accept(File dir, String name) {
																			return (name.matches("^PFR-.+Y-" + opvf.getYear() + ".+DCK-.+?" + numberReg + "-001-DOC-SPIS-FSB.+?XML$")) ? true : false;
																		}});
		if (filesSPIS.length != 1) 
			throw new Exception("������ ��������� SPIS. ���������� ������ != 1");
		
		//������� � ������������� SPIS
		ParserSPIS parserSPIS = new ParserSPIS();
		parserSPIS.parse(filesSPIS[0]);
		SPIS spis = new SPIS(parserSPIS);
		spis.init();
		
		docOZAC.getOzac().compareToSpis(spis);
}
	
	
	@Override
	protected void exec() throws Exception {
		prepare();
		
		//�������� OZAC
		for (DocOZAC docOZAC : this.setDocOZAC) {
			OZAC ozac = docOZAC.getOzac();
			ozac.check();
			
			//������ ������ OZAC � SPIS
			compareOZACToSPIS(docOZAC);
		}
		
		for (DocOZAC docOZAC : this.setDocOZAC) {
			//����������� ������ OZAC � ������� 1� (��� ���)
			copyTo1C(docOZAC);
			
			//�������� ������ ������ OZAC
			createArchive(docOZAC);
			
			//����������� OZAC � ������� ��������������� ��� Vipnet � ���� (� ���)
			copyToOPFRVipnet(docOZAC);
	
			//�������� �������� ������
			deleteSourceFiles(docOZAC);
		}
		
	}

}
