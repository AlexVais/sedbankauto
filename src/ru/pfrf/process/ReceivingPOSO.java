package ru.pfrf.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Transient;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import main.MainFrame;
import main.Utils;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.order.psmp.MergedPSMP;
import ru.pfrf.entityinfoexchange.order.psmp.POSZ;
import ru.pfrf.entityinfoexchange.order.vozv.MergedVOZV;
import ru.pfrf.entityinfoexchange.order.vozv.POSO;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSO;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSZ;

public class ReceivingPOSO extends Process {
	private static final long serialVersionUID = 1L;
	
	@Transient 
	private Path pathSourceSber;
	
	@Transient
	private Path pathArchive;
	
	@Transient
	private List<POSO> listPOSO = new ArrayList<>();
	
	
	public List<POSO> getListPOSO() {
		return listPOSO;
	}


	public ReceivingPOSO(SessionFactory sessionFactory, File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.pathSourceSber = Paths.get(new String(p.getProperty("sourcePathPOSOSber").getBytes("ISO8859-1")));
			this.pathArchive = Paths.get(new String(p.getProperty("archivePathPOSO").getBytes("ISO8859-1")));
		
		} catch (IOException e) {
			throw new Exception(e);
		}
	}
	
	
	private List<File> copyFilesPOSOToTemp(File[] files) throws Exception {
		List<File> filesPOSOTemp = new ArrayList<>();
		if (files.length != 0) {
			//�������� �������� tempPOSO ��� ������������� ��������� POSO
			Path pathTempPOSO = Paths.get(this.pathSourceSber + "/temp/tempPOSO");
			if (pathTempPOSO.toFile().exists()) 
				Utils.clearDir(pathTempPOSO);
			else
				pathTempPOSO.toFile().mkdirs();
	
			//����������� ������ � ������� ������ ���. ������\temp\tempPOSO
			for(File file : files) {
				File fileDest = new File(pathTempPOSO + "/" + file.getName());
				Files.copy(file.toPath(), fileDest.toPath());
				filesPOSOTemp.add(fileDest);
			}
		}
		return filesPOSOTemp;
	}
	
	
	private List<POSO> createPOSO(List<File> listFilesPOSOTemp) throws Exception {
		
		for(File file : listFilesPOSOTemp) {
			ParserFileNameOut parserFileNameOut = new ParserFileNameOut(file, MainFrame.dictNameOrgInFile);
			parserFileNameOut.parse();
			ParserPOSO parserPOSO = new ParserPOSO();
			parserPOSO.parse(file);
		
			
			//���������
			Session session = null;
			Transaction t = null;
			Criteria criteria = null;
			
				//��� ���������
			if (parserPOSO.getDeliveryOrg().equals(MainFrame.dictDeliveryOrgByShortName.get("8636"))) {
				//������� POSO
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					criteria = session.createCriteria(POSO.class)
											   .add(Restrictions.eq("deliveryOrg", parserPOSO.getDeliveryOrg()))
											   .add(Restrictions.eq("year", parserPOSO.getYear()))
											   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
											   .add(Restrictions.eq("decision", parserPOSO.getDecision()));
					
					List<POSO> listDocPOSOFind = criteria.list();
					if (!listDocPOSOFind.isEmpty()) {
						System.out.println("��� ���");
						continue;
					}
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� POSO", e);
				} finally {
					session.close();	
				}
				
				
				//������� mergedPOSO
				MergedVOZV mergedVOZV = null;
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					criteria = session.createCriteria(MergedVOZV.class)
											   .add(Restrictions.eq("year", parserPOSO.getYear()))
											   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
											   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					
					List<MergedVOZV> listMergedVOZV = criteria.list();
					if (listMergedVOZV.isEmpty()) {
						throw new Exception("�� ������ mergedVOZV, ��������������� POSO: " + parserPOSO.toString());
					}
					
					if (listMergedVOZV.size() > 1) {
						throw new Exception("���������� mergedVOZV > 1");
					}
					
					mergedVOZV = listMergedVOZV.get(0);
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� POSO", e);
				} finally {
					session.close();	
				}
				
				//������������� POSO � ���������� � ��
				POSO poso = new POSO(parserPOSO, null, new Date(), mergedVOZV);
				listPOSO.add(poso);
			}
		}
		
		return listPOSO;
	}
	
	
	
	private void saveToArchive(List<POSO> listPOSO) throws Exception {
		for (POSO poso : listPOSO) {
			List<File> listFiles = new ArrayList<>(Arrays.asList(poso.getFile()));
			File fileDest = new File(pathArchive + "/" + poso.getFileName() + (poso.getPostfix() == null ? "" : "_" + poso.getPostfix()) + ".zip");
			Utils.compress(listFiles, fileDest);
		}
	}
	
	
	@Override
	protected void exec() throws Exception {
		//��������� ������ ������ POSO
		
		File[] files = this.pathSourceSber.toFile().listFiles(new FilenameFilter() {
																public boolean accept(File dir, String name) {
																	 if (new File(dir + "/" + name).isFile()) {
																		 Pattern p = Pattern.compile("^OUT-.+POSO.+XML$");
																		 Matcher m = p.matcher(name);
																		 if (m.find()) return true;
																	 }
																	 return false;			 
																}});
		
		//����������� ������ � ������� ������ ���. ������\temp\tempPOSO
		List<File> listFilesPOSOTemp = this.copyFilesPOSOToTemp(files);
		
		//���� ���
		for(File file : listFilesPOSOTemp) {
			FileOfInfoExchange.deleteDigitalSignature(file);
		}
		
		//�������� �������� POSD
		List<POSO> listPOSO = createPOSO(listFilesPOSOTemp);
		
		//���������� � ������
		saveToArchive(listPOSO);
		
		//�������� �������� ������
		for (File file : files) 
			file.delete();
	}

}
