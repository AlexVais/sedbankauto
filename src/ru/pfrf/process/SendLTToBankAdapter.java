package ru.pfrf.process;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.register.ListTransferToBank;


@Entity
@Table(name = "send_list_transfer_to_bank")
public class SendLTToBankAdapter implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@OneToOne
	@JoinColumn(name = "process")
	protected ProcessAdapter processAdapter;
	
	@OneToMany(mappedBy = "sendLTToBank", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ListTransferToBank> setListTransferToBank = new HashSet<>();

	public Set<ListTransferToBank> getSetListTransferToBank() {
		return setListTransferToBank;
	}

	public void setSetListTransferToBank(
			Set<ListTransferToBank> setListTransferToBank) {
		this.setListTransferToBank = setListTransferToBank;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProcessAdapter getProcessAdapter() {
		return processAdapter;
	}

	public void setProcessAdapter(ProcessAdapter processAdapter) {
		this.processAdapter = processAdapter;
	}
	
	public SendLTToBankAdapter() {}
	
	@Override
	public String toString() {
		return String.valueOf(processAdapter.id);
	}
	
	@Override 
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
			
		SendLTToBankAdapter send = (SendLTToBankAdapter) obj;
		return new EqualsBuilder().append(this.processAdapter, send.processAdapter)
						   		  .isEquals();
	}
	

}
