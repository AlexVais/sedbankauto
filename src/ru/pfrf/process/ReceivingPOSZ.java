package ru.pfrf.process;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Transient;

import main.MainFrame;
import main.Utils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.psmp.MergedPSMP;
import ru.pfrf.entityinfoexchange.order.psmp.POSZ;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSD;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSZ;
import ru.pfrf.register.CreatedRegisterTF;

public class ReceivingPOSZ extends Process{
	private static final long serialVersionUID = 1L;

	@Transient
	private Path pathSourceCombanks;
	
	@Transient 
	private Path pathSourceSber;
	
	@Transient
	private Path pathArchive;
	
	@Transient
	private List<POSZ> listPOSZ = new ArrayList<>();
	
	public List<POSZ> getListPOSZ() {
		return listPOSZ;
	}


	public ReceivingPOSZ(SessionFactory sessionFactory, File fileConfig) throws Exception {
		super(sessionFactory, fileConfig);
		
		Properties p = new Properties();
		try {
			p.load(new FileInputStream(fileConfig));
			
			this.pathSourceSber = Paths.get(new String(p.getProperty("sourcePathPOSZSber").getBytes("ISO8859-1")));
			this.pathSourceCombanks = Paths.get(new String(p.getProperty("sourcePathPOSZCombanks").getBytes("ISO8859-1")));	
			this.pathArchive = Paths.get(new String(p.getProperty("archivePathPOSZ").getBytes("ISO8859-1")));		
		} catch (IOException e) {
			throw new Exception(e);
		}
	}
	
	
	private List<File> copyFilesPOSZToTemp(File[] files) throws Exception {
		List<File> filesPOSZTemp = new ArrayList<>();
		if (files.length != 0) {
			//�������� �������� tempPOSZ ��� ������������� ��������� POSZ
			Path pathTempPOSZ = Paths.get(this.pathSourceSber + "/temp/tempPOSZ");
			if (pathTempPOSZ.toFile().exists()) 
				Utils.clearDir(pathTempPOSZ);
			else
				pathTempPOSZ.toFile().mkdirs();
	
			//����������� ������ � ������� ������ ���. ������\temp\tempPOSZ
			for(File file : files) {
				File fileDest = new File(pathTempPOSZ + "/" + file.getName());
				Files.copy(file.toPath(), fileDest.toPath());
				filesPOSZTemp.add(fileDest);
			}
		}
		return filesPOSZTemp;
	}
	
	private void copyFilesComBankPOSZToTemp(File file) throws Exception {
		List<File> filesPOSZTemp = new ArrayList<>();
			//�������� �������� tempPOSZ ��� ������������� ��������� POSZ
			Path pathTempPOSZ = Paths.get(this.pathSourceSber + "/temp/tempPOSZ");
			if (pathTempPOSZ.toFile().exists()==false) 
				pathTempPOSZ.toFile().mkdirs();
	
			//����������� ������ � ������� ������ ���. ������\temp\tempPOSZ
				File fileDest = new File(pathTempPOSZ + "/" + file.getName());
				Files.copy(file.toPath(), fileDest.toPath());
				filesPOSZTemp.add(fileDest);
	}
	
	private List<POSZ> createPOSZ(List<File> listFilesPOSZTemp) throws Exception {
		
		for(File file : listFilesPOSZTemp) {
			ParserFileNameOut parserFileNameOut = new ParserFileNameOut(file, MainFrame.dictNameOrgInFile);
			parserFileNameOut.parse();
			ParserPOSZ parserPOSZ = new ParserPOSZ();
			parserPOSZ.parse(file);
			
			//���������
			Session session = null;
			Transaction t = null;
			Criteria criteria = null;
			
				//��� ���������
			//if (parserPOSZ.getDeliveryOrg().equals(MainFrame.dictDeliveryOrgByShortName.get("8636"))) {
				//������� POSZ
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					criteria = session.createCriteria(POSZ.class)
											   .add(Restrictions.eq("deliveryOrg", parserPOSZ.getDeliveryOrg()))
											   .add(Restrictions.eq("year", parserPOSZ.getYear()))
											   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
											   .add(Restrictions.eq("decision", parserPOSZ.getDecision()));
					
					List<POSZ> listDocPOSZFind = criteria.list();
					if (!listDocPOSZFind.isEmpty()) {
						System.out.println("��� ���");
						continue;
					}
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� POSZ", e);
				} finally {
					session.close();	
				}
			//	}
			//else
			//	{
				
			//	}
				
				
				//������� mergedPSMP
				MergedPSMP mergedPSMP = null;
				try {
					session = sessionFactory.openSession();
					t = session.beginTransaction();
					criteria = session.createCriteria(MergedPSMP.class)
											   .add(Restrictions.eq("year", parserPOSZ.getYear()))
											   .add(Restrictions.eq("numberPack", Integer.valueOf(parserFileNameOut.getPackNumber())))
											   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					
					List<MergedPSMP> listMergedPSMP = criteria.list();
					if (listMergedPSMP.isEmpty()) {
						throw new Exception("�� ������ mergedPSMP, ��������������� POSZ: " + parserPOSZ.toString());
					}
					
					if (listMergedPSMP.size() > 1) {
						throw new Exception("���������� mergedPSMP > 1");
					}
					
					mergedPSMP = listMergedPSMP.get(0);
					
					t.commit();
				} catch (Exception e) {
					t.rollback();
					throw new Exception("������ ��� ������� POSZ", e);
				} finally {
					session.close();	
				}
				
				//������������� POSZ � ���������� � ��
				POSZ posz = new POSZ(parserPOSZ, null, new Date(), mergedPSMP);
				listPOSZ.add(posz);
			}

		
		return listPOSZ;
	}
	
	private void saveToArchive(List<POSZ> listPOSZ) throws Exception {
		for (POSZ posz : listPOSZ) {
			List<File> listFiles = new ArrayList<>(Arrays.asList(posz.getFile()));
			File fileDest = new File(pathArchive + "/" + posz.getFileName() + (posz.getPostfix() == null ? "" : "_" + posz.getPostfix()) + ".zip");
			Utils.compress(listFiles, fileDest);
		}
	}

	@Override
	protected void exec() throws Exception {
		//��������� ������ ������ POSZ
		
		File[] files = this.pathSourceSber.toFile().listFiles(new FilenameFilter() {
																public boolean accept(File dir, String name) {
																	 if (new File(dir + "/" + name).isFile()) {
																		 Pattern p = Pattern.compile("^OUT-.+POSZ.+XML$");
																		 Matcher m = p.matcher(name);
																		 if (m.find()) return true;
																	 }
																	 return false;			 
																}});
		
		//����������� ������ � ������� ������ ���. ������\temp\tempPOSZ
		List<File> listFilesPOSZTemp = this.copyFilesPOSZToTemp(files);
		
		//���� ���
		for(File file : listFilesPOSZTemp) {
			FileOfInfoExchange.deleteDigitalSignature(file);
		}
		
		//�������� ������ ���������, � ������� � ����� ����� _ID***
		File[] dirs = this.pathSourceCombanks.toFile().listFiles(new FileFilter() {
													public boolean accept(File pathname) {
														if (!pathname.isDirectory()) return false;
														if (pathname.isHidden()) return false;
														return (pathname.getName().matches("^OUT.+DOC-POSZ.+XML_ID.+$")) ? true : false;
												}});
		
		//�������� ������� ������ POSZ �� ���������
		for (File dir : dirs) {
			File[] tempListOPVFByCombanks = dir.listFiles();
			if (tempListOPVFByCombanks.length != 1)
				throw new Exception("������ ��� ��������� ������ ������ POSZ �� ���. ������");
			listFilesPOSZTemp.add(tempListOPVFByCombanks[0]);
			this.copyFilesComBankPOSZToTemp(tempListOPVFByCombanks[0]);
		}		
		//�������� �������� POSZ
		List<POSZ> listPOSZ = createPOSZ(listFilesPOSZTemp);
		
		//���������� � ������
		saveToArchive(listPOSZ);
		
		//�������� �������� ������
		for (File file : files) 
			file.delete();
	}

}
