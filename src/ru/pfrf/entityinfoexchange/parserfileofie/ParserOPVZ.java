package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;
import java.util.List;

import main.MainFrame;

import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;

public class ParserOPVZ extends ParserRegisterOut{
	
	@Override
	public void parse(File file) throws Exception {
		super.parse(file);
		
		//���� ��� ������ ������, ��
		if (resultOfRead == null) {
			Element elReg = this.rootElement.element("������������������������")
						    				.element("�����_������������_������_���_��������");
			List listSentFiles = elReg.elements("����������������");
			for (Object obj : listSentFiles) {
				this.namesMainFile.add(((Element) obj).element("��������").getTextTrim());
			}
		}
	}
	
	@Override
	protected DeliveryOrg getDeliveryOrg(Element rootElement) throws Exception {
		String strDeliveryOrg = this.getElementRegister(rootElement).element("����������������")
																	.element("�����������������������").getText();
		return MainFrame.dictDeliveryOrg.get(strDeliveryOrg);
		
	}

}
