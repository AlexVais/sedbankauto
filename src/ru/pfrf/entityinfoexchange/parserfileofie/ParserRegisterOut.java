package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import main.MainFrame;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public abstract class ParserRegisterOut extends ParserFileOfIEOut {

	protected List<String> namesMainFile = new ArrayList<>();
	protected Integer countFile;
	
	public void setNamesMainFile(List<String> listFileNames) {
		this.namesMainFile = listFileNames;
	}

	public List<String> getNamesMainFile() {
		return namesMainFile;
	}

	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + this.countFile;
	}

	@Override
	protected Element getElementRegister(Element rootElement) throws Exception {
		return rootElement.element("������������������������").element("���������_�����");
	}

}
