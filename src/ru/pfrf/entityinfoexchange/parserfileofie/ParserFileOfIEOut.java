package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Element;

public abstract class ParserFileOfIEOut extends ParserFileOfIE{
	
	protected Long outNumber;


	public Long getOutNumber() {
		return outNumber;
	}
	
	
	@Override
	public void parse(File file) throws Exception {
		super.parse(file);
		
		if (resultOfRead == null) {
			//����������� ���������� ������
			Element elFileName = rootElement.element("��������");
			Pattern p = Pattern.compile("-OUTNMB-(\\d{10})\\.XML$", Pattern.CASE_INSENSITIVE);
			Matcher m = p.matcher(elFileName.getText());
			m.find();
			this.outNumber = Long.valueOf(m.group(1));
		}
	}
	
	@Override
	public String toString() {
		return super.toString() +
				" | " + outNumber;
	}
}
