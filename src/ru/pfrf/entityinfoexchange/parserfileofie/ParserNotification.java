package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.pfrf.classifier.TypeResultNotification;
import ru.pfrf.entityinfoexchange.parser.ParserEntityIE;

public class ParserNotification extends ParserEntityIE{
	
	private String attachment;

	private Date date;

	private Integer numberMail;
	
	private Boolean isSignatureValid;

	private String typeResultNotification;

	public Boolean getIsSignatureValid() {
		return isSignatureValid;
	}

	public String getAttachment() {
		return attachment;
	}

	public Date getDate() {
		return date;
	}

	public Integer getNumberMail() {
		return numberMail;
	}

	public String getTypeResultNotification() {
		return typeResultNotification;
	}

	public ParserNotification(File file) {
		super(file);
	}

	
	@Override
	public String toString() {
		return file + 
			" | " + typeResultNotification + 
			" | " + String.valueOf(numberMail) + 
			" | " + attachment +
			" | " + date + 
			" | " + String.valueOf(isSignatureValid);	
		
	}

	@Override
	public void parse() throws Exception {
		char[] buf = new char[(int) file.length()];
		try (FileReader fr = new FileReader(this.file)) {
			fr.read(buf);
		} catch (IOException e){
			throw new Exception(e);
		}
		
		try {
			Pattern p = Pattern.compile(".*?��������� ������ ���� (���������|��������).*?(\\d{1,2}\\.\\d{2}\\.\\d{4} \\d+:\\d{2})" +
										".*?��������������� �����: � (\\d+).*?�������� \"(.*?)\".*", Pattern.DOTALL);
			
			Matcher m = p.matcher(new String(buf));
			m.find();
	
			//���������
			this.typeResultNotification = m.group(1);
			
			//���� �������
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
			sdf.setLenient(false);
			this.date = sdf.parse(m.group(2));
			
			//��������������� �����
			this.numberMail = Integer.valueOf(m.group(3));
			
			//��������
			this.attachment = m.group(4);
			
			//������������ ���
			p = Pattern.compile("������ ���������������", Pattern.DOTALL);
			m = p.matcher(new String(buf));
			isSignatureValid = true;
			while (m.find()) {
				isSignatureValid = false;
				break;
			}
			
		
		} catch (Exception e) {
			throw new Exception("������ ��� �������� ��������� " + this.file, e);
		}
	}
}
