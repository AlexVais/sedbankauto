package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.*;

import main.MainFrame;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.parser.ParserEntityIE;


public class ParserRegsFromOPFR extends ParserEntityIE {

	private Integer numberReg;
	private Date dateOfFunding;
	private Integer yearPay;
	private Integer monthPay;
	private Integer numberPaymentOrder;
	private Date datePaymentOrder;
	private Integer totalRecipients;
	private BigDecimal totalSum;
	
	private List<TypeError> errors = new ArrayList<>();
	
	public Integer getYearPay() {
		return yearPay;
	}

	public void setYearPay(Integer yearPay) {
		this.yearPay = yearPay;
	}

	public List<TypeError> getErrors() {
		return errors;
	}

	public Integer getMonthPay() {
		return monthPay;
	}

	public void setMonthPay(Integer monthPay) {
		this.monthPay = monthPay;
	}


	public Integer getTotalRecipients() {
		return totalRecipients;
	}

	public void setTotalRecipients(Integer totalRecipients) {
		this.totalRecipients = totalRecipients;
	}


	public BigDecimal getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(BigDecimal totalSum) {
		this.totalSum = totalSum;
	}

	public Integer getNumberReg() {
		return numberReg;
	}

	public void setNumberReg(Integer numberReg) {
		this.numberReg = numberReg;
	}

	public Date getDateOfFunding() {
		return dateOfFunding;
	}

	public void setDateOfFunding(Date dateOfFunding) {
		this.dateOfFunding = dateOfFunding;
	}

	public Integer getNumberPaymentOrder() {
		return numberPaymentOrder;
	}

	public void setNumberPaymentOrder(Integer numberPaymentOrder) {
		this.numberPaymentOrder = numberPaymentOrder;
	}

	public Date getDatePaymentOrder() {
		return datePaymentOrder;
	}

	public void setDatePaymentOrder(Date datePaymentOrder) {
		this.datePaymentOrder = datePaymentOrder;
	}

	public ParserRegsFromOPFR(File file) {
		super(file);
	}

	
	
	/**
	 * ���������� ������ �������
	 * @param string
	 * @return Integer
	 */
	private Integer extractRegNum(String string) {
		try {
			Pattern p = Pattern.compile("�(\\d+) ��");
			Matcher m = p.matcher(string);
	
			m.find();
			return Integer.valueOf(m.group(1));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * ���������� ���� ��������������
	 * @param string
	 * @return Integer
	 */
	private Date extractDateOfFunding(String string) {
		try {
			Pattern p = Pattern.compile("(\\d{2}\\.\\d{2}\\.\\d{4})");
			Matcher m = p.matcher(string);
	
			m.find();
			SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
			format.setLenient(false);
			
			return format.parse(m.group(1));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * ���������� ���������� �����������
	 * @param string
	 * @return Integer
	 */
	private Integer extractTotalRecipients(XSSFSheet sheet) {
		try {
			Pattern p = Pattern.compile("����� �����������.*?(\\d+)\\b");
			Matcher m = null;
			XSSFRow row = null;
			
			Integer i = 15;
			do {
				row = sheet.getRow(++i);
				m = p.matcher(row.getCell(0).getStringCellValue());
			} while (!m.find());
			
			return Integer.valueOf(m.group(1));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * ���������� ����� �����
	 * @param string
	 * @return BigDecimal
	 */
	private BigDecimal extractTotalSum(XSSFSheet sheet) {
		try {
			Pattern p = Pattern.compile("����� �����.*?(\\d{1}.*\\d{1})\\b \\(");
			Matcher m = null;
			XSSFRow row = null;
			
			Integer i = 15;
			do {
				row = sheet.getRow(++i);
				m = p.matcher(row.getCell(0).getStringCellValue());
			} while (!m.find());
			
			String result = m.group(1).replace(" " , "");
			result = result.replace(",", ".");
			return new BigDecimal(result);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * ���������� ������ ��������� ���������
	 * @param string
	 * @return Integer
	 */
	private Integer extractNumberPaymentOrder(String string) {
		try {
			Pattern p = Pattern.compile("^\\D+(\\d+)\\D*$");
			Matcher m = p.matcher(string);
	
			m.find();
			return Integer.valueOf(m.group(1));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * ���������� ���� ��������� ���������
	 * @param string
	 * @return java.util.Date
	 */
	private Date extractDatePaymentOrder(String string) {
		try {
			Pattern p = Pattern.compile("(\\d{2}\\.\\d{2}\\.\\d{4})");
			Matcher m = p.matcher(string);
	
			m.find();
			SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
			format.setLenient(false);
			
			return format.parse(m.group(1));
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * ���������� ���� �������
	 */
	private void extractDatePay(String string) {
		try {
			Pattern p = Pattern.compile("(\\d+)\\.(\\d+)");
			Matcher m = p.matcher(string);
			m.find();
			
			yearPay = Integer.valueOf(m.group(2));
			monthPay = Integer.valueOf(m.group(1));
		} catch (Exception e) {
			yearPay = null;
			monthPay = null;
		}
			
		
	}

	@Override
	public void parse() throws Exception {
		//�������� ������������ ����� �����
		Pattern p = Pattern.compile("^��1_(\\d{6})_(.+?)_(\\d+)\\.(xls|xlsx)$"); //�-�: ��1_201611_VEB_68.xls
		Matcher m = p.matcher(this.file.getName());
		if (!m.matches()) {
			errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_FILENAME));
			return;
		}
			
		//������ � xls
		try (FileInputStream fis = new FileInputStream(file);
			 XSSFWorkbook book = new XSSFWorkbook(fis)) {
			
			XSSFSheet sheet = book.getSheet("����1");
			
			//���������� ������ �������
			numberReg = extractRegNum(sheet.getRow(3).getCell(0).getStringCellValue());
			if (numberReg == null)
				errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_NUMBERREG));

			//���������� ���� ��������������
			dateOfFunding = extractDateOfFunding(sheet.getRow(3).getCell(0).getStringCellValue());
			if (dateOfFunding == null) 
				errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_DATEFUNDING));
		
			//���������� ���� �������
			this.extractDatePay(sheet.getRow(7).getCell(0).getStringCellValue());
			if (this.monthPay == null || this.yearPay == null) 
				errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_DATEPAY));
			
			//���������� ���������� �����������
			totalRecipients = extractTotalRecipients(sheet);
			if (totalRecipients == null) 
				errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_COUNTRECIPIENTS));
			
			//���������� �������� �����
			totalSum = extractTotalSum(sheet);
			if (totalSum == null)
				errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_TOTALSUM));
			
			//���������� ������ ��������� ���������
			numberPaymentOrder = extractNumberPaymentOrder(sheet.getRow(11).getCell(0).getStringCellValue());
			if (numberPaymentOrder == null)
				errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_NUMBERORDER));
			
			//���������� ���� ��������� ���������
			datePaymentOrder = extractDatePaymentOrder(sheet.getRow(12).getCell(0).getStringCellValue());
			if (datePaymentOrder == null) 
				errors.add(MainFrame.dictTypeError.get(TypeError.ERR_REGS_DATEORDER));
			
		} catch (Exception e) {
			throw new Exception("������ ������ � excel ������ " + file.toString(), e);  
		}
			
	}
	
	@Override
	public String toString() {
		return String.valueOf(numberReg) + 
				" | " + String.valueOf(dateOfFunding) + 
				" | " + String.valueOf(yearPay) + 
				" | " + String.valueOf(monthPay) + 
				" | " + String.valueOf(numberPaymentOrder) + 
				" | " + String.valueOf(datePaymentOrder) + 
				" | " + String.valueOf(totalRecipients) + 
				" | " + String.valueOf(totalSum);
	}

}
