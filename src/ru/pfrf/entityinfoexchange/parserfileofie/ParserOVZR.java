package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;
import java.math.BigDecimal;

import org.dom4j.Element;

public class ParserOVZR extends ParserMainFileOut {
	
	public BigDecimal getSum() {
		return sum;
	}

	public Integer getCountRecipients() {
		return countRecipients;
	}

	private BigDecimal sum;

	private Integer countRecipients;

	@Override
	protected Element getElementRegister(Element rootElement) throws Exception {
		return rootElement.element("������������������������").element("��������_�����");
	}
	
	@Override
	public void parse(File file) throws Exception {
		super.parse(file);
		try {
			Element elReportPay = rootElement.element("������������������������")
											 .element("�����_�_��������_����");
			
			this.sum = new BigDecimal(elReportPay.element("��������������").getText());
			this.countRecipients = new Integer(elReportPay.element("���������������������").getText());
			
		} catch (Exception e) {
			throw new Exception("������ �������� " + file, e);
		}
	}

}
