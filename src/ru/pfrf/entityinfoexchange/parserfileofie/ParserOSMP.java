package ru.pfrf.entityinfoexchange.parserfileofie;

import main.MainFrame;

import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;

public class ParserOSMP extends ParserMainFileOut{
	
	@Override
	protected Element getElementRegister(Element rootElement) throws Exception {
		return rootElement.element("������������������������").element("���������_�����");
	}
	
	@Override
	protected DeliveryOrg getDeliveryOrg(Element rootElement) throws Exception {
		String strDeliveryOrg = this.getElementRegister(rootElement).element("����������������")
																	.element("�����������������������").getText();
		return MainFrame.dictDeliveryOrg.get(strDeliveryOrg);
		
	}
}
