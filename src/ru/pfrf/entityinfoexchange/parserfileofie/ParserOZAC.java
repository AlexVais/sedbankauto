package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;
import java.math.BigDecimal;

import main.MainFrame;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;

public class ParserOZAC extends ParserMainFileOut{
	
	private BigDecimal sum;
	private BigDecimal sumCredited;
	private Integer countRecipientsCredited;
	private BigDecimal sumNotCredited;
	private Integer countRecipientsNotCredited;
	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public BigDecimal getSumCredited() {
		return sumCredited;
	}

	public void setSumCredited(BigDecimal sumCredited) {
		this.sumCredited = sumCredited;
	}

	public Integer getCountRecipientsCredited() {
		return countRecipientsCredited;
	}

	public void setCountRecipientsCredited(Integer countRecipientsCredited) {
		this.countRecipientsCredited = countRecipientsCredited;
	}

	public BigDecimal getSumNotCredited() {
		return sumNotCredited;
	}

	public void setSumNotCredited(BigDecimal sumNotCredited) {
		this.sumNotCredited = sumNotCredited;
	}

	public Integer getCountRecipientsNotCredited() {
		return countRecipientsNotCredited;
	}

	public void setCountRecipientsNotCredited(Integer countRecipientsNotCredited) {
		this.countRecipientsNotCredited = countRecipientsNotCredited;
	}

	@Override
	protected Element getElementRegister(Element rootElement) throws Exception {
		return rootElement.element("������������������������").element("��������_�����");
	}
	
	@Override
	public void parse(File file) throws Exception {
		super.parse(file);
		try {
			Element elReportPay = rootElement.element("������������������������")
											 .element("�����_�_����������_�_��_����������_����");
			
			this.sum = new BigDecimal(elReportPay.element("��������������").getText());
			this.sumCredited = new BigDecimal(elReportPay.element("��������������").getText());
			this.sumNotCredited = new BigDecimal(elReportPay.element("����������������").getText());
			this.countRecipientsCredited = Integer.valueOf(elReportPay.element("������������������������������").getText());
			this.countRecipientsNotCredited = Integer.valueOf(elReportPay.element("��������������������������������").getText());
			
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
