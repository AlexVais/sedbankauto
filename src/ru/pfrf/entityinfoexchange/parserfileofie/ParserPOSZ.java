package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;

import main.MainFrame;

import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;

public class ParserPOSZ extends ParserFileOfIEOut {

	private Boolean decision;
	
	private Boolean isRead;

	public Boolean getDecision() {
		return decision;
	}

	public Boolean getIsRead() {
		return isRead;
	}
	
	@Override
	protected Element getElementRegister(Element rootElement) throws Exception {
		return rootElement.element("������������������������")
						  .element("���������_�����");
	}
	
	
	@Override
	protected DeliveryOrg getDeliveryOrg(Element rootElement) throws Exception {
		try {
			ParserFileNameOut parser = new ParserFileNameOut(new File(rootElement.element("��������").getText()), 
												MainFrame.dictNameOrgInFile);
			parser.parse();
			return parser.getDeliveryOrg();
	
		} catch (Exception e) {
			throw new Exception("������ ����������� ����������� ����������� �� ����� �����");
		}
		
	}
	
	@Override
	public void parse(File file) throws Exception {
		super.parse(file);
		
		try {
			String strDecision = rootElement.element("������������������������")
					   						.element("�������������_�_���������_�������_�_���������")
					   						.element("�������").getText();
			if (strDecision.equals("�������")) 
				this.decision = true;
			else if (strDecision.equals("�� �������"))
				this.decision = false;
			else 
				throw new Exception("������ ����������� �������");
			
		} catch (Exception e) {
			throw new Exception("\n������ ��� �������� POSZ " + this.file.getName(), e);
		}
		
		this.isRead = true; //TODO ��������
	}
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + decision + 
				" | " + isRead;
	}

}
