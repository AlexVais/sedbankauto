package ru.pfrf.entityinfoexchange.parserfileofie;

import java.io.File;

import org.dom4j.Element;

public class ParserPOSO extends ParserFileOfIEOut {

	private Boolean decision;

	private Boolean isRead;

	@Override
	protected Element getElementRegister(Element rootElement) throws Exception {
		return rootElement.element("������������������������")
						  .element("��������_�����");
	}
	
	
	public Boolean getDecision() {
		return decision;
	}


	public Boolean getIsRead() {
		return isRead;
	}


	@Override
	public void parse(File file) throws Exception {
		super.parse(file);
		
		try {
			String strDecision = rootElement.element("������������������������")
					   						.element("�������������_�_���������_�������_��_�����")
					   						.element("�������").getText();
			if (strDecision.equals("�������")) 
				this.decision = true;
			else if (strDecision.equals("�� �������"))
				this.decision = false;
			else 
				throw new Exception("������ ����������� �������");
			
		} catch (Exception e) {
			throw new Exception("\n������ ��� �������� POSO " + this.file.getName(), e);
		}
		
		this.isRead = true; //TODO ��������
	}

	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + decision + 
				" | " + isRead;
	}
}
