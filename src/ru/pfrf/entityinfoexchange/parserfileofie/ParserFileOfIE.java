package ru.pfrf.entityinfoexchange.parserfileofie;

import static java.lang.System.out;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.MainFrame;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.SAXWriter;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.TypeArrayAssignment;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;


public abstract class ParserFileOfIE {
	
	protected File file;
	
	protected Integer year;
	
	protected Integer month;
	
	protected Integer area;
	
	protected Integer packNumber;
	
	protected Integer part2PackNumber;
	
	protected TypeArrayAssignment typeArray;
	
	protected Element rootElement;
	
	protected DeliveryOrg deliveryOrg;
	
	protected Boolean isRead = false;
	
	protected String resultOfRead;
	

	public Integer getArea() {
		return area;
	}

	public String getResultOfRead() {
		return resultOfRead;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public Element getRootElement() {
		return rootElement;
	}

	public Integer getPackNumber() {
		return packNumber;
	}

	public Integer getPart2PackNumber() {
		return part2PackNumber;
	}

	public File getFile() {
		return file;
	}

	public Integer getYear() {
		return year;
	}

	public Integer getMonth() {
		return month;
	}

	public TypeArrayAssignment getTypeArray() {
		return typeArray;
	}

	//public ParserFileOfIE() {};
	
	public void parse(File file) throws Exception {
		this.file = file;
		this.rootElement = null;
		this.year = null;
		this.month = null;
		this.typeArray = null;
		
		SAXReader reader = new SAXReader();

		try {
			Document document = reader.read(this.file);
			rootElement = document.getRootElement();
		
			Element elRegister = this.getElementRegister(rootElement);
			
			this.month = Integer.valueOf(elRegister.element("�����").getTextTrim());
			this.year = Integer.valueOf(elRegister.element("���").getTextTrim());
			
			String strTypeArrayAssignment = elRegister.element("�������������������").getTextTrim();
			this.typeArray = MainFrame.dictTypeArrayAssignment.get(strTypeArrayAssignment);
			
			//����������� ����������� �����������
			this.deliveryOrg = getDeliveryOrg(rootElement); 
			
			//����������� ������ 
			ParserFileNameOut parserFileNameOut = new ParserFileNameOut(this.file, MainFrame.dictNameOrgInFile);
			parserFileNameOut.parse();
			this.area = parserFileNameOut.getArea();
			
			//����������� ������ ������
			Element elFileName = rootElement.element("��������");
			Pattern p = Pattern.compile("-DCK-" + "(\\d{5})-(\\d{3})");
			Matcher m = p.matcher(elFileName.getText());
			m.find();
			this.packNumber = Integer.valueOf(m.group(1));
			this.part2PackNumber = Integer.valueOf(m.group(2));
			
			this.isRead = true;
			
		} catch (DocumentException e) {
			this.resultOfRead = e.getMessage();
			this.isRead = false;
			throw new Exception("\n������ ��� �������� xml ����� " + this.file.getName(), e);
			
		} catch (Exception e) {
			throw new Exception("\n������ ��� �������� xml ����� " + this.file.getName(), e);
		}
		
	}
	
	protected abstract Element getElementRegister(Element rootElement) throws Exception;

	protected DeliveryOrg getDeliveryOrg(Element rootElement) throws Exception {
		String strDeliveryOrg = this.getElementRegister(rootElement).element("����").element("�����������������������").getText();
		DeliveryOrg deliveryOrg = MainFrame.dictDeliveryOrg.get(strDeliveryOrg.trim());
		if (deliveryOrg == null)
			throw new Exception("�� ���������� ����������� ����������: " + strDeliveryOrg);
		return deliveryOrg;
		
	}

	@Override
	public String toString() {
		return String.valueOf(this.file) + 
				" | " + String.valueOf(year) + 
				" | " + String.valueOf(month) +
				" | " + String.valueOf(packNumber) + 
				" | " + String.valueOf(part2PackNumber) + 
				" | " + isRead + 
				" | " + resultOfRead;
	}
}
