package ru.pfrf.entityinfoexchange;

import java.io.File;
import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.reflect.TypeUtilsTest.This;
import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIE;

@MappedSuperclass
public class DocOfInfoExchange extends EntityIEBank {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "numberPack")
	protected Integer numberPack;
	
	@Column(name = "year")
	protected Integer year;

	@Column(name = "month")
	protected Integer month;
	
	@Column(name = "isCancelled")
	protected Boolean isCancelled = false;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typeError")
	protected TypeError typeError;
	
	@Column(name = "dateCancel")
	protected Date dateCancel;
	
	@Column(name = "dateSendVipnet")
	protected Date dateSendVipnet;

	public Date getDateSendVipnet() {
		return dateSendVipnet;
	}

	public void setDateSendVipnet(Date dateSendVipnet) {
		this.dateSendVipnet = dateSendVipnet;
	}

	public TypeError getTypeError() {
		return typeError;
	}

	public void setTypeError(TypeError typeError) {
		this.typeError = typeError;
	}

	public Date getDateCancel() {
		return dateCancel;
	}

	public void setDateCancel(Date dateCancel) {
		this.dateCancel = dateCancel;
	}

	public Boolean getIsCancelled() {
		return isCancelled;
	}

	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getNumberPack() {
		return numberPack;
	}

	public void setNumberPack(Integer numberPack) {
		this.numberPack = numberPack;
	}
	
	public DocOfInfoExchange() {}
	
	public DocOfInfoExchange(ParserFileOfIE parser) {
		this(parser.getYear(),
		     parser.getMonth(),
		     parser.getDeliveryOrg(),
		     parser.getPackNumber());
		     
	}
	
	public DocOfInfoExchange(Integer year, 
					Integer month, 
					DeliveryOrg deliveryOrg, 
					Integer numberPack) {
		super(deliveryOrg);
		
		this.year = year;
		this.month = month;
		this.numberPack = numberPack;
		
	}
	
	@Override
	public String toString() {
		return String.valueOf(year) +
				" | " + String.valueOf(month) +
				" | " + String.valueOf(numberPack) + 
				" | " + isCancelled + 
				" | " + dateCancel;
	}
	
	@Override 
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		DocOfInfoExchange doc = (DocOfInfoExchange) obj;
		return new EqualsBuilder()
		                 .append(this.year, doc.year)
		                 .append(this.month, doc.month)
		                 .append(this.numberPack, doc.numberPack)
		                 .append(this.isCancelled, doc.isCancelled)
		                 .append(this.dateCancel, doc.dateCancel)
		                 .append(this.typeError, doc.typeError)
		                 .isEquals();
	}
	
}
