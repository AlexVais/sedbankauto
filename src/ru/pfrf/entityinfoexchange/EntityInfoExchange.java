package ru.pfrf.entityinfoexchange;

import java.io.Serializable;

import javax.persistence.*;

import ru.pfrf.classifier.DeliveryOrg;

@MappedSuperclass
public abstract class EntityInfoExchange implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public EntityInfoExchange() {}
	
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	
}
