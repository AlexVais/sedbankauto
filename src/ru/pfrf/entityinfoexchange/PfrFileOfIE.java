package ru.pfrf.entityinfoexchange;

import java.io.File;

import javax.persistence.*;

import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIEPfr;

@MappedSuperclass
public abstract class PfrFileOfIE extends FileOfInfoExchange {
	private static final long serialVersionUID = 1L;
	
	
	public PfrFileOfIE() {}
	
	public PfrFileOfIE(ParserFileOfIEPfr parser) {
		this(parser.getYear(),
			 parser.getMonth(),
			 parser.getDeliveryOrg(),
			 parser.getPackNumber(),
			 parser.getFile(),
			 parser.getRootElement());
	}
	
	public PfrFileOfIE(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			File file,
			Element rootElement) {
		super(year, month, deliveryOrg, numberPack, file, rootElement);
	}

}
