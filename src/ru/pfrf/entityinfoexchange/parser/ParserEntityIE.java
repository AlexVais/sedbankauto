package ru.pfrf.entityinfoexchange.parser;

import java.io.File;

public abstract class ParserEntityIE {
protected File file;
	
	public File getFile() {
		return file;
	}

	public abstract void parse() throws Exception;
	
	public ParserEntityIE(File file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return file.getName();
	}
}
