package ru.pfrf.entityinfoexchange.parserfilename;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.MainFrame;
import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.Dictionary;
import ru.pfrf.classifier.NameOrgInFile;

public class ParserFileNameOut extends ParserFileNameXml{
	
	private Long outNumber;

	public Long getOutNumber() {
		return outNumber;
	}

	public ParserFileNameOut(File file, Dictionary<NameOrgInFile> dictNameOrgInFile) {
		super(file, dictNameOrgInFile);
	}

	@Override
	public void parse() throws Exception {
		super.parse();
		
		Pattern p = Pattern.compile("^OUT-" + 
								"(\\d{3})" + 
								"-Y-" + "(\\d{4})" +
								"-ORG-" + "(\\d{3}-\\d{3}-\\d{6})" +
								"-DIS-" + "(\\d{3})" + 
								"-DCK-" + "(\\d{5})-(\\d{3})" +
								"-DOC-" + "(.{4})" +
								"-FSB-" + "(\\d{4})" +
								"-OUTNMB-" + "(\\d{10})\\.XML$", Pattern.CASE_INSENSITIVE);
		
		Matcher m = p.matcher(file.getName());
		if (m.matches()) {
			this.version = m.group(1);
			this.year = Integer.valueOf(m.group(2));
			this.org = m.group(3);
			this.area = Integer.valueOf(m.group(4));
			this.packNumber = m.group(5);
			this.part2PackNumber = m.group(6);
			this.typeDoc = m.group(7);
			this.deliveryOrg = dictNameOrgInFile.get(m.group(8)).getDeliveryOrg();
			this.outNumber = Long.valueOf(m.group(9));
		}
		
	}
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + outNumber;
	}
}
