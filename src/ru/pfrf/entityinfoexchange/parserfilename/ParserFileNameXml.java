package ru.pfrf.entityinfoexchange.parserfilename;

import java.io.File;
import java.util.regex.Pattern;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.Dictionary;
import ru.pfrf.classifier.NameOrgInFile;

public abstract class ParserFileNameXml extends ParserFileName{
	
	protected String version;
	
	protected Integer year;
	
	protected String org;
	
	protected Integer area;
	
	protected String packNumber;
	
	protected String part2PackNumber;
	
	protected String typeDoc;
	
	protected DeliveryOrg deliveryOrg;
	
	protected Dictionary<NameOrgInFile> dictNameOrgInFile;

	public Integer getArea() {
		return area;
	}

	public Integer getYear() {
		return year;
	}

	public String getOrg() {
		return org;
	}

	public String getPackNumber() {
		return packNumber;
	}

	public String getPart2PackNumber() {
		return part2PackNumber;
	}

	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public ParserFileNameXml(File file, Dictionary<NameOrgInFile> dictNameOrgInFile) {
		super(file);
		this.dictNameOrgInFile = dictNameOrgInFile;
	}
	
	public void parse() throws Exception{
		this.version = null;
		this.year = null;
		this.org = null;
		this.area = null;
		this.packNumber = null;
		this.part2PackNumber = null;
		this.typeDoc = null;
		this.deliveryOrg = null;
	}
	
	public String getTypeDoc() {
		return typeDoc;
	}

	public String getVersion() {
		return version;
	}

	@Override
	public String toString() {
		return super.toString() +
				" | " + version + 
				" | " + String.valueOf(year) + 
				" | " + org + 
				" | " + String.valueOf(area) + 
				" | " + packNumber + 
				" | " + String.valueOf(part2PackNumber) + 
				" | " + typeDoc;
	}
	
}
