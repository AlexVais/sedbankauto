package ru.pfrf.entityinfoexchange.parserfilename;

import java.io.File;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.Dictionary;
import ru.pfrf.classifier.NameOrgInFile;

public class ParserFileNameFactory {
	static ParserFileNameXml createParserFileName(File file, Dictionary<NameOrgInFile> dictNameOrgInFile) {
		
		String typeDoc = file.getName().substring(0, 3);
		
		switch (typeDoc) {
			case "PFR" :
				return new ParserFileNamePfr(file, dictNameOrgInFile);
				
			case "OUT" : 
				return new ParserFileNameOut(file, dictNameOrgInFile);
				
			default : return null;
		}
		
	}
}
