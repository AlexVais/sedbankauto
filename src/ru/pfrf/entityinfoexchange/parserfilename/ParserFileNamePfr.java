package ru.pfrf.entityinfoexchange.parserfilename;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.Dictionary;
import ru.pfrf.classifier.NameOrgInFile;

public class ParserFileNamePfr extends ParserFileNameXml {
	
	public ParserFileNamePfr(File file, Dictionary<NameOrgInFile> dictNameOrgInFile) {
		super(file, dictNameOrgInFile);
	}

	@Override
	public void parse() throws Exception {
		super.parse();
		//������: PFR-700-Y-2016-ORG-038-999-999999-DIS-000-DCK-00000-049-DOC-POOS-FSB-9070.XML
		Pattern p = Pattern.compile("^PFR-" + 
								"(\\d{3})" + 
								"-Y-" + "(\\d{4})" +
								"-ORG-" + "(\\d{3}-\\d{3}-\\d{6})" +
								"-DIS-" + "(\\d{3})" + 
								"-DCK-" + "(\\d{5})-(\\d{3})" +
								"-DOC-" + "(.{4})" +
								"-FSB-" + "(\\d{4}).xml$", Pattern.CASE_INSENSITIVE);
		
		Matcher m = p.matcher(file.getName());
		if (m.matches()) {
			this.version = m.group(1);
			this.year = Integer.valueOf(m.group(2));
			this.org = m.group(3);
			this.area = Integer.valueOf(m.group(4));
			this.packNumber = m.group(5);
			this.part2PackNumber = m.group(6);
			this.typeDoc = m.group(7);
			this.deliveryOrg = dictNameOrgInFile.get(m.group(8)).getDeliveryOrg();
		}
		
	}
}
