package ru.pfrf.entityinfoexchange.parserfilename;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.pfrf.classifier.DeliveryOrg;

public class ParserPackFromCombank extends ParserFileName {

	private DeliveryOrg deliveryOrg;
	
	private String shortNameDeliveryOrg;
	
	private String numberPack;
	
	private String indexOfPartipant;
	
	private String symbolPrefix;
	
	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public String getShortNameDeliveryOrg() {
		return shortNameDeliveryOrg;
	}

	public void setShortNameDeliveryOrg(String shortNameDeliveryOrg) {
		this.shortNameDeliveryOrg = shortNameDeliveryOrg;
	}

	public String getNumberPack() {
		return numberPack;
	}

	public void setNumberPack(String numberPack) {
		this.numberPack = numberPack;
	}

	public String getSymbolPrefix() {
		return symbolPrefix;
	}

	public void setSymbolPrefix(String symbolPrefix) {
		this.symbolPrefix = symbolPrefix;
	}

	public ParserPackFromCombank(File file, DeliveryOrg deliveryOrg) {
		super(file);
		
		this.deliveryOrg = deliveryOrg;
		this.shortNameDeliveryOrg = deliveryOrg.getShortName();
	}
	
	@Override
	public String toString() {
		return super.toString() +
				" | " + this.indexOfPartipant + 
				" | " + this.symbolPrefix + 
				" | " + this.numberPack + 
				" | " + this.shortNameDeliveryOrg;
	}
	
	@Override
	public void parse() throws Exception{
		super.parse();
		
		this.indexOfPartipant = null;
		this.symbolPrefix = null;
		this.numberPack = null;
		
		Pattern p = Pattern.compile("^(\\S{3})([a-z]{1})(\\d{4})\\." + this.shortNameDeliveryOrg + "$");
		Matcher m = p.matcher(file.getName());
		if (m.find()) {
			this.indexOfPartipant = m.group(1);
			this.symbolPrefix = m.group(2);
			this.numberPack = m.group(3);
		}
	}
}
