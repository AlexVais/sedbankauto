package ru.pfrf.entityinfoexchange.parserfilename;

import java.io.File;

import ru.pfrf.entityinfoexchange.parser.ParserEntityIEBank;

public abstract class ParserFileName extends ParserEntityIEBank {
	
	public ParserFileName(File file) {
		super(file);
	}

	@Override
	public void parse() throws Exception {
		
	}

}
