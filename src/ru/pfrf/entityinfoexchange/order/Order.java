package ru.pfrf.entityinfoexchange.order;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.DocOfInfoExchange;

@MappedSuperclass
public class Order extends DocOfInfoExchange {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "area")
	protected Integer area;

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public Order() {}
	
	public Order(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			Integer area) {
		super (year, month, deliveryOrg, numberPack);
		
		this.area = area;
	}
	
	@Override 
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		Order order = (Order) obj;
		return new EqualsBuilder()
		                 .append(this.area, order.area)
		                 .isEquals();
	}

}
