package ru.pfrf.entityinfoexchange.order.ozac;

import java.io.Serializable;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;


@Entity
@Table(name = "ozac_receiv_error")
public class OzacReceivError implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@Column(name = "yearPay")
	private Integer yearPay;
	
	@Column(name = "numberPack")
	private Integer numberPack;
	
	public OzacReceivError(Integer yearPay, Integer numberPack) {
		this.yearPay = yearPay;
		this.numberPack = numberPack;
	}
	
	@Override
	public String toString() {
		return String.valueOf(numberPack) + " | " + String.valueOf(yearPay);
	}
	
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		OzacReceivError ore = (OzacReceivError) obj;
		return new EqualsBuilder()
		                 .append(this.numberPack, ore.numberPack)
		                 .append(this.yearPay, ore.yearPay)
		                 .isEquals();
		
	}
}
