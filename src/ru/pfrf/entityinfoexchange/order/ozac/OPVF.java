package ru.pfrf.entityinfoexchange.order.ozac;

import java.io.File;
import java.util.List;

import javax.persistence.*;

import org.dom4j.Element;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.RegisterOut;
import ru.pfrf.entityinfoexchange.order.osmp.OPVZ;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVF;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ReceivingOZACAdapter;
import ru.pfrf.process.ReceivingPOSDAdapter;

@Entity
@Table(name = "opvf")
public class OPVF extends RegisterOut {
	private static final long serialVersionUID = 1L;
	
	@BatchSize(size = 200)
	@OneToOne(mappedBy = "opvf", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected DocOZAC docOZAC;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fileOfPackFromCombank")
	protected FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ReceivingRCA")
	protected ReceivingOZACAdapter receivingOZACAdapter;

	public ReceivingOZACAdapter getReceivingOZACAdapter() {
		return receivingOZACAdapter;
	}

	public void setReceivingOZACAdapter(ReceivingOZACAdapter receivingOZACAdapter) {
		this.receivingOZACAdapter = receivingOZACAdapter;
	}

	public FileOfPackFromCombankAdapter getFileOfPackFromCombankAdapter() {
		return fileOfPackFromCombankAdapter;
	}

	public void setFileOfPackFromCombankAdapter(
			FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter) {
		this.fileOfPackFromCombankAdapter = fileOfPackFromCombankAdapter;
	}

	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}

	public DocOZAC getDocOZAC() {
		return docOZAC;
	}

	public void setDocOZAC(DocOZAC docOZAC) {
		this.docOZAC = docOZAC;
	}

	public OPVF() {}
	
	public OPVF(ParserOPVF parser,
			PackFromCombank packFromCombank,
			DocOZAC docOZAC) {
		super(parser);
		
		this.packFromCombank = packFromCombank;
		this.docOZAC = docOZAC;
	}
	
	public OPVF(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			File file,
			Element rootElement,
			Long outNumber,
			Boolean isRead,
			String resultOfRead,
			List<String> namesMainFile,
			PackFromCombank packFromCombank,
			DocOZAC docOZAC) {
		
		super(year, 
			month, 
			deliveryOrg, 
			numberPack, 
			file, 
			rootElement, 
			outNumber,
			isRead, 
			resultOfRead,
			namesMainFile);
		
		this.packFromCombank = packFromCombank;
		this.docOZAC = docOZAC;
	}
	
	@Override
	public void init() throws Exception {
		new Throwable("no imlements");
		
	}
	
	

}
