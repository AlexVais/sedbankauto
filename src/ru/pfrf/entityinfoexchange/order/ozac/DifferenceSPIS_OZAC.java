package ru.pfrf.entityinfoexchange.order.ozac;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.TypeDifferenceRecipient;
import ru.pfrf.classifier.TypeDifferenceSPIS;
import ru.pfrf.entityinfoexchange.order.spis.SPIS;
import ru.pfrf.person.DifferenceRecipient;
import ru.pfrf.person.RecipientOZAC;

@Entity
@Table(name = "differences_spis_ozac")
public class DifferenceSPIS_OZAC implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@ManyToOne
	@JoinColumn(name = "TypeDifferences")
	private TypeDifferenceSPIS typeDifferenceSPIS;
	
	@ManyToOne
	@JoinColumn(name = "ozac")
	private OZAC ozac;
	
	@Transient
	private SPIS spis;

	public SPIS getSpis() {
		return spis;
	}

	public void setSpis(SPIS spis) {
		this.spis = spis;
	}

	public OZAC getOzac() {
		return ozac;
	}

	public void setOzac(OZAC ozac) {
		this.ozac = ozac;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TypeDifferenceSPIS getTypeDifferenceSPIS() {
		return typeDifferenceSPIS;
	}

	public void setTypeDifferenceSPIS(TypeDifferenceSPIS typeDifferenceSPIS) {
		this.typeDifferenceSPIS = typeDifferenceSPIS;
	}

	public DifferenceSPIS_OZAC() {}
	
	public DifferenceSPIS_OZAC(OZAC ozac, SPIS spis, TypeDifferenceSPIS typeDifferenceSPIS) {
		this.ozac = ozac;
		this.spis = spis;
		this.typeDifferenceSPIS = typeDifferenceSPIS;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		DifferenceSPIS_OZAC dif = (DifferenceSPIS_OZAC) obj;
		return new EqualsBuilder()
		                 .append(this.ozac, dif.ozac)
		                 .append(this.spis, dif.spis)
		                 .append(this.typeDifferenceSPIS, dif.typeDifferenceSPIS)
		                 .isEquals();
	}
	
	@Override
	public String toString() {
		return this.ozac.getFileName() + 
				" | " + this.spis.getFileName() +
				" | " + this.typeDifferenceSPIS.getName();
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	
	public String whereIsDifference() {
		switch (this.typeDifferenceSPIS.getName()) {
		case(TypeDifferenceSPIS.COUNTS_RECIPIENTS_BY_TAG) : 
			return this.ozac.getCountRecipients() + " != " + this.spis.getCountRecipients();
		
		case(TypeDifferenceSPIS.COUNTS_RECIPIENTS_BY_DOC) : 
			return this.ozac.getSetRecipientOZAC().size() + " != " + this.spis.getSetRecipientListTransfer().size();
		
		case(TypeDifferenceSPIS.SUM) : 
			return this.ozac.getSum() + " != " + this.spis.getSum();
		}
		return null;
	}
}
