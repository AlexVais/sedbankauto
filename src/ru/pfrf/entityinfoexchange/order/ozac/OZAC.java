package ru.pfrf.entityinfoexchange.order.ozac;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import main.MainFrame;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.reflect.TypeUtilsTest.This;
import org.dom4j.Element;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.TypeDifferenceRecipient;
import ru.pfrf.classifier.TypeDifferenceSPIS;
import ru.pfrf.entityinfoexchange.OutMainFile;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.osmp.OSMPByArea;
import ru.pfrf.entityinfoexchange.order.spis.SPIS;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOZAC;
import ru.pfrf.payment.Payment;
import ru.pfrf.payment.PaymentOZAC;
import ru.pfrf.payment.PaymentSPIS;
import ru.pfrf.person.DifferenceRecipient;
import ru.pfrf.person.Recipient;
import ru.pfrf.person.RecipientDead;
import ru.pfrf.person.RecipientListTransfer;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.person.RecipientSPIS;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ReceivingOZACAdapter;

@Entity
@Table(name = "ozac")
public class OZAC extends OutMainFile{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "Sum")
	protected BigDecimal sum;
	
	@Column(name = "SumCredited")
	protected BigDecimal sumCredited;
	
	@Column(name = "CountRecipientsCredited")
	protected Integer countRecipientsCredited;
	
	@Column(name = "SumNotCredited")
	protected BigDecimal sumNotCredited;
	
	@Column(name = "CountRecipientsNotCredited")
	protected Integer countRecipientsNotCredited;
	
	@Column(name = "isErrorReceiv")
	protected Boolean isErrorReceiv = false;
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "ozac", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected Set<RecipientOZAC> setRecipientOZAC = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToOne(mappedBy = "ozac", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected DocOZAC docOZAC;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fileOfPackFromCombank")
	protected FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ReceivingRCA")
	protected ReceivingOZACAdapter receivingOZACAdapter;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;

	@OneToMany(mappedBy = "ozac", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<DifferenceSPIS_OZAC> setDifferenceSPIS_OZAC = new HashSet<>();
	
	@OneToMany(mappedBy = "ozac", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<SeparatedOZAC> setSeparatedOZAC = new HashSet<>();

	public Set<SeparatedOZAC> getSetSeparatedOZAC() {
		return setSeparatedOZAC;
	}


	public void setSetSeparatedOZAC(Set<SeparatedOZAC> setSeparatedOZAC) {
		this.setSeparatedOZAC = setSeparatedOZAC;
	}


	public Boolean getIsErrorReceiv() {
		return isErrorReceiv;
	}


	public void setIsErrorReceiv(Boolean isErrorReceiv) {
		this.isErrorReceiv = isErrorReceiv;
	}


	public ReceivingOZACAdapter getReceivingOZACAdapter() {
		return receivingOZACAdapter;
	}


	public Set<DifferenceSPIS_OZAC> getSetDifferenceSPIS_OZAC() {
		return setDifferenceSPIS_OZAC;
	}


	public void setSetDifferenceSPIS_OZAC(
			Set<DifferenceSPIS_OZAC> setDifferenceSPIS_OZAC) {
		this.setDifferenceSPIS_OZAC = setDifferenceSPIS_OZAC;
	}


	public void setReceivingOZACAdapter(ReceivingOZACAdapter receivingOZACAdapter) {
		this.receivingOZACAdapter = receivingOZACAdapter;
	}


	public FileOfPackFromCombankAdapter getFileOfPackFromCombankAdapter() {
		return fileOfPackFromCombankAdapter;
	}


	public void setFileOfPackFromCombankAdapter(
			FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter) {
		this.fileOfPackFromCombankAdapter = fileOfPackFromCombankAdapter;
	}


	public DocOZAC getDocOZAC() {
		return docOZAC;
	}


	public void setDocOZAC(DocOZAC docOZAC) {
		this.docOZAC = docOZAC;
	}


	public BigDecimal getSum() {
		return sum;
	}


	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}


	public BigDecimal getSumCredited() {
		return sumCredited;
	}


	public void setSumCredited(BigDecimal sumCredited) {
		this.sumCredited = sumCredited;
	}


	public Integer getCountRecipientsCredited() {
		return countRecipientsCredited;
	}


	public void setCountRecipientsCredited(Integer countRecipientsCredited) {
		this.countRecipientsCredited = countRecipientsCredited;
	}


	public BigDecimal getSumNotCredited() {
		return sumNotCredited;
	}


	public void setSumNotCredited(BigDecimal sumNotCredited) {
		this.sumNotCredited = sumNotCredited;
	}


	public Integer getCountRecipientsNotCredited() {
		return countRecipientsNotCredited;
	}


	public void setCountRecipientsNotCredited(Integer countRecipientsNotCredited) {
		this.countRecipientsNotCredited = countRecipientsNotCredited;
	}

	public Set<RecipientOZAC> getSetRecipientOZAC() {
		return setRecipientOZAC;
	}


	public void setSetRecipientOZAC(Set<RecipientOZAC> setRecipientOZAC) {
		this.setRecipientOZAC = setRecipientOZAC;
	}


	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}

	public OZAC() {}
	
	public OZAC(ParserOZAC parser, 
			PackFromCombank packFromCombank, 
			DocOZAC docOZAC) {
		super(parser);
		
		this.sum = parser.getSum();
		this.sumCredited = parser.getSumCredited();
		this.countRecipientsCredited = parser.getCountRecipientsCredited();
		this.sumNotCredited = parser.getSumNotCredited();
		this.countRecipientsNotCredited = parser.getCountRecipientsNotCredited();
		
		this.packFromCombank = packFromCombank;
		this.docOZAC = docOZAC;
	}
	
	public OZAC(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack, 
			File file,
			Element rootElement,
			Long outNumber,
			Boolean isRead,
			String resultOfRead,
			PackFromCombank packFromCombank,
			DocOZAC docOZAC,
			BigDecimal sum,
			BigDecimal sumCredited,
			BigDecimal sumNotCredited,
			Integer countRecipientsCredited,
			Integer countRecipientsNotCredited) {
		
		super(year, 
			month, 
			deliveryOrg, 
			numberPack, 
			file, 
			rootElement, 
			outNumber,
			isRead,
			resultOfRead
			);
		
		this.packFromCombank = packFromCombank;
		this.docOZAC = docOZAC;
		
		this.sum = sum;
		this.sumCredited = sumCredited;
		this.sumNotCredited = sumNotCredited;
		this.countRecipientsCredited = countRecipientsCredited;
		this.countRecipientsNotCredited = countRecipientsNotCredited; this.deleteDigitalSignature(file);
	}
	

	@Override
	public void init() throws Exception {
		try {
			@SuppressWarnings("unchecked")
			List<Element> listElReportRecipient = rootElement.element("������������������������")
					        					.element("�����_�_����������_�_��_����������_����")
					        					.elements("�����������������");
			int i = 1;
			for (Element element : listElReportRecipient) {
				RecipientOZAC recipientOZAC = new RecipientOZAC(element, this);
				recipientOZAC.parse();
				this.setRecipientOZAC.add(recipientOZAC);
				
				
				//�������� ������� ���������� ������ � �������
				if (recipientOZAC.getNumberInArray() != i) 
					throw new Exception("������ � ������� ���������� ������ � �������: " + i + " != " + recipientOZAC.getNumberInArray() + " " + recipientOZAC.FIO());
			
				i++;
			}
			
			//���������� �����������
			this.countRecipients = Integer.valueOf(rootElement.element("������������������������")
											  				  .element("�����_�_����������_�_��_����������_����")
											  				  .element("���������������������").getText());
			
		} catch (Exception e) {
			throw new Exception("\n������ ��� �������� ozac " + this.fileName, e);
		}
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString() + " | ");
		sb.append(String.valueOf(this.sum) + " | ");
		sb.append(String.valueOf(this.sumCredited) + " | ");
		sb.append(String.valueOf(this.sumNotCredited) + " | ");
		sb.append(String.valueOf(this.countRecipientsCredited) + " | ");
		sb.append(String.valueOf(this.countRecipientsNotCredited) + " | ");
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		OZAC ozac = (OZAC) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(ozac))
		                 .append(this.sum, ozac.sum)
		                 .append(this.sumCredited, ozac.sumCredited)
		                 .append(this.sumNotCredited, ozac.sumNotCredited)
		                 .append(this.countRecipientsCredited, ozac.countRecipientsCredited)
		                 .append(this.countRecipientsNotCredited, ozac.countRecipientsNotCredited)
		                 .isEquals();
	}
	
	
	/**
	 * �������� �����
	 * @throws Exception 
	 */
	private void checkTotalSumByTags() throws Exception {
		//����� �� ������� ������ ���� ����� ����� ����������� � ������������� ����
		BigDecimal sumCreditedAndNotCredited = sumCredited.add(sumNotCredited);
		if (sum.compareTo(sumCreditedAndNotCredited) != 0) {
			throw new Exception("����� �� ������� �� ����� ������ �� ����� ������ ����������� � ������������� �����������: " + sum + " != " + sumCreditedAndNotCredited);
		}
			
	}
	
	
	/**
	 * �������� ���������� ����������� �� ����� ������
	 * @throws Exception 
	 */
	private void checkCountRecipientsByTags() throws Exception {
		//����� ���������� ����������� ������ ���� ����� ����� ����������� � ������������� 
		if (countRecipients != (countRecipientsCredited + countRecipientsNotCredited)) {
			throw new Exception("����� ����������� �� ����� ������ �� ����� ����� ����������� � �������������  ");
		}
			
	}
	
	/**
	 * �������� ������������ ����� � ������� � �����������
	 * @throws Exception 
	 */
	private void checkAmountPayment() throws Exception {
		for(RecipientOZAC recipient : this.setRecipientOZAC) {
			for (Payment payment : recipient.getSetPaymentOZAC()) {
				//���� ����� ������� <= 0, �� ������ 
				if (payment.getAmountPayment().compareTo(new BigDecimal(0)) < 1)
					throw new Exception("����� ������� �� ��������� (����� <= 0). ����� � �������: " + recipient.getNumberInArray());
			}
		}
	}
	
	/**
	 * ��������. ����� ���� � �������� ������ ���� ����� ����� ���� � �������
	 * @throws Exception 
	 */
	private void compareSumToSumAmountDelivery() throws Exception {
		BigDecimal sumAmountDelivery = new BigDecimal(0);
		
		for(RecipientOZAC recipient : this.setRecipientOZAC) {
			sumAmountDelivery = sumAmountDelivery.add(recipient.getAmountDelivery());
		}
		
		if (sumAmountDelivery.compareTo(this.sum) != 0)
			throw new Exception("����� ���� � �������� �� ����� �������� ����� �� ����. " + sumAmountDelivery + " != " + this.sum);
	}
	
	/**
	 * ������ ������������ ���������� ����������� �� ��������� � ����������� ����������� �� ����
	 * @throws Exception
	 */
	private void compareCountRecipientsToCountByTag() throws Exception {
		if (this.setRecipientOZAC.size() != this.countRecipients) {
			System.out.println(this.setRecipientOZAC);
			throw new Exception("����������� ���������� ����������� � ��������� �� ����� ���������� �� ��������� ����. " + this.setRecipientOZAC.size() + " != " + this.countRecipients);
		}
	}
	
	
	public void check() throws Exception {
		try {
			//�������� ������ ���������
			checkTotalSumByTags();
			checkCountRecipientsByTags();
			
			//�������� �����������
			compareCountRecipientsToCountByTag();
			checkAmountPayment();
			compareSumToSumAmountDelivery();
		} catch (Throwable t) {
			throw new Exception("������ �������� OZAC: " + this.fileName, t);
		}
		
	}
	
	public void initByParser(ParserOZAC parser) {
		this.year = parser.getYear();
		this.month = parser.getMonth();
		this.deliveryOrg = parser.getDeliveryOrg();
		this.numberPack = parser.getPackNumber();
		this.file = parser.getFile();
		this.rootElement = parser.getRootElement();
		this.sum = parser.getSum();
		this.sumCredited = parser.getSumCredited();
		this.countRecipientsCredited = parser.getCountRecipientsCredited();
		this.sumNotCredited = parser.getSumNotCredited();
		this.countRecipientsNotCredited = parser.getCountRecipientsNotCredited();
	}
	
	
	
	public void compareToSpis(SPIS spis) throws Exception {
		List<String> listStrTypeDifferenceSPIS = new ArrayList<>();
		
		//��������� ���������� ����������� �� ����� � ����������
		if (!this.countRecipients.equals(spis.getCountRecipients()))
			listStrTypeDifferenceSPIS.add(TypeDifferenceSPIS.COUNTS_RECIPIENTS_BY_TAG);
		
		//��������� ���������� ����������� �� ���������� (XML ���������)
		if (this.setRecipientOZAC.size() != spis.getSetRecipientListTransfer().size())
			listStrTypeDifferenceSPIS.add(TypeDifferenceSPIS.COUNTS_RECIPIENTS_BY_DOC);
			
		//��������� ���� 
		if (this.sum.compareTo(spis.getSum()) != 0)
			listStrTypeDifferenceSPIS.add(TypeDifferenceSPIS.SUM);
		
		
		//���������� OZAC ��������
		for(String strTypeDifSPIS : listStrTypeDifferenceSPIS) {
			TypeDifferenceSPIS typeDifSPIS = MainFrame.dictTypeDifferenceSPIS.get(strTypeDifSPIS);
			DifferenceSPIS_OZAC diff = new DifferenceSPIS_OZAC(this, spis, typeDifSPIS);
			this.setDifferenceSPIS_OZAC.add(diff);
		}
		
		//���� ���� ������, �� �����
		if (!listStrTypeDifferenceSPIS.isEmpty()) return;
		
		////////////////////////��������� �����������
		List<RecipientOZAC> listRecipientsOZAC = new ArrayList<>(this.setRecipientOZAC);
		Collections.sort(listRecipientsOZAC, Recipient.comparatorByNumberInArrayAsc());
		
		List<RecipientListTransfer> listRecipientsLT = new ArrayList<>(spis.getSetRecipientListTransfer());
		Collections.sort(listRecipientsLT, Recipient.comparatorByNumberInArrayAsc());
		Iterator<RecipientListTransfer> itLT = listRecipientsLT.iterator();
	
		for (RecipientOZAC recipientOZAC : listRecipientsOZAC) {
			RecipientListTransfer recipientListTransfer = itLT.next();
			
			List<String> listStrTypeDif = new ArrayList<>();
			//��������� �� ������ � �������
			if (!recipientOZAC.getNumberInArray().equals(recipientListTransfer.getNumberInArray())) 
				listStrTypeDif.add(TypeDifferenceRecipient.NUMBER_IN_ARRAY);
			
			//�� ������ ���������� ����
			if (!recipientOZAC.getNumberVD().equals(recipientListTransfer.getNumberVD())) 
				listStrTypeDif.add(TypeDifferenceRecipient.NUMBER_VD);
				
				
			//�� ������
			if (!recipientOZAC.getArea().equals(recipientListTransfer.getArea()))
				listStrTypeDif.add(TypeDifferenceRecipient.AREA);
			
			//��������� �� �������
			if (!recipientOZAC.getSurname().equals(recipientListTransfer.getSurname()))
				listStrTypeDif.add(TypeDifferenceRecipient.SURNAME);
			
			//��������� �� �����
			if (!recipientOZAC.getName().equals(recipientListTransfer.getName()))
				listStrTypeDif.add(TypeDifferenceRecipient.NAME);
			
			//��������� �� ��������
			if (!recipientOZAC.getPatr().equals(recipientListTransfer.getPatr()))
				listStrTypeDif.add(TypeDifferenceRecipient.PATR);
			
			//��������� �� ����� � ��������
			if (!recipientOZAC.getAmountDelivery().equals(recipientListTransfer.getAmountDelivery()))
				listStrTypeDif.add(TypeDifferenceRecipient.AMOUNT_DELIVERY);
			
			//////��������� ������/////
			//���������� �� �����
			List<PaymentOZAC> listPaymentsOZAC = new ArrayList<>(recipientOZAC.getSetPaymentOZAC());
			Collections.sort(listPaymentsOZAC, Payment.comparatorByAmountPaymentAndSignPayAsc());
			List<PaymentSPIS> listPaymentsSPIS = new ArrayList<>(recipientListTransfer.getSetPaymentSPIS());
			Collections.sort(listPaymentsSPIS, Payment.comparatorByAmountPaymentAndSignPayAsc());
			Iterator<PaymentSPIS> itPaySPIS = listPaymentsSPIS.iterator();
			
			for(Payment paymentOZAC : listPaymentsOZAC) {
				Payment paymentSPIS = itPaySPIS.next();
				//�� �����
				if (paymentOZAC.getAmountPayment().compareTo(paymentSPIS.getAmountPayment()) != 0)
					listStrTypeDif.add(TypeDifferenceRecipient.AMOUNT_PAYMENT);
				
				//�� �������� �������
				if(!paymentOZAC.getSignPayment().equals(paymentSPIS.getSignPayment()))
					listStrTypeDif.add(TypeDifferenceRecipient.SIGN_PAY);
				
				//�� ����������������� � ����������������
				if (!(paymentOZAC.getDateStartPeriod().equals(paymentSPIS.getDateStartPeriod()) 
					  && paymentOZAC.getDateEndPeriod().equals(paymentSPIS.getDateEndPeriod())) )
					listStrTypeDif.add(TypeDifferenceRecipient.PERIOD_PAY);
				
				//�� ���� ������� �� ��
				if (!paymentOZAC.getViewPayPZ().equals(paymentSPIS.getViewPayPZ()))
					listStrTypeDif.add(TypeDifferenceRecipient.PZ);
			}
			
			//���������� RecipientOZAC ��������
			for(String strTypeDif : listStrTypeDif) {
				TypeDifferenceRecipient typeDifRec = MainFrame.dictTypeDifferenceRecipient.get(strTypeDif);
				DifferenceRecipient diff = new DifferenceRecipient(recipientOZAC, recipientListTransfer, typeDifRec);
				recipientOZAC.getSetDifferenceRecipients().add(diff);
			}
		
		}
		
		//���� ���� �����������, ���������� ����������
		if (!this.getSetDifferenceSPIS_OZAC().isEmpty()) {
			//������������ ������ � �������������
			StringBuilder sbDiff = new StringBuilder();
			for(DifferenceSPIS_OZAC diffSPIS : this.getSetDifferenceSPIS_OZAC())
				sbDiff.append(diffSPIS.getTypeDifferenceSPIS().getName() + ": " + diffSPIS.whereIsDifference() + " | ");

			throw new Exception(this.getFileName() + " ������� ����������� ����� SPIS � OZAC: " + sbDiff);
		}
			
			
		//����� ����������� � �������������
		List<RecipientOZAC> listRecipientOZAC = new ArrayList<>();
		for(RecipientOZAC recipientOZAC : this.getSetRecipientOZAC())
			if (!recipientOZAC.getSetDifferenceRecipients().isEmpty())
				listRecipientOZAC.add(recipientOZAC);
		
		//������������ ������ � �������������
		if (!listRecipientOZAC.isEmpty()) {
			StringBuilder sbDiffRec = new StringBuilder();
			for(RecipientOZAC recipientOZAC : listRecipientOZAC) {
				sbDiffRec.append(recipientOZAC.getSetDifferenceRecipients());
			}
			throw new Exception("������� ����������� ����� ������������ SPIS � OZAC: " + sbDiffRec);
		}
				
	}
	

}
