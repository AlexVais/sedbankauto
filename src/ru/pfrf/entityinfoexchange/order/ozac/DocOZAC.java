package ru.pfrf.entityinfoexchange.order.ozac;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.Order;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.process.ReceivingOSMP;
import ru.pfrf.process.ReceivingOZAC;
import ru.pfrf.process.ReceivingPOSD;

@Entity
@Table(name = "doc_ozac")
public class DocOZAC extends Order{
	private static final long serialVersionUID = 1L;
	
	@BatchSize(size = 200)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "opvf")
	protected OPVF opvf;
	
	@BatchSize(size = 200)
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ozac")
	protected OZAC ozac;
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "docPOSD")
	protected DocPOSD docPOSD;
	
	@OneToOne(mappedBy = "docOZAC", fetch = FetchType.LAZY)
	protected DocPOOD docPOOD;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SPRA")
	protected SPRA spra;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiving_new")
	protected ReceivingOZAC receivingOZAC;
	
	@Column(name = "dateReceivVipnet")
	protected Date dateReceivVipnet;

	public Date getDateReceivVipnet() {
		return dateReceivVipnet;
	}

	public void setDateReceivVipnet(Date dateReceivVipnet) {
		this.dateReceivVipnet = dateReceivVipnet;
	}

	public ReceivingOZAC getReceivingOZAC() {
		return receivingOZAC;
	}

	public void setReceivingOZAC(ReceivingOZAC receivingOZAC) {
		this.receivingOZAC = receivingOZAC;
	}

	public DocPOOD getDocPOOD() {
		return docPOOD;
	}

	public void setDocPOOD(DocPOOD docPOOD) {
		this.docPOOD = docPOOD;
	}

	public DocPOSD getDocPOSD() {
		return docPOSD;
	}

	public void setDocPOSD(DocPOSD docPOSD) {
		this.docPOSD = docPOSD;
	}

	public SPRA getSpra() {
		return spra;
	}

	public void setSpra(SPRA spra) {
		this.spra = spra;
	}

	public OPVF getOpvf() {
		return opvf;
	}

	public void setOpvf(OPVF opvf) {
		this.opvf = opvf;
	}

	public OZAC getOzac() {
		return ozac;
	}

	public void setOzac(OZAC ozac) {
		this.ozac = ozac;
	}
	
	public DocOZAC() {}
	
	public DocOZAC(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			Integer area,
			ReceivingOZAC receivingOZAC) {
		super(year, month, deliveryOrg, numberPack, area);
		
		this.receivingOZAC = receivingOZAC;
		
	}
	
	@Override
	public String toString() {
		return this.opvf.getId() + 
				" | " + this.ozac.getId() + 
				" | " + this.docPOSD.getId();
	}
	
	
	@Override 
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		DocOZAC docOZAC = (DocOZAC) obj;
		return new EqualsBuilder()
		                 .append(this.opvf.getId(), docOZAC.opvf.getId())
		                 .append(this.ozac.getId(), docOZAC.ozac.getId())
		                 .append(this.docPOSD.getId(), docOZAC.docPOSD.getId())
		                 .isEquals();
	}

}
