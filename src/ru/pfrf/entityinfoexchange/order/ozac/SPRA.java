package ru.pfrf.entityinfoexchange.order.ozac;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.OutFileOfIE;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserSPRA;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ReceivingOZACAdapter;

@Entity
@Table(name = "spra")
public class SPRA extends OutFileOfIE {
	private static final long serialVersionUID = 1L;

	@BatchSize(size = 200)
	@OneToOne(mappedBy = "opvf", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected DocOZAC docOZAC;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fileOfPackFromCombank")
	protected FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ReceivingRCA")
	protected ReceivingOZACAdapter receivingOZACAdapter;
	
	public ReceivingOZACAdapter getReceivingOZACAdapter() {
		return receivingOZACAdapter;
	}

	public void setReceivingOZACAdapter(ReceivingOZACAdapter receivingOZACAdapter) {
		this.receivingOZACAdapter = receivingOZACAdapter;
	}

	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}

	public FileOfPackFromCombankAdapter getFileOfPackFromCombankAdapter() {
		return fileOfPackFromCombankAdapter;
	}

	public void setFileOfPackFromCombankAdapter(
			FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter) {
		this.fileOfPackFromCombankAdapter = fileOfPackFromCombankAdapter;
	}

	public DocOZAC getDocOZAC() {
		return docOZAC;
	}

	public void setDocOZAC(DocOZAC docOZAC) {
		this.docOZAC = docOZAC;
	}
	
	public SPRA() {}
	
	public SPRA(ParserSPRA parser, 
			PackFromCombank packFromCombank, 
			DocOZAC docOZAC) {
		super(parser);
		
		this.packFromCombank = packFromCombank;
		this.docOZAC = docOZAC;
	}

	@Override
	public void init() throws Throwable {
		new Exception("method not implements");
		
	}

}
