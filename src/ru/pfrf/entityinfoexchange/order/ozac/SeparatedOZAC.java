package ru.pfrf.entityinfoexchange.order.ozac;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.dom4j.Element;

import ru.pfrf.entityinfoexchange.OutMainFile;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOZAC;
import ru.pfrf.person.Recipient;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.person.RecipientSPIS;

@Entity
@Table(name = "separated_ozac")
public class SeparatedOZAC extends OutMainFile {
	private static final long serialVersionUID = 1L;
	
	
	@Column(name = "area")
	protected Integer area;
	
	@Column(name = "Sum")
	protected BigDecimal sum;
	
	@Column(name = "SumCredited")
	protected BigDecimal sumCredited;
	
	@Column(name = "CountRecipientsCredited")
	protected Integer countRecipientsCredited;
	
	@Column(name = "SumNotCredited")
	protected BigDecimal sumNotCredited;
	
	@Column(name = "CountRecipientsNotCredited")
	protected Integer countRecipientsNotCredited;
	
	@Column(name = "isMoved")
	protected Boolean isMoved;
	
	@Transient
	protected Set<RecipientSPIS> setRecipientsSPIS = new HashSet<>();
	
	@OneToMany(mappedBy = "separatedOZAC", fetch = FetchType.LAZY)
	protected Set<RecipientOZAC> setRecipientsOZAC = new HashSet<>();
	
	@ManyToOne
	@JoinColumn(name = "ozac")
	protected OZAC ozac;
	
	public Boolean getIsMoved() {
		return isMoved;
	}

	public void setIsMoved(Boolean isMoved) {
		this.isMoved = isMoved;
	}

	public OZAC getOzac() {
		return ozac;
	}

	public void setOzac(OZAC ozac) {
		this.ozac = ozac;
	}

	public void setSetRecipientsOZAC(Set<RecipientOZAC> setRecipientsOZAC) {
		this.setRecipientsOZAC = setRecipientsOZAC;
	}

	public Set<RecipientOZAC> getSetRecipientsOZAC() {
		return setRecipientsOZAC;
	}


	public Set<RecipientSPIS> getSetRecipientsSPIS() {
		return setRecipientsSPIS;
	}

	public void setSetRecipientsSPIS(Set<RecipientSPIS> setRecipientsSPIS) {
		this.setRecipientsSPIS = setRecipientsSPIS;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public BigDecimal getSumCredited() {
		return sumCredited;
	}

	public void setSumCredited(BigDecimal sumCredited) {
		this.sumCredited = sumCredited;
	}

	public Integer getCountRecipientsCredited() {
		return countRecipientsCredited;
	}

	public void setCountRecipientsCredited(Integer countRecipientsCredited) {
		this.countRecipientsCredited = countRecipientsCredited;
	}

	public BigDecimal getSumNotCredited() {
		return sumNotCredited;
	}

	public void setSumNotCredited(BigDecimal sumNotCredited) {
		this.sumNotCredited = sumNotCredited;
	}

	public Integer getCountRecipientsNotCredited() {
		return countRecipientsNotCredited;
	}

	public void setCountRecipientsNotCredited(Integer countRecipientsNotCredited) {
		this.countRecipientsNotCredited = countRecipientsNotCredited;
	}

	public SeparatedOZAC() {}
	
	public SeparatedOZAC(ParserOZAC parser) {
		super(parser);
		
		this.area = parser.getArea();
		this.countRecipientsCredited = parser.getCountRecipientsCredited();
		this.countRecipientsNotCredited = parser.getCountRecipientsNotCredited();
		this.sum = parser.getSum();
		this.sumCredited = parser.getSumCredited();
		this.sumNotCredited = parser.getSumNotCredited();
	}

	@Override
	public void init() throws Exception {
		try {
			@SuppressWarnings("unchecked")
			List<Element> listElReportRecipient = rootElement.element("������������������������")
					        					.element("�����_�_����������_�_��_����������_����")
					        					.elements("�����������������");
			
			for (Element element : listElReportRecipient) {
				RecipientSPIS recipient = new RecipientSPIS(element);
				recipient.parse();
				this.setRecipientsSPIS.add(recipient);
				
			}
			
			//���������� �����������
			this.countRecipients = Integer.valueOf(rootElement.element("������������������������")
											  				  .element("�����_�_����������_�_��_����������_����")
											  				  .element("���������������������").getText());
			
		} catch (Exception e) {
			throw new Exception("������ ��� �������� SeparatedOZAC " + this.fileName, e);
		}
		
	};
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString() + " | ");
		sb.append(String.valueOf(this.area) + " | ");
		sb.append(String.valueOf(this.sum) + " | ");
		sb.append(String.valueOf(this.sumCredited) + " | ");
		sb.append(String.valueOf(this.sumNotCredited) + " | ");
		sb.append(String.valueOf(this.countRecipientsCredited) + " | ");
		sb.append(String.valueOf(this.countRecipientsNotCredited) + " | ");
		
		return sb.toString();
	}
	
	
}
