package ru.pfrf.entityinfoexchange.order.ozac;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.ConfirmRead;
import ru.pfrf.entityinfoexchange.notification.NotificationPOOD;

@Entity
@Table(name = "doc_pood")
public class DocPOOD extends ConfirmRead {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "packName")
	private String packName;
	
	@Column(name = "postfix")
	private Integer postfix;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "docOZAC")
	private DocOZAC docOZAC;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "notificationPOOD")
	protected NotificationPOOD notificationPOOD;

	public NotificationPOOD getNotificationPOOD() {
		return notificationPOOD;
	}

	public void setNotificationPOOD(NotificationPOOD notificationPOOD) {
		this.notificationPOOD = notificationPOOD;
	}

	public Integer getPostfix() {
		return postfix;
	}

	public void setPostfix(Integer postfix) {
		this.postfix = postfix;
	}

	public String getPackName() {
		return packName;
	}

	public void setPackName(String packName) {
		this.packName = packName;
	}

	public DocPOOD() {}

	public DocOZAC getDocOZAC() {
		return docOZAC;
	}

	public void setDocOZAC(DocOZAC docOZAC) {
		this.docOZAC = docOZAC;
	}
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + packName + 
				" | " + String.valueOf(postfix);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		DocPOOD doc = (DocPOOD) obj;
		return new EqualsBuilder()
					     .appendSuper(super.equals(doc))
					     .append(this.packName, doc.packName)
		                 .append(this.postfix, doc.postfix)
		                 .isEquals();
	}
	
	
	

}
