package ru.pfrf.entityinfoexchange.order.ozac;

import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.ConfirmRead;
import ru.pfrf.entityinfoexchange.DocOfInfoExchange;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSD;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ReceivingPOSD;
import ru.pfrf.process.ReceivingPOSDAdapter;
import ru.pfrf.process.ReceivingPacksFromCombanks;
import ru.pfrf.register.CreatedRegisterTF;

@Entity
@Table(name = "posd")
public class DocPOSD extends ConfirmRead {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "postfix")
	private Integer postfix;
	
	@OneToOne(mappedBy = "docPOSD", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected DocOZAC docOZAC;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ReceivingPOSD")
	protected ReceivingPOSDAdapter receivingPOSDAdapter;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receiving_new")
	protected ReceivingPOSD receivingPOSD;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fileOfPackFromCombank")
	protected FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "registerTF")
	protected CreatedRegisterTF createdRegisterTF;
	
	public Integer getPostfix() {
		return postfix;
	}

	public void setPostfix(Integer postfix) {
		this.postfix = postfix;
	}

	public CreatedRegisterTF getCreatedRegisterTF() {
		return createdRegisterTF;
	}

	public void setCreatedRegisterTF(CreatedRegisterTF createdRegisterTF) {
		this.createdRegisterTF = createdRegisterTF;
	}

	public ReceivingPOSDAdapter getReceivingPOSDAdapter() {
		return receivingPOSDAdapter;
	}

	public void setReceivingPOSDAdapter(ReceivingPOSDAdapter receivingPOSDAdapter) {
		this.receivingPOSDAdapter = receivingPOSDAdapter;
	}

	public ReceivingPOSD getReceivingPOSD() {
		return receivingPOSD;
	}

	public void setReceivingPOSD(ReceivingPOSD receivingPOSD) {
		this.receivingPOSD = receivingPOSD;
	}

	public FileOfPackFromCombankAdapter getFileOfPackFromCombankAdapter() {
		return fileOfPackFromCombankAdapter;
	}

	public void setFileOfPackFromCombankAdapter(
			FileOfPackFromCombankAdapter fileOfPackFromCombankAdapter) {
		this.fileOfPackFromCombankAdapter = fileOfPackFromCombankAdapter;
	}

	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}

	public DocOZAC getDocOZAC() {
		return docOZAC;
	}

	public void setDocOZAC(DocOZAC docOZAC) {
		this.docOZAC = docOZAC;
	}

	public DocPOSD() {}
	
	public DocPOSD(ParserPOSD parser, 
			PackFromCombank packFromCombank,
			ReceivingPOSD receivingPOSD,
			Date dateSend) {
		super(parser, dateSend, parser.getDecision());
		
		this.packFromCombank = packFromCombank;
		this.receivingPOSD = receivingPOSD;
	}
	
	@Override
	public String toString() {
		return super.toString() +
				" | " + String.valueOf(postfix);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		DocPOSD doc = (DocPOSD) obj;
		return new EqualsBuilder()
					     .appendSuper(super.equals(doc))
		                 .append(this.postfix, doc.postfix)
		                 .isEquals();
	}

}
