package ru.pfrf.entityinfoexchange.order.osmp;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.*;

import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.OutMainFile;
import ru.pfrf.person.RecipientDead;

@Entity
@Table(name = "osmp_by_area")
public class OSMPByArea extends FileOfInfoExchange {
	private static final long serialVersionUID = 1L;
	
	@OneToMany(mappedBy = "osmpByArea", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<RecipientDead> listRecipientDead = new ArrayList<>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "osmp")
	private OSMP osmp;
	
	@Column(name = "countRecipients")
	private Integer countRecipients;
	
	@Column(name = "area")
	protected Integer area;

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public Integer getCountRecipients() {
		return countRecipients;
	}

	public void setCountRecipients(Integer countRecipients) {
		this.countRecipients = countRecipients;
	}

	public OSMP getOsmp() {
		return osmp;
	}

	public void setOsmp(OSMP osmp) {
		this.osmp = osmp;
	}

	public List<RecipientDead> getListRecipientDead() {
		return listRecipientDead;
	}

	public void setListRecipientDead(List<RecipientDead> listRecipientDead) {
		this.listRecipientDead = listRecipientDead;
	}

	public OSMPByArea() {}
	
	public OSMPByArea(OSMP osmp, 
			File file, 
			Integer area, 
			List<RecipientDead> listRecipientDead) {
		
		super (osmp.getYear(), 
			osmp.getMonth(), 
			osmp.getDeliveryOrg(), 
			null,
			file, 
			null);
		
		this.area = area;
		this.osmp = osmp;
		this.listRecipientDead = listRecipientDead;
		this.countRecipients = listRecipientDead.size();
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
	@Override
	public void init() throws Exception {
		throw new Exception("����� init �� ����������");
		
	}
	
	public static Comparator<OSMPByArea> comparatorByAreaAsc() {
		return new Comparator<OSMPByArea>() {
			@Override
			public int compare(OSMPByArea o1, OSMPByArea o2) {
				return o1.area.compareTo(o2.area);
			}
		};
	}

}
