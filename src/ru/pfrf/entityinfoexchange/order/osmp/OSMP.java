package ru.pfrf.entityinfoexchange.order.osmp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileAttribute;
import java.util.*;

import javax.persistence.*;

import main.Utils;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.Text;
import org.dom4j.io.SAXWriter;
import org.dom4j.tree.DefaultElement;
import org.dom4j.tree.DefaultText;
import org.dom4j.tree.FlyweightText;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.OutMainFile;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.Order;
import ru.pfrf.person.Person;
import ru.pfrf.person.Recipient;
import ru.pfrf.person.RecipientDead;

@Entity
@Table(name = "osmp")
public class OSMP extends OutMainFile {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "docOSMP")
	private DocOSMP docOSMP;
	
	@BatchSize(size = 20000)
	@OneToMany(mappedBy = "osmp", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<RecipientDead> listRecipientsDead = new ArrayList<>();
	
	@BatchSize(size = 20000)
	@OneToMany(mappedBy = "osmp", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<OSMPByArea> listOsmpByArea = new HashSet<>();
	
	@Column(name = "dateOfVipnet")
	protected Date dateOfVipnet;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;

	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}

	public void setListRecipientsDead(List<RecipientDead> listRecipientsDead) {
		this.listRecipientsDead = listRecipientsDead;
	}

	public Date getDateOfVipnet() {
		return dateOfVipnet;
	}

	public void setDateOfVipnet(Date dateOfVipnet) {
		this.dateOfVipnet = dateOfVipnet;
	}

	public Set<OSMPByArea> getListOsmpByArea() {
		return listOsmpByArea;
	}

	public void setListOsmpByArea(Set<OSMPByArea> listOsmpByArea) {
		this.listOsmpByArea = listOsmpByArea; 
	}

	public List<RecipientDead> getListRecipientsDead() {
		return listRecipientsDead;
	}

	public void setListRecipienstDead(List<RecipientDead> listRecipientDead) {
		this.listRecipientsDead = listRecipientDead;
	}

	public DocOSMP getDocOSMP() {
		return docOSMP;
	}

	public void setDocOSMP(DocOSMP docOSMP) {
		this.docOSMP = docOSMP;
	}

	public OSMP() {}
	
	public OSMP(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			File file,
			Element rootElement,
			Long outNumber,
			Boolean isRead,
			String resultOfRead,
			PackFromCombank packFromCombank,
			DocOSMP docOSMP, 
			Date dateOfVipnet) {
		
		super(year, 
			month, 
			deliveryOrg, 
			numberPack, 
			file, 
			rootElement, 
			outNumber, 
			isRead, 
			resultOfRead);
		
		this.packFromCombank = packFromCombank;
		this.docOSMP = docOSMP;
		this.dateOfVipnet = dateOfVipnet;
		
	}
	
	@Override
	public String toString() {
		return super.toString() + " | " + String.valueOf(dateOfVipnet);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		OSMP osmp = (OSMP) obj;
		return new EqualsBuilder().appendSuper(super.equals(osmp))
							      .append(this.dateOfVipnet, osmp.dateOfVipnet)
						   		  .isEquals();
	}
	
	
	public static void init(OSMP osmp) throws Exception {
		
		try {
			@SuppressWarnings("unchecked")
			List<Element> listStateAccount = osmp.rootElement.element("������������������������")
					        					.element("�����������_�_������_�����������")
					        					.elements("������������������������");
			for (Element element : listStateAccount) {
				RecipientDead recipientDead = new RecipientDead(osmp, element);
				recipientDead.parse();
				osmp.listRecipientsDead.add(recipientDead);
			}
			
			//���������� �����������
			osmp.countRecipients = osmp.listRecipientsDead.size();
			
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	

	@Override
	public void init() throws Exception {
		this.init(this);		
	}
	
	//�������� ����� ��� ������������� ������
	private void createFileOsmpByArea(List<RecipientDead> listRecipientsDead, 
						Path targetPath, 
						Integer area) throws Exception {
		
		String newFileName = this.fileName.replaceFirst("DIS-.*-DCK", "DIS-000-DCK");
		String areaCode = "038-" + new Integer(area + 1000).toString().substring(1) + "-001508";
		newFileName = newFileName.replaceFirst("ORG-.*-DIS", "ORG-" + areaCode + "-DIS");
		
		//�������� ������� OSMPByArea
		OSMPByArea osmpByArea = new OSMPByArea(this, new File(targetPath + "/" + newFileName), area, listRecipientsDead);
		this.listOsmpByArea.add(osmpByArea);
		
		Document document = this.rootElement.getDocument();
		
		//��������� ����� ����� � ���� ��������
		this.rootElement.element("��������").setText(newFileName);
		
		//���������� ���� <�����������_�_������_�����������>
		Element elementNotification = this.rootElement.element("������������������������").element("�����������_�_������_�����������");
		Element elementNumberInPack = elementNotification.element("�����������");
		Element elementCountRecipients = elementNotification.element("���������������������");
		Element elementDateDoc = elementNotification.element("�������������������");

		//�������� ����������� ���� �����������_�_������_�����������
		elementNotification.clearContent();
		
		elementNotification.addText("\n            ");
		elementNotification.add(elementNumberInPack);
		elementNotification.addText("\n");
		
		//���������� �����������
		  //���������� �� ������ � �������
		Collections.sort(listRecipientsDead, Recipient.comparatorByNumberInArrayAsc());
		
		for (RecipientDead recipientDead : listRecipientsDead) {
			elementNotification.addText("            ");
			elementNotification.add(recipientDead.getElement());
			elementNotification.addText("\n");
			
			//������ �� osmpByArea
			recipientDead.setOsmpByArea(osmpByArea);
		}
		
		elementCountRecipients.setText(String.valueOf(listRecipientsDead.size()));
		elementNotification.add(elementCountRecipients);
		elementNotification.addText("\n");
		elementNotification.add(elementDateDoc);
		elementNotification.addText("\n");
		
		//���������� xml
		Path newFilePath = Paths.get(targetPath + "/" + newFileName);
		String content = document.asXML();
		try (FileWriter fw = new FileWriter(newFilePath.toFile())) {
			fw.write(content, 0, content.length());
			
		} catch (Exception e) {
			throw new Exception(e);
		}
		
	}
	
	
	
	public void separation(Path targetPath, Boolean isReceived) throws Exception {
		Path separationPath = Paths.get(targetPath.toString() + "/" + this.fileName);
		if (!Files.exists(separationPath))
			Files.createDirectory(separationPath);
		
		//��������� ��������� �������
		Set<Integer> setRecipients = new HashSet<>();
		for (RecipientDead r : this.getListRecipientsDead()) {
			setRecipients.add(r.getArea());
		}
		
		//�������� ������� ������� �����������, ��������������� �� �������
		List<RecipientDead>[] arrayListRecipients = new List[setRecipients.size()];
		
		Collections.sort(this.getListRecipientsDead(), Recipient.comparatorByAreaAsc());
		int i = 0;
		for (Integer area : setRecipients) {
			//������� ����
			RecipientDead key = new RecipientDead();
			key.setArea(area);
			arrayListRecipients[i++] = Utils.binarySearch(this.getListRecipientsDead(), key, Recipient.comparatorByAreaAsc());
		}
	
		for (List<RecipientDead> listRecipients : arrayListRecipients) {
			
			//�������� ����� OSMP ��� ������������ ������
			Integer area = listRecipients.get(0).getArea();
			this.createFileOsmpByArea(listRecipients, targetPath, area);
		}
		
		
	}
	
}
