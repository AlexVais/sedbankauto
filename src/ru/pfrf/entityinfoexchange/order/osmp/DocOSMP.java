package ru.pfrf.entityinfoexchange.order.osmp;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.Order;
import ru.pfrf.process.ReceivingOSMP;

@Entity
@Table(name = "docosmp")
public class DocOSMP extends Order {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "receivingOSMP")
	protected ReceivingOSMP receivingOSMP;
	
	@BatchSize(size = 200)
	@OneToOne(mappedBy = "docOSMP", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected OPVZ opvz;
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "docOSMP", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<OSMP> listOSMP = new HashSet<OSMP>();
	
	@BatchSize(size = 200)
	@OneToOne(mappedBy = "docOSMP", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected DocPOOS docPOOS;

	public DocPOOS getDocPOOS() {
		return docPOOS; 
	}

	public void setDocPOOS(DocPOOS docPOOS) {
		this.docPOOS = docPOOS;
	}

	public OPVZ getOpvz() {
		return opvz;
	}

	public void setOpvz(OPVZ opvz) {
		this.opvz = opvz;
	}

	public Set<OSMP> getListOSMP() {
		return listOSMP;
	}

	public void setListOSMP(Set<OSMP> listOSMP) {
		this.listOSMP = listOSMP;
	}

	public ReceivingOSMP getReceivingOSMP() {
		return receivingOSMP;
	}

	public void setReceivingOSMP(ReceivingOSMP receivingOSMP) {
		this.receivingOSMP = receivingOSMP;
	}

	public DocOSMP() {}
	
	public DocOSMP(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			Integer area,
			ReceivingOSMP receivingOSMP) {
		super(year, month, deliveryOrg, numberPack, area);
		
		this.receivingOSMP = receivingOSMP;
	}

}
