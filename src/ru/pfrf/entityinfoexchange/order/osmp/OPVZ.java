package ru.pfrf.entityinfoexchange.order.osmp;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.RegisterOut;

@Entity
@Table(name = "opvz")
public class OPVZ extends RegisterOut {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "dateOfVipnet")
	protected Date dateOfVipnet;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "docOSMP")
	protected DocOSMP docOSMP;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;
	
	public Date getDateOfVipnet() {
		return dateOfVipnet;
	}

	public void setDateOfVipnet(Date dateOfVipnet) {
		this.dateOfVipnet = dateOfVipnet;
	}

	public DocOSMP getDocOSMP() {
		return docOSMP;
	}

	public void setDocOSMP(DocOSMP docOSMP) {
		this.docOSMP = docOSMP;
	}

	public OPVZ() {}
	
	public OPVZ(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			File file,
			Element rootElement,
			Long outNumber,
			Boolean isRead,
			String resultOfRead,
			List<String> namesMainFile,
			PackFromCombank packFromCombank,
			DocOSMP docOSMP,
			Date dateOfVipnet) {
		
		super(year, 
			month, 
			deliveryOrg, 
			numberPack, 
			file, 
			rootElement, 
			outNumber, 
			isRead, 
			resultOfRead,
			namesMainFile);
		
		this.packFromCombank = packFromCombank;
		this.docOSMP = docOSMP;
		this.dateOfVipnet = dateOfVipnet;
	}

	@Override
	public void init() throws Exception {
		throw new Exception("method not implements");
	}
	
	@Override
	public boolean equals(Object obj) {
		OPVZ opvz = (OPVZ) obj;
		return new EqualsBuilder().appendSuper(super.equals(opvz))
							      .append(this.dateOfVipnet, opvz.dateOfVipnet)
						   		  .isEquals();
	}
	
	@Override
	public String toString() {
		return super.toString() + " | " + String.valueOf(dateOfVipnet);
	}

}
