package ru.pfrf.entityinfoexchange.order.osmp;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import main.MainFrame;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.ConfirmRead;
import ru.pfrf.entityinfoexchange.notification.NotificationPOOS;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNameOut;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNamePfr;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSD;

@Entity
@Table(name = "doc_poos")
public class DocPOOS extends ConfirmRead {
	private static final long serialVersionUID = 1L;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "docOSMP")
	protected DocOSMP docOSMP;
	
	@OneToMany(mappedBy = "docPOOS", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<NotificationPOOS> setNotificationsPOOS = new HashSet<NotificationPOOS>();

	@Column(name = "namePack")
	private String namePack;

	public String getNamePack() {
		return namePack;
	}

	public void setNamePack(String namePack) {
		this.namePack = namePack;
	}

	public Set<NotificationPOOS> getSetNotificationsPOOS() {
		return setNotificationsPOOS;
	}

	public void setSetNotificationsPOOS(Set<NotificationPOOS> setNotificationsPOOS) {
		this.setNotificationsPOOS = setNotificationsPOOS;
	}

	public DocOSMP getDocOSMP() {
		return docOSMP;
	}

	public void setDocOSMP(DocOSMP docOSMP) {
		this.docOSMP = docOSMP;
	}
	
	public DocPOOS() {}
	
	
	
	public DocPOOS(File file, 
			DocOSMP docOSMP, 
			Date dateSend,
			Boolean decision) {	
		super(docOSMP.getYear(), 
				docOSMP.getMonth(), 
				docOSMP.getDeliveryOrg(),
				file,
				docOSMP.getNumberPack(),
				dateSend,
				null,
				decision); 
		
		this.docOSMP = docOSMP;
		//���� ����� �� ��������, �� ����� �������
		if (docOSMP.getMonth() == null)
			this.month = Integer.valueOf(new SimpleDateFormat("M").format(this.docOSMP.getReceivingOSMP().getDateBegin()));
	}
	
	@Override
	public String toString() {
		return super.toString() + " | " + namePack;
	}
	
	@Override
	public boolean equals(Object obj) {
		DocPOOS docPOOS = (DocPOOS) obj;
		return new EqualsBuilder()
					     .appendSuper(super.equals(docPOOS))
		                 .append(this.namePack, docPOOS.namePack)
		                 .isEquals();
	}
	
	
	
	public void createFilePOOS(Path targetPath, File templatePOOS) throws Exception {
		
		OPVZ opvz = this.docOSMP.getOpvz();
		OSMP osmp = null;
		if (!this.docOSMP.getListOSMP().isEmpty())
			osmp = this.docOSMP.getListOSMP().iterator().next();
		
		//������� ������� POOS
		ParserPOOS parserTemplate = new ParserPOOS();
		parserTemplate.parse(templatePOOS);
		Element rootElement = parserTemplate.getRootElement();
		
		//����������� ����� ����� (������ PFR-700-Y-2016-ORG-038-999-999999-DIS-000-DCK-00000-049-DOC-POOS-FSB-9070.XML)
		  //������� ����� ����� ������� � ���� ��������
		ParserFileNamePfr parserFileNameTemplate = new ParserFileNamePfr(parserTemplate.getFile(), 
															MainFrame.dictNameOrgInFile);
		parserFileNameTemplate.parse();
			//������� ����� ����� OPVZ
		ParserFileNameOut parserFileNameOSMP = new ParserFileNameOut(new File(opvz.getFileName()),
														MainFrame.dictNameOrgInFile);
		parserFileNameOSMP.parse();
		
		  //����������� ����
		String newFileNamePOOS = parserTemplate.getFileName().replaceFirst("Y-(\\d{4})", "Y-" + this.docOSMP.getYear().toString());
		  //DCK
		newFileNamePOOS = newFileNamePOOS.replaceFirst("DCK-\\d{5}-\\d{3}-DOC", "DCK-" + parserFileNameOSMP.getPackNumber() + "-" +  parserFileNameOSMP.getPart2PackNumber().toString() + "-DOC");
		
		//���������� ����� ��������� DocPOOS
		this.file = new File(targetPath + "/" + newFileNamePOOS);
		this.fileName = newFileNamePOOS;
		
		//����������� ����������� ����� POOS
		Element elPackDocIn = rootElement.element("�����������������������");
		  //��� �����
		rootElement.element("��������").setText(newFileNamePOOS);
		  //�����, ���
		Element RegisterIn = elPackDocIn.element("��������_�����");
		RegisterIn.element("���").setText(this.year.toString());
		
		//���� ����� OPVZ �� ���� ���������, �� ����� ��������������� �������
		if (opvz.getResultOfRead() == null) 
			RegisterIn.element("�����").setText(this.month.toString());
		else 
			RegisterIn.element("�����").setText(new SimpleDateFormat("MM").format(new Date()));
		
		  //���� ������ ���������, ���� �����������, ����� ������������
		Date dateDocPreparation = new Date();
		Date dateDocDelivery = dateDocPreparation;
		Date timeDocFormation = dateDocPreparation;
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		
		elPackDocIn.element("�������������_�_���������_�������_�_�����������_���_��������")
				   .element("�������������������").setText(format.format(dateDocDelivery));
		
		elPackDocIn.element("�������������_�_���������_�������_�_�����������_���_��������")
		           .element("�����������������").setText(new SimpleDateFormat("HH:mm").format(timeDocFormation));
		
		RegisterIn.element("���������������").setText(format.format(dateDocPreparation));
		  //���������������������
		String numberSystemArray = "038-999-999-" + this.year.toString() + "-" + parserFileNameOSMP.getPackNumber();
		RegisterIn.element("���������������������").setText(numberSystemArray);
		  //��� �����, ������������������������������
		Element elPodtv = elPackDocIn.element("�������������_�_���������_�������_�_�����������_���_��������");
		List<Element> listInfoArray = elPodtv.elements("�������������������������");
		
		Element elOPVZ = listInfoArray.get(0);
		elOPVZ.element("��������").setText(opvz.getFileName());
		elOPVZ.element("������������������������������").setText(numberSystemArray);
		if (opvz.getResultOfRead() != null) {
			elOPVZ.element("�����������������").setText(opvz.getResultOfRead());
		}
		if (!opvz.getIsRead()) 
			elOPVZ.element("�����������������������").setText("�� ��������");
		
		//���� ����� �� ���� ���������, �� ��� ����� ������, ����� ������� ��������� ������, ���� �� ��������, ��������� �������� ������
		Element elOSMP = listInfoArray.get(1);
		if (osmp != null) {
			elOSMP.element("��������").setText(osmp.getFileName());
			elOSMP.element("������������������������������").setText(numberSystemArray);
			if (osmp.getResultOfRead() != null) 
				elOSMP.element("�����������������").setText(osmp.getResultOfRead());
			
			if (!osmp.getIsRead())
				elOSMP.element("�����������������������").setText("�� ��������");
		} else {
			elOSMP.element("��������").setText("");
			elOSMP.element("������������������������������").setText("");
			elOSMP.element("�����������������������").setText("�� ��������");
			elOSMP.element("�����������������").setText("");
		}
		
		//��������� �������
		Element elDecision = elPodtv.element("�������");
		if ((osmp == null) 
			|| (osmp.getResultOfRead() != null) 
			|| (opvz.getResultOfRead() != null)) 
			
			this.decision = false;
		else
			this.decision = true;
			
			
		
		//���������� ���� "�������"
		if (this.decision)
			elDecision.setText("�������");
		else 
			elDecision.setText("�� �������");
	
		
		//���������� � ����
		String content = rootElement.getDocument().asXML();
		try (FileWriter fw = new FileWriter(this.file)) {
			fw.write(content, 0, content.length());
			
		} catch (Exception e) {
			throw new Exception(e);
		}
		
		
	}

}
