package ru.pfrf.entityinfoexchange.order.spis;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import ru.pfrf.entityinfoexchange.order.Order;

//@Entity
//@Table(name = "docs_list_transfer")
public class DocSPIS extends Order {
	private static final long serialVersionUID = 1L;
	
	//@OneToMany(mappedBy = "docSPIS", fetch = FetchType.EAGER)
	private Set<DocLTIncRegisterTF> setDocLTIncRegisterTF = new HashSet<>();
	
	public Set<DocLTIncRegisterTF> getSetDocLTIncRegisterTF() {
		return setDocLTIncRegisterTF;
	}

	public void setSetDocLTIncRegisterTF(
			Set<DocLTIncRegisterTF> setDocLTIncRegisterTF) {
		this.setDocLTIncRegisterTF = setDocLTIncRegisterTF;
	}

	public DocSPIS() {}

}
