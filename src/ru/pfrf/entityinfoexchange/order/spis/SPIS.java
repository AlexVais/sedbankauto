package ru.pfrf.entityinfoexchange.order.spis;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.entityinfoexchange.PfrMainFile;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIEPfr;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserSPIS;
import ru.pfrf.person.RecipientListTransfer;
import ru.pfrf.person.RecipientOZAC;

public class SPIS extends PfrMainFile {
	private static final long serialVersionUID = 1L;
	
	private Set<RecipientListTransfer> setRecipientListTransfer = new HashSet<>();


	public Set<RecipientListTransfer> getSetRecipientListTransfer() {
		return setRecipientListTransfer;
	}

	public void setSetRecipientListTransfer(
			Set<RecipientListTransfer> setRecipientListTransfer) {
		this.setRecipientListTransfer = setRecipientListTransfer;
	}

	public SPIS() {}
	
	public SPIS(ParserSPIS parser) {
		super(parser);
		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString() + " | ");
		
		return sb.toString();
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		SPIS spis = (SPIS) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(spis))
		                 .isEquals();
	}
	
	

	@Override
	public void init() throws Exception {
		try {
			Element elListTransfer = rootElement.element("�����������������������").element("������_��_����������");
			
			@SuppressWarnings("unchecked")
			List<Element> listElReportRecipient = elListTransfer.elements("�������������������");
			for (Element element : listElReportRecipient) {
				RecipientListTransfer recipient = new RecipientListTransfer(element, this);
				recipient.parse();
				setRecipientListTransfer.add(recipient);
			}
			
			//���������� �����������
			this.countRecipients = Integer.valueOf(elListTransfer.element("���������������������").getText());
			
			this.sum = new BigDecimal(elListTransfer.element("��������������").getText());
			
		} catch (Exception e) {
			throw new Exception("\n������ ��� �������� ozac " + this.fileName, e);
		}
		
	}
	
	
	

}
