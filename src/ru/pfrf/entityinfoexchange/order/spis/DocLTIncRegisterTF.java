package ru.pfrf.entityinfoexchange.order.spis;

import javax.persistence.*;

import ru.pfrf.entityinfoexchange.EntityInfoExchange;
import ru.pfrf.register.CreatedRegisterTF;

@Entity
@Table(name = "docs_list_transfer_inc_registertf")
public class DocLTIncRegisterTF extends EntityInfoExchange {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "registerTF")
	private CreatedRegisterTF createdRegisterTF;
	
	//@ManyToOne
	//@JoinColumn(name = "DocListTransfer")
	@Transient
	private DocSPIS docSPIS;
	
	public CreatedRegisterTF getCreatedRegisterTF() {
		return createdRegisterTF;
	}

	public void setCreatedRegisterTF(CreatedRegisterTF createdRegisterTF) {
		this.createdRegisterTF = createdRegisterTF;
	}

	public DocSPIS getDocSPIS() {
		return docSPIS;
	}


	public void setDocSPIS(DocSPIS docSPIS) {
		this.docSPIS = docSPIS;
	}


	public DocLTIncRegisterTF() {}
	
	public String toString() {
		return "?";
	}
	
	
}
