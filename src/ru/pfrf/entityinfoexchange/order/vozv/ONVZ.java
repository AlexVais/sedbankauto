package ru.pfrf.entityinfoexchange.order.vozv;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.pfrf.entityinfoexchange.OutMainFile;

@Entity
@Table(name = "onvz")
public class ONVZ extends OutMainFile{
	private static final long serialVersionUID = 1L;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VozvOut")
	private VOZVOut vozvOut;
	
	public VOZVOut getVozvOut() {
		return vozvOut;
	}

	public void setVozvOut(VOZVOut vozvOut) {
		this.vozvOut = vozvOut;
	}

	public ONVZ() {}
	
	public ONVZ(VOZVOut vozvOut) {}

	@Override
	public void init() throws Throwable {
		throw new Exception("not realized");
		
	}

}
