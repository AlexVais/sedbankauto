package ru.pfrf.entityinfoexchange.order.vozv;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.pfrf.entityinfoexchange.order.Order;

@Entity
@Table(name = "vozv_out")
public class VOZVOut extends Order {
	private static final long serialVersionUID = 1L;
	
	@OneToOne(mappedBy = "vozvOut", fetch = FetchType.LAZY)
	private OPVO opvo;
	
	@OneToOne(mappedBy = "vozvOut", fetch = FetchType.LAZY)
	private ONVZ onvz;
	
	@OneToOne(mappedBy = "vozvOut", fetch = FetchType.LAZY)
	private OVZR ovzr;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "poso")
	private POSO poso;
	
	
	public POSO getPoso() {
		return poso;
	}

	public void setPoso(POSO poso) {
		this.poso = poso;
	}

	public OPVO getOpvo() {
		return opvo;
	}

	public void setOpvo(OPVO opvo) {
		this.opvo = opvo;
	}

	public ONVZ getOnvz() {
		return onvz;
	}

	public void setOnvz(ONVZ onvz) {
		this.onvz = onvz;
	}

	public OVZR getOvzr() {
		return ovzr;
	}

	public void setOvzr(OVZR ovzr) {
		this.ovzr = ovzr;
	}

	public VOZVOut() {}
	

}
