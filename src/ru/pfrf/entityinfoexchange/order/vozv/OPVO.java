package ru.pfrf.entityinfoexchange.order.vozv;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.RegisterOut;
import ru.pfrf.entityinfoexchange.order.ozac.DocOZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVF;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOPVO;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ReceivingOZACAdapter;

@Entity
@Table(name = "opvo")
public class OPVO extends RegisterOut {
	private static final long serialVersionUID = 1L;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vozvOut")
	private VOZVOut vozvOut;
	
	public VOZVOut getVozvOut() {
		return vozvOut;
	}


	public void setVozvOut(VOZVOut vozvOut) {
		this.vozvOut = vozvOut;
	}


	public OPVO() {}
	
	public OPVO(ParserOPVO parser,
			/*PackFromCombank packFromCombank,*/
			VOZVOut vozvOut) {
		super(parser);
		
		//this.packFromCombank = packFromCombank;
		this.vozvOut = vozvOut;
	}
	

	@Override
	public void init() throws Throwable {
		throw new Exception("not realized");
		
		
		
	}

}
