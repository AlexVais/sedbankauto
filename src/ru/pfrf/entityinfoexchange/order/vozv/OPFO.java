package ru.pfrf.entityinfoexchange.order.vozv;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.pfrf.entityinfoexchange.RegisterPfr;
import ru.pfrf.process.ReceivingOZACAdapter;

@Entity
@Table(name = "opfo")
public class OPFO extends RegisterPfr {
	private static final long serialVersionUID = 1L;
	
	public DocVOZV getDocVOZV() {
		return docVOZV;
	}

	public void setDocVOZV(DocVOZV docVOZV) {
		this.docVOZV = docVOZV;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "docVOZV")
	protected DocVOZV docVOZV;

	public OPFO() {}
	
	@Override
	public void init() throws Throwable {
		throw new Exception("not realized");
		
	}
	
	

}
