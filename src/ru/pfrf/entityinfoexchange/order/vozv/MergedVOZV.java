package ru.pfrf.entityinfoexchange.order.vozv;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.pfrf.entityinfoexchange.notification.NotificationVOZV;
import ru.pfrf.entityinfoexchange.order.Order;
import ru.pfrf.entityinfoexchange.order.psmp.POSZ;

@Entity
@Table(name = "merged_VOZV")
public class MergedVOZV extends Order {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "fileName")
	protected String fileName;

	@Column(name = "packName")
	protected String packName;
	
	@OneToOne(mappedBy = "mergedVOZV", fetch = FetchType.LAZY)
	protected POSO poso;
	
	@OneToMany(mappedBy = "mergedVOZV", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<NotificationVOZV> setNotificationsVOZV = new HashSet<>();

	public POSO getPoso() {
		return poso;
	}

	public void setPoso(POSO poso) {
		this.poso = poso;
	}

	public Set<NotificationVOZV> getSetNotificationsVOZV() {
		return setNotificationsVOZV;
	}

	public void setSetNotificationsVOZV(Set<NotificationVOZV> setNotificationsVOZV) {
		this.setNotificationsVOZV = setNotificationsVOZV;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPackName() {
		return packName;
	}

	public void setPackName(String packName) {
		this.packName = packName;
	}

	public MergedVOZV() {}
	
	

}
