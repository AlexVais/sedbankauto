package ru.pfrf.entityinfoexchange.order.vozv;

import java.math.BigDecimal;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.entityinfoexchange.OutMainFile;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserOVZR;

@Entity
@Table(name = "ovzr")
public class OVZR extends OutMainFile {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "sum")
	private BigDecimal sum;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vozvOut")
	private VOZVOut vozvOut;

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}


	public VOZVOut getVozvOut() {
		return vozvOut;
	}

	public void setVozvOut(VOZVOut vozvOut) {
		this.vozvOut = vozvOut;
	}

	public OVZR() {}
	
	public OVZR(ParserOVZR parser) {
		super(parser);
		
		this.sum = parser.getSum();
		this.countRecipients = parser.getCountRecipients();
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString() + " | ");
		sb.append(String.valueOf(this.sum) + " | ");
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		OVZR ovzr = (OVZR) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(ovzr))
		                 .append(this.sum, ovzr.sum)
		                 .isEquals();
	}

	
	@Override
	public void init() throws Exception {
		//throw new Exception("not realized");
		
	}
	
	

}
