package ru.pfrf.entityinfoexchange.order.vozv;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.TypeCauseTermination;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.payment.PaymentOZAC;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.person.RecipientSPIS;

@Entity
@Table(name = "recipients_vozv")
public class RecipientVOZV extends RecipientSPIS {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "causeDate")
	private Date causeDate;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "causeTermination")
	private TypeCauseTermination typeCauseTermination;

	public TypeCauseTermination getTypeCauseTermination() {
		return typeCauseTermination;
	}

	public void setTypeCauseTermination(TypeCauseTermination typeCauseTermination) {
		this.typeCauseTermination = typeCauseTermination;
	}


	@BatchSize(size = 20)
	@OneToMany(mappedBy = "recipientVOZV", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PaymentVOZV> setPaymentVOZV = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "VOZV")
	private VOZV vozv;
	
	public Date getCauseDate() {
		return causeDate;
	}

	public void setCauseDate(Date causeDate) {
		this.causeDate = causeDate;
	}

	public VOZV getVozv() {
		return vozv;
	}

	public void setVozv(VOZV vozv) {
		this.vozv = vozv;
	}

	public Set<PaymentVOZV> getSetPaymentVOZV() {
		return setPaymentVOZV;
	}

	public void setSetPaymentVOZV(Set<PaymentVOZV> setPaymentVOZV) {
		this.setPaymentVOZV = setPaymentVOZV;
	}
	
	
	public RecipientVOZV() {}

	
	@Override
	protected void parsePayments() throws Exception {
		throw new Exception("not realized");
		
	}

}
