package ru.pfrf.entityinfoexchange.order.vozv;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.PfrMainFile;
import ru.pfrf.person.RecipientOZAC;
import ru.pfrf.process.ReceivingOZACAdapter;

@Entity
@Table(name = "vozv")
public class VOZV extends PfrMainFile{
	private static final long serialVersionUID = 1L;
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "vozv", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<RecipientVOZV> setRecipientVOZV = new HashSet<>();
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "docVOZV")
	protected DocVOZV docVOZV;

	public DocVOZV getDocVOZV() {
		return docVOZV;
	}

	public void setDocVOZV(DocVOZV docVOZV) {
		this.docVOZV = docVOZV;
	}

	public Set<RecipientVOZV> getSetRecipientVOZV() {
		return setRecipientVOZV;
	}

	public void setSetRecipientVOZV(Set<RecipientVOZV> setRecipientVOZV) {
		this.setRecipientVOZV = setRecipientVOZV;
	}

	@Override
	public void init() throws Throwable {
		throw new Exception("not realized");
		
	}
	
	public VOZV() {}

}
