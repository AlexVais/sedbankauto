package ru.pfrf.entityinfoexchange.order.vozv;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.pfrf.entityinfoexchange.ConfirmRead;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.order.psmp.MergedPSMP;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSO;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSZ;

@Entity
@Table(name = "poso")
public class POSO extends ConfirmRead {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "postfix")
	private Integer postfix;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mergedVOZV")
	protected  MergedVOZV mergedVOZV;
	
	@OneToOne(mappedBy = "poso", fetch = FetchType.LAZY)
	private VOZVOut vozvOut;
	
	@Column(name = "dateReceiv")
	protected Date dateReceiv;

	public VOZVOut getVozvOut() {
		return vozvOut;
	}

	public void setVozvOut(VOZVOut vozvOut) {
		this.vozvOut = vozvOut;
	}

	public Integer getPostfix() {
		return postfix;
	}

	public void setPostfix(Integer postfix) {
		this.postfix = postfix;
	}

	public MergedVOZV getMergedVOZV() {
		return mergedVOZV;
	}

	public void setMergedVOZV(MergedVOZV mergedVOZV) {
		this.mergedVOZV = mergedVOZV;
	}

	public Date getDateReceiv() {
		return dateReceiv;
	}

	public void setDateReceiv(Date dateReceiv) {
		this.dateReceiv = dateReceiv;
	}
	
	public POSO() {}
	
	public POSO(ParserPOSO parser, 
			PackFromCombank packFromCombank,
			Date dateReceiv,
			MergedVOZV mergedVOZV) {
		super(parser, null, parser.getDecision());
		
		this.dateReceiv = dateReceiv;
		this.mergedVOZV = mergedVOZV;
	}

}
