package ru.pfrf.entityinfoexchange.order.vozv;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import ru.pfrf.entityinfoexchange.order.Order;
import ru.pfrf.person.RecipientOZAC;

@Entity
@Table(name = "docvozv")
public class DocVOZV extends Order {
	private static final long serialVersionUID = 1L;

	@OneToOne(mappedBy = "docVOZV", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private OPFO opfo;
	
	@OneToOne(mappedBy = "docVOZV", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private VOZV vozv;
	
	public VOZV getVozv() {
		return vozv;
	}

	public void setVozv(VOZV vozv) {
		this.vozv = vozv;
	}

	public OPFO getOpfo() {
		return opfo;
	}

	public void setOpfo(OPFO opfo) {
		this.opfo = opfo;
	}

	public DocVOZV() {}

	
}
