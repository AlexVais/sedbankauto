package ru.pfrf.entityinfoexchange.order.vozv;

import javax.persistence.*;

import ru.pfrf.payment.Payment;
import ru.pfrf.person.RecipientOZAC;

@Entity
@Table(name = "payments_vozv")
public class PaymentVOZV extends Payment {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "recipientVOZV")
	private RecipientOZAC recipientVOZV;
	
	
	
	public RecipientOZAC getRecipientVOZV() {
		return recipientVOZV;
	}



	public void setRecipientVOZV(RecipientOZAC recipientVOZV) {
		this.recipientVOZV = recipientVOZV;
	}



	public PaymentVOZV() {}
	
}
