package ru.pfrf.entityinfoexchange.order.psmp;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.notification.NotificationPOOS;
import ru.pfrf.entityinfoexchange.notification.NotificationPSMP;
import ru.pfrf.entityinfoexchange.order.Order;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOOD;

@Entity
@Table(name = "merged_PSMP")
public class MergedPSMP extends Order {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "fileName")
	protected String fileName;
	
	@Column(name = "packName")
	protected String packName;

	@OneToOne(mappedBy = "mergedPSMP", fetch = FetchType.LAZY)
	protected POSZ posz;
	
	@OneToMany(mappedBy = "mergedPSMP", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<NotificationPSMP> setNotificationsPSMP = new HashSet<>();
	
	

	public Set<NotificationPSMP> getSetNotificationsPSMP() {
		return setNotificationsPSMP;
	}

	public void setSetNotificationsPSMP(Set<NotificationPSMP> setNotificationsPSMP) {
		this.setNotificationsPSMP = setNotificationsPSMP;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getPackName() {
		return packName;
	}

	public void setPackName(String packName) {
		this.packName = packName;
	}


	public POSZ getPosz() {
		return posz;
	}

	public void setPosz(POSZ posz) {
		this.posz = posz;
	}
	
	
	public MergedPSMP() {};
	
}
