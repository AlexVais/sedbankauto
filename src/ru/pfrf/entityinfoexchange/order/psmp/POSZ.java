package ru.pfrf.entityinfoexchange.order.psmp;

import java.util.Date;

import javax.persistence.*;

import ru.pfrf.entityinfoexchange.ConfirmRead;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIE;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSD;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserPOSZ;
import ru.pfrf.process.ReceivingPOSD;

@Entity
@Table(name = "posz")
public class POSZ extends ConfirmRead {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "postfix")
	private Integer postfix;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mergedPSMP")
	protected  MergedPSMP mergedPSMP;
	
	@Column(name = "dateReceiv")
	protected Date dateReceiv;

	public Integer getPostfix() {
		return postfix;
	}

	public void setPostfix(Integer postfix) {
		this.postfix = postfix;
	}

	public MergedPSMP getMergedPSMP() {
		return mergedPSMP;
	}

	public void setMergedPSMP(MergedPSMP mergedPSMP) {
		this.mergedPSMP = mergedPSMP;
	}

	public Date getDateReceiv() {
		return dateReceiv;
	}

	public void setDateReceiv(Date dateReceiv) {
		this.dateReceiv = dateReceiv;
	}
	
	public POSZ() {}
	
	public POSZ(ParserPOSZ parser, 
			PackFromCombank packFromCombank,
			Date dateReceiv,
			MergedPSMP mergedPSMP) {
		super(parser, null, parser.getDecision());
		
		this.dateReceiv = dateReceiv;
		this.mergedPSMP = mergedPSMP;
	}

}
