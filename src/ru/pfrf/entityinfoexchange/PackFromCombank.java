package ru.pfrf.entityinfoexchange;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.reflect.TypeUtilsTest.This;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.order.osmp.*;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OPVF;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.SPRA;
import ru.pfrf.entityinfoexchange.parserfilename.ParserPackFromCombank;
import ru.pfrf.entityinfoexchange.receipt.ReceiptProcessToBank;
import ru.pfrf.person.RecipientDead;
import ru.pfrf.process.FileOfPackFromCombankAdapter;
import ru.pfrf.process.ReceivingPacksFromCombanks;
import ru.pfrf.process.ReceivingPacksFromCombanksAdapter;

@Entity
@Table(name = "packs_from_com_bank")
public class PackFromCombank extends EntityIEBank {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "fileName")
	private String fileName;
	
	@Column(name = "postfix")
	private Integer postfix;
	
	@Column(name = "dateReceivVipnet")
	private Date dateReceivVipnet;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "receiving_new")
	protected ReceivingPacksFromCombanks receivingPacksFromCombanks;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Receiving")
	protected ReceivingPacksFromCombanksAdapter receivingPacksFromCombanksAdapter;
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<FileOfPackFromCombankAdapter> setFileOfPackFromCombankAdapter = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "error")
	protected TypeError typeError;
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<OPVZ> setOSMP = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<OSMP> setOPVZ = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<OZAC> setOZAC = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<SPRA> setSPRA = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<OPVF> setOPVF = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<DocPOSD> setPOSD = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected Set<UnknownFile> setUnknownFile = new HashSet<>();
	
	@OneToOne(mappedBy = "packFromCombank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	protected ReceiptProcessToBank receiptProcessToBank;

	@Transient
	protected File file;

	public Date getDateReceivVipnet() {
		return dateReceivVipnet;
	}

	public void setDateReceivVipnet(Date dateReceivVipnet) {
		this.dateReceivVipnet = dateReceivVipnet;
	}

	public ReceiptProcessToBank getReceiptProcessToBank() {
		return receiptProcessToBank;
	}

	public void setReceiptProcessToBank(ReceiptProcessToBank receiptProcessToBank) {
		this.receiptProcessToBank = receiptProcessToBank;
	}

	public Set<OPVZ> getSetOSMP() {
		return setOSMP;
	}

	public void setSetOSMP(Set<OPVZ> setOSMP) {
		this.setOSMP = setOSMP;
	}

	public Set<OSMP> getSetOPVZ() {
		return setOPVZ;
	}

	public void setSetOPVZ(Set<OSMP> setOPVZ) {
		this.setOPVZ = setOPVZ;
	}

	public Set<FileOfPackFromCombankAdapter> getSetFileOfPackFromCombankAdapter() {
		return setFileOfPackFromCombankAdapter;
	}

	public void setSetFileOfPackFromCombankAdapter(
			Set<FileOfPackFromCombankAdapter> setFileOfPackFromCombankAdapter) {
		this.setFileOfPackFromCombankAdapter = setFileOfPackFromCombankAdapter;
	}

	public ReceivingPacksFromCombanksAdapter getReceivingPacksFromCombanksAdapter() {
		return receivingPacksFromCombanksAdapter;
	}

	public void setReceivingPacksFromCombanksAdapter(
			ReceivingPacksFromCombanksAdapter receivingPacksFromCombanksAdapter) {
		this.receivingPacksFromCombanksAdapter = receivingPacksFromCombanksAdapter;
	}

	public Integer getPostfix() {
		return postfix;
	}

	public File getFile() {
		return file;
	}

	public void setPostfix(Integer i) {
		this.postfix = i;
	}

	public Set<UnknownFile> getSetUnknownFile() {
		return setUnknownFile;
	}

	public void setSetUnknownFile(Set<UnknownFile> setUnknownFile) {
		this.setUnknownFile = setUnknownFile;
	}

	public Set<SPRA> getSetSPRA() {
		return setSPRA;
	}

	public void setSetSPRA(Set<SPRA> setSPRA) {
		this.setSPRA = setSPRA;
	}

	public TypeError getTypeError() {
		return typeError;
	}

	public void setTypeError(TypeError typeError) {
		this.typeError = typeError;
	}

	public ReceivingPacksFromCombanks getReceivingPacksFromCombanks() {
		return receivingPacksFromCombanks;
	}

	public void setReceivingPacksFromCombanks(
			ReceivingPacksFromCombanks receivingPacksFromCombanks) {
		this.receivingPacksFromCombanks = receivingPacksFromCombanks;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Set<DocPOSD> getSetPOSD() {
		return setPOSD;
	}

	public void setSetPOSD(Set<DocPOSD> setPOSD) {
		this.setPOSD = setPOSD;
	}

	public Set<OPVF> getSetOPVF() {
		return setOPVF;
	}

	public void setSetOPVF(Set<OPVF> setOPVF) {
		this.setOPVF = setOPVF;
	}

	public Set<OPVZ> getListOSMP() {
		return setOSMP;
	}

	public Set<OZAC> getSetOZAC() {
		return setOZAC;
	}

	public void setSetOZAC(Set<OZAC> setOZAC) {
		this.setOZAC = setOZAC;
	}

	public void setListOSMP(Set<OPVZ> setOSMP) {
		this.setOSMP = setOSMP;
	}

	public Set<OSMP> getListOPVZ() {
		return setOPVZ;
	}

	public void setListOPVZ(Set<OSMP> setOPVZ) {
		this.setOPVZ = setOPVZ;
	}

	public PackFromCombank() {}
	
	public PackFromCombank(ParserPackFromCombank parser, ReceivingPacksFromCombanks receiving) {
		super(parser.getDeliveryOrg()); 
		
		this.file = parser.getFile();
		this.fileName = file.getName();
		this.receivingPacksFromCombanks = receiving;
	}
	
	@Override
	public String toString() {
		return fileName + 
				" | " + postfix;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		PackFromCombank pack = (PackFromCombank) obj;
		return new EqualsBuilder()
		                 .append(this.fileName, pack.fileName)
		                 .append(this.postfix, pack.postfix)
		                 .isEquals();
	}

}
