package ru.pfrf.entityinfoexchange;

import java.io.File;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIEOut;

@MappedSuperclass
public abstract class OutFileOfIE extends FileOfInfoExchange {
	private static final long serialVersionUID = 1L;
	
	@Transient
	protected ParserFileOfIEOut parser;
	
	@Column(name = "outNumber")
	protected Long outNumber;
	
	@Column(name = "isRead")
	protected Boolean isRead;
	
	@Column(name = "resultOfRead")
	protected String resultOfRead;
	
	public ParserFileOfIEOut getParser() {
		return parser;
	}

	public String getResultOfRead() {
		return resultOfRead;
	}

	public void setResultOfRead(String resultOfRead) {
		this.resultOfRead = resultOfRead;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public Long getOutNumber() {
		return outNumber;
	}

	public void setOutNumber(Long outNumber) {
		this.outNumber = outNumber;
	}

	public OutFileOfIE() {};
	
	public OutFileOfIE(ParserFileOfIEOut parser) {
		this(parser.getYear(),
			 parser.getMonth(),
			 parser.getDeliveryOrg(),
			 parser.getPackNumber(),
			 parser.getFile(),
			 parser.getRootElement(),
			 parser.getOutNumber(),
			 parser.getIsRead(),
			 parser.getResultOfRead());
		
		this.parser = parser;
	}
	
	public OutFileOfIE(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			File file,
			Element rootElement,
			Long outNumber,
			Boolean isRead,
			String resultOrRead) {
		super(year, month, deliveryOrg, numberPack, file, rootElement);
		
		this.outNumber = outNumber;
		this.isRead = isRead;
		this.resultOfRead = resultOrRead;
	}
	
	@Override
	public String toString() {
		return super.toString() + " | " +
				String.valueOf(outNumber) + " | " + 
				this.isRead;
	}
	
	@Override
	public boolean equals(Object obj) {
		OutFileOfIE of = (OutFileOfIE) obj;
		return new EqualsBuilder().appendSuper(super.equals(of))
							      .append(this.outNumber, of.outNumber)
							      .append(this.isRead, of.isRead)
						   		  .isEquals();
		
		
	}

}
