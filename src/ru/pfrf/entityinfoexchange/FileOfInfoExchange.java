package ru.pfrf.entityinfoexchange;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.out;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import static java.lang.System.out;


@MappedSuperclass
public abstract class FileOfInfoExchange extends EntityIEBank {
	public void setFile(File file) {
		this.file = file;
	}

	private static final long serialVersionUID = 1L;
	
	@Column(name = "fileName")
	protected String fileName;
	
	@Column(name = "numberPack")
	protected Integer numberPack;
	
	@Column(name = "year")
	protected Integer year;

	@Column(name = "month")
	protected Integer month;
	
	
	@Transient
	protected File file;

	@Transient
	protected Element rootElement;

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getMonth() {
		return month;
	}

	public Element getRootElement() {
		return rootElement;
	}

	public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getNumberPack() {
		return numberPack;
	}

	public void setNumberPack(Integer numberPack) {
		this.numberPack = numberPack;
	}

	public File getFile() {
		return file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	public FileOfInfoExchange() {}

	public FileOfInfoExchange(Integer year, 
						Integer month, 
						DeliveryOrg deliveryOrg,
						Integer numberPack,
						File file, 
						Element rootElement) {
		
		super(deliveryOrg);
		
		this.year = year;
		this.month = month;
		this.numberPack = numberPack;
		this.file = file;
		this.fileName = file.getName();
		this.rootElement = rootElement;
	}
	
	@Override
	public String toString() {
		return  this.year +
				" | " + this.month +
				" | " + this.numberPack +
				" | " + this.fileName;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		FileOfInfoExchange foie = (FileOfInfoExchange) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(foie))
		                 .append(this.file, foie.file)
		                 .append(this.numberPack, foie.numberPack)
		                 .append(this.year, foie.year)
		                 .append(this.month, foie.month)
		                 .isEquals();
		
	}
	
	public static void deleteDigitalSignature(File file) {
		char[] buffer = null;
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			//��������� ���������� ����� � ����� 
			buffer = new char[(int) file.length()];
			br.read(buffer);
			Path p = null;
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			String string = new String(buffer, buffer.length - 400, 400);
			Pattern p = Pattern.compile(".*(</�������>).*", Pattern.MULTILINE | Pattern.DOTALL);
			Matcher m = p.matcher(string);
			
			if (m.matches()) {	
				Integer position = buffer.length - 400 + m.start(1) + "</�������>".length();
				bw.write(buffer, 0, position);
			}
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	public void deleteDigitalSignature() {
		deleteDigitalSignature(this.file);
	}
	
	public abstract void init() throws Throwable;
}
