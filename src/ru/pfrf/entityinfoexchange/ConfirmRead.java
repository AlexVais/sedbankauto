package ru.pfrf.entityinfoexchange;

import java.io.File;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIE;

@MappedSuperclass
public class ConfirmRead extends DocOfInfoExchange {
	private static final long serialVersionUID = 1L;
	
	@Transient
	protected File file;
	
	@Transient
	protected Element rootElement;
	
	@Column(name = "fileName")
	protected String fileName;
	
	@Column(name = "decision")
	protected Boolean decision;
	
	@Column(name = "dateSend")
	protected Date dateSend;

	public Date getDateSend() {
		return dateSend;
	}

	public void setDateSend(Date dateSend) {
		this.dateSend = dateSend;
	}

	public Boolean getDecision() {
		return decision;
	}

	public void setDecision(Boolean decision) {
		this.decision = decision;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getFile() {
		return file;
	}

	public Element getRootElement() {
		return rootElement;
	}

	public ConfirmRead() {}

	public ConfirmRead(ParserFileOfIE parser, 
			Date dateSend,
			Boolean decision) {
		super(parser);
		
		this.file = parser.getFile();
		this.rootElement = parser.getRootElement();
		this.fileName = parser.getFile().getName();
		this.dateSend = dateSend;
		this.decision = decision;
	}

	public ConfirmRead(Integer year, 
				Integer month, 
				DeliveryOrg deliveryOrg, 
				File file,
				Integer numberPack,
				Date dateSend,
				Element rootElement,
				Boolean decision) {
		super(year, month, deliveryOrg, numberPack);
		
		this.file = file;
		this.rootElement = rootElement;
		this.fileName = file.getName();
		this.decision = decision;
	}
	
	@Override
	public String toString() {
		return super.toString() +
				" | " + fileName + 
				" | " + decision + 
				" | " + dateSend;
	}
	
	@Override
	public boolean equals(Object obj) {
		ConfirmRead confirmRead = (ConfirmRead) obj;
		return new EqualsBuilder()
					     .appendSuper(super.equals(confirmRead))
		                 .append(this.fileName, confirmRead.fileName)
		                 .append(this.decision, confirmRead.decision)
		                 .append(this.dateSend, confirmRead.dateSend)
		                 .isEquals();
	}

}
