package ru.pfrf.entityinfoexchange;

import java.io.File;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.DeliveryOrg;

@Entity
@Table(name = "unknown_files")
public class UnknownFile extends EntityIEBank{
	private static final long serialVersionUID = 1L;

	@Column(name = "fileName")
	protected String fileName;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;
	
	@Transient
	protected File file;

	public File getFile() {
		return file;
	}

	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public UnknownFile() {}
	
	public UnknownFile(DeliveryOrg deliveryOrg, File file, PackFromCombank packFromCombank) {
		super(deliveryOrg);
		
		this.file = file;
		this.fileName = file.getName();
		this.packFromCombank = packFromCombank;
	}
	
	public String toString() {
		return this.fileName;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
			
		UnknownFile unknownFile = (UnknownFile) obj;
		return new EqualsBuilder().append(this.fileName, unknownFile.fileName)
						   		  .isEquals();
	}

}
