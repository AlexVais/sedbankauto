package ru.pfrf.entityinfoexchange;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import ru.pfrf.classifier.DeliveryOrg;

@MappedSuperclass
public class EntityIEBank extends EntityInfoExchange {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deliveryOrg")
	protected DeliveryOrg deliveryOrg;

	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public void setDeliveryOrg(DeliveryOrg deliveryOrg) {
		this.deliveryOrg = deliveryOrg;
	}

	public EntityIEBank() {};
	
	public EntityIEBank(DeliveryOrg deliveryOrg) {
		this.deliveryOrg = deliveryOrg;
		
	}
	
	
}
