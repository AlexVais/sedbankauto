package ru.pfrf.entityinfoexchange;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserRegisterOut;

@MappedSuperclass
public abstract class RegisterOut extends OutFileOfIE {
	private static final long serialVersionUID = 1L;
	
	@Transient
	protected List<String> namesMainFile = new ArrayList<>();
	 
	
	public List<String> getNamesMainFile() {
		return namesMainFile;
	}

	public void setNamesMainFile(List<String> namesMainFile) {
		this.namesMainFile = namesMainFile;
	}

	public RegisterOut() {}
	
	public RegisterOut(ParserRegisterOut parser) {
		this(parser.getYear(),
				parser.getMonth(),
				parser.getDeliveryOrg(),
				parser.getPackNumber(),
				parser.getFile(),
				parser.getRootElement(),
				parser.getOutNumber(),
				parser.getIsRead(),
				parser.getResultOfRead(),
				parser.getNamesMainFile());
	}
	
	public RegisterOut(Integer year, 
				Integer month, 
				DeliveryOrg deliveryOrg,
				Integer numberPack,
				File file,
				Element rootElement,
				Long outNumber,
				Boolean isRead,
				String resultOfRead,
				List<String> namesMainFile) {
		super(year, 
			month, 
			deliveryOrg, 
			numberPack, 
			file, 
			rootElement, 
			outNumber, 
			isRead, 
			resultOfRead);
		
		this.namesMainFile = namesMainFile;
		
	}
	

}
