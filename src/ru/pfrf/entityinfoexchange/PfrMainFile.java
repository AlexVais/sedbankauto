package ru.pfrf.entityinfoexchange;

import java.io.File;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIEOut;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserFileOfIEPfr;

@MappedSuperclass
public abstract class PfrMainFile extends PfrFileOfIE {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "countRecipients")
	protected Integer countRecipients;
	
	@Column(name = "sum")
	protected BigDecimal sum;
	
	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public Integer getCountRecipients() {
		return countRecipients;
	}

	public void setCountRecipients(Integer countRecipients) {
		this.countRecipients = countRecipients;
	}

	public PfrMainFile() {}
	
	public PfrMainFile(ParserFileOfIEPfr parser) {
		this(parser.getYear(),
			 parser.getMonth(),
			 parser.getDeliveryOrg(),
			 parser.getPackNumber(),
			 parser.getFile(),
			 parser.getRootElement());
	}
	
	public PfrMainFile(Integer year, 
			Integer month, 
			DeliveryOrg deliveryOrg,
			Integer numberPack,
			File file,
			Element rootElement) {
		super(year, month, deliveryOrg, numberPack, file, rootElement);
	}
	
	
	@Override
	public String toString() {
		return super.toString() + " | " + String.valueOf(countRecipients) + 
				" | " + this.sum;
	}
	
	@Override
	public boolean equals(Object obj) {
		PfrMainFile omf = (PfrMainFile) obj;
		return new EqualsBuilder().appendSuper(super.equals(omf))
							      .append(this.countRecipients, omf.countRecipients)
							      .append(this.sum, omf.sum)
						   		  .isEquals();
		
		
	}

	
	

}
