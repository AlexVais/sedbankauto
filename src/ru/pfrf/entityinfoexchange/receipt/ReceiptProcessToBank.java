package ru.pfrf.entityinfoexchange.receipt;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.EntityIEBank;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.PackFromCombank;
import ru.pfrf.process.FileOfPackFromCombankAdapter;


@Entity
@Table(name = "receipts_process_to_bank")
public class ReceiptProcessToBank extends EntityIEBank {
	private static final long serialVersionUID = 1L;

	@Column(name = "decision")
	protected Boolean decision;
	
	@Transient
	protected File file;
	
	@Column(name = "fileName")
	protected String fileName;
	
	@Column(name = "packNameSend")
	protected String packNameSend;
	
	@Column(name = "version")
	protected Integer version;
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "packFromCombank")
	protected PackFromCombank packFromCombank;

	public File getFile() {
		return file;
	}

	public String getPackNameSend() {
		return packNameSend;
	}

	public void setPackNameSend(String packNameSend) {
		this.packNameSend = packNameSend;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public PackFromCombank getPackFromCombank() {
		return packFromCombank;
	}

	public void setPackFromCombank(PackFromCombank packFromCombank) {
		this.packFromCombank = packFromCombank;
	}

	public Boolean getDecision() {
		return decision;
	}

	public void setDecision(Boolean decision) {
		this.decision = decision;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	public ReceiptProcessToBank() {}
	
	public ReceiptProcessToBank(PackFromCombank packFromCombank, Integer version) {
		super(packFromCombank.getDeliveryOrg());
		
		this.packFromCombank = packFromCombank;
		this.fileName = "receipt_" + packFromCombank.getFileName();
		this.version = version;
		
		//����������� �������� �������
		if (this.packFromCombank.getTypeError() == null)
			decision = true;
		else
			decision = false;
	}
	
	@Override
	public String toString() {
		return fileName + 
				" | " + String.valueOf(decision) + 
				" | " + packNameSend + 
				" | " + String.valueOf(version);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		ReceiptProcessToBank receipt = (ReceiptProcessToBank) obj;
		return new EqualsBuilder()
		                 .append(this.fileName, receipt.fileName)
		                 .append(this.decision, receipt.decision)
		                 .append(this.packNameSend, receipt.packNameSend)
		                 .append(this.version, receipt.version)
		                 .isEquals();
	}
	
	private String contentTxtVersion1() {
		StringBuilder sb = new StringBuilder();
		sb.append("--------��������� � ����� ������ � ���������-------------\r\n");
		sb.append("��� ������: " + this.packFromCombank.getFileName() + "\r\n");
		
		Date dateReceiving = this.getPackFromCombank().getReceivingPacksFromCombanks().getDateBegin();
		sb.append("���� �����: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(dateReceiving) + "\r\n");
		
		//����� ������
		sb.append("����� � ������:");
		Integer i = 1;
		for(FileOfPackFromCombankAdapter fileOfPack : packFromCombank.getSetFileOfPackFromCombankAdapter()) {
			sb.append("\r\n" + i + ") " + fileOfPack.getFileName());
			i++;
		}
		
		sb.append("\r\n\r\n" + "������� � ���������: " + (decision ? "��" : "���") + "\r\n");
		
		//������
		if (!decision) {
			sb.append("������: " + this.packFromCombank.getTypeError().getDescription());
		}
		
		return sb.toString();
	}
	
	public String contentTxt() {
		switch (this.version) {
		case 1 : return contentTxtVersion1();
		//case 2 : return contentTxtVersion2();
		default : return null;
		}
	}
	
	public void saveToFileTxt(Path pathDest) {
		File file = new File(pathDest + "/" + this.fileName + ".txt");
		try(FileOutputStream fos = new FileOutputStream(file)) {
			fos.write(this.contentTxt().getBytes());
			this.file = file;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
