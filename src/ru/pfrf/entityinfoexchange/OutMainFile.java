package ru.pfrf.entityinfoexchange;

import java.io.File;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserMainFileOut;

@MappedSuperclass
public abstract class OutMainFile extends OutFileOfIE {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "countRecipients")
	protected Integer countRecipients;
	
	public Integer getCountRecipients() {
		return countRecipients;
	}

	public void setCountRecipients(Integer countRecipients) {
		this.countRecipients = countRecipients;
	}

	public OutMainFile() {}
	
	public OutMainFile(ParserMainFileOut parser) {
		super(parser);
	}

	public OutMainFile(Integer year, 
				Integer month, 
				DeliveryOrg deliveryOrg,
				Integer numberPack, 
				File file,
				Element rootElement,
				Long outNumber,
				Boolean isRead,
				String resultOfRead) {
		super(year, 
			month, 
			deliveryOrg, 
			numberPack, 
			file, 
			rootElement, 
			outNumber,
			isRead,
			resultOfRead);
		
	}
	
	@Override
	public String toString() {
		return super.toString() + " | " + String.valueOf(countRecipients);
	}
	
	@Override
	public boolean equals(Object obj) {
		OutMainFile omf = (OutMainFile) obj;
		return new EqualsBuilder().appendSuper(super.equals(omf))
							      .append(this.countRecipients, omf.countRecipients)
						   		  .isEquals();
		
		
	}

}
