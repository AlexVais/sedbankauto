package ru.pfrf.entityinfoexchange.notification;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import main.MainFrame;
import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.osmp.DocPOOS;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOOD;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNamePfr;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;
import ru.pfrf.register.CreatedRegisterTF;

@Entity
@Table(name = "notifications_poos")
public class NotificationPOOS extends NotificationBank{
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "poos")
	protected DocPOOS docPOOS;

	
	public DocPOOS getDocPOOS() {
		return docPOOS;
	}

	public void setDocPOOS(DocPOOS docPOOS) {
		this.docPOOS = docPOOS;
	}

	public NotificationPOOS() {}
	
	public NotificationPOOS(ParserNotification parser, 
			Path pathArchive, 
			DeliveryOrg deliveryOrg,
			DocPOOS docPOOS) {
		super(parser, pathArchive, deliveryOrg);
		this.docPOOS = docPOOS;
	}

	@Override
	public void saveToArchive() throws Exception {
		ParserFileNamePfr parser = new ParserFileNamePfr(new File(this.docPOOS.getFileName()), MainFrame.dictNameOrgInFile);
		parser.parse();
		Integer month = Integer.valueOf(new SimpleDateFormat("MM").format(this.date));
		
		File destDir = new File(pathArchive + "/�������� POOS � ����/" + 
							parser.getYear() + "/" + 
							month + "/" + 
							docPOOS.getNamePack()); 
		if (!destDir.exists())
			if (!destDir.mkdirs())
				throw new Exception("�� ������� ������� ������� " + destDir);

		String destFileName = "blank_" + parser.getYear() + "_" + 
										 month + "_" + 
										 docPOOS.getNamePack() + 
										 "_" + typeResultNotification.getName() + ".txt";

		Files.copy(file.toPath(), Paths.get(destDir + "/" + destFileName), StandardCopyOption.REPLACE_EXISTING);	
		
	}

}
