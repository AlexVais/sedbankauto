package ru.pfrf.entityinfoexchange.notification;

import java.nio.file.Path;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.psmp.MergedPSMP;
import ru.pfrf.entityinfoexchange.order.vozv.MergedVOZV;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;

@Entity
@Table(name = "notifications_vozv")
public class NotificationVOZV extends NotificationBank {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mergedVOZV")
	protected MergedVOZV mergedVOZV;
		
	
	@Override
	public void saveToArchive() throws Exception {
		throw new Exception("not realized");

	}


	public MergedVOZV getMergedVOZV() {
		return mergedVOZV;
	}


	public void setMergedVOZV(MergedVOZV mergedVOZV) {
		this.mergedVOZV = mergedVOZV;
	}

	public NotificationVOZV() {}
	
	public NotificationVOZV(ParserNotification parser, 
			Path pathArchive, 
			DeliveryOrg deliveryOrg,
			MergedVOZV mergedVOZV) {
		super(parser, pathArchive, deliveryOrg);
		
		this.mergedVOZV = mergedVOZV;
	}

}
