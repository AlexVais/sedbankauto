package ru.pfrf.entityinfoexchange.notification;

import java.io.File;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.persistence.*;

import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;
import ru.pfrf.process.ReceivingNotifications;
import ru.pfrf.register.CreatedRegisterTF;


@Entity
@Table(name = "notifications_register_tf")
public class NotificationRegisterTF extends Notification {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "registerTF")
	private CreatedRegisterTF createdRegisterTF;
	
	
	public CreatedRegisterTF getCreatedRegisterTF() {
		return createdRegisterTF;
	}

	public void setCreatedRegisterTF(CreatedRegisterTF createdRegisterTF) {
		this.createdRegisterTF = createdRegisterTF;
	}


	public NotificationRegisterTF() {}
	
	public NotificationRegisterTF(ParserNotification parser, 
						Path pathArchive,
						CreatedRegisterTF createdRegisterTF) {
		super(parser, pathArchive);
		
		this.createdRegisterTF = createdRegisterTF;
	}

	@Override
	public void saveToArchive() throws Exception {
		File destDir = new File(pathArchive + "/�������� �������� � ����/" + 
											createdRegisterTF.getYearPay() + "/" + 
											createdRegisterTF.getMonthPay() + "/" + 
											"������_" + createdRegisterTF.getNumber()); 
		if (!destDir.exists())
			if (!destDir.mkdirs())
				throw new Exception("�� ������� ������� ������� " + destDir);
		
		String destFileName = "blank_" + createdRegisterTF.getYearPay() + "_" + 
										 createdRegisterTF.getMonthPay() + "_" + 
										 "�" + createdRegisterTF.getNumber() + 
										 "_" + typeResultNotification.getName() + ".txt";
		
		Files.copy(file.toPath(), Paths.get(destDir + "/" + destFileName), StandardCopyOption.REPLACE_EXISTING);	
	}
	
	
	
}
