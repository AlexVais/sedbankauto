package ru.pfrf.entityinfoexchange.notification;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.*;

import main.MainFrame;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.osmp.DocPOOS;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOOD;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.psmp.MergedPSMP;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.ListTransferToBank;

public class NotificationFactory {
	
	private static boolean isNotificationRegisterTF(String attachment) {
		Pattern p = Pattern.compile("^��1_\\d{4}\\d{2}_.+?_\\d+\\.(xls|xlsx)$", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(attachment);
		if (m.find())
			return true;
		else	
			return false;
	}
	
	private static boolean isNotificationBank(String attachment, Object[] args) {
		Pattern p = Pattern.compile("^(.{3})a\\d{4}\\.(PFR|038)$", Pattern.CASE_INSENSITIVE); //�.�: SKBa0010.PFR
		Matcher m = p.matcher(attachment);
		if (m.find()) {
			args[0] = m.group(1);
			return true;
		} else {
			p = Pattern.compile("^Y54.+P22$", Pattern.CASE_INSENSITIVE);   //��� Y54a1111.P22
			m = p.matcher(attachment);
			if (m.find()) {
				args[0] = "8636";
				return true;
			}
		}
			
		return false;
	}
	
	private static CreatedRegisterTF getCreatedRegisterTF(SessionFactory sessionFactory, 
											String attachment) throws Exception {
		
		//����������� ������ �������, ���� � ������ ������� �� ����� �������
		Pattern p = Pattern.compile("^��1_(\\d{4})(\\d{2})_.+?_(\\d+)\\.(xls|xlsx)$", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(attachment);
		m.find();
		
		Integer yearPay = Integer.valueOf(m.group(1));
		Integer monthPay = Integer.valueOf(m.group(2));
		Integer number = Integer.valueOf(m.group(3));
		
		//������� ��������
		Session session = null;
		Transaction t = null;
		CreatedRegisterTF createdRegisterTF = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(CreatedRegisterTF.class)
							  		   .add(Restrictions.eq("yearPay", yearPay))
									   .add(Restrictions.eq("monthPay", monthPay))
									   .add(Restrictions.eq("number", number))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
								
			List<CreatedRegisterTF> listRegisters = criteria.list();
			if (listRegisters.isEmpty() || listRegisters.size() > 1)
				throw new Exception("������ ��������� �������. ��� = " + yearPay + ", ����� = " + monthPay + ", ����� = " + number);
			
			createdRegisterTF = listRegisters.get(0);
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ��������� �������", e);
			
		} finally {
			session.close();
		}
		
		return createdRegisterTF;	
	}
	
	
	private static boolean isListTransferToBank(SessionFactory sessionFactory, 
											String attachment,
											Object[] args) throws Exception {
		
		//������� ListTransferToBank
		Session session = null;
		Transaction t = null;
		ListTransferToBank listTransferToBank = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(ListTransferToBank.class)
									   .add(Restrictions.eq("namePack", attachment))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
								
			List<ListTransferToBank> listLTToBank = criteria.list();
			if (listLTToBank.isEmpty()) 
				return false;

			args[0] = listLTToBank;
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ��������� ������ �� ���������� � ����", e);
			
		} finally {
			session.close();
		}
		return true;
	}
	
	/**
	 * 
	 * @param sessionFactory
	 * @param attachment
	 * @param args
	 * @return
	 * @throws Exception
	 */
	private static boolean isDocPOOD(SessionFactory sessionFactory, 
								String attachment,
								Object[] args) throws Exception {
		
		//������� POOD
		Session session = null;
		Transaction t = null;
		DocPOOD docPOOD = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(DocPOOD.class)
									   .add(Restrictions.eq("packName", attachment))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
								
			List<DocPOOD> listDocPOOD = criteria.list();
			if (listDocPOOD.isEmpty()) 
				return false;
			
			args[0] = listDocPOOD;
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ��������� ������ POOD", e);
			
		} finally {
			session.close();
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param sessionFactory
	 * @param attachment
	 * @param args
	 * @return
	 * @throws Exception
	 */
	private static boolean isDocPOOS(SessionFactory sessionFactory, 
								String attachment,
								Object[] args) throws Exception {
		//������� POOS
		Session session = null;
		Transaction t = null;
		DocPOOS docPOOS = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(DocPOOS.class)
									   .add(Restrictions.eq("namePack", attachment))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
								
			List<DocPOOS> listDocPOOS = criteria.list();
			if (listDocPOOS.isEmpty()) 
				return false;
			
			args[0] = listDocPOOS;
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ��������� ������ POOS", e);
			
		} finally {
			session.close();
		}
		return true;
	}
	
	
	
	/**
	 * 
	 * @param sessionFactory
	 * @param attachment
	 * @param args
	 * @return
	 * @throws Exception
	 */
	private static boolean isDocPSMP(SessionFactory sessionFactory, 
								String attachment,
								Object[] args) throws Exception {
		//������� POOS
		Session session = null;
		Transaction t = null;
		MergedPSMP mergedPSMP = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(MergedPSMP.class)
									   .add(Restrictions.eq("packName", attachment))
									   .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
								
			List<MergedPSMP> listMergedPSMP = criteria.list();
			if (listMergedPSMP.isEmpty()) 
				return false;
			
			args[0] = listMergedPSMP;
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ��������� ������ mergedPSMP", e);
			
		} finally {
			session.close();
		}
		return true;
	}
	
	
	
	
	
	
	/**
	 * 
	 * @param file
	 * @param sessionFactory
	 * @param pathArchive
	 * @return
	 * @throws Exception
	 */
	public static Notification getNotification(File file, 
									SessionFactory sessionFactory,
									Path pathArchive) throws Exception {
		
		ParserNotification parserNotification = new ParserNotification(file);
		parserNotification.parse();
		
		Object[] args = new Object[1];
		args[0] = new String();
		
		//�������� ��������
		//�������������� � ������� �� ��� ����
		if (isNotificationRegisterTF(parserNotification.getAttachment())) {
			CreatedRegisterTF createdRegisterTF = getCreatedRegisterTF(sessionFactory, parserNotification.getAttachment());
			return new NotificationRegisterTF(parserNotification, 
										pathArchive, 
										createdRegisterTF);
			
		} else if (isNotificationBank(parserNotification.getAttachment(), args)) {
			String shortName = (String) args[0];
			DeliveryOrg deliveryOrg = MainFrame.dictDeliveryOrgByShortName.get(shortName);
			if (deliveryOrg == null) 
				throw new Exception("�� �������� ���� �� ����� ��������");
	
			//����������� ���� ������������� ���������
			if (isListTransferToBank(sessionFactory, parserNotification.getAttachment(), args)) {
				List<ListTransferToBank> listLTToBank = (List<ListTransferToBank>) args[0];
				NotificationLTToBank findNotificationLTToBank = listLTToBank.get(0).getNotificationLTToBank();
				NotificationLTToBank notificationToBank = null;
				
				if ((findNotificationLTToBank == null) || 
				   ((findNotificationLTToBank.getTypeResultNotification().getName().equals("��������")) && (parserNotification.getTypeResultNotification().equals("���������")))) {
					
					notificationToBank = new NotificationLTToBank(parserNotification, pathArchive, deliveryOrg);
					
					for (ListTransferToBank lt : listLTToBank) {
						notificationToBank.getSetListTransferToBank().add(lt);
						lt.setNotificationLTToBank(notificationToBank);
					}
				}
						
				return notificationToBank;
				
			} else if (isDocPOOD(sessionFactory, parserNotification.getAttachment(), args)) {
					
					List<DocPOOD> listDocPOOD = (List<DocPOOD>) args[0];
					NotificationPOOD findNotificationPOOD = listDocPOOD.get(0).getNotificationPOOD();
					NotificationPOOD notificationPOOD = null;
					
					if ((findNotificationPOOD == null) || 
					   ((findNotificationPOOD.getTypeResultNotification().getName().equals("��������")) && (parserNotification.getTypeResultNotification().equals("���������")))) {
						
						notificationPOOD = new NotificationPOOD(parserNotification, pathArchive, deliveryOrg);	
						
						for (DocPOOD docPOOD : listDocPOOD) {
							notificationPOOD.getSetDocPOOD().add(docPOOD);
							docPOOD.setNotificationPOOD(notificationPOOD);
						}
					}
					
					return notificationPOOD;
				
			} else if (isDocPOOS(sessionFactory, parserNotification.getAttachment(), args)) {
				List<DocPOOS> listDocPOOS = (List<DocPOOS>) args[0];
				DocPOOS docPOOS = listDocPOOS.get(0);
				
				NotificationPOOS notificationPOOS = new NotificationPOOS(parserNotification, pathArchive, deliveryOrg, docPOOS);	
				docPOOS.getSetNotificationsPOOS().add(notificationPOOS);	
				return notificationPOOS;
				
			} else if (isDocPSMP(sessionFactory, parserNotification.getAttachment(), args)) {
				List<MergedPSMP> listMergedPSMP = (List<MergedPSMP>) args[0];
				MergedPSMP mergedPSMP = listMergedPSMP.get(0);
				
				NotificationPSMP notificationPSMP = new NotificationPSMP(parserNotification, pathArchive, deliveryOrg, mergedPSMP);	
				mergedPSMP.getSetNotificationsPSMP().add(notificationPSMP);	
				return notificationPSMP;
			}
		}
		
		return null;
		
		
	}

}
