package ru.pfrf.entityinfoexchange.notification;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import javax.persistence.*;

import main.MainFrame;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.NameOrgInFile;
import ru.pfrf.classifier.TypeResultNotification;
import ru.pfrf.entityinfoexchange.EntityInfoExchange;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;

@MappedSuperclass
public abstract class Notification extends EntityInfoExchange {
	private static final long serialVersionUID = 1L;

	@Column(name = "attachment")
	protected String attachment;

	@Column(name = "date")
	protected Date date;

	@Column(name = "numberMail")
	protected Integer numberMail;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typeResultNotification")
	protected TypeResultNotification typeResultNotification;
	
	@Column(name = "dateProcessing")
	protected Date dateProcessing;
	
	@Column(name = "isSignatureValid")
	protected Boolean isSignatureValid;

	@Transient
	protected File file;
	
	@Transient
	protected Path pathArchive;

	public Boolean getIsSignatureValid() {
		return isSignatureValid;
	}

	public void setIsSignatureValid(Boolean isSignatureValid) {
		this.isSignatureValid = isSignatureValid;
	}

	public Path getPathArchive() {
		return pathArchive;
	}

	public Date getDateProcessing() {
		return dateProcessing;
	}

	public void setDateProcessing(Date dateProcessing) {
		this.dateProcessing = dateProcessing;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getNumberMail() {
		return numberMail;
	}

	public void setNumberMail(Integer numberMail) {
		this.numberMail = numberMail;
	}

	public TypeResultNotification getTypeResultNotification() {
		return typeResultNotification;
	}

	public void setTypeResultNotification(
			TypeResultNotification typeResultNotification) {
		this.typeResultNotification = typeResultNotification;
	}

	public File getFile() {
		return file;
	}

	public Notification() {}
	
	public Notification(ParserNotification parser, Path pathArchive) {
		this.file = parser.getFile();
		this.pathArchive = pathArchive;
		this.attachment = parser.getAttachment();
		this.date = parser.getDate();
		this.numberMail = parser.getNumberMail();
		this.isSignatureValid = parser.getIsSignatureValid();
		this.typeResultNotification = MainFrame.dictTypesResultsNotifications.get(parser.getTypeResultNotification());
		this.dateProcessing = new Date();
	}
	
	@Override
	public String toString() {
		return  typeResultNotification + 
				" | " + String.valueOf(numberMail) + 
				" | " + attachment +
				" | " + date + 
				" | " + dateProcessing;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		Notification notification = (Notification) obj;
		return new EqualsBuilder()
		                 .append(this.typeResultNotification, notification.typeResultNotification)
		                 .append(this.numberMail, notification.numberMail)
		                 .append(this.attachment,  notification.attachment)
		                 .append(this.date,  notification.date)
		                 .append(this.dateProcessing,  notification.dateProcessing)
		                 .isEquals();
	}
	
	public abstract void saveToArchive() throws Exception;
	

}
