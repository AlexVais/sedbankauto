package ru.pfrf.entityinfoexchange.notification;

import java.nio.file.Path;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;

@MappedSuperclass
public abstract class NotificationBank extends Notification{
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "deliveryOrg")
	private DeliveryOrg deliveryOrg;
	
	
	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public void setDeliveryOrg(DeliveryOrg deliveryOrg) {
		this.deliveryOrg = deliveryOrg;
	}

	public NotificationBank() {}
	
	public NotificationBank(ParserNotification parser, 
					Path pathArchive, 
					DeliveryOrg deliveryOrg) {
		super(parser, pathArchive);
		this.deliveryOrg = deliveryOrg;
	}

	@Override
	public boolean equals(Object obj) {
		NotificationBank notification = (NotificationBank) obj;
		return new EqualsBuilder()
		                 .appendSuper(super.equals(notification))
		                 .append(this.deliveryOrg,  notification.deliveryOrg)
		                 .isEquals();
	}
	
}
