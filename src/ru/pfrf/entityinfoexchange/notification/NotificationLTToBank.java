package ru.pfrf.entityinfoexchange.notification;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.ListTransferToBank;

@Entity
@Table(name = "notifications_list_transfer_to_bank")
public class NotificationLTToBank extends NotificationBank {
	private static final long serialVersionUID = 1L;

	
	@OneToMany(mappedBy = "notificationLTToBank", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<ListTransferToBank> setListTransferToBank = new HashSet<>();
	

	public Set<ListTransferToBank> getSetListTransferToBank() {
		return setListTransferToBank;
	}

	public void setSetListTransferToBank(
			Set<ListTransferToBank> setListTransferToBank) {
		this.setListTransferToBank = setListTransferToBank;
	}


	@Override
	public void saveToArchive() throws Exception {
		ListTransferToBank lt = setListTransferToBank.iterator().next();
		CreatedRegisterTF reg = lt.getRegisterTFToOPFR2()
 		  						  .getRegisterTFMerger()
 		  						  .getRegisterTFFromOPFR()
 		  						  .getRegisterTFToOPFR()
 		  						  .getCreatedRegisterTF();
		
		File destDir = new File(pathArchive + "/�������� ������� �� ���������� � ����/" + 
							reg.getYearPay() + "/" + 
							reg.getMonthPay() + "/" + 
							lt.getNamePack()); 
		if (!destDir.exists())
			if (!destDir.mkdirs())
				throw new Exception("�� ������� ������� ������� " + destDir);

		String destFileName = "blank_" + reg.getYearPay() + "_" + 
										 reg.getMonthPay() + "_" + 
										 lt.getNamePack() + 
										 "_" + typeResultNotification.getName() + ".txt";

		Files.copy(file.toPath(), Paths.get(destDir + "/" + destFileName), StandardCopyOption.REPLACE_EXISTING);	
		
	}
	
	public NotificationLTToBank() {}
	
	public NotificationLTToBank(ParserNotification parser, 
					Path pathArchive, 
					DeliveryOrg deliveryOrg) {
		super(parser, pathArchive, deliveryOrg);
		
	}

}
