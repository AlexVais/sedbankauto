package ru.pfrf.entityinfoexchange.notification;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.transaction.Transaction;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import main.MainFrame;
import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOOD;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNamePfr;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;
import ru.pfrf.register.CreatedRegisterTF;
import ru.pfrf.register.ListTransferToBank;

@Entity
@Table(name = "notifications_pood")
public class NotificationPOOD extends NotificationBank{
	private static final long serialVersionUID = 1L;
	
	@OneToMany(mappedBy = "notificationPOOD", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<DocPOOD> setDocPOOD = new HashSet<>();

	@Override
	public void saveToArchive() throws Exception {
		
		DocPOOD docPOOD = this.setDocPOOD.iterator().next();
		ParserFileNamePfr parser = new ParserFileNamePfr(new File(docPOOD.getFileName()), MainFrame.dictNameOrgInFile);
		parser.parse();
		Integer month = Integer.valueOf(new SimpleDateFormat("MM").format(this.date));
		
		File destDir = new File(pathArchive + "/�������� POOD � ����/" + 
							parser.getYear() + "/" + 
							month + "/" + 
							docPOOD.getPackName()); 
		if (!destDir.exists())
			if (!destDir.mkdirs())
				throw new Exception("�� ������� ������� ������� " + destDir);

		String destFileName = "blank_" + parser.getYear() + "_" + 
										 month + "_" + 
										 docPOOD.getPackName() + 
										 "_" + typeResultNotification.getName() + ".txt";

		Files.copy(file.toPath(), Paths.get(destDir + "/" + destFileName), StandardCopyOption.REPLACE_EXISTING);		
	}
	
	
	public Set<DocPOOD> getSetDocPOOD() {
		return setDocPOOD;
	}

	public void setSetDocPOOD(Set<DocPOOD> setDocPOOD) {
		this.setDocPOOD = setDocPOOD;
	}
	
	public NotificationPOOD() {}

	public NotificationPOOD(ParserNotification parser, 
					Path pathArchive, 
					DeliveryOrg deliveryOrg) {
		super(parser, pathArchive, deliveryOrg);
	}

}
