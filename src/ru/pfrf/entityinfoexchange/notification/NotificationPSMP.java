package ru.pfrf.entityinfoexchange.notification;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;

import javax.persistence.*;

import main.MainFrame;
import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.entityinfoexchange.order.osmp.DocPOOS;
import ru.pfrf.entityinfoexchange.order.psmp.MergedPSMP;
import ru.pfrf.entityinfoexchange.parserfilename.ParserFileNamePfr;
import ru.pfrf.entityinfoexchange.parserfileofie.ParserNotification;

@Entity
@Table(name = "notifications_psmp")
public class NotificationPSMP extends NotificationBank {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mergedPSMP")
	protected MergedPSMP mergedPSMP;
	
	public NotificationPSMP() {};
	
	public NotificationPSMP(ParserNotification parser, 
			Path pathArchive, 
			DeliveryOrg deliveryOrg,
			MergedPSMP mergedPSMP) {
		super(parser, pathArchive, deliveryOrg);
		this.mergedPSMP = mergedPSMP;
	}

	public MergedPSMP getMergedPSMP() {
		return mergedPSMP;
	}

	public void setMergedPSMP(MergedPSMP mergedPSMP) {
		this.mergedPSMP = mergedPSMP;
	}

	@Override
	public void saveToArchive() throws Exception {
		ParserFileNamePfr parser = new ParserFileNamePfr(new File(this.mergedPSMP.getFileName()), MainFrame.dictNameOrgInFile);
		parser.parse();
		Integer month = Integer.valueOf(new SimpleDateFormat("MM").format(this.date));
		
		File destDir = new File(pathArchive + "/�������� PSMP � ����/" + 
							parser.getYear() + "/" + 
							month + "/" + 
							mergedPSMP.getPackName()); 
		if (!destDir.exists())
			if (!destDir.mkdirs())
				throw new Exception("�� ������� ������� ������� " + destDir);

		String destFileName = "blank_" + parser.getYear() + "_" + 
										 month + "_" + 
										 mergedPSMP.getPackName() + 
										 "_" + typeResultNotification.getName() + ".txt";

		Files.copy(file.toPath(), Paths.get(destDir + "/" + destFileName), StandardCopyOption.REPLACE_EXISTING);	
		
	}
}
