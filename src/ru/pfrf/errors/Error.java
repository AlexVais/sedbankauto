package ru.pfrf.errors;

import java.io.Serializable;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.TypeError;
import ru.pfrf.register.CommonRegisterTF;

@MappedSuperclass
public abstract class Error implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typeError")
	private TypeError typeError;
	

	public Integer getId() {
		return id;
	}

	public TypeError getTypeError() {
		return typeError;
	}

	public void setTypeError(TypeError typeError) {
		this.typeError = typeError;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	public Error() {}
	
	public Error(TypeError typeError) {
		this.typeError = typeError;
	}
	
	@Override
	public String toString() {
		return this.typeError.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		Error error = (Error) obj;
		return new EqualsBuilder()
		                 .append(this.typeError, error.typeError)
		                 .isEquals();	
	}
	

}
