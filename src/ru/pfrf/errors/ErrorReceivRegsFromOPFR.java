package ru.pfrf.errors;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.TypeError;
import ru.pfrf.register.RegisterTFFromOPFR;

@Entity
@Table(name = "errors_regs_tf_from_opfr")
public class ErrorReceivRegsFromOPFR extends Error {
	
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "registerTFFromOPFR")
	private RegisterTFFromOPFR registerTFFromOPFR;

	public RegisterTFFromOPFR getRegTFFromOPFR() {
		return registerTFFromOPFR;
	}

	public void setRegTFFromOPFR(RegisterTFFromOPFR regTFFromOPFR) {
		this.registerTFFromOPFR = regTFFromOPFR;
	}
	
	public ErrorReceivRegsFromOPFR() {}
	
	public ErrorReceivRegsFromOPFR(TypeError typeError, RegisterTFFromOPFR registerTFFromOPFR) {
		super(typeError);
		
		this.registerTFFromOPFR = registerTFFromOPFR;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		ErrorReceivRegsFromOPFR error = (ErrorReceivRegsFromOPFR) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(obj))
		                 .append(this.registerTFFromOPFR, error.registerTFFromOPFR)
		                 .isEquals();
	}
	
	
	
}
