package ru.pfrf.register;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.entityinfoexchange.EntityInfoExchange;
import ru.pfrf.entityinfoexchange.notification.NotificationLTToBank;
import ru.pfrf.process.SendLTToBankAdapter;

@Entity
@Table(name = "list_transfer_to_bank")
public class ListTransferToBank extends EntityInfoExchange {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "namePack")
	private String namePack;
	
	@Column(name = "dateSendVipnet")
	private Date dateSendVipnet;
	
	@ManyToOne
	@JoinColumn(name = "registerTFToOPFR2")
	private RegisterTFToOPFR2 registerTFToOPFR2;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "notificationLTToBank")
	private NotificationLTToBank notificationLTToBank;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sendListTransferToBank")
	private SendLTToBankAdapter sendLTToBank;

	
	public SendLTToBankAdapter getSendLTToBank() {
		return sendLTToBank;
	}

	public void setSendLTToBank(SendLTToBankAdapter sendLTToBank) {
		this.sendLTToBank = sendLTToBank;
	}

	public Date getDateSendVipnet() {
		return dateSendVipnet;
	}

	public void setDateSendVipnet(Date dateSendVipnet) {
		this.dateSendVipnet = dateSendVipnet;
	}

	public NotificationLTToBank getNotificationLTToBank() {
		return notificationLTToBank;
	}

	public void setNotificationLTToBank(NotificationLTToBank notificationLTToBank) {
		this.notificationLTToBank = notificationLTToBank;
	}



	public String getNamePack() {
		return namePack;
	}

	public void setNamePack(String namePack) {
		this.namePack = namePack;
	}

	public RegisterTFToOPFR2 getRegisterTFToOPFR2() {
		return registerTFToOPFR2;
	}

	public void setRegisterTFToOPFR2(RegisterTFToOPFR2 registerTFToOPFR2) {
		this.registerTFToOPFR2 = registerTFToOPFR2;
	}
	
	public ListTransferToBank() {}
	
	@Override
	public String toString() {
		return namePack + 
				" | " + String.valueOf(this.registerTFToOPFR2.getId()) + 
				" | " + String.valueOf(this.sendLTToBank.getId()) +
				" | " + this.dateSendVipnet;
			
				
							
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		ListTransferToBank l = (ListTransferToBank) obj;
		return new EqualsBuilder()
		                 .append(this.namePack, l.namePack)
		                 .append(this.getRegisterTFToOPFR2(), l.getRegisterTFToOPFR2())
		                 .append(this.sendLTToBank, l.sendLTToBank)
		                 .isEquals();	
	}

}
