package ru.pfrf.register;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.SubtypePay;
import ru.pfrf.classifier.TypePay;
import ru.pfrf.entityinfoexchange.EntityInfoExchange;
import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.notification.NotificationRegisterTF;
import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.entityinfoexchange.order.spis.DocLTIncRegisterTF;
import ru.pfrf.process.FileOfPackFromCombankAdapter;

@Entity
@Table(name = "registers_tf")
public class CreatedRegisterTF extends CommonRegisterTF {
	private static final long serialVersionUID = 1L;

	@Column(name = "number")
	private Integer number;
	
	@Column(name = "dateOfFunding")
	private Date dateOfFunding;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "counterparty")
	private DeliveryOrg deliveryOrg;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typePay")
	private TypePay typePay;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subtypePay")
	private SubtypePay subtypePay;
	
	@Column(name = "monthPay")
	private Integer monthPay;
	
	@Column(name = "yearPay")
	private Integer yearPay;
	
//	@Column(name = "headCenter")
//	private Integer headCenter;
//	
//	@Column(name = "accountantGeneral")
//	private Integer accountantGeneral;
//	
//	@Column(name = "registerPreparated")
//	private Integer registerPreparated;
	
	@Column(name = "sum")
	private BigDecimal sum;
	
	@Column(name = "countRecipients")
	private Integer countRecipients;
	
	@Column(name = "isWorkedMoveOZAC")
	private Boolean isWorkedMoveOZAC;
	
	@Column(name = "dateMoveOZAC")
	private Date dateMoveOZAC; 
	
	@Column(name = "isWorkedCancelMoveOZAC")
	private Boolean isWorkedCancelMoveOZAC;
	
	@Column(name = "dateCancelMoveOZAC")
	private Date dateCancelMoveOZAC;
	
//	@Column(name = "formationRegisterTF")
//	private Integer formationRegisterTF;
//	
	@BatchSize(size = 200)
	@OneToOne(mappedBy = "createdRegisterTF", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private RegisterTFToOPFR registerTFToOPFR;
	
	@OneToMany(mappedBy = "createdRegisterTF", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected Set<DocPOSD> setDocPOSD = new HashSet<>();
	
	@BatchSize(size = 200)
	@OneToMany(mappedBy = "createdRegisterTF", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected Set<NotificationRegisterTF> setNotificationRegisterTF = new HashSet<>();
	
	@OneToMany(mappedBy = "createdRegisterTF", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	protected Set<DocLTIncRegisterTF> setDocLTIncRegisterTF = new HashSet<>();
	


	public Boolean getIsWorkedCancelMoveOZAC() {
		return isWorkedCancelMoveOZAC;
	}

	public void setIsWorkedCancelMoveOZAC(Boolean isWorkedCancelMoveOZAC) {
		this.isWorkedCancelMoveOZAC = isWorkedCancelMoveOZAC;
	}

	public Date getDateCancelMoveOZAC() {
		return dateCancelMoveOZAC;
	}

	public void setDateCancelMoveOZAC(Date dateCancelMoveOZAC) {
		this.dateCancelMoveOZAC = dateCancelMoveOZAC;
	}

	public Boolean getIsWorkedMoveOZAC() {
		return isWorkedMoveOZAC;
	}

	public void setIsWorkedMoveOZAC(Boolean isWorkedMoveOZAC) {
		this.isWorkedMoveOZAC = isWorkedMoveOZAC;
	}

	public Date getDateMoveOZAC() {
		return dateMoveOZAC;
	}

	public void setDateMoveOZAC(Date dateMoveOZAC) {
		this.dateMoveOZAC = dateMoveOZAC;
	}

	public Set<DocLTIncRegisterTF> getSetDocLTIncRegisterTF() {
		return setDocLTIncRegisterTF;
	}

	public void setSetDocLTIncRegisterTF(
			Set<DocLTIncRegisterTF> setDocLTIncRegisterTF) {
		this.setDocLTIncRegisterTF = setDocLTIncRegisterTF;
	}

	public RegisterTFToOPFR getRegisterTFToOPFR() {
		return registerTFToOPFR;
	}

	public void setRegisterTFToOPFR(RegisterTFToOPFR registerTFToOPFR) {
		this.registerTFToOPFR = registerTFToOPFR;
	}

	public TypePay getTypePay() {
		return typePay;
	}

	public void setTypePay(TypePay typePay) {
		this.typePay = typePay;
	}

	public SubtypePay getSubtypePay() {
		return subtypePay;
	}

	public void setSubtypePay(SubtypePay subtypePay) {
		this.subtypePay = subtypePay;
	}


	public Set<NotificationRegisterTF> getSetNotificationRegisterTF() {
		return setNotificationRegisterTF;
	}

	public void setSetNotificationRegisterTF(
			Set<NotificationRegisterTF> setNotificationRegisterTF) {
		this.setNotificationRegisterTF = setNotificationRegisterTF;
	}

	public Set<DocPOSD> getSetDocPOSD() {
		return setDocPOSD;
	}

	public void setSetDocPOSD(Set<DocPOSD> setDocPOSD) {
		this.setDocPOSD = setDocPOSD;
	}

	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public void setDeliveryOrg(DeliveryOrg deliveryOrg) {
		this.deliveryOrg = deliveryOrg;
	}
	

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Date getDateOfFunding() {
		return dateOfFunding;
	}

	public void setDateOfFunding(Date dateOfFunding) {
		this.dateOfFunding = dateOfFunding;
	}

	public Integer getMonthPay() {
		return monthPay;
	}

	public void setMonthPay(Integer monthPay) {
		this.monthPay = monthPay;
	}

	public Integer getYearPay() {
		return yearPay;
	}

	public void setYearPay(Integer yearPay) {
		this.yearPay = yearPay;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public Integer getCountRecipients() {
		return countRecipients;
	}

	public void setCountRecipients(Integer countRecipients) {
		this.countRecipients = countRecipients;
	}

	public CreatedRegisterTF() {}
	
	public CreatedRegisterTF(Integer number,
						Integer yearPay,
						Integer monthPay,
						Integer version) {
		super(version);
		this.number = number;
		this.yearPay = yearPay;
		this.monthPay = monthPay;
	}
	
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + String.valueOf(number) +
				" | " + dateOfFunding.toString() + 
				" | " + monthPay.toString() + 
				" | " + yearPay.toString() +
				" | " + String.valueOf(sum) +
				" | " + String.valueOf(countRecipients);
	}
	
	@Override
	public boolean equals(Object obj) {
		CreatedRegisterTF r = (CreatedRegisterTF) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(r))
		                 .append(this.number, r.number)
		                 .append(this.dateOfFunding, r.dateOfFunding)
		                 .append(this.deliveryOrg, r.deliveryOrg)
		                 .append(this.yearPay, r.yearPay)
		                 .append(this.sum, r.sum)
		                 .append(this.countRecipients, r.countRecipients)
		                 .isEquals();	
	}
	
}
