package ru.pfrf.register;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.EntityInfoExchange;

@MappedSuperclass
public abstract class CommonRegisterTF extends EntityInfoExchange {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "version")
	protected Integer version;
	
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public CommonRegisterTF() {}

	public CommonRegisterTF(Integer version) {
		this.version = version;
	}
	
	@Override
	public String toString() {
		return String.valueOf(version);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		CommonRegisterTF r = (CommonRegisterTF) obj;
		return new EqualsBuilder()
		                 .append(this.version, r.version)
		                 .isEquals();	
	}
}
