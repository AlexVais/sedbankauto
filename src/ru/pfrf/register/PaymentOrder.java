package ru.pfrf.register;

import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.entityinfoexchange.EntityInfoExchange;

@Entity
@Table(name = "payment_orders")
public class PaymentOrder extends EntityInfoExchange{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "number")
	private Integer number;
	
	@Column(name = "date")
	private Date date;
	
	@OneToOne
	@JoinColumn(name = "registerTFFromOPFR")
	private RegisterTFFromOPFR registerTFFromOPFR;
	
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public RegisterTFFromOPFR getRegisterTFFromOPFR() {
		return registerTFFromOPFR;
	}

	public void setRegisterTFFromOPFR(RegisterTFFromOPFR registerTFFromOPFR) {
		this.registerTFFromOPFR = registerTFFromOPFR;
	}

	public PaymentOrder() {}
	
	public PaymentOrder(Integer number, Date date, RegisterTFFromOPFR registerTFFromOPFR) {
		this.number = number;
		this.date = date;
		this.registerTFFromOPFR = registerTFFromOPFR;
	}
	
	@Override
	public String toString() {
		return String.valueOf(number) +
				" | " + date;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		PaymentOrder p = (PaymentOrder) obj;
		return new EqualsBuilder()
		                 .append(this.number, p.number)
		                 .append(this.date, p.date)
		                 .isEquals();	
	}

}
