package ru.pfrf.register;

import javax.persistence.*;

@Entity
@Table(name = "registers_tf_merger_xmlmanager")
public class RegisterTFMerger extends CommonRegisterTF {
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	@JoinColumn(name = "registerTFFromOPFR")
	private RegisterTFFromOPFR registerTFFromOPFR;
	
	@OneToOne(mappedBy = "registerTFMerger", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private RegisterTFToOPFR2 registerTFToOPFR2;

	public RegisterTFToOPFR2 getRegisterTFToOPFR2() {
		return registerTFToOPFR2;
	}

	public void setRegisterTFToOPFR2(RegisterTFToOPFR2 registerTFToOPFR2) {
		this.registerTFToOPFR2 = registerTFToOPFR2;
	}

	public RegisterTFFromOPFR getRegisterTFFromOPFR() {
		return registerTFFromOPFR;
	}

	public void setRegisterTFFromOPFR(RegisterTFFromOPFR registerTFFromOPFR) {
		this.registerTFFromOPFR = registerTFFromOPFR;
	}
	
	public RegisterTFMerger() {}
	

}
