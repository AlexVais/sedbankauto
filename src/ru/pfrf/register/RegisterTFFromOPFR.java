package ru.pfrf.register;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.reflect.TypeUtilsTest.This;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.order.ozac.DocPOSD;
import ru.pfrf.errors.ErrorReceivRegsFromOPFR;
import ru.pfrf.payment.PaymentOZAC;
import ru.pfrf.process.ReceivingPOSDAdapter;
import ru.pfrf.process.ReceivingRegsFromOPFR;
import ru.pfrf.process.ReceivingRegsFromOPFRAdapter;

@Entity
@Table(name = "registers_tf_from_opfr")
public class RegisterTFFromOPFR extends CommonRegisterTF {
	private static final long serialVersionUID = 1L;
	
	@Transient
	private File file;
	
	@ManyToOne
	@JoinColumn(name = "registerTFToOPFR")
	private RegisterTFToOPFR registerTFToOPFR;
	
	@OneToOne(mappedBy = "registerTFFromOPFR", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private PaymentOrder paymentOrder;
	
	@OneToOne(mappedBy = "registerTFFromOPFR", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private RegisterTFMerger registerTFMerger;
	
	@ManyToOne
	@JoinColumn(name = "newReceivingRegsFromOPFR")
	private ReceivingRegsFromOPFR newReceivingRegsFromOPFR;
	
	@OneToMany(mappedBy = "registerTFFromOPFR", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<ErrorReceivRegsFromOPFR> setErrorReceivRegsFromOPFR = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ReceivingRegsFromOPFR")
	private ReceivingRegsFromOPFRAdapter receivingRegsTFFromOPFRAdapter;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public ReceivingRegsFromOPFR getReceiving() {
		return newReceivingRegsFromOPFR;
	}

	public ReceivingRegsFromOPFR getNewReceivingRegsFromOPFR() {
		return newReceivingRegsFromOPFR;
	}

	public void setNewReceivingRegsFromOPFR(
			ReceivingRegsFromOPFR newReceivingRegsFromOPFR) {
		this.newReceivingRegsFromOPFR = newReceivingRegsFromOPFR;
	}

	public ReceivingRegsFromOPFRAdapter getReceivingRegsTFFromOPFRAdapter() {
		return receivingRegsTFFromOPFRAdapter;
	}

	public void setReceivingRegsTFFromOPFRAdapter(
			ReceivingRegsFromOPFRAdapter receivingRegsTFFromOPFRAdapter) {
		this.receivingRegsTFFromOPFRAdapter = receivingRegsTFFromOPFRAdapter;
	}

	public void setReceiving(ReceivingRegsFromOPFR receiving) {
		this.newReceivingRegsFromOPFR = receiving;
	}

	public Set<ErrorReceivRegsFromOPFR> getSetErrorReceivRegsFromOPFR() {
		return setErrorReceivRegsFromOPFR;
	}

	public void setSetErrorReceivRegsFromOPFR(
			Set<ErrorReceivRegsFromOPFR> setErrorReceivRegsFromOPFR) {
		this.setErrorReceivRegsFromOPFR = setErrorReceivRegsFromOPFR;
	}

	public RegisterTFMerger getRegisterTFMerger() {
		return registerTFMerger;

	}

	public void setRegisterTFMerger(RegisterTFMerger registerTFMerger) {
		this.registerTFMerger = registerTFMerger;
	}

	public PaymentOrder getPaymentOrder() {
		return paymentOrder;
	}

	public void setPaymentOrder(PaymentOrder paymentOrder) {
		this.paymentOrder = paymentOrder;
	}

	public RegisterTFToOPFR getRegisterTFToOPFR() {
		return registerTFToOPFR;
	}

	public void setRegisterTFToOPFR(RegisterTFToOPFR registerTFToOPFR) {
		this.registerTFToOPFR = registerTFToOPFR;
	}
	
	
	public RegisterTFFromOPFR() {}
	
	public RegisterTFFromOPFR(Integer version, 
				RegisterTFToOPFR registerTFToOPFR, 
				ReceivingRegsFromOPFR receiving,
				File file) {
		super(version);
		
		this.registerTFToOPFR = registerTFToOPFR;
		this.newReceivingRegsFromOPFR = receiving;
		this.file = file;
		
	}
	
	
	@Override
	public boolean equals(Object obj) {
		RegisterTFFromOPFR reg = (RegisterTFFromOPFR) obj;
		return new EqualsBuilder()
					     .appendSuper(super.equals(reg))
		                 .append(this.paymentOrder, reg.paymentOrder)
		                 .append(this.newReceivingRegsFromOPFR, reg.newReceivingRegsFromOPFR)
		                 .isEquals();
	}


}
