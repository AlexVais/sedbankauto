package ru.pfrf.register;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.entityinfoexchange.order.ozac.OZAC;

@Entity
@Table(name = "registers_tf_to_opfr")
public class RegisterTFToOPFR extends CommonRegisterTF {
	private static final long serialVersionUID = 1L;

	@OneToOne
	@JoinColumn(name = "registerTF")
	private CreatedRegisterTF createdRegisterTF;
	
	@OneToMany(mappedBy = "registerTFToOPFR", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<RegisterTFFromOPFR> setRegisterTFFromOPFR = new HashSet<>();
	
	@Column(name = "dateSendVipnet")
	private Date dateSendVipnet;
	
	public Date getDateSendVipnet() {
		return dateSendVipnet;
	}

	public void setDateSendVipnet(Date dateSendVipnet) {
		this.dateSendVipnet = dateSendVipnet;
	}

	public Set<RegisterTFFromOPFR> getSetRegisterTFFromOPFR() {
		return setRegisterTFFromOPFR;
	}

	public void setSetRegisterTFFromOPFR(
			Set<RegisterTFFromOPFR> setRegisterTFFromOPFR) {
		this.setRegisterTFFromOPFR = setRegisterTFFromOPFR;
	}

	public CreatedRegisterTF getCreatedRegisterTF() {
		return createdRegisterTF;
	}

	public void setCreatedRegisterTF(CreatedRegisterTF createdRegisterTF) {
		this.createdRegisterTF = createdRegisterTF;
	}

	
	public RegisterTFToOPFR() {}
	
	
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + this.createdRegisterTF.getId();
	}
	

	@Override
	public boolean equals(Object obj) {
		RegisterTFToOPFR reg = (RegisterTFToOPFR) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(reg))
		                 .append(this.createdRegisterTF.getId(), reg.getCreatedRegisterTF().getId())
		                 .isEquals();
	}
	
	
}
