package ru.pfrf.register;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "registers_tf_to_opfr2")
public class RegisterTFToOPFR2 extends CommonRegisterTF {
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	@JoinColumn(name = "registerTFMerger")
	private RegisterTFMerger registerTFMerger;
	
	@OneToMany(mappedBy = "registerTFToOPFR2", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ListTransferToBank> listTransferToBank = new HashSet<>();

	public Set<ListTransferToBank> getListTransferToBank() {
		return listTransferToBank;
	}

	public void setListTransferToBank(Set<ListTransferToBank> listTransferToBank) {
		this.listTransferToBank = listTransferToBank;
	}

	public RegisterTFMerger getRegisterTFMerger() {
		return registerTFMerger;
	}

	public void setRegisterTFMerger(RegisterTFMerger registerTFMerger) {
		this.registerTFMerger = registerTFMerger;
	}

	
	public RegisterTFToOPFR2() {}
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + this.registerTFMerger.getId();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		RegisterTFToOPFR2 r = (RegisterTFToOPFR2) obj;
		return new EqualsBuilder()
		                 .append(this.version, r.version)
		                 .append(this.id, r.id)
		                 .isEquals();	
	}

}
