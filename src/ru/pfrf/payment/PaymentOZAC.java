package ru.pfrf.payment;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.person.RecipientListTransfer;
import ru.pfrf.person.RecipientOZAC;

@Entity
@Table(name = "payments_ozac")
public class PaymentOZAC extends Payment {
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "RecipientOZAC")
	private RecipientOZAC recipientOZAC;
	
	public RecipientOZAC getRecipientOZAC() {
		return recipientOZAC;
	}

	public void setRecipientOZAC(RecipientOZAC recipientOZAC) {
		this.recipientOZAC = recipientOZAC;
	}

	public PaymentOZAC() {}
	
	public PaymentOZAC(Element elPayment, 
					Integer serialNumber,
					RecipientOZAC recipientOZAC) {
		super(elPayment, serialNumber);
		this.recipientOZAC = recipientOZAC;
	}
	
	@Override
	public boolean equals(Object obj) {
		PaymentOZAC paymentOZAC = (PaymentOZAC) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(paymentOZAC))
		                 .append(this.recipientOZAC, paymentOZAC.recipientOZAC)
		                 .isEquals();
	}
	
}
