package ru.pfrf.payment;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.SignPayment;
import ru.pfrf.entityinfoexchange.order.osmp.OSMPByArea;
import ru.pfrf.person.RecipientListTransfer;
import ru.pfrf.person.RecipientSPIS;


//@Entity
//@Table(name = "payments")
public class PaymentSPIS extends Payment {
	private static final long serialVersionUID = 1L;

	//@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	//@JoinColumn(name = "Recipient")
	private RecipientListTransfer recipientListTransfer;
	
	public RecipientListTransfer getRecipientListTransfer() {
		return recipientListTransfer;
	}

	public void setRecipientListTransfer(RecipientListTransfer recipientListTransfer) {
		this.recipientListTransfer = recipientListTransfer;
	}

	public PaymentSPIS() {}
	
	public PaymentSPIS(Element elPayment, 
					Integer serialNumber,
					RecipientListTransfer recipientListTransfer) {
		super(elPayment, serialNumber);
		
		this.recipientListTransfer = recipientListTransfer;
	}
	
	public PaymentSPIS(BigDecimal amountPayment,
					Date dateStartPeriod,
					Date dateEndPeriod,
					Integer viewPayPZ,
					SignPayment signPayment,
					Integer serialNumber,
					RecipientListTransfer recipientListTransfer) {
		
		super(amountPayment, dateStartPeriod, dateEndPeriod, viewPayPZ, signPayment, serialNumber);
		
		this.recipientListTransfer = recipientListTransfer;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		PaymentSPIS paymentSPIS = (PaymentSPIS) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(paymentSPIS))
		                 .append(this.recipientListTransfer, paymentSPIS.recipientListTransfer)
		                 .isEquals();
	}
	

}
