package ru.pfrf.payment;

import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import javax.persistence.*;

import main.MainFrame;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

import ru.pfrf.classifier.SignPayment;
import ru.pfrf.person.Person;
import ru.pfrf.person.Recipient;

@MappedSuperclass
public abstract class Payment implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID") 
	protected Integer id;
	
	@Column(name = "Amount")
	protected BigDecimal amountPayment;
	
	@Column(name = "dateBegin")
	protected Date dateStartPeriod;
	
	@Column(name = "dateEnd")
	protected Date dateEndPeriod;
	
	@Column(name = "viewPayPZ")
	protected Integer viewPayPZ;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "signPay")
	protected SignPayment signPayment;
	
	@Column(name = "serialNumber")
	protected Integer serialNumber;
	
	@Transient
	protected Element elPayment;

	public Element getElPayment() {
		return elPayment;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public SignPayment getSignPayment() {
		return signPayment;
	}

	public void setSignPayment(SignPayment signPayment) {
		this.signPayment = signPayment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getAmountPayment() {
		return amountPayment;
	}

	public void setAmountPayment(BigDecimal amountPayment) {
		this.amountPayment = amountPayment;
	}

	public Date getDateStartPeriod() {
		return dateStartPeriod;
	}

	public void setDateStartPeriod(Date dateStartPeriod) {
		this.dateStartPeriod = dateStartPeriod;
	}

	public Date getDateEndPeriod() {
		return dateEndPeriod;
	}

	public void setDateEndPeriod(Date dateEndPeriod) {
		this.dateEndPeriod = dateEndPeriod;
	}

	public Integer getViewPayPZ() {
		return viewPayPZ;
	}

	public void setViewPayPZ(Integer viewPayPZ) {
		this.viewPayPZ = viewPayPZ;
	}
	
	public Payment() {};
	
	public Payment(Element elPayment, Integer serialNumber) {
		this.elPayment = elPayment;
		this.serialNumber = serialNumber;
	}

	public Payment(BigDecimal amountPayment,
				Date dateStartPeriod,
				Date dateEndPeriod,
				Integer viewPayPZ,
				SignPayment signPayment,
				Integer serialNumber) {
		
		this.amountPayment = amountPayment;
		this.dateStartPeriod = dateStartPeriod;
		this.dateEndPeriod = dateEndPeriod;
		this.viewPayPZ = viewPayPZ;
		this.serialNumber = serialNumber;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.valueOf(amountPayment)).append(" | ")
		  .append(String.valueOf(dateStartPeriod)).append(" | ")
		  .append(String.valueOf(dateEndPeriod)).append(" | ")
		  .append(String.valueOf(viewPayPZ)).append(" | ")
		  .append(String.valueOf(serialNumber));
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		Payment payment = (Payment) obj;
		return new EqualsBuilder()
		                 .append(this.amountPayment, payment.amountPayment)
		                 .append(this.dateStartPeriod, payment.dateStartPeriod)
		                 .append(this.dateEndPeriod,  payment.dateEndPeriod)
		                 .append(this.viewPayPZ, payment.viewPayPZ)
		                 .append(this.serialNumber, payment.serialNumber)
		                 .isEquals();
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	public void parse() throws Exception {
		try {
			this.signPayment = MainFrame.dictSignPayments.get(elPayment.element("��������������").getText());
			if (signPayment == null)
				throw new Exception("������� ������� �� ��������");
			
			this.amountPayment = new BigDecimal(elPayment.element("�������������").getText());
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ROOT);
			simpleDateFormat.setLenient(false);
			this.dateStartPeriod = simpleDateFormat.parse(elPayment.element("�����������������").getText());
			this.dateEndPeriod = simpleDateFormat.parse(elPayment.element("����������������").getText());
			this.viewPayPZ = Integer.valueOf(elPayment.element("��������������").getText());
			
		} catch(Throwable e) {
			throw new Exception("������ ��� �������� <�������>." + elPayment.asXML(), e);
		}
	}
	
	/**
	 * ���������� �� �����
	 * @return
	 */
	public static Comparator<Payment> comparatorByAmountPaymentAsc() {
		return new Comparator<Payment>() {
			@Override
			public int compare(Payment o1, Payment o2) {
				return o1.getAmountPayment().compareTo(o2.getAmountPayment());
			}
		};
	}
	
	/**
	 * ���������� �� ����� � �������� �������
	 * @return
	 */
	public static Comparator<Payment> comparatorByAmountPaymentAndSignPayAsc() {
		return new Comparator<Payment>() {
			@Override
			public int compare(Payment o1, Payment o2) {
				String str1 = o1.getAmountPayment() + " " + o1.getSignPayment();
				String str2 = o2.getAmountPayment() + " " + o2.getSignPayment();
				return str1.compareTo(str2);
			}
		};
	}
	
	
}
