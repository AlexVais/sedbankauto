package ru.pfrf.classifier;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Dictionary<V extends Classifier> extends HashMap<Object, V> {
	private static final long serialVersionUID = 1L;
	
	private SessionFactory sessionFactory;
	private String className;
	private ValueKey vk;
	
	public Dictionary(SessionFactory sessionFactory, String className, ValueKey vk) {
		this.sessionFactory = sessionFactory;
		this.className = className;
		this.vk = vk;
	}
	
	public void init() throws Exception {
		Session session = null;
		Transaction t = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			Criteria criteria = session.createCriteria(className);
			
			List<V> list = criteria.list();
			for (V v : list) {
				this.put(vk.getValueKey(v), v);
			}
			
			t.commit();	
			
		} catch (Exception e) {
			t.rollback();
			throw new Exception(e);
			
		} finally {
			session.close();
		}
	}
	
	public interface ValueKey<V> {
		public Object getValueKey(V obj);
	}

}
