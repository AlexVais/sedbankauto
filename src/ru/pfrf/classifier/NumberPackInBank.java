package ru.pfrf.classifier;

import java.util.List;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;


@Entity
@Table(name = "numbers_packs_in_bank")
public class NumberPackInBank extends Classifier{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "number")
	private Integer number;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "deliveryOrg")
	protected DeliveryOrg deliveryOrg;
	
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public void setDeliveryOrg(DeliveryOrg deliveryOrg) {
		this.deliveryOrg = deliveryOrg;
	}

	public NumberPackInBank() {}
	
	public NumberPackInBank(DeliveryOrg deliveryOrg, Integer number) {
		this.deliveryOrg = deliveryOrg;
		this.number = number;
	}
	
	@Override
	public String toString() {
		return String.valueOf(number);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		NumberPackInBank np = (NumberPackInBank) obj;
		return new EqualsBuilder()
		                 .append(this.number, np.number)
		                 .isEquals();
	}
	
	static public Integer lastNumberPack(SessionFactory sessionFactory, DeliveryOrg deliveryOrg) throws Exception {
		Session session = null;
		Transaction t = null;
		Integer numberPack = null;
		try {
			session = sessionFactory.openSession();
			t = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(NumberPackInBank.class)
									   .add(Restrictions.eq("deliveryOrg", deliveryOrg))	
									   .addOrder(Order.desc("number"))
									   .setMaxResults(1);
			List<NumberPackInBank> list = criteria.list();
			numberPack = list.get(0).number;
			
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw new Exception("������ ��������� ���������� ������ ������ � ��: " + deliveryOrg, e);
			
		} finally {
			session.close();
		}
		return numberPack;
	}
}
