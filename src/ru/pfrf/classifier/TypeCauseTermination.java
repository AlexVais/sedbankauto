package ru.pfrf.classifier;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "types_cause_termination")
public class TypeCauseTermination extends Classifier {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "name")
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TypeCauseTermination() {}
	
	@Override
	public String toString() {
		return name;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}

		TypeCauseTermination type = (TypeCauseTermination) obj;
		return new EqualsBuilder()
		                 .append(this.name, type.name)
		                 .isEquals();
	}
	

}
