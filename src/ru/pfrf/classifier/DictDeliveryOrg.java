package ru.pfrf.classifier;

import java.util.HashMap;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class DictDeliveryOrg extends HashMap<String, DeliveryOrg> {
	private static final long serialVersionUID = 1L; 
	
	public void init(SessionFactory sessionFactory) throws Exception {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			SQLQuery query = session.createSQLQuery("SELECT * FROM delivery_orgs");
			query.addEntity(DeliveryOrg.class);
			List<DeliveryOrg> list = query.list();
			transaction.commit();
			
			for (DeliveryOrg deliveryOrg : list) {
				this.put(deliveryOrg.getNameNVP(), deliveryOrg);
			}
		} catch (Exception e) {
			throw new Exception(e);
			
		} finally {
			session.close();
		}
	}
}