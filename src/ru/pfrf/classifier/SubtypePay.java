package ru.pfrf.classifier;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;


@Entity
@Table(name = "subtypes_pay")
public class SubtypePay extends Classifier{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "typePay")
	private TypePay typePay;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TypePay getTypePay() {
		return typePay;
	}

	public void setTypePay(TypePay typePay) {
		this.typePay = typePay;
	}
	
	public SubtypePay() {}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}

		SubtypePay subtypePay = (SubtypePay) obj;
		return new EqualsBuilder()
		                 .append(this.name, subtypePay.name)
		                 .isEquals();
	}
	

}
