package ru.pfrf.classifier;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import ru.pfrf.person.RecipientDead;

@Entity
@Table(name = "state_account_number")
public class StateAccountNumber implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID") 
	private Integer id;
	
	@Column(name = "value")
	private String value;
	
	@OneToMany(mappedBy = "stateAccountNumber", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<RecipientDead> recipientsDead = new HashSet<>();

	public String getValue() {
		return value;
	}

	public Set<RecipientDead> getRecipientsDead() {
		return recipientsDead;
	}

	public void setRecipientsDead(Set<RecipientDead> recipientsDead) {
		this.recipientsDead = recipientsDead;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public StateAccountNumber() {}
	
	@Override
	public String toString() {
		return this.value;
	}
		
}
