package ru.pfrf.classifier;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "signs_pay_recipient")
public class SignPayment extends Classifier {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "name") 
	protected String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SignPayment() {}
	
	public SignPayment(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return this.name;
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		SignPayment sp = (SignPayment) obj;
		return new EqualsBuilder()
		                 .append(this.name, sp.name)
		                 .isEquals();
	}

	
}
