package ru.pfrf.classifier;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "code_credited")
public class CodeCredited extends Classifier {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public CodeCredited() {}
	
	@Override
	public String toString() {
		return this.name + " | " + this.description;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}

		CodeCredited cc = (CodeCredited) obj;
		return new EqualsBuilder()
		                 .append(this.name, cc.name)
		                 .append(this.description, cc.description)
		                 .isEquals();
	}

}
