package ru.pfrf.classifier;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "types_differences_recipients")
public class TypeDifferenceRecipient extends Classifier{
	private static final long serialVersionUID = 1L;
	
	//���� �����������
	public final static String NUMBER_VD = "����� ���������� ����";
	public final static String AREA      = "�����";
	public final static String SURNAME   = "�������";
	public final static String NAME      = "���";
	public final static String PATR      = "��������";
	public final static String COUNT_PAYMENTS  = "���������� ������";
	public final static String AMOUNT_DELIVERY = "����� � ��������";
	public final static String AMOUNT_PAYMENT  = "����� �������";
	public final static String NUMBER_IN_ARRAY = "����� � �������";
	public final static String SIGN_PAY   = "������� �������";
	public final static String PERIOD_PAY = "������ �������";
	public final static String PZ         = "��� ������� �� ��";
	
	
	@Column(name = "name") 
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public TypeDifferenceRecipient() {}
	
	@Override
	public String toString() {
		return this.name;
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		TypeDifferenceRecipient td = (TypeDifferenceRecipient) obj;
		return new EqualsBuilder()
		                 .append(this.name, td.name)
		                 .isEquals();
	}
	

}
