package ru.pfrf.classifier;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "vidDocDead")
public class TypeDocDead extends Classifier{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "code")
	private Integer code;
	
	@Column(name = "name")
	private String name;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public TypeDocDead() {}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		TypeDocDead typeDocDead = (TypeDocDead) obj;
		return new EqualsBuilder()
						 .append(this.code, typeDocDead.code)	
		                 .append(this.name, typeDocDead.name)
		                 .isEquals();
	}
	
	@Override
	public String toString() {
		return code + 
				" | " + name;
	}

}
