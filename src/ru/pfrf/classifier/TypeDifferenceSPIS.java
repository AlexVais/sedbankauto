package ru.pfrf.classifier;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "types_differences_spis")
public class TypeDifferenceSPIS extends Classifier {
	private static final long serialVersionUID = 1L;
	
	
	//���� �����������
	public final static String COUNTS_RECIPIENTS_BY_TAG = "����������� � ���������� ����������� �� ����";
	public final static String COUNTS_RECIPIENTS_BY_DOC = "����������� � ���������� ����������� �� XML ���������";
	public final static String SUM = "����������� � ����� �� �������";

	@Column(name = "name") 
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public TypeDifferenceSPIS() {}
	
	@Override
	public String toString() {
		return this.name;
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		TypeDifferenceSPIS td = (TypeDifferenceSPIS) obj;
		return new EqualsBuilder()
		                 .append(this.name, td.name)
		                 .isEquals();
	}
	
	
}
