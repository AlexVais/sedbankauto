package ru.pfrf.classifier;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "types_errors")
public class TypeError extends Classifier {
	private static final long serialVersionUID = 1L;
	
	//������
	public final static String ERR_COMMONS_UNKNOWN = "����������� ������";
	
	//������ ����� �������� �� ����
	public final static String ERR_REGS_FILENAME    = "������ � ����� �����";
	public final static String ERR_REGS_NUMBERREG   = "������ � ������ �������";
	public final static String ERR_REGS_DATEFUNDING = "������ � ���� ��������������";
	public final static String ERR_REGS_DATEPAY     = "������ � ���� �������";
	public final static String ERR_REGS_COUNTRECIPIENTS = "������ � ���������� �����������";
	public final static String ERR_REGS_TOTALSUM    = "������ � �������� �����";
	public final static String ERR_REGS_NUMBERORDER = "������ � ������ ��������� ���������";
	public final static String ERR_REGS_DATEORDER   = "������ � ���� ��������� ���������";
	public final static String ERR_REGS_NUMBERORDEREXIST    = "��������� ����� ��������� ���������";
	public final static String ERR_REGS_DATEFUNDING_NOTEQUALS_DATEORDER    = "�� ��������� ���� �������������� � ��������� ���������";
	public final static String ERR_REGS_EXCELDOC    = "������ ������ � Excel ����������";
	
	//������ ��� �������� �����������
	public final static String ERR_RECIP_SNILS         = "�������� �����";
	public final static String ERR_RECIP_ACCOUNTNUMBER = "�������� ����� �����";
	public final static String ERR_RECIP_NUMBERVD      = "�������� ����� ��";
	public final static String ERR_RECIP_NUMBERINARRAY = "�������� ����� � �������";
	public final static String ERR_RECIP_PAYSUM        = "������ � ����� �������";
	public final static String ERR_RECIP_EMPTY_FIO     = "������� ��� ��� ���������� ������";
	
	//������ ��� ����� ������� �� ���. ������
	//public final static String ERR_PACKCOMBANK_ATTACHEDFILES = "������ ��������� ������";
	public final static String ERR_PACKCOMBANK_PACKNAME = "�������� ��������� ����� ������";
	public final static String ERR_PACKCOMBANK_NULLSIZE = "������� ������ ������";
	public final static String ERR_PACKCOMBANK_EMPTY    = "������ �����";
	public final static String ERR_PACKCOMBANK_UNKNOWNFILES    = "���������� ����������� ������";
	public final static String ERR_PACKCOMBANK_STRUCT_ARCHIVE  = "�������� ���������� ������";
	
	//������ ��� ����� ������� �������
	public final static String ERR_OSMP_REPEAT = "��������� ���� ������ �������";
	
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "discriminator")
	private String discriminator;

	public String getDiscriminator() {
		return discriminator;
	}

	public void setDiscriminator(String discriminator) {
		this.discriminator = discriminator;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public TypeError() {}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		TypeError typeError = (TypeError) obj;
		return new EqualsBuilder()
		                 .append(this.description, typeError.description)
		                 .append(this.discriminator, typeError.discriminator)
		                 .isEquals();
	}
	
	@Override
	public String toString() {
		return this.description + " | " + this.discriminator;
	}
	
	

}
