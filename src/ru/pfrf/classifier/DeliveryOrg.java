package ru.pfrf.classifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.entityinfoexchange.EntityIEBank;
import ru.pfrf.entityinfoexchange.EntityInfoExchange;
import ru.pfrf.person.Person;
import ru.pfrf.person.RecipientDead;

@Entity
@Table(name = "delivery_orgs")
public class DeliveryOrg extends Classifier implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//������������ ������
	public static final String ORG_SBER = "�������������� ��������� � 8636 ��� ��������";
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "nameNVP")
	private String nameNVP;
	
	@Column(name = "KodNVP")
	private String codeInNVP;
	
	@Column(name = "ShortName")
	private String shortName;
	
	@Column(name = "IndexOfParticipant")
	private String indexOfParticipant;
	
	@Column(name = "Name1C")
	private String name1C;

	public String getCodeInNVP() {
		return codeInNVP;
	}

	public void setCodeInNVP(String codeInNVP) {
		this.codeInNVP = codeInNVP;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getIndexOfParticipant() {
		return indexOfParticipant;
	}

	public void setIndexOfParticipant(String indexOfParticipant) {
		this.indexOfParticipant = indexOfParticipant;
	}

	public String getName1C() {
		return name1C;
	}

	public void setName1C(String name1c) {
		name1C = name1c;
	}

	public String getNameNVP() {
		return nameNVP;
	}

	public void setNameNVP(String nameNVP) {
		this.nameNVP = nameNVP;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public DeliveryOrg() {};
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name).append(" | ")
		  .append(nameNVP).append(" | ")
		  .append(codeInNVP).append(" | ")
		  .append(shortName).append(" | ")
		  .append(indexOfParticipant).append(" | ")
		  .append("name1C");
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}

		DeliveryOrg deliveryOrg = (DeliveryOrg) obj;
		return new EqualsBuilder()
		                 .append(this.name, deliveryOrg.name)
		                 .append(this.nameNVP, deliveryOrg.nameNVP)
		                 .append(this.codeInNVP,  deliveryOrg.codeInNVP)
		                 .append(this.shortName,  deliveryOrg.shortName)
		                 .append(this.indexOfParticipant,  deliveryOrg.indexOfParticipant)
		                 .append(this.name1C,  deliveryOrg.name1C)
		                 .isEquals();
	}
	

}
