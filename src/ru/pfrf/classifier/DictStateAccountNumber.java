package ru.pfrf.classifier;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ru.pfrf.register.CreatedRegisterTF;

public class DictStateAccountNumber extends HashMap<String, StateAccountNumber>{
	private static final long serialVersionUID = 1L; 
	
	public void init(SessionFactory sessionFactory) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		SQLQuery query = session.createSQLQuery("SELECT * FROM state_account_number");
		query.addEntity(StateAccountNumber.class);
		List<StateAccountNumber> list = query.list();
		transaction.commit();	
		
		for (StateAccountNumber stateAccountNumber : list) {
			this.put(stateAccountNumber.getValue(), stateAccountNumber);
		}
	}
	
	
	
}
