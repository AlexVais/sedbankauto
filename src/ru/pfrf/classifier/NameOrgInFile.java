package ru.pfrf.classifier;

import java.util.Date;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.person.Person;

@Entity
@Table(name = "names_org_in_file")
public class NameOrgInFile extends Classifier{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "Prefix")
	private String prefix;
	
	@Column(name = "codeBranch")
	private String codeBranch;
	
	@Column(name = "dateAdd")
	private Date dateAdd;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DeliveryOrg")
	private DeliveryOrg deliveryOrg;
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getCodeBranch() {
		return codeBranch;
	}

	public void setCodeBranch(String codeBranch) {
		this.codeBranch = codeBranch;
	}

	public Date getDateAdd() {
		return dateAdd;
	}

	public void setDateAdd(Date dateAdd) {
		this.dateAdd = dateAdd;
	}

	public DeliveryOrg getDeliveryOrg() {
		return deliveryOrg;
	}

	public void setDeliveryOrg(DeliveryOrg deliveryOrg) {
		this.deliveryOrg = deliveryOrg;
	}

	public NameOrgInFile() {}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(prefix).append(" | ")
		  .append(codeBranch).append(" | ")
		  .append(dateAdd);
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		NameOrgInFile name = (NameOrgInFile) obj;
		return new EqualsBuilder()
		                 .append(this.prefix, name.prefix)
		                 .append(this.codeBranch, name.codeBranch)
		                 .append(this.dateAdd,  name.dateAdd)
		                 .isEquals();
	}

}
