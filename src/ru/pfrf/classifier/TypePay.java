package ru.pfrf.classifier;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "types_pay")
public class TypePay extends Classifier {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "name")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public TypePay() {}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}

		TypePay typePay = (TypePay) obj;
		return new EqualsBuilder()
		                 .append(this.name, typePay.name)
		                 .isEquals();
	}
	
}
