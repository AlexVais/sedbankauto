package ru.pfrf.main.logproc.osmp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;

public class TableModelDocOSMP extends AbstractTableModel{
	private static final long serialVersionUID = 1L;
	
	private List<DocOSMP> listDocOSMP = new ArrayList<>(); 
	
	//private Integer[] marker = new Integer[12000000];
	
	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public List<DocOSMP> getListDocOSMP() {
		return listDocOSMP;
	}

	public TableModelDocOSMP(List<DocOSMP> listDocOSMP) {
		this.listDocOSMP = listDocOSMP;
	}

	@Override
	public String getColumnName(int c) {
		switch (c) {
			case 0: 
				return "��/�";
			
			case 1: 
				return "����� �����";
				
			case 2: 
				return "���. �����";
				
			case 3: 
				return "��� �����";
				
			case 4: 
				return "����";
				
			case 5: 
				return "���� �����(~Vipnet)";
				
			case 6: 
				return "���� ���������";
				
			default: 
				return "Other column";
		}
	}
	
	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public int getRowCount() {
		return listDocOSMP.size();
	}

	@Override
	public Object getValueAt(int r, int c) {
		switch (c) {
			case 0:
				return (r + 1);
						
			case 1: 
				return listDocOSMP.get(r).getListOSMP().iterator().next().getFileName().substring(52, 55);
				
			case 2: 
				return listDocOSMP.get(r).getListOSMP().iterator().next().getOutNumber();
				
			case 3: 
				return listDocOSMP.get(r).getListOSMP().iterator().next().getFileName();
				
			case 4: 
				return listDocOSMP.get(r).getDeliveryOrg().getName();
				
			case 5: 
				return dateFormat.format(listDocOSMP.get(r).getListOSMP().iterator().next().getDateOfVipnet());
				
			case 6: 
				return dateFormat.format(listDocOSMP.get(r).getReceivingOSMP().getDateEnd());
			
			default: 
				return "none";
		}
	}

}
