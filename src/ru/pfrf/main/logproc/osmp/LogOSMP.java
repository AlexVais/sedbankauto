package ru.pfrf.main.logproc.osmp;
import java.awt.EventQueue;

import javax.swing.JDialog;

import java.awt.Dialog.ModalityType;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JButton;

import java.awt.FlowLayout;

import javax.swing.JSplitPane;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JDesktopPane;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.MaskFormatter;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.osmp.DocPOOS;
import ru.pfrf.entityinfoexchange.order.osmp.OSMP;
import ru.pfrf.person.RecipientDead;
import ru.pfrf.process.ReceivingOSMP;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollPane;

import java.awt.Insets;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Cursor;
import java.awt.Dialog.ModalExclusionType;

import javax.swing.ListSelectionModel;

import java.awt.Window.Type;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.HierarchyEvent;

import javax.swing.JTextPane;

import java.awt.ComponentOrientation;

import javax.swing.DebugGraphics;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;

import java.awt.Rectangle;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;



public class LogOSMP extends JDialog {
	
	//Integer[] marker = new Integer[20000000];
	
	private SessionFactory sessionFactory;
	private JTable tableDocOSMP;
	
	private TableModel model;
	private TableModel modelFilesOfOSMP;
	private TableModel defaultTableModel;
	
	private JTable tableFilesOfDocOSMP;
	private JSplitPane splitPane;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JPanel panel_1;
	private JLabel lblDocOSMP;
	private JPanel panel_2;
	private JLabel lblPOOS;
	private JFormattedTextField formattedTextField;
	private JLabel lblNewLabel;
	private JLabel label;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JTextField tfNumberPack;
	private JButton btnFind;
	private JTextField tfDateBegin;
	private JTextField tfDateEnd;
	private JLabel lblNewLabel_3;
	


	/**
	 * Create the dialog.
	 */
	public LogOSMP(final SessionFactory sessionFactory) {
		setModalityType(ModalityType.APPLICATION_MODAL);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.sessionFactory = sessionFactory;
		
		setTitle("������ ����������� ����������� OSMP");
		setModal(true);
		setBounds(100, 100, 1186, 708);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		
		panel.setBorder(UIManager.getBorder("CheckBox.border"));
		panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		panel.setAutoscrolls(true);
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		getContentPane().add(panel, BorderLayout.SOUTH);
		
		
		btnFind = new JButton("�����");
		btnFind.setPreferredSize(new Dimension(72, 22));
		
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Integer numberPack = null;
				try {
					if (!"".equals(tfNumberPack.getText()))
						numberPack = Integer.valueOf(tfNumberPack.getText());
				} catch (Exception ex) {
					Object[] options = {"Ok"};
					int n = JOptionPane.showOptionDialog(btnFind, 
											"�������� �������� ������", 
											"�������������", JOptionPane.YES_OPTION, 
											JOptionPane.ERROR_MESSAGE, 
											null, 
											options, 
											options[0]);
					return;
				}
				
				SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ROOT);
				Date dateBegin = null;
				Date dateEnd = null;
				try {
					dateBegin = format.parse(tfDateBegin.getText());
					dateEnd = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss").parse(tfDateEnd.getText() + " 23.59.59");
				} catch (ParseException e) {
					e.printStackTrace();
					return;
				}
				
				
				Session session = sessionFactory.openSession();
				Transaction t = session.beginTransaction();
	
				Criteria criteria = session.createCriteria(DocOSMP.class, "doc")
										.createCriteria("doc.opvz", "opvz", JoinType.INNER_JOIN)
										.createCriteria("doc.receivingOSMP", "receivingOSMP", JoinType.INNER_JOIN)
										.createCriteria("doc.docPOOS", JoinType.INNER_JOIN)
										.createCriteria("doc.listOSMP", "osmp", JoinType.INNER_JOIN)
										.add(Restrictions.ge("osmp.dateOfVipnet", dateBegin))
										.add(Restrictions.le("osmp.dateOfVipnet", dateEnd))
										.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
										.addOrder(Order.asc("doc.numberPack"));
				if (numberPack != null) {
					criteria.add(Restrictions.eq("doc.numberPack", numberPack));
				}
				
							
				
				final List<DocOSMP> docs = criteria.list();
				//System.out.println("end query criteria");
				
				t.commit();
				session.close();
				
				
				//������� �� ���������
				if (modelFilesOfOSMP != null && ((AbstractTableModel) modelFilesOfOSMP).getTableModelListeners().length > 0) {
					modelFilesOfOSMP.removeTableModelListener(tableFilesOfDocOSMP);
					tableFilesOfDocOSMP.setModel(defaultTableModel);
					
				}
				if ( model != null && ((AbstractTableModel) model).getTableModelListeners().length > 0) {
					model.removeTableModelListener(tableDocOSMP);
					tableDocOSMP.setModel(defaultTableModel);
				}
				
				model = new TableModelDocOSMP(docs);
				tableDocOSMP.setModel(model);
				TableColumnModel tcm = tableDocOSMP.getColumnModel();
				tcm.getColumn(0).setMaxWidth(30);
				tcm.getColumn(1).setMaxWidth(80);
				tcm.getColumn(2).setMaxWidth(100);
			
				
			}
		});
		
		lblNewLabel_2 = new JLabel("\u041D\u043E\u043C\u0435\u0440 \u043F\u0430\u043A\u0435\u0442\u0430");
		panel.add(lblNewLabel_2);
		
		
		tfNumberPack = new JTextField();
		tfNumberPack.setColumns(8);
		panel.add(tfNumberPack);
		
		lblNewLabel_3 = new JLabel("  ");
		panel.add(lblNewLabel_3);
		
		lblNewLabel = new JLabel("\u0414\u0430\u0442\u0430 \u043F\u0440\u0438\u0451\u043C\u0430 \u0441 ");
		lblNewLabel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		lblNewLabel.setBounds(new Rectangle(8, 0, 0, 0));
		panel.add(lblNewLabel);
		
		tfDateBegin = new JTextField();
		tfDateBegin.setText("01.01.2016");
		panel.add(tfDateBegin);
		tfDateBegin.setColumns(10);
		
		label = new JLabel("\u043F\u043E");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel.add(label);
		
		tfDateEnd = new JTextField();
		tfDateEnd.setText("01.01.2016");
		panel.add(tfDateEnd);
		tfDateEnd.setColumns(10);
		
		
		
		panel.add(btnFind);
		
		panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		
		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		splitPane.setIgnoreRepaint(true);
		panel_1.add(splitPane, BorderLayout.CENTER);
		splitPane.setAutoscrolls(true);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		
		tableFilesOfDocOSMP = new JTable();
		tableFilesOfDocOSMP.setFont(new Font("Tahoma", Font.PLAIN, 11));
		getContentPane().add(tableFilesOfDocOSMP, BorderLayout.NORTH);
		tableFilesOfDocOSMP.setRowMargin(10);
		tableFilesOfDocOSMP.setRowHeight(23);
		

		
		tableDocOSMP = new JTable();
		getContentPane().add(tableDocOSMP, BorderLayout.NORTH);
		
		tableDocOSMP.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableDocOSMP.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableDocOSMP.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		tableDocOSMP.setRowMargin(10);
		tableDocOSMP.setRowHeight(23);
		tableDocOSMP.setIntercellSpacing(new Dimension(0, 0));
		tableDocOSMP.setFont(new Font("Tahoma", Font.PLAIN, 11));
		
		scrollPane = new JScrollPane(tableDocOSMP);
		scrollPane.setBounds(10, 11, 452, 402);
		
		scrollPane.setAutoscrolls(true);
		scrollPane.setMinimumSize(new Dimension(23, 50));
		
		scrollPane_1 = new JScrollPane(tableFilesOfDocOSMP);
		splitPane.setRightComponent(scrollPane_1);
		splitPane.setLeftComponent(scrollPane);
		
		panel_2 = new JPanel();
		panel_2.setBorder(UIManager.getBorder("PopupMenu.border"));
		panel_1.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		lblNewLabel_1 = new JLabel("POOS:");
		lblNewLabel_1.setForeground(Color.BLUE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		panel_2.add(lblNewLabel_1);
		
		lblPOOS = new JLabel("");
		lblPOOS.setHorizontalAlignment(SwingConstants.LEFT);
		panel_2.add(lblPOOS);
		
		lblDocOSMP = new JLabel("������ ����������� ������");
		getContentPane().add(lblDocOSMP, BorderLayout.NORTH);
		lblDocOSMP.setHorizontalAlignment(SwingConstants.CENTER);
		
		tableDocOSMP.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (!arg0.getValueIsAdjusting() && tableDocOSMP.getSelectedRow() >= 0) {
					DocOSMP docOSMP = ((TableModelDocOSMP) tableDocOSMP.getModel()).getListDocOSMP().get(tableDocOSMP.getSelectedRow());
					
					//������� �� ���������
					if ( modelFilesOfOSMP != null && ((AbstractTableModel) modelFilesOfOSMP).getTableModelListeners().length > 0) {
						modelFilesOfOSMP.removeTableModelListener(tableFilesOfDocOSMP);
					}
					
					modelFilesOfOSMP = new TableModelFilesOfOSMP(docOSMP);
					tableFilesOfDocOSMP.setModel(modelFilesOfOSMP);
					TableColumnModel tcm = tableFilesOfDocOSMP.getColumnModel();
					tcm.getColumn(0).setMaxWidth(30);
					tcm.getColumn(1).setWidth(30);
					
					//POOS
					DocPOOS docPOOS = docOSMP.getDocPOOS();
					String sendStr;
					if (docPOOS.getDateSend() != null) {
						sendStr = "  ���� ��������: " + docPOOS.getDateSend();
					} else {
						sendStr = "  �� �����������";
					}
					
					lblPOOS.setText(docPOOS.getFileName() + sendStr);
					
				}
				
			}
			
		});
		
		//��������� ���� ������� �� ���������
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.YYYY", Locale.ROOT);
		this.tfDateBegin.setText(format.format(new Date()));
		this.tfDateEnd.setText(format.format(new Date()));
		
		//������ �� ���������
		this.defaultTableModel = this.tableDocOSMP.getModel();
		
		

	}
}
