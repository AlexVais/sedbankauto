package ru.pfrf.main.logproc.osmp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ru.pfrf.entityinfoexchange.FileOfInfoExchange;
import ru.pfrf.entityinfoexchange.order.osmp.DocOSMP;
import ru.pfrf.entityinfoexchange.order.osmp.OSMPByArea;

public class TableModelFilesOfOSMP extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	
	private DocOSMP docOSMP;
	
	private List<FileOfInfoExchange> listFiles = new ArrayList<>();
	
	//private Integer[] marker = new Integer[11000000];
	
	public TableModelFilesOfOSMP(DocOSMP docOSMP) {
		this.docOSMP = docOSMP;
		this.listFiles.add(docOSMP.getOpvz());
		this.listFiles.addAll(docOSMP.getListOSMP());
		
		LinkedList<OSMPByArea> helperList = new LinkedList<>(docOSMP.getListOSMP().iterator().next().getListOsmpByArea());
		Collections.sort(helperList, OSMPByArea.comparatorByAreaAsc()); 
		this.listFiles.addAll(helperList);
	
	}

	@Override
	public String getColumnName(int c) {
		switch (c) {
			case 0: 
				return "��/�";
			
			case 1: 
				return "��� �����";
				
			default: 
				return "Other column";
		}
	}
	
	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		return this.listFiles.size();
	}

	@Override
	public Object getValueAt(int r, int c) {
		switch (c) {
			case 0:
				return (r + 1);
						
			case 1: 
				return listFiles.get(r).getFileName();
				
				
			case 2: 
				return docOSMP.getListOSMP().iterator().next().getFileName();
			
			default: 
				return "none";
		}
	}
}
