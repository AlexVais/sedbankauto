package ru.pfrf.person;

import java.util.*;

import javax.persistence.*;

import org.dom4j.Element;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.entityinfoexchange.order.spis.SPIS;
import ru.pfrf.payment.PaymentSPIS;

@Entity
@Table(name = "recipients")
public class RecipientListTransfer extends RecipientSPIS {
	private static final long serialVersionUID = 1L;
	
	
	//@BatchSize(size = 20000)
	//@OneToMany(mappedBy = "recipientListTransfer", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Transient
	private Set<PaymentSPIS> setPaymentSPIS = new HashSet<>();

//	@Transient
//	private SPIS spis;
	
	//@OneToOne(mappedBy = "recipientListTransfer", fetch = FetchType.LAZY)
	//private CorrectRecipient setCorrectRecipients;

//	public CorrectRecipient getSetCorrectRecipients() {
//		return setCorrectRecipients;
//	}
//
//	public void setSetCorrectRecipients(CorrectRecipient setCorrectRecipients) {
//		this.setCorrectRecipients = setCorrectRecipients;
//	}

//	public SPIS getSpis() {
//		return spis;
//	}
//	
//	public void setSpis(SPIS spis) {
//		this.spis = spis;
//	}
	
	public Set<PaymentSPIS> getSetPaymentSPIS() {
		return setPaymentSPIS;
	}
	
	public void setSetPaymentSPIS(Set<PaymentSPIS> setPaymentSPIS) {
		this.setPaymentSPIS = setPaymentSPIS;
	}
	
	public RecipientListTransfer() {}

	public RecipientListTransfer(Element element, SPIS spis) {
		super(element);
		
		//this.spis = spis;
	}
	
	@Override
	protected void parsePayments() throws Exception {
		List<Element> listElPayments = element.element("����������").elements("�������");
		for (Element elPayment : listElPayments) {
			PaymentSPIS paymentSPIS = new PaymentSPIS(elPayment, listElPayments.indexOf(elPayment) + 1, this);
			this.setPaymentSPIS.add(paymentSPIS);
			paymentSPIS.parse();
		}
	}
	
}
