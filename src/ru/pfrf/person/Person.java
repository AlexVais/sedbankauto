package ru.pfrf.person;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


import javax.persistence.Transient;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

@MappedSuperclass
public abstract class Person implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID") 
	protected Integer id;
	
	@Column(name = "name")
	protected String name;
	
	@Column(name = "surname")
	protected String surname;
	
	@Column(name = "patr")
	protected String patr;

	@Transient
	protected Element element;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPatr() {
		return patr;
	}

	public void setPatr(String patr) {
		this.patr = patr;
	}

	public Element getElement() {
		return element;
	}

	public Person() {}
	
	public Person(Element element) {
		this.element = element;
	}

	public Person(String surname, String name, String patr) {
		super();
		this.name = name;
		this.surname = surname;
		this.patr = patr;
	}
	
	public abstract void parse() throws Throwable;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(surname).append(" | ")
		  .append(name).append(" | ")
		  .append(patr);
		
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		Person person = (Person) obj;
		return new EqualsBuilder()
		                 .append(this.surname, person.surname)
		                 .append(this.name, person.name)
		                 .append(this.patr,  person.patr)
		                 .isEquals();
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	

	public String FIO() {
		return surname + " " + name + " " + patr;
	}
	
	public static Comparator<Person> comparatorBySurnameAsc() {
		return new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getSurname().compareTo(o2.getSurname());
			}
		};
	}
	
	public static Comparator<Person> comparatorByFIOAsc() {
		return new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.FIO().compareTo(o2.FIO());
			}
		};
	}
	
}
