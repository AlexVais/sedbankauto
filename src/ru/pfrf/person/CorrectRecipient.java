package ru.pfrf.person;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.payment.PaymentSPIS;

@Entity
@Table(name = "correct_recipients")
@AttributeOverrides({
	@AttributeOverride(name="surname", column=@Column(name="CorrectFamily")),
	@AttributeOverride(name="name", column=@Column(name="CorrectName")),
    @AttributeOverride(name="patr", column=@Column(name="CorrectSurname"))
})
public class CorrectRecipient extends Person{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "CorrectSnils")
	protected String SNILS;
	
	@Column(name = "CorrectAccount")
	protected String accountNumber;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "recipient_id")
	protected RecipientListTransfer recipientListTransfer;
	
	@Column(name = "moddate")
	protected Date dateModification;

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}


	public RecipientListTransfer getRecipientListTransfer() {
		return recipientListTransfer;
	}

	public void setRecipientListTransfer(RecipientListTransfer recipientListTransfer) {
		this.recipientListTransfer = recipientListTransfer;
	}

	public String getSNILS() {
		return SNILS;
	}

	public void setSNILS(String sNILS) {
		SNILS = sNILS;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	

	@Override
	public void parse() throws Throwable {
		throw new Exception("not realized");
		
	}
	
	public CorrectRecipient() {}
	
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + this.accountNumber + 
				" | " + this.SNILS +
				" | " + this.dateModification;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		CorrectRecipient cr = (CorrectRecipient) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(obj))
		                 .append(this.accountNumber, cr.accountNumber)
		                 .append(this.SNILS,  cr.SNILS)
		                 .append(this.dateModification,  cr.dateModification)
		                 .isEquals();
	}
		

}
