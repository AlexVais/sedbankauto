package ru.pfrf.person;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

@MappedSuperclass
public class Recipient extends Person implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "accountNumber")
	protected String accountNumber;
	
	@Column(name = "numberInArray")
	protected Integer numberInArray;
	
	@Column(name = "area")
	protected Integer area;

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getNumberInArray() {
		return numberInArray;
	}

	public void setNumberInArray(Integer numberInArray) {
		this.numberInArray = numberInArray;
	}

	public Recipient() {}
	
	public Recipient(Element element) {
		this.element = element;
	}
	
	public Recipient(String name, 
			String surname, 
			String patr, 
			String accountNumber, 
			Integer numberInArray,
			Integer area,
			Element element) {
		super(surname, name, patr);
		this.accountNumber = accountNumber;
		this.numberInArray = numberInArray;
		this.area = area;
		this.element = element;
	}
	
	@Override
	public void parse() throws Exception {
		try {
			Element elFIO = element.element("���");
			//<���>
			this.surname = elFIO.element("�������").getText();
			this.name = elFIO.element("���").getText();
			this.patr = elFIO.element("��������").getText();
			
			this.numberInArray = Integer.valueOf(element.element("�������������").getText());
			this.accountNumber = element.element("����������").getText();
			this.area = Integer.valueOf((element.element("���������").getText().substring(4, 7)));
					
		} catch (Throwable e) {
			throw new Exception("\n������ ��� �������� ������ �� ����������\n" + element.asXML(), e);
		}
	}
	
	
	@Override
	public String toString() {
		return super.toString() + 
				" | " + accountNumber + 
				" | " + String.valueOf(numberInArray) + 
				" | " + String.valueOf(area); 
	}
	
	@Override
	public boolean equals(Object obj) {
		Recipient recipient = (Recipient) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(obj))
		                 .append(this.accountNumber, recipient.accountNumber)
		                 .append(this.numberInArray, recipient.numberInArray)
		                 .append(this.area,  recipient.area)
		                 .isEquals();
	}

	public static Comparator<Recipient> comparatorByAreaAsc() {
		return new Comparator<Recipient>() {
			@Override
			public int compare(Recipient o1, Recipient o2) {
				return Integer.compare(o1.getArea(), o2.getArea());
			}
		};
	}
	
	public static Comparator<Recipient> comparatorByAreaDesc() {
		return new Comparator<Recipient>() {
			@Override
			public int compare(Recipient o1, Recipient o2) {
				return Integer.compare(o2.getArea(), o1.getArea());
			}
		};
	}
	
	public static Comparator<Recipient> comparatorByAccountNumberAsc() {
		return new Comparator<Recipient>() {
			@Override
			public int compare(Recipient o1, Recipient o2) {
				return o1.getAccountNumber().compareTo(o2.getAccountNumber());
			}
		};
	}
	
	public static Comparator<Recipient> comparatorByAccountNumberDesc() {
		return new Comparator<Recipient>() {
			@Override
			public int compare(Recipient o1, Recipient o2) {
				return o2.getAccountNumber().compareTo(o1.getAccountNumber());
			}
		};
	}
	
	public static Comparator<Recipient> comparatorByNumberInArrayAsc() {
		return new Comparator<Recipient>() {
			@Override
			public int compare(Recipient o1, Recipient o2) {
				return o1.getNumberInArray().compareTo(o2.getNumberInArray());
			}
		};
	}
	
}
