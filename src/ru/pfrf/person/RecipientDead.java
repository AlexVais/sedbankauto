package ru.pfrf.person;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.*;

import main.MainFrame;

import org.dom4j.Element;

import ru.pfrf.classifier.DeliveryOrg;
import ru.pfrf.classifier.TypeDocDead;
import ru.pfrf.entityinfoexchange.order.osmp.OSMP;
import ru.pfrf.entityinfoexchange.order.osmp.OSMPByArea;

@Entity
@Table(name = "recipients_dead")
public class RecipientDead extends Recipient implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "dateDead")
	private Date dateDead;
	
	@Column(name = "dateAct")
	private Date dateAct;
	
	@Column(name = "numberAct")
	private String numberAct;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vidDocDead")
	protected TypeDocDead typeDocDead;
	
	@Column(name = "requisitesDocDead")
	private String requisitesDocDead;
	
	@Column(name = "placeOfDead")
	private String placeOfDead;
	
	@Column(name = "stateAccountNumber")
	private String stateAccountNumber;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "osmp")
	private OSMP osmp;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "osmp_by_area")
	private OSMPByArea osmpByArea;

	public TypeDocDead getTypeDocDead() {
		return typeDocDead;
	}

	public void setTypeDocDead(TypeDocDead typeDocDead) {
		this.typeDocDead = typeDocDead;
	}

	public String getRequisitesDocDead() {
		return requisitesDocDead;
	}

	public void setRequisitesDocDead(String requisitesDocDead) {
		this.requisitesDocDead = requisitesDocDead;
	}

	public String getPlaceOfDead() {
		return placeOfDead;
	}

	public void setPlaceOfDead(String placeOfDead) {
		this.placeOfDead = placeOfDead;
	}

	public OSMPByArea getOsmpByArea() {
		return osmpByArea;
	}

	public void setOsmpByArea(OSMPByArea osmpByArea) {
		this.osmpByArea = osmpByArea;
	}

	public OSMP getOsmp() {
		return osmp;
	}

	public void setOsmp(OSMP osmp) {
		this.osmp = osmp;
	}

	public Integer getArea() {
		return area;
	}

	public void setArea(Integer area) {
		this.area = area;
	}

	public Date getDateDead() {
		return dateDead;
	}

	public void setDateDead(Date dateDead) {
		this.dateDead = dateDead;
	}

	public Date getDateAct() {
		return dateAct;
	}

	public void setDateAct(Date dateAct) {
		this.dateAct = dateAct;
	}

	public String getNumberAct() {
		return numberAct;
	}

	public void setNumberAct(String numberAct) {
		this.numberAct = numberAct;
	}

	public String getStateAccountNumber() {
		return stateAccountNumber;
	}

	public void setStateAccountNumber(String stateAccountNumber) {
		this.stateAccountNumber = stateAccountNumber;
	}  
	
	public RecipientDead() {}
	
	public RecipientDead(OSMP osmp, Element element) {
		this.osmp = osmp;
		this.element = element;
	}
	
	public RecipientDead(String name, 
			String surname, 
			String patr, 
			String accountNumber, 
			Integer numberInArray,
			Integer area,
			Element element,
			Date dateDead,
			Date dateAct,
			String numberAct,
			String stateAccountNumber,
			TypeDocDead typeDocDead,
			String requisitesDocDead,
			String placeOfDead,
			OSMP osmp) {
		
		super(name, surname, patr, accountNumber, numberInArray, area, element);
		
		this.dateDead = dateDead;
		this.dateAct = dateAct;
		this.numberAct = numberAct;
		this.stateAccountNumber = stateAccountNumber;
		this.typeDocDead = typeDocDead;
		this.requisitesDocDead = requisitesDocDead;
		this.placeOfDead = placeOfDead;
		this.osmp = osmp;
	}
	
	@Override
	public String toString() {
		return super.toString() +
				" | " + String.valueOf(area) + 
				" | " + String.valueOf(dateDead) + 
				" | " + String.valueOf(dateAct) +
				" | " + String.valueOf(numberAct) + 
				" | " + String.valueOf(stateAccountNumber) + 
				" | " + this.typeDocDead.getCode() + 
				" | " + String.valueOf(requisitesDocDead) + 
				" | " + String.valueOf(placeOfDead) + 
				" | @" + "osmp";
	}

	@Override
	public void parse() throws Exception {
		try {
			this.numberInArray = Integer.valueOf(element.element("�������������").getText());
			this.accountNumber = element.element("����������").getText();
			this.area = Integer.valueOf(element.element("���������").getText().substring(4, 7));
			
			Element elementFIO = element.element("���");
			this.surname = elementFIO.element("�������").getText();
			this.name = elementFIO.element("���").getText();
			this.patr = elementFIO.element("��������").getText();
			
			Element elNumberAct = element.element("���������������");
			if (elNumberAct != null) {
				this.numberAct = elNumberAct.getText();
			} else {
				this.numberAct = null;
			}
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ROOT);
			simpleDateFormat.setLenient(false);
			
			Element elDateDead = element.element("����������");
			if (elDateDead != null && !"".equals(elDateDead.getText())) {
				this.dateDead = simpleDateFormat.parse(elDateDead.getText());
			}
			
			Element elDateAct = element.element("��������������");
			if (elDateAct != null && !"".equals(elDateAct.getText())) {
				this.dateAct = simpleDateFormat.parse(elDateAct.getText());
			}
			
			Element elVidDocDead = element.element("�������������������");
			if (elVidDocDead != null && !"".equals(elVidDocDead.getText())) {
				this.typeDocDead = MainFrame.dictTypeDocDead.get(Integer.valueOf(elVidDocDead.getText()));
			}
			
			Element elRequisites = element.element("�������������������������");
			if (elRequisites != null && !"".equals(elRequisites.getText())) {
				this.requisitesDocDead = elRequisites.getText();
			}
			
			Element elPlaceOfDead = element.element("�����������");
			if (elPlaceOfDead != null && !"".equals(elPlaceOfDead.getText())) {
				this.placeOfDead = elPlaceOfDead.getText();
			}
			
			
			Element elAllValuesStateAccount = element.element("�������������������������");
			List<Element> listAllValuesStateAccount = elAllValuesStateAccount.elements("���������������������");
			StringBuilder sb = new StringBuilder();
			for (Element element : listAllValuesStateAccount) {
				sb.append(element.element("��������������").getText() + " ");
			}
			this.stateAccountNumber = sb.toString().trim();
			
			
		} catch (Exception e) {
			throw new Exception("\n������ ��� �������� <������������������������>\n" + element.asXML(), e);
		}
		
	}

}
