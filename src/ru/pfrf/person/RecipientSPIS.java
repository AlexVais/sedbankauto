package ru.pfrf.person;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.dom4j.Element;

@MappedSuperclass
public class RecipientSPIS extends Recipient{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "amountDelivery")
	protected BigDecimal amountDelivery;
	
	@Column(name = "numberVD")
	private String numberVD;
	
	@Column(name = "SNILS")
	private String SNILS;
	
	@Column(name = "countPayments")
	protected Integer countPayments;

	public Integer getCountPayments() {
		return countPayments;
	}

	public void setCountPayments(Integer countPayments) {
		this.countPayments = countPayments;
	}

	public BigDecimal getAmountDelivery() {
		return amountDelivery;
	}

	public void setAmountDelivery(BigDecimal amountDelivery) {
		this.amountDelivery = amountDelivery;
	}

	public String getNumberVD() {
		return numberVD;
	}

	public void setNumberVD(String numberVD) {
		this.numberVD = numberVD;
	}

	public String getSNILS() {
		return SNILS;
	}

	public void setSNILS(String sNILS) {
		SNILS = sNILS;
	}
	
	public RecipientSPIS() {}
	
	public RecipientSPIS(Element element) {
		super(element);
	}
	
	public RecipientSPIS(String name, 
			String surname, 
			String patr, 
			String accountNumber, 
			Integer numberInArray,
			Integer area,
			Element element,
			BigDecimal amountDelivery,
			String numberVD,
			String SNILS,
			Integer countPayments) {
		
		super(name, surname, patr, accountNumber, numberInArray, area, element);
		
		this.amountDelivery = amountDelivery;
		this.numberVD = numberVD;
		this.SNILS = SNILS;
		this.countPayments = countPayments;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString() + " | ")
		  .append(String.valueOf(amountDelivery) + " | ")
		  .append(String.valueOf(numberVD) + " | ")
		  .append(String.valueOf(SNILS) + " | ")
		  .append(String.valueOf(countPayments));
		
		return sb.toString();
		
	}
	
	@Override
	public boolean equals(Object obj) {
		RecipientSPIS r = (RecipientSPIS) obj;
		return new EqualsBuilder()
						 .appendSuper(super.equals(r))
		                 .append(this.amountDelivery, r.amountDelivery)
		                 .append(this.numberVD, r.numberVD)
		                 .append(this.SNILS,  r.SNILS)
		                 .append(this.countPayments,  r.countPayments)
		                 .isEquals();
	}
	
	protected void parsePayments() throws Exception {}

	@Override
	public void parse() throws Exception {
		super.parse();
		
		try {
			this.amountDelivery = new BigDecimal(element.element("��������������").getText());
			this.numberVD = element.element("�������������������").getText();
			this.SNILS = element.element("��������������").getText();
			this.countPayments = Integer.valueOf(element.element("����������").element("����������").getText());
			
			
			//������� ������
			this.parsePayments();
			
					
		} catch (Throwable e) {
			throw new Exception("\n������ ��� �������� ������ �� ����������\n" + element.asXML(), e);
		}
		
	}


}
