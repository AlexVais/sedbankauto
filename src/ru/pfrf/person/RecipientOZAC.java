package ru.pfrf.person;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import main.MainFrame;

import org.dom4j.Element;
import org.hibernate.annotations.BatchSize;

import ru.pfrf.classifier.CodeCredited;
import ru.pfrf.entityinfoexchange.order.ozac.OZAC;
import ru.pfrf.entityinfoexchange.order.ozac.SeparatedOZAC;
import ru.pfrf.payment.PaymentOZAC;
import ru.pfrf.payment.PaymentSPIS;

@Entity
@Table(name = "recipients_ozac")
public class RecipientOZAC extends RecipientSPIS{
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "codeCredited")
	private CodeCredited codeCredited;
	
	@BatchSize(size = 20000)
	@OneToMany(mappedBy = "recipientOZAC", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<PaymentOZAC> setPaymentOZAC = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "OZAC")
	private OZAC ozac;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "separatedOZAC")
	private SeparatedOZAC separatedOZAC;
	
	@OneToMany(mappedBy = "recipientOZAC", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<DifferenceRecipient> setDifferenceRecipients = new HashSet<>();

	public SeparatedOZAC getSeparatedOZAC() {
		return separatedOZAC;
	}

	public void setSeparatedOZAC(SeparatedOZAC separatedOZAC) {
		this.separatedOZAC = separatedOZAC;
	}

	public OZAC getOzac() {
		return ozac;
	}

	public Set<DifferenceRecipient> getSetDifferenceRecipients() {
		return setDifferenceRecipients;
	}

	public void setSetDifferenceRecipients(
			Set<DifferenceRecipient> setDifferenceRecipients) {
		this.setDifferenceRecipients = setDifferenceRecipients;
	}

	public void setOzac(OZAC ozac) {
		this.ozac = ozac;
	}

	public CodeCredited getCodeCredited() {
		return codeCredited;
	}

	public void setCodeCredited(CodeCredited codeCredited) {
		this.codeCredited = codeCredited;
	}

	public Set<PaymentOZAC> getSetPaymentOZAC() {
		return setPaymentOZAC;
	}

	public void setSetPaymentOZAC(Set<PaymentOZAC> setPaymentOZAC) {
		this.setPaymentOZAC = setPaymentOZAC;
	}

	public RecipientOZAC() {}
	
	public RecipientOZAC(Element element, OZAC ozac) {
		super(element);
		this.ozac = ozac;
	}
	
	@Override
	public void parse() throws Exception {
		super.parse();
		
		try {
			String strCodeCredited = element.element("�������������").getText();
			this.codeCredited =  MainFrame.dictCodeCredited.get(strCodeCredited);
			if (codeCredited == null) 
				throw new Exception("�� ��������� ��� ����������: " + strCodeCredited);
			
			if (this.setPaymentOZAC.size() != this.countPayments) {
				throw new Exception("����������� ���������� ������ �� ��������� � ����������� ������ �� ����. NumberInArray: " + this.numberInArray);
			}
					
		} catch (Throwable e) {
			throw new Exception("������ ��� �������� ������ �� ����������" + element.asXML(), e);
		}
	}
	
	@Override
	protected void parsePayments() throws Exception {
		List<Element> listElPayments = element.element("����������").elements("�������");
		for (Element elPayment : listElPayments) {
			PaymentOZAC paymentOZAC = new PaymentOZAC(elPayment, listElPayments.indexOf(elPayment) + 1, this);
			this.setPaymentOZAC.add(paymentOZAC);
			paymentOZAC.parse();
		}
	}
	
}
