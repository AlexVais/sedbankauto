package ru.pfrf.person;

import java.io.Serializable;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;

import ru.pfrf.classifier.TypeDifferenceRecipient;
import ru.pfrf.classifier.TypeDifferenceSPIS;
import ru.pfrf.classifier.TypeError;
import ru.pfrf.entityinfoexchange.order.spis.SPIS;

@Entity
@Table(name = "differences_recipients")
public class DifferenceRecipient implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@ManyToOne
	@JoinColumn(name = "recipientOZAC")
	private RecipientOZAC recipientOZAC;
	
	@Transient
	private RecipientSPIS recipientSPIS;
	
	@ManyToOne
	@JoinColumn(name = "TypeDifferences")
	private TypeDifferenceRecipient typeDifferenceRecipient;


	public RecipientSPIS getRecipientSPIS() {
		return recipientSPIS;
	}

	public void setRecipientSPIS(RecipientSPIS recipientSPIS) {
		this.recipientSPIS = recipientSPIS;
	}

	public RecipientOZAC getRecipientOZAC() {
		return recipientOZAC;
	}

	public TypeDifferenceRecipient getTypeDifferenceRecipient() {
		return typeDifferenceRecipient;
	}

	public void setTypeDifferenceRecipient(
			TypeDifferenceRecipient typeDifferenceRecipient) {
		this.typeDifferenceRecipient = typeDifferenceRecipient;
	}

	public void setRecipientOZAC(RecipientOZAC recipientOZAC) {
		this.recipientOZAC = recipientOZAC;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public DifferenceRecipient() {}
	
	public DifferenceRecipient(RecipientOZAC recipientOZAC, RecipientSPIS recipientSPIS, TypeDifferenceRecipient typeDifferenceRecipient) {
		this.recipientOZAC = recipientOZAC;
		this.recipientSPIS = recipientSPIS;
		this.typeDifferenceRecipient = typeDifferenceRecipient;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) { return false; }
		if (obj == this) { return true; }
		if (obj.getClass() != getClass()) {
			return false;
		}
		
		DifferenceRecipient dif = (DifferenceRecipient) obj;
		return new EqualsBuilder()
		                 .append(this.recipientOZAC, dif.recipientOZAC)
		                 .append(this.recipientSPIS, dif.recipientSPIS)
		                 .append(this.typeDifferenceRecipient, dif.typeDifferenceRecipient)
		                 .isEquals();
	}
	
	@Override
	public String toString() {
		return this.recipientOZAC.FIO() + 
				" | " + this.recipientSPIS.FIO() + 
				" | " + this.typeDifferenceRecipient.getName();
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
}
